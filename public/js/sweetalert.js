$(document).ready(function () {
    $(document.body).on('click', '.js-submit-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-cancel-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, cancel it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-cancel-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, cancel it!',
                cancelButtonText: "Back",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-publish-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3598DC',
                confirmButtonText: 'Yes, publish it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3598DC',
                confirmButtonText: 'Yes, confirm it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-confirm-finish', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#00a65a',
                confirmButtonText: 'Yes, set finish!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-approve-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3598DC',
                confirmButtonText: 'Yes, approve it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-dont-approve-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, don\'t approve it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })

    $(document.body).on('click', '.js-modal-approve-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        var modal = $(this).closest('.modal').modal('hide')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3598DC',
                confirmButtonText: 'Yes, approve it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $form.submit()

                } else {
                    modal.modal('show')
                }
            });
    })

    $(document.body).on('click', '.js-modal-dont-approve-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        var modal = $(this).closest('.modal').modal('hide')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, don\'t approve it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $form.submit()

                } else {
                    modal.modal('show')
                }
            });
    })

    $(document.body).on('click', '.js-save-confirm', function (event) {
        event.preventDefault()
        var $form = $(this).closest('form')
        swal({
                title: "Are you sure?",
                text: "You can not undo this process!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3598DC',
                confirmButtonText: 'Yes, save it!',
                cancelButtonText: "Cancel",
                closeOnConfirm: true
            },
            function () {
                $form.submit()
            });
    })
})