<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login.act');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['middleware' => 'auth'], function () {
    Route::middleware('role:owner_or_manager_kantorpusat|manager_or_admin_toko|sales')->group(function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        Route::post('dashboard', 'DashboardController@getData')->name('dashboard.getdata');
    
        Route::get('dashboard/view-top-produk', 'DashboardController@viewTopPrd')->name('view-top-produk');
        Route::post('dashboard/view-top-produk', 'DashboardController@getDataTopPrd')->name('view-top-produk.getdata');
    
        Route::get('dashboard/view-all-customer', 'DashboardController@viewAllCst')->name('view-all-customer');
        Route::post('dashboard/view-all-customer', 'DashboardController@getAllCst')->name('view-all-customer.getdata');
        Route::post('dashboard/view-all-customer/broadcast', 'DashboardController@broadcastCst')->name('view-all-customer.broadcast');
    
        Route::get('dashboard/view-all-category', 'DashboardController@viewAllCat')->name('view-all-category');
        Route::post('dashboard/view-all-category', 'DashboardController@getAllCat')->name('view-all-category.getdata');
    
        Route::get('dashboard/view-all-sales-user', 'DashboardController@viewAllSales')->name('view-all-sales.user');
        Route::post('dashboard/view-all-sales-user', 'DashboardController@getAllSales')->name('view-all-sales.getdata'); 
        
        Route::get('dashboard/view-all-store-contribution', 'DashboardController@viewAllStoreContribution')->name('view-all-store-contribution.store');
        Route::post('dashboard/view-all-store-contribution', 'DashboardController@getAllStoreContribution')->name('view-all-store-contribution.getdata'); 
    });
    
    Route::get('my-profile', 'AccountSettingController@myprofile')->name('myprofile.index');
    Route::post('edit-profile', 'AccountSettingController@updateprofile')->name('myprofile.update');

    Route::get('change-password', 'AccountSettingController@changepassword')->name('change-password.index');
    Route::post('change-password', 'AccountSettingController@updatepassword')->name('change-password.update');

    Route::prefix('master-data')->group(function () {
        Route::middleware('role:owner_or_manager_kantorpusat|manager_or_admin_toko')->group(function () {
            Route::resource('supplier', 'MasterData\SupplierController')->except(['show', 'create']);
            Route::resource('produsen', 'MasterData\ProdusenController')->except(['show', 'create']);
            Route::resource('satuan', 'MasterData\SatuanController')->except(['show', 'create']);
            Route::resource('kategori-material', 'MasterData\KategoriMaterialController')->except(['show', 'create']);
            Route::resource('kategori-produk', 'MasterData\KategoriProdukController')->except(['index', 'show', 'create']);
            Route::resource('daerah', 'MasterData\DaerahController')->except(['show', 'create']);
            Route::resource('collection', 'MasterData\CollectionController')->except(['show', 'create']);
            Route::get('currency', 'MasterData\CurrencyController@index')->name('currency.index');
        });
        Route::resource('location', 'MasterData\LocationController')->middleware('role:owner_or_manager_kantorpusat')->except(['show', 'create']);
        Route::resource('data-user', 'MasterData\DataUserController')->middleware('role:owner_or_manager_kantorpusat')->except(['show', 'create']);
    });

    Route::prefix('inventory')->group(function () {
        Route::middleware('role:owner_or_manager_kantorpusat|manager_or_admin_toko')->group(function () {
            Route::post('produk/stock/getlocation', 'Inventory\StokProdukController@getlocation')->name('stok-produk.getlocation');
            Route::get('produk/stock/{prd_id}', 'Inventory\StokProdukController@index')->name('stok-produk.index');
            Route::post('produk/stock/{prd_id}', 'Inventory\StokProdukController@store')->name('stok-produk.store');
            Route::delete('produk/stock/{id}', 'Inventory\StokProdukController@destroy')->name('stok-produk.destroy');

            Route::get('produk/barcode/{prd_id}/print', 'Inventory\ProdukController@barcodePrint')->name('produk.print-barcode');
            Route::get('produk/cekkdproduk/{kdproduk}', 'Inventory\ProdukController@cekkdproduk')->name('produk.cekkdproduk');
            Route::get('produk/getdata', 'Inventory\ProdukController@getData')->name('produk.getData');
            Route::post('produk/getkode', 'Inventory\ProdukController@getKode')->name('produk.getKode');
            Route::resource('produk', 'Inventory\ProdukController')->except(['show', 'create']);

            Route::get('produk/discount', 'Inventory\DiscountProdukController@index')->name('diskon-produk.index');
            Route::post('produk/set-discount', 'Inventory\DiscountProdukController@setDiskon')->name('diskon-produk.set-diskon');

            Route::post('produk/komposisi/getmaterial', 'Inventory\KomposisiProdukController@getmaterial')->name('komposisi-produk.getmaterial');
            Route::get('produk/komposisi/{prd_id}', 'Inventory\KomposisiProdukController@index')->name('komposisi-produk.index');
            Route::post('produk/komposisi/{prd_id}', 'Inventory\KomposisiProdukController@store')->name('komposisi-produk.store');
            Route::get('produk/komposisi/{id}/edit', 'Inventory\KomposisiProdukController@edit')->name('komposisi-produk.edit');
            Route::patch('produk/komposisi/{id}', 'Inventory\KomposisiProdukController@update')->name('komposisi-produk.update');
            Route::delete('produk/komposisi/{id}', 'Inventory\KomposisiProdukController@destroy')->name('komposisi-produk.destroy');

            Route::get('stock-opname', 'Inventory\StockOpnameController@index')->name('stock-opname.index');
            Route::get('stock-opname/download-stock/{kdloc}', 'Inventory\StockOpnameController@downloadStock')->name('stock-opname.downloadstock');
            Route::get('stock-opname/create', 'Inventory\StockOpnameController@create')->name('stock-opname.create');
            Route::post('stock-opname/create', 'Inventory\StockOpnameController@readImport')->name('stock-opname.readimport');
            Route::post('stock-opname/store', 'Inventory\StockOpnameController@store')->name('stock-opname.store');
            Route::get('stock-opname/{id}/detail', 'Inventory\StockOpnameController@detail')->name('stock-opname.detail');
            Route::get('stock-opname/{id}/export', 'Inventory\StockOpnameController@export')->name('stock-opname.export');
            Route::patch('stock-opname/{id}', 'Inventory\StockOpnameController@update')->name('stock-opname.update');
            Route::delete('stock-opname/{id}', 'Inventory\StockOpnameController@cancel')->name('stock-opname.cancel');

            Route::get('stock-card', 'Inventory\StockCardController@index')->name('stock-card.index');
            Route::post('stock-card', 'Inventory\StockCardController@getdata')->name('stock-card.getdata');
            Route::post('stock-card/get-list-item', 'Inventory\StockCardController@getListItem')->name('stock-card.getListItem');
            Route::get('stock-card/{start}/{end}', 'Inventory\StockCardController@cetakStock')->name('stock-card.cetak');

            //Route::get('summary-stock', 'Inventory\SummaryStockController@index')->name('summary-stock.index');
            //Route::post('summary-stock', 'Inventory\SummaryStockController@getdata')->name('summary-stock.getdata');
            //Route::get('summary-stock/{start}/{end}', 'Inventory\SummaryStockController@cetakStock')->name('summary-stock.cetak');

            Route::get('summary-stock-outlet', 'Inventory\SummaryStockOutletController@index')->name('summary-stock-outlet.index');
            Route::post('summary-stock-outlet', 'Inventory\SummaryStockOutletController@getdata')->name('summary-stock-outlet.getdata');
            Route::get('summary-stock-outlet/{start}/{end}', 'Inventory\SummaryStockOutletController@exportStock');

            Route::get('summary-stock-outlet2', 'Inventory\SummaryStockOutletController@index2')->name('summary-stock-outlet.index2');
            Route::post('summary-stock-outlet2', 'Inventory\SummaryStockOutletController@getdata2')->name('summary-stock-outlet.getdata2');

            Route::get('summary-stock-global', 'Inventory\SummaryStockGlobalController@index')->name('summary-stock-global.index');
            Route::post('summary-stock-global', 'Inventory\SummaryStockGlobalController@getdata')->name('summary-stock-global.getdata');

            Route::get('stock-allert', 'Inventory\StockAllertController@index')->name('stock-allert.index');
            Route::post('stock-allert/create-costing', 'Inventory\StockAllertController@createCosting')->name('stock-allert.create-costing');

            Route::resource('product-reject', 'Inventory\ProdukRejectController')->only(['index', 'store']);
        });

        Route::middleware('role:owner_or_manager_kantorpusat|manager_or_admin_toko|sales')->group(function () {
            Route::match(['get', 'post'], 'transfer-stok/read-import', 'Inventory\TransferStokController@readImport')->name('transfer-stok.readimport');
            Route::get('transfer-stok/download-stock', 'Inventory\TransferStokController@downloadStock')->name('transfer-stok.downloadstock');

            Route::get('transfer-stok/detail-approval/{id}', 'Inventory\TransferStokController@detailApproval')->name('transfer-stok.detailapproval');
            Route::post('transfer-stok/detail-approval/{id}', 'Inventory\TransferStokController@storeApproval')->name('transfer-stok.storeapproval');
            Route::post('transfer-stok/set-finish/{id}', 'Inventory\TransferStokController@setFinish')->name('transfer-stok.setfinish');

            Route::get('transfer-stok', 'Inventory\TransferStokController@index')->name('transfer-stok.index');
            Route::get('transfer-stok/create', 'Inventory\TransferStokController@create')->name('transfer-stok.create');
            Route::post('transfer-stok/store', 'Inventory\TransferStokController@store')->name('transfer-stok.store');
            Route::get('transfer-stok/{id}', 'Inventory\TransferStokController@show')->name('transfer-stok.show');
            Route::get('transfer-stok/{id}/print', 'Inventory\TransferStokController@cetakTransfer')->name('transfer-stok.print');
            Route::get('transfer-stok/{id}/edit', 'Inventory\TransferStokController@edit')->name('transfer-stok.edit');
            Route::patch('transfer-stok/{id}', 'Inventory\TransferStokController@update')->name('transfer-stok.update');
            Route::delete('transfer-stok/{id}', 'Inventory\TransferStokController@destroy')->name('transfer-stok.destroy');
            Route::post('transfer-stok/{id}/publish', 'Inventory\TransferStokController@publish')->name('transfer-stok.publish');
        });

        Route::get('stock-opname/{kd_location}', 'Inventory\StockOpnameController@location')->name('stock-opname.location')->middleware('role:owner_or_manager_kantorpusat');
        Route::get('stock-opname/{id}/penyesuaian', 'Inventory\StockOpnameController@penyesuaian')->name('stock-opname.penyesuaian')->middleware('role:owner_or_manager_kantorpusat');
        Route::patch('stock-opname/{id}/penyesuaian', 'Inventory\StockOpnameController@updatePenyesuaian')->name('stock-opname.updatePenyesuaian')->middleware('role:owner_or_manager_kantorpusat');


        Route::middleware('role:owner_or_manager_kantorpusat|sales')->group(function () {
            Route::get('produk-stock-location', 'Inventory\StokProdukLocationController@index')->name('stok-bylocation.index');
            Route::post('produk-stock-location/getstock', 'Inventory\StokProdukLocationController@getStock')->name('stok-bylocation.getstock');
            Route::get('produk-stock-location/{kd_produk}/{kd_location}/detailseri', 'Inventory\StokProdukLocationController@detailSeri')->name('stok-bylocation.detailseri');
            Route::get('produk-stock-location/print/{kd_location}', 'Inventory\StokProdukLocationController@printStock')->name('stok-bylocation.print');
            Route::get('produk-stock-location/export/{kd_location}', 'Inventory\StokProdukLocationController@exportStock')->name('stok-bylocation.export');
        });

        Route::middleware('role:owner_or_manager_kantorpusat')->group(function () {
            Route::post('material/detailmaterial', 'Inventory\MaterialController@detailMaterial')->name('material.detailmaterial');
            Route::resource('material', 'Inventory\MaterialController')->except(['edit', 'update', 'show', 'create']);

            Route::get('material-opname', 'Inventory\MaterialOpnameController@index')->name('material-opname.index');
            Route::get('material-opname/create', 'Inventory\MaterialOpnameController@create')->name('material-opname.create');
            Route::post('material-opname/store', 'Inventory\MaterialOpnameController@store')->name('material-opname.store');
            Route::get('material-opname/{id}/detail', 'Inventory\MaterialOpnameController@detail')->name('material-opname.detail');
            Route::patch('material-opname/{id}/update', 'Inventory\MaterialOpnameController@update')->name('material-opname.update');
            Route::get('material-opname/{id}/penyesuaian', 'Inventory\MaterialOpnameController@penyesuaian')->name('material-opname.penyesuaian');
            Route::patch('material-opname/{id}/updatePenyesuaian', 'Inventory\MaterialOpnameController@updatePenyesuaian')->name('material-opname.updatePenyesuaian');
            Route::delete('material-opname/{id}', 'Inventory\MaterialOpnameController@cancel')->name('material-opname.cancel');

            Route::resource('material-reject', 'Inventory\MaterialRejectController')->only(['index', 'store']);
        });
    });

    Route::middleware('role:owner_or_manager_kantorpusat|production')->prefix('production')->group(function () {
        Route::post('costing/get-list-material', 'Production\CostingController@getListMaterial')->name('costing.getlistmaterial');
        Route::post('costing/get-list-material/{kd_costing}', 'Production\CostingController@getListMaterialEdit')->name('costing.getlistmaterial-edit');
        Route::post('costing/detailmaterial', 'Production\CostingController@getDetailMaterial')->name('costing.detailmaterial');
        Route::post('costing/setfinish/{id}', 'Production\CostingController@setfinish')->name('costing.setfinish');
        Route::post('costing/publish/{id}', 'Production\CostingController@publish')->name('costing.publish');
        Route::post('costing/cancel/{id}', 'Production\CostingController@cancel')->name('costing.cancel');
        Route::resource('costing', 'Production\CostingController');

        Route::post('produksi/get-list-material', 'Production\ProduksiController@getListMaterial')->name('produksi.getlistmaterial');
        Route::post('produksi/get-list-material/{kd_produksi}', 'Production\ProduksiController@getListMaterialEdit')->name('produksi.getlistmaterial-edit');
        Route::post('produksi/detailmaterial', 'Production\ProduksiController@getDetailMaterial')->name('produksi.detailmaterial');
        Route::post('produksi/setfinish/{id}', 'Production\ProduksiController@setfinish')->name('produksi.setfinish');
        Route::post('produksi/publish/{id}', 'Production\ProduksiController@publish')->name('produksi.publish');
        Route::get('produksi/print/{id}', 'Production\ProduksiController@printProduksi')->name('produksi.print');

        Route::get('produksi/progress/{id}', 'Production\ProduksiController@progress')->name('produksi.progress');
        Route::post('produksi/progress/{id}', 'Production\ProduksiController@updateProgress')->name('produksi.updateProgress');

        Route::resource('produksi', 'Production\ProduksiController');

        Route::get('saran-produksi', 'Production\SaranProduksiController@index')->name('saran-produksi.index');

        Route::get('pekerjaan-vendor', 'Production\PekerjaanVendorController@index')->name('pekerjaan-vendor.index');
        Route::get('pekerjaan-vendor/{kd_produsen}', 'Production\PekerjaanVendorController@show')->name('pekerjaan-vendor.show');
        Route::post('pekerjaan-vendor/{kd_produsen}', 'Production\PekerjaanVendorController@getHistory')->name('pekerjaan-vendor.gethistory');
        Route::get('pekerjaan-vendor/{start}/{end}', 'Production\PekerjaanVendorController@cetakLaporan')->name('pekerjaan-vendor.cetak-laporan');
    });

    Route::prefix('transaction')->group(function () {
        Route::prefix('pos')->middleware('role:owner_or_manager_kantorpusat|sales')->group(function () {
            Route::get('session-order', 'Transaction\POSController@sessionOrder');

            //Add customer
            Route::get('/', 'Transaction\POSController@index')->middleware('toadditem_if_pelangganset')->name('pos.index');
            Route::get('session-order/forget', 'Transaction\POSController@forgetOrder');
            Route::post('get-detail-konsinyasi', 'Transaction\POSController@getDetailKonsinyasi')->name('pos.get-detail-konsinyasi');
            Route::post('store-customerpk', 'Transaction\POSController@storeCustomerPK')->name('pos.store-customerpk');
            Route::post('store-customerpl', 'Transaction\POSController@storeCustomerPL')->name('pos.store-customerpl');

            //Add item/cart pos
            Route::get('additem/getcart', 'Transaction\POSController@getCartItem')->name('pos.additem.getcartitem');
            Route::post('additem/get-listproduk', 'Transaction\POSController@getListProduk')->name('pos.additem.getlistproduk');
            Route::post('additem/change-currency', 'Transaction\POSController@changeCurrency')->name('pos.additem.changecurrency');
            Route::get('additem/{id}', 'Transaction\POSController@addDetailItem')->name('pos.additem.detailitem');
            Route::post('additem/{id}', 'Transaction\POSController@storeItem')->name('pos.additem.storeitem');
            Route::get('additem/{id}/edit', 'Transaction\POSController@editDetailItem')->name('pos.additem.editdetailitem');
            Route::patch('additem/{id}', 'Transaction\POSController@updateItem')->name('pos.additem.updateitem');
            Route::get('additem', 'Transaction\POSController@additem')->middleware('toposdashboard_if_pelangganempty')->name('pos.additem.index');
            Route::post('delete-item/{id}', 'Transaction\POSController@deleteItem')->name('pos.deleteitem');
            Route::post('store-surcharge', 'Transaction\POSController@storeSurcharge')->name('pos.storesurcharge');
            Route::post('delete-surcharge/{index}', 'Transaction\POSController@deleteSurcharge')->name('pos.deletesurcharge');

            //View detail & add payment
            Route::get('order-detail', 'Transaction\POSController@orderDetail')->middleware('toadditem_if_orderempty')->name('pos.orderdetail');
            Route::post('order-detail', 'Transaction\POSController@storeAllOrder')->name('pos.storeallorder');
            Route::get('saved-order-detail/{id}', 'Transaction\POSController@savedOrder')->name('pos.saved-order');
            Route::get('print-order-detail/{id}', 'Transaction\POSController@printOrder')->name('pos.print-order');

            Route::post('batalkan-order', 'Transaction\POSController@batalkanOrder')->name('pos.batalkanorder');

            Route::get('retur-sales', 'Transaction\ReturSalesController@index')->name('retur-sales.index');
            Route::get('retur-sales/{id}/create', 'Transaction\ReturSalesController@create')->name('retur-sales.create');
            Route::post('retur-sales/{id}', 'Transaction\ReturSalesController@store')->name('retur-sales.store');
            Route::get('retur-sales/{id}/detail', 'Transaction\ReturSalesController@detail')->name('retur-sales.detail');
            Route::get('retur-sales/{id}/print', 'Transaction\ReturSalesController@printRetur')->name('retur-sales.print');
        });

        Route::get('pembelian', 'Transaction\PembelianController@index')->middleware('role:owner_or_manager_kantorpusat')->name('pembelian.index');
        Route::get('pembelian/add-form-material', 'Transaction\PembelianController@addForm')->middleware('role:owner_or_manager_kantorpusat')->name('pembelian.addform');
        Route::post('pembelian', 'Transaction\PembelianController@storePembelian')->middleware('role:owner_or_manager_kantorpusat')->name('pembelian.store');
        Route::post('pembelian/get-detail-material', 'Transaction\PembelianController@detailMaterial')->middleware('role:owner_or_manager_kantorpusat')->name('pembelian.detailMaterial');

        Route::get('retur-purchasing', 'Transaction\ReturPurchasingController@index')->middleware('role:owner_or_manager_kantorpusat')->name('retur-purchasing.index');
        Route::get('retur-purchasing/{id}/create', 'Transaction\ReturPurchasingController@create')->middleware('role:owner_or_manager_kantorpusat')->name('retur-purchasing.create');
        Route::post('retur-purchasing/{id}', 'Transaction\ReturPurchasingController@store')->middleware('role:owner_or_manager_kantorpusat')->name('retur-purchasing.store');
        Route::get('retur-purchasing/{id}/detail', 'Transaction\ReturPurchasingController@detail')->middleware('role:owner_or_manager_kantorpusat')->name('retur-purchasing.detail');
        Route::get('retur-purchasing/{id}/print', 'Transaction\ReturPurchasingController@printRetur')->middleware('role:owner_or_manager_kantorpusat')->name('retur-purchasing.print');
    });

    Route::prefix('laporan')->group(function () {
        Route::middleware('role:owner_or_manager_kantorpusat|manager_or_admin_toko|sales')->group(function () {
            Route::get('penjualan-global', 'Laporan\PenjualanGlobalController@index')->name('penjualan-global.index');
            Route::post('penjualan-global', 'Laporan\PenjualanGlobalController@getdata')->name('penjualan-global.getdata');
            Route::post('penjualan-global/set-terbayar', 'Laporan\PenjualanGlobalController@setTerbayar')->name('penjualan-global.setTerbayar');
            Route::get('penjualan-global/detail/{id}', 'Laporan\PenjualanGlobalController@show')->name('penjualan-global.show');
            Route::get('penjualan-global/export/{start}/{end}', 'Laporan\PenjualanGlobalController@exportPenjualan')->name('penjualan-global.export');
            Route::get('penjualan-global/{start}/{end}', 'Laporan\PenjualanGlobalController@cetakPenjualan')->name('penjualan-global.cetak');

            Route::get('penjualan-stok', 'Laporan\PenjualanStokController@index')->name('penjualan-stok.index');
            Route::post('penjualan-stok', 'Laporan\PenjualanStokController@getdata')->name('penjualan-stok.getdata');
            Route::get('penjualan-stok/{start}/{end}', 'Laporan\PenjualanStokController@cetakPenjualan')->name('penjualan-stok.cetak');
            Route::get('penjualan-stok/export/{start}/{end}', 'Laporan\PenjualanStokController@exportPenjualan')->name('penjualan-stok.export');

            Route::get('history-penjualan-stok', 'Laporan\HistoryPStockController@index')->name('history-penjualan-stok.index');
            Route::get('history-penjualan-stok/{start}/{end}', 'Laporan\HistoryPStockController@cetakPenjualan')->name('history-penjualan-stok.cetak');

            Route::get('history-penjualan-customer', 'Laporan\HistoryPCustomerController@index')->name('history-penjualan-customer.index');
            Route::get('history-penjualan-customer/{start}/{end}', 'Laporan\HistoryPCustomerController@cetakPenjualan')->name('history-penjualan-customer.cetak');

            Route::get('retur-penjualan', 'Laporan\ReturPenjualanController@index')->name('retur-penjualan.index');
            Route::post('retur-penjualan', 'Laporan\ReturPenjualanController@getData')->name('retur-penjualan.getdata');
            Route::get('retur-penjualan/{start}/{end}', 'Laporan\ReturPenjualanController@cetakRetur')->name('retur-penjualan.cetak');
            Route::get('retur-penjualan/export/{start}/{end}', 'Laporan\ReturPenjualanController@exportRetur')->name('retur-penjualan.export');
        });

        Route::middleware('role:owner_or_manager_kantorpusat')->group(function () {
            Route::get('pembelian-global', 'Laporan\PembelianController@index')->name('pembelian-global.index');
            Route::post('pembelian-global', 'Laporan\PembelianController@getdata')->name('pembelian-global.getdata');
            Route::get('pembelian-global/detail/{id}', 'Laporan\PembelianController@show')->name('pembelian-global.show');
            Route::get('pembelian-global/{start}/{end}', 'Laporan\PembelianController@cetakPembelian')->name('pembelian-global.cetak');
            Route::get('pembelian-global/export/{start}/{end}', 'Laporan\PembelianController@exportPembelian')->name('pembelian-global.export');

            Route::get('pembelian-material', 'Laporan\PembelianMaterialController@index')->name('pembelian-material.index');
            Route::post('pembelian-material', 'Laporan\PembelianMaterialController@getdata')->name('pembelian-material.getdata');
            Route::get('pembelian-material/{start}/{end}', 'Laporan\PembelianMaterialController@cetakPembelian')->name('pembelian-material.cetak');
            Route::get('pembelian-material/export/{start}/{end}', 'Laporan\PembelianMaterialController@exportPembelian')->name('pembelian-material.export');

            Route::get('produksi-global', 'Laporan\ProduksiGlobalController@index')->name('produksi-global.index');
            Route::post('produksi-global', 'Laporan\ProduksiGlobalController@getdata')->name('produksi-global.getdata');
            Route::get('produksi-global/detail/{id}', 'Laporan\ProduksiGlobalController@show')->name('produksi-global.show');
            Route::get('produksi-global/{start}/{end}', 'Laporan\ProduksiGlobalController@cetakProduksi')->name('produksi-global.cetak');
            Route::get('produksi-global/export/{start}/{end}', 'Laporan\ProduksiGlobalController@exportProduksi')->name('produksi-global.export');

            Route::get('produksi-produk', 'Laporan\ProduksiProdukController@index')->name('produksi-produk.index');
            Route::post('produksi-produk', 'Laporan\ProduksiProdukController@getdata')->name('produksi-produk.getdata');
            Route::get('produksi-produk/{start}/{end}', 'Laporan\ProduksiProdukController@cetakProduksi')->name('produksi-produk.cetak');
            Route::get('produksi-produk/export/{start}/{end}', 'Laporan\ProduksiProdukController@exportProduksi')->name('produksi-produk.export');

            Route::get('penggunaan-bahan', 'Laporan\PenggunaanBahanController@index')->name('penggunaan-bahan.index');
            Route::post('penggunaan-bahan', 'Laporan\PenggunaanBahanController@getdata')->name('penggunaan-bahan.getdata');
            Route::get('penggunaan-bahan/{start}/{end}', 'Laporan\PenggunaanBahanController@cetakPenggunaan')->name('penggunaan-bahan.cetak');
            Route::get('penggunaan-bahan/export/{start}/{end}', 'Laporan\PenggunaanBahanController@exportPenggunaan')->name('penggunaan-bahan.export');
        });
    });

    Route::prefix('hutang-piutang')->group(function () {
        Route::get('penerimaan-hutang-piutang', 'HutangPiutang\HutangPiutang@index')->name('penerimaan-hutang-piutang.index');
        Route::get('hutang-piutang/add-form-hutang-piutang', 'HutangPiutang\HutangPiutang@addForm')->name('hutang-piutang.addform');
        Route::post('insert-penerimaan-hutang-piutang','HutangPiutang\HutangPiutang@insert')->name('hutangPiutang.store');

        Route::get('hutang-supplier', 'HutangPiutang\HutangSupplier@index')->name('hutang-supplier.index');
        Route::get('hutang-supplier/{kode}', 'HutangPiutang\HutangSupplier@get_data')->name('hutang-supplier.get-data');
        Route::post('insert-pembayaran-hutang','HutangPiutang\HutangSupplier@insert')->name('hutang-supplier.insert-pembayaran');

        Route::get('piutang-pelanggan', 'HutangPiutang\Piutang@index')->name('piutang-pelanggan.index');
        Route::get('piutang-pelanggan/{kode}', 'HutangPiutang\Piutang@get_data')->name('piutang-pelanggan.get-data');
        Route::post('insert-pembayaran','HutangPiutang\Piutang@insert')->name('piutang-pelanggan.insert-pembayaran');

        Route::get('hutang-lain', 'HutangPiutang\HutangSupplier@hutang_lain')->name('hutang-lain.index');
        Route::get('piutang-lain', 'HutangPiutang\Piutang@piutang_lain')->name('piutang-lain.index');

    });

    Route::prefix('accounting')->group(function () {
        Route::get('jurnalHarian', 'Akunting\JurnalHarianController@index')->name('jurnalHarian.index');
        Route::post('jurnalHarian', 'Akunting\JurnalHarianController@getdata')->name('jurnalHarian.getdata');
        Route::get('jurnalHarian/{start}/{end}', 'Akunting\JurnalHarianController@cetakJurnal')->name('jurnalHarian.cetak');
        Route::get('jurnalHarianCreate', 'Akunting\JurnalHarianController@create')->name('jurnalHarian.create');
        Route::post('jurnalHarianPost', 'Akunting\JurnalHarianController@store')->name('jurnalHarian.store');
        Route::get('editJurnalHarian/{id_jurnal}/','Akunting\JurnalHarianController@edit')->name('jurnalHarian.edit');
        Route::post('updateJurnalHarian/{id_jurnal}','Akunting\JurnalHarianController@update')->name('jurnalHarian.update');
        Route::get('postingJurnal','Akunting\JurnalHarianController@posting')->name('jurnalHarian.posting');

        Route::get('kodePerkiraan', 'Akunting\KodePerkiraanController@index')->name('kodePerkiraan.index');
        Route::post('kodePerkiraanPost', 'Akunting\KodePerkiraanController@store')->name('kodePerkiraan.store');
        Route::get('kodePerkiraan/{master_id}/','Akunting\KodePerkiraanController@edit')->name('kodePerkiraan.edit');
        Route::post('updateKodePerkiraan/{master_id}','Akunting\KodePerkiraanController@update')->name('kodePerkiraan.update');
        Route::delete('deleteKodePerkiraan/{master_id}/','Akunting\KodePerkiraanController@destroy')->name('kodePerkiraan.destroy');
        Route::get('addSaldoAwal/{master_id}/','Akunting\KodePerkiraanController@addSaldoAwal')->name('kodePerkiraan.addSaldoAwal');
        Route::post('simpanSaldoAwal/{master_id}','Akunting\KodePerkiraanController@simpanSaldoAwal')->name('kodePerkiraan.simpanSaldoAwal');
        Route::get('saldoAwal', 'Akunting\KodePerkiraanController@saldo_awal')->name('kodePerkiraan.saldo-awal');
        Route::post('saldoAwal', 'Akunting\KodePerkiraanController@getSaldoAwal')->name('kodePerkiraan.get-saldo-awal');
        Route::get('saldoAwal/{master_id}/','Akunting\KodePerkiraanController@edit_saldo_awal')->name('kodePerkiraan.edit-saldo-awal');
        Route::post('updateSaldoAwal/{master_id}','Akunting\KodePerkiraanController@update_saldo_awal')->name('kodePerkiraan.update-saldo-awal');

        Route::get('bukuBesar', 'Akunting\BukuBesarController@index')->name('bukuBesar.index');
        Route::get('bukuBesar/{start_date}/{end_date}/{master_id}', 'Akunting\BukuBesarController@cetak')->name('bukuBesar.cetak');
        Route::post('bukuBesar', 'Akunting\BukuBesarController@getdata')->name('bukuBesar.getdata');

        Route::get('rugiLaba', 'Akunting\RugiLabaController@index')->name('rugiLaba.index');
        Route::post('rugiLaba', 'Akunting\RugiLabaController@getdata')->name('rugiLaba.getdata');
        Route::get('rugiLaba/{start_date}/{end_date}', 'Akunting\RugiLabaController@cetak')->name('rugiLaba.cetak');

        Route::get('neraca', 'Akunting\NeracaController@index')->name('neraca.index');
        Route::post('neraca', 'Akunting\NeracaController@getdata')->name('neraca.getdata');
        Route::get('neraca/{bulan}/{tahun}', 'Akunting\NeracaController@cetak')->name('neraca.cetak');

        Route::get('arus-kas', 'Akunting\ArusKasController@index')->name('arusKas.index');
        Route::post('arus-kas', 'Akunting\ArusKasController@getdata')->name('arus-kas.getdata');
        Route::get('arus-kas/{start_date}/{end_date}', 'Akunting\ArusKasController@cetak')->name('arusKas.cetak');
    });

    Route::get('currency-conversion/{from}/{to}/{ammount}', 'CurrencyConversionController@convert');
});
Route::get('storage/{folder}/{filename}', 'StorageController@setstorage');
