<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbProdusenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_produsen', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_produsen', 100);
			$table->string('nama_produsen', 100);
			$table->text('alamat_produsen', 65535);
			$table->string('telp_produsen', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_produsen');
	}

}
