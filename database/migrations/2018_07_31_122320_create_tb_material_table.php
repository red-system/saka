<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_material', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_material', 100);
			$table->string('name', 100);
			$table->string('kd_kat', 100);
			$table->string('type_material', 100);
			$table->bigInteger('minimal_stock');
			$table->bigInteger('qty');
			$table->string('kd_satuan', 100);
			$table->string('kd_supplier', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_material');
	}

}
