<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbProductRejectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_product_reject', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_location', 100);
            $table->date('tgl_reject_product');
            $table->string('kd_product', 100);
            $table->text('deskripsi');
            $table->text('material');
            $table->bigInteger('qty');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_product_reject');
    }
}
