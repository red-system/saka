<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbJurnalUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jurnal_umum', function (Blueprint $table) {
            $table->increments('id_jurnal');
            $table->string('no_jurnal',25);
            $table->date('jmu_tanggal');
            $table->string('jmu_no',10);
            $table->text('jmu_keterangan');
            $table->string('jmu_year',4);
            $table->string('jmu_month',2);
            $table->string('jmu_day',2);
            $table->date('jmu_date_insert',10);
            $table->string('created_by',50);
            $table->string('updated_by',50);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jurnal_umum');
    }
}
