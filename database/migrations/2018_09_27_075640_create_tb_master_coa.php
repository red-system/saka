<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMasterCoa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_coa', function (Blueprint $table) {
            $table->increments('master_id');
            $table->Integer('mst_master_id');
            $table->string('mst_kode_rekening',10);
            $table->string('mst_nama_rekening',150);
            $table->date('mst_tanggal_awal');
            $table->string('mst_posisi',10);
            $table->string('mst_normal',10);
            $table->string('mst_status',10);
            $table->string('mst_tipe_laporan',10);
            $table->string('mst_tipe_nominal',10);
            $table->string('mst_neraca_tipe',10);
            $table->string('mst_kas_status',10);
            $table->string('created_by',50);
            $table->string('updated_by',50);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_coa');
    }
}
