<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDetailPurchasingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_purchasing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_material', 100);
            $table->string('name');
            $table->string('kd_kat', 100);
            $table->string('type', 100);
            $table->decimal('price', [13, 4]);
            $table->integer('minimal_stocks');
            $table->integer('qty');
            $table->string('satuan');
            $table->string('kd_purchasing', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_purchasing');
    }
}
