<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_supplier', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_supplier', 100);
			$table->string('nama_supplier', 100);
			$table->text('alamat_supplier', 65535);
			$table->text('kontak_supplier', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_supplier');
	}

}
