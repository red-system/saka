<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbReturSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_retur_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_retur');
            $table->date('tgl_pengembalian');
            $table->string('pelaksana', 100);
            $table->string('alasan', 100);
            $table->text('alasan_lain');
            $table->integer('disc_persen')->default(0);
            $table->decimal('disc_nominal', [13, 4])->default(0);
            $table->decimal('total_retur', [13, 4]);
            $table->decimal('tambahan_biaya', [13, 4]);
            $table->string('no_faktur', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_retur_sales');
    }
}
