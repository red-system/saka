<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbProductionMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_production_material', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_material', 100);
			$table->bigInteger('qty');
			$table->string('kd_satuan', 100);
            $table->string('kd_produksi', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_production_material');
	}

}
