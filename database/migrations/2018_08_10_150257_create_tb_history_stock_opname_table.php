<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbHistoryStockOpnameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_history_stock_opname', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tgl');
            $table->string('kode');
            $table->text('deskripsi');
            $table->string('location');
            $table->bigInteger('stock_awal');
            $table->bigInteger('stock_akhir');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_history_stock_opname');
    }
}
