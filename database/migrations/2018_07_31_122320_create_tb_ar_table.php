<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbArTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_ar', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_ar', 100);
			$table->string('no_faktur', 100);
			$table->dateTime('tgl');
			$table->date('jatuh_tempo');
			$table->bigInteger('total_ap');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_ar');
	}

}
