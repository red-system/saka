<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDetailReturPurchasing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_retur_purchasing', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_detail_retur');
            $table->string('kd_material', 100);
            $table->decimal('price', [13, 4]);
            $table->integer('qty_retur');
            $table->decimal('total_retur', [13, 4]);
            $table->string('no_retur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_retur_purchasing');
    }
}
