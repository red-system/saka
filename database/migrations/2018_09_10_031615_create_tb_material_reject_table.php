<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMaterialRejectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_material_reject', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_reject_material');
            $table->string('kd_material', 100);
            $table->string('name');
            $table->string('type_material');
            $table->bigInteger('qty');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_material_reject');
    }
}
