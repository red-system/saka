<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbDetailProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_detail_production', function(Blueprint $table)
		{
			$table->integer('kd_detail_production', true);
			$table->text('deskripsi', 65535);
			$table->bigInteger('qty');
            $table->bigInteger('qty_progress')->default(0);
			$table->string('img_design');
			$table->string('kd_produksi', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_detail_production');
	}

}
