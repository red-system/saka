<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbHistoryTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_history_transfer', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tgl');
            $table->string('kode');
            $table->string('no_seri');
            $table->text('deskripsi');
            $table->bigInteger('qty_stock');
            $table->string('dari');
            $table->string('tujuan');
            $table->bigInteger('qty_transfer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_history_transfer');
    }
}
