<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_transaksi', function (Blueprint $table) {
            $table->increments('id_transaksi');
            $table->Integer('id_jurnal_umum');
            $table->Integer('master_id');
            $table->string('trs_jenis_transaksi',50);
            $table->bigInteger('trs_debet')->default(0);
            $table->bigInteger('trs_kredit')->default(0);
            $table->Integer('user_id');
            $table->string('trs_year',4);
            $table->string('trs_month',2);
            $table->string('trs_kode_rekening',10);
            $table->string('trs_nama_rekening',50); 
            $table->string('trs_tipe_arus_kas');
            $table->text('trs_catatan',25)->nullable();
            $table->date('trs_date_insert');
            $table->date('trs_date_update');
            $table->string('trs_charge',10)->default(0);
            $table->string('trs_no_check_bg',4)->nullable();
            $table->date('trs_tgl_pencairan');
            $table->bigInteger('trs_setor')->default(0);
            $table->string('created_by',50);
            $table->string('updated_by',50);           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_transaksi');
    }
}
