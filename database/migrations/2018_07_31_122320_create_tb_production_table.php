<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbProductionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_production', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_produksi', 100);
			$table->date('tgl_produksi');
			$table->string('kd_produsen', 100);
			$table->decimal('total', [13, 2]);
			$table->integer('ppn_persen');
			$table->decimal('ppn_nominal', [13, 2]);
			$table->decimal('grand_total', [13, 2]);
			$table->string('status', 100)->default('draft');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_production');
	}

}
