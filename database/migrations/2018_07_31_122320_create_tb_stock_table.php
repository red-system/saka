<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbStockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_stock', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_produk', 100);
            $table->string('no_seri_produk', 100);
			$table->string('kd_location', 100);
			$table->bigInteger('qty')->default('0');
            $table->bigInteger('hpp')->default('0');
			$table->dateTime('created_at');
			$table->dateTime('updated_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_stock');
	}

}
