<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbSalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_sales', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('no_faktur', 100);
			$table->dateTime('tgl');
			$table->string('nama_pelanggan', 100);
			$table->text('alamat', 65535);
			$table->string('telp', 100);
            $table->string('email');
            $table->string('customer_type')->default('retail');
            $table->string('used_currency', 100)->default('IDR');
            $table->double('usd1_to_usecurr');
            $table->double('idr1_to_usecurr');
			$table->decimal('total', [13, 4]);
			$table->integer('disc_persen');
			$table->decimal('disc_nominal', [13, 4]);
            $table->bigInteger('komisi_persen');
            $table->decimal('komisi_nominal', [13, 4]);
			$table->decimal('grand_total', [13, 4]);
			$table->string('type_sales', 100);
			$table->string('kd_location', 100);
            $table->text('surcharge')->nullable();
            $table->decimal('usercurr1_to_idr', [13, 4]);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_sales');
	}

}
