<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_product', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_produk', 100);
			$table->text('deskripsi', 65535);
			$table->string('kd_kat', 100);
			$table->string('material', 100);
            $table->float('weight')->default(0);
			$table->decimal('price', [13,2])->default(0);
            $table->decimal('wholesale_price', [13,2])->default(0);
			$table->bigInteger('minimal_stock')->default(0);
            $table->bigInteger('disc_persen')->default(0);
            $table->decimal('disc_nominal', [13,2])->default(0);
            $table->string('img_produk', 255)->nullable();
            $table->integer('collection_id')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_product');
	}

}
