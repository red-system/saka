<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbHistoryMaterialOpnameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_history_material_opname', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tgl');
            $table->string('kode');
            $table->string('nama_material');
            $table->string('kategori');
            $table->string('type_material');
            $table->string('satuan');
            $table->string('supplier');
            $table->bigInteger('qty_awal');
            $table->bigInteger('qty_akhir');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_history_material_opname');
    }
}
