<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_location', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_location', 100);
            $table->string('location_name');
			$table->text('pic', 65535);
			$table->text('alamat', 65535);
			$table->string('telp', 100);
			$table->bigInteger('potongan');
			$table->string('kd_provinsi', 100);
            $table->string('type');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_location');
	}

}
