<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbGoodsFlowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_goods_flow', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('tgl');
            $table->string('kode');
			$table->text('nama', 65535);
            $table->string('no_seri')->nullable()->default(null);
			$table->bigInteger('qty_in');
			$table->string('satuan_in', 100);
			$table->bigInteger('qty_out');
			$table->string('satuan_out', 100);
			$table->text('keterangan', 65535);
			$table->string('kd_location', 100);
			$table->string('type_flow', 100);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_goods_flow');
	}

}
