<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPurchasingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_purchasing', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('kd_purchasing', 100);
			$table->string('no_faktur', 100);
			$table->dateTime('tgl');
			$table->string('kd_supplier', 100);
			$table->decimal('total', [13, 4]);
            $table->decimal('biaya_tambahan', [13, 4]);
            $table->integer('disc_persen');
            $table->decimal('disc_nominal', [13, 4]);
			$table->integer('ppn_persen');
			$table->decimal('ppn_nominal', [13, 4]);
			$table->decimal('grand_total', [13, 4]);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_purchasing');
	}

}
