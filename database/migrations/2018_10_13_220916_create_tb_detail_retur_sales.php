<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDetailReturSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_retur', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_detail_retur');
            $table->string('kd_produk', 100);
            $table->string('no_seri', 100);
            $table->decimal('price', [13, 4]);
            $table->decimal('hpp', [13, 4]);
            $table->integer('disc_persen')->default(0);
            $table->decimal('disc_nominal', [13, 4])->default(0);
            $table->integer('qty_retur');
            $table->decimal('total_retur', [13, 4]);
            $table->string('no_retur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_retur_sales');
    }
}
