<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbDetailSalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_detail_sales', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->string('no_faktur', 100);
			$table->string('kd_produk', 100);
            $table->string('no_seri', 100);
            $table->decimal('base_retail_price', [13, 4])->default(0);
            $table->decimal('base_wholesale_price', [13, 4])->default(0);
            $table->decimal('retail_price', [13, 4])->default(0);
            $table->decimal('wholesale_price', [13, 4])->default(0);
            $table->string('price_type')->default('retail_price');
			$table->decimal('price', [13, 4]);
			$table->decimal('hpp', [13, 4]);
			$table->integer('qty');
            $table->string('catatan')->nullable();
			$table->integer('disc_persen');
			$table->decimal('disc_nominal', [13, 4]);
			$table->decimal('subtotal', [13, 4]);
            $table->integer('qty_retur')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_detail_sales');
	}

}
