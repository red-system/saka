<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin1 = \App\Models\User::create([
            'name' => 'Admin 1',
            'username' => 'admin1',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'owner_or_manager_kantorpusat'
        ]);

        $admin2 = \App\Models\User::create([
            'name' => 'Admin 2',
            'username' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'owner_or_manager_kantorpusat'
        ]);
    }
}
