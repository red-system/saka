@component('mail::message')
# Hallo {{ $name }}, <br>

{!! $message !!}

Best regards,<br>
Saka Karya Bali
@endcomponent
