<header class="main-header">
    <nav class="navbar navbar-fixed-top">
        <div class="">
            <div class="navbar-header">
                <a href="{{ route('pos.index') }}" class="navbar-brand"><b>SAKA </b>POS</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    @if(Auth::user()->role!='sales')
                        <li @if(\Request::segment(1) == 'dashboard') class="active" @endif><a href="{{ route('dashboard') }}">Home</a></li>
                    @endif
                    <li @if(\Request::segment(2) == 'pos' && \Request::segment(3) == '') class="active" @endif><a href="{{ route('pos.index') }}">POS</a></li>
                    {{--<li><a href="#">History</a></li>--}}
                    <li @if(\Request::segment(2) == 'pos' && \Request::segment(3) == 'retur-sales') class="active" @endif><a href="{{ route('retur-sales.index') }}">Retur Sales</a></li>
                    <li @if(\Request::segment(1) == 'inventory' && \Request::segment(2) == 'produk-stock-location') class="active" @endif><a href="{{ route('stok-bylocation.index') }}">Product Stock By Location</a></li>
                    <li @if(\Request::segment(1) == 'inventory' && \Request::segment(2) == 'transfer-stok') class="active" @endif><a href="{{ route('transfer-stok.index') }}">Transfer Stock</a></li>
                    {{--<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cash Management <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Cash In</a></li>
                            <li><a href="#">Cash Out</a></li>

                            <li class="divider"></li>
                            <li><a href="#">Pembelanjaan</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Jurnal Transaksi</a></li>
                        </ul>
                    </li>--}}
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Notifications Menu -->
                   {{-- <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li><!-- start notification -->
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                    <!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>--}}

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ auth()->user()->user_profile }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">My Account</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ auth()->user()->user_profile }}" class="img-circle" alt="User Image">
                                <p>
                                    {{ auth()->user()->name }}
                                    <small>{{ auth()->user()->human_role }}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('myprofile.index') }}" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                    {{--<li>
                        <a href="{{ route('dashboard') }}" data-toggle="control-sidebar"><i class="fa fa-home" style="font-size: 20px;"></i></a>
                    </li>--}}
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>