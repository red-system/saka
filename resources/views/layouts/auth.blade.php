<!DOCTYPE html>
<html lang="id">
<head>
    <title>Login Saka Karya Bali App</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('auth/images/icons/favicon.ico') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/vendor/animate/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/vendor/css-hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('auth/css/pnotify.css') }}">
    @yield('custom-css')
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        @yield('content')
    </div>
</div>

<script src="{{ asset('auth/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('auth/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('auth/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('auth/vendor/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('auth/js/pnotify.min.js') }}"></script>
<script src="{{ asset('auth/js/main.js') }}"></script>
@yield('custom-script')
@if(Session::has('notification'))
    <script>
        $(function () {
            new PNotify({
                title: '{{ Session::get('notification.title', 'Info') }}',
                text: '{{ Session::get('notification.message') }}',
                addclass: 'alert-styled-left',
                type: '{{ Session::get('notification.level', 'info') }}'
            });
        });
    </script>
@elseif($errors->has('login_eror'))
    <script>
        $(function () {
            new PNotify({
                title: 'Error!',
                text: '{!! $errors->first('login_eror', ':message') !!}',
                addclass: 'alert-styled-left',
                type: 'error'
            });
        });
    </script>
@elseif(Session::has('status'))
    <script>
        $(function () {
            new PNotify({
                title: 'Success',
                text: '{{ session('status') }}',
                addclass: 'alert-styled-left',
                type: 'success'
            });
        });
    </script>
@endif
</body>
</html>