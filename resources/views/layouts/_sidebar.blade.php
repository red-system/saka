<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ auth()->user()->user_profile }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li @if(\Request::segment(1) == 'dashboard') class="active" @endif>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Home</span>
                </a>
            </li>

            @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
            <li class="treeview @if(\Request::segment(1) == 'master-data') active @endif">
                <a href="#">
                    <i class="fa fa-clone"></i>
                    <span>Master Data</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                    <li @if(\Request::segment(2) == 'supplier') class="active" @endif><a href="{{ route('supplier.index') }}"><i class="fa fa-angle-right"></i> Supplier</a></li>
                    <li @if(\Request::segment(2) == 'produsen') class="active" @endif><a href="{{ route('produsen.index') }}"><i class="fa fa-angle-right"></i> Vendor</a></li>
                    <li @if(\Request::segment(2) == 'daerah') class="active" @endif><a href="{{ route('daerah.index') }}"><i class="fa fa-angle-right"></i> Area</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'location') class="active" @endif><a href="{{ route('location.index') }}"><i class="fa fa-angle-right"></i> Sales Location</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                    <li @if(\Request::segment(2) == 'satuan') class="active" @endif><a href="{{ route('satuan.index') }}"><i class="fa fa-angle-right"></i> Unit</a></li>
                    <li @if(\Request::segment(2) == 'kategori-material') class="active" @endif><a href="{{ route('kategori-material.index') }}"><i class="fa fa-angle-right"></i> Category</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                    <li @if(\Request::segment(2) == 'data-user') class="active" @endif><a href="{{ route('data-user.index') }}"><i class="fa fa-angle-right"></i> User</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                        <li @if(\Request::segment(2) == 'collection') class="active" @endif><a href="{{ route('collection.index') }}"><i class="fa fa-angle-right"></i> Collection</a></li>
                        <li @if(\Request::segment(2) == 'currency') class="active" @endif><a href="{{ route('currency.index') }}"><i class="fa fa-angle-right"></i> Currency</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
            <li class="treeview @if(\Request::segment(1) == 'inventory') active @endif">
                <a href="#">
                    <i class="fa fa-cubes"></i>
                    <span>Inventory</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'material') class="active" @endif><a href="{{ route('material.index') }}"><i class="fa fa-angle-right"></i> Material</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                        <li @if(\Request::segment(2) == 'produk') class="active" @endif><a href="{{ route('produk.index') }}"><i class="fa fa-angle-right"></i> Product</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'produk-stock-location') class="active" @endif><a href="{{ route('stok-bylocation.index') }}"><i class="fa fa-angle-right"></i> Product Stock by Location</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                        <li @if(\Request::segment(2) == 'transfer-stok') class="active" @endif><a href="{{ route('transfer-stok.index') }}"><i class="fa fa-angle-right"></i> Transfer Stock</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'material-opname') class="active" @endif><a href="{{ route('material-opname.index') }}"><i class="fa fa-angle-right"></i> Stock Opname Material</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                        <li @if(\Request::segment(2) == 'stock-opname') class="active" @endif><a href="{{ route('stock-opname.index') }}"><i class="fa fa-angle-right"></i> Stock Opname</a></li>
                        <li @if(\Request::segment(2) == 'stock-card') class="active" @endif><a href="{{ route('stock-card.index') }}"><i class="fa fa-angle-right"></i> Stock Card</a></li>
                        <li @if(\Request::segment(2) == 'stock-allert') class="active" @endif>
                            <a href="{{ route('stock-allert.index') }}"><i class="fa fa-angle-right"></i>
                                <span>Stock Allert</span>
                                @if($jmlallert > 0)
                                    <span class="pull-right-container">
                                      <small class="label pull-right bg-red">{{ $jmlallert }}</small>
                                    </span>
                                @endif
                            </a>
                        </li>
                        {{--<li @if(\Request::segment(2) == 'summary-stock') class="active" @endif><a href="{{ route('summary-stock.index') }}"><i class="fa fa-angle-right"></i> Summary Stock</a></li>--}}
                        <li @if(\Request::segment(2) == 'summary-stock-outlet') class="active" @endif><a href="{{ route('summary-stock-outlet.index') }}"><i class="fa fa-angle-right"></i> Summary Stock Outlet</a></li>
                        <li @if(\Request::segment(2) == 'summary-stock-global') class="active" @endif><a href="{{ route('summary-stock-global.index') }}"><i class="fa fa-angle-right"></i> Summary Stock Global</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'material-reject') class="active" @endif><a href="{{ route('material-reject.index') }}"><i class="fa fa-angle-right"></i> Material Reject</a></li>
                    @endif
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
                        <li @if(\Request::segment(2) == 'product-reject') class="active" @endif><a href="{{ route('product-reject.index') }}"><i class="fa fa-angle-right"></i> Product Reject</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_production'))
            <li class="treeview @if(\Request::segment(1) == 'production') active @endif">
                <a href="#">
                    <i class="fa fa-puzzle-piece"></i>
                    <span>Production</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(\Request::segment(2) == 'costing') class="active" @endif><a href="{{ route('costing.index') }}"><i class="fa fa-angle-right"></i> Costing</a></li>
                    <li @if(\Request::segment(2) == 'produksi') class="active" @endif><a href="{{ route('produksi.index') }}"><i class="fa fa-angle-right"></i> Production</a></li>
                    {{--<li @if(\Request::segment(2) == 'saran-produksi') class="active" @endif><a href="{{ route('saran-produksi.index') }}"><i class="fa fa-angle-right"></i> Saran Produksi</a></li>--}}
                    <li @if(\Request::segment(2) == 'pekerjaan-vendor') class="active" @endif><a href="{{ route('pekerjaan-vendor.index') }}"><i class="fa fa-angle-right"></i> Vendor work</a></li>
                </ul>
            </li>
            @endif

            @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_sales'))
            <li class="treeview @if(\Request::segment(1) == 'transaction') active @endif">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Transaction</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_sales'))
                        <li @if(\Request::segment(2) == 'pos') class="active" @endif><a href="{{ route('pos.index') }}"><i class="fa fa-angle-right"></i> POS</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_sales'))
                        <li @if(\Request::segment(2) == 'retur-sales') class="active" @endif><a href="{{ route('retur-sales.index') }}"><i class="fa fa-angle-right"></i> Retur Sales</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'pembelian') class="active" @endif><a href="{{ route('pembelian.index') }}"><i class="fa fa-angle-right"></i> Purchasing</a></li>
                    @endif

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'retur-purchasing') class="active" @endif><a href="{{ route('retur-purchasing.index') }}"><i class="fa fa-angle-right"></i> Retur Purchasing</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(\Gate::allows('as_owner_or_manager_kantorpusat') || \Gate::allows('as_manager_or_admin_toko'))
            <li class="treeview @if(\Request::segment(1) == 'laporan') active @endif">
                <a href="#">
                    <i class="fa fa-folder-open"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(\Request::segment(2) == 'penjualan-global') class="active" @endif><a href="{{ route('penjualan-global.index') }}"><i class="fa fa-angle-right"></i> Sales Global</a></li>
                    <li @if(\Request::segment(2) == 'penjualan-stok') class="active" @endif><a href="{{ route('penjualan-stok.index') }}"><i class="fa fa-angle-right"></i> Sales Stock</a></li>
                    <li @if(\Request::segment(2) == 'history-penjualan-stok') class="active" @endif><a href="{{ route('history-penjualan-stok.index') }}"><i class="fa fa-angle-right"></i> History Sales Stock</a></li>
                    <li @if(\Request::segment(2) == 'history-penjualan-customer') class="active" @endif><a href="{{ route('history-penjualan-customer.index') }}"><i class="fa fa-angle-right"></i> History Sales by Customer</a></li>
                    <li @if(\Request::segment(2) == 'retur-penjualan') class="active" @endif><a href="{{ route('retur-penjualan.index') }}"><i class="fa fa-angle-right"></i> Retur Sales</a></li>
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <li @if(\Request::segment(2) == 'pembelian-global') class="active" @endif><a href="{{ route('pembelian-global.index') }}"><i class="fa fa-angle-right"></i> Purchasing Global</a></li>
                        <li @if(\Request::segment(2) == 'pembelian-material') class="active" @endif><a href="{{ route('pembelian-material.index') }}"><i class="fa fa-angle-right"></i> Purchasing Material</a></li>
                        <li @if(\Request::segment(2) == 'produksi-global') class="active" @endif><a href="{{ route('produksi-global.index') }}"><i class="fa fa-angle-right"></i> Production Global</a></li>
                        <li @if(\Request::segment(2) == 'produksi-produk') class="active" @endif><a href="{{ route('produksi-produk.index') }}"><i class="fa fa-angle-right"></i> Production Produk</a></li>
                        <li @if(\Request::segment(2) == 'penggunaan-bahan') class="active" @endif><a href="{{ route('penggunaan-bahan.index') }}"><i class="fa fa-angle-right"></i> Material Use</a></li>
                    @endif
                </ul>
            </li>
            @endif
            <li class="treeview @if(\Request::segment(1) == 'hutang-piutang') active @endif">
                <a href="#">
                    <i class="fa fa-credit-card"></i> 
                    <span>Hutang Piutang</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(\Request::segment(2) == 'penerimaan-hutang-piutang') class="active" @endif><a href="{{ route('penerimaan-hutang-piutang.index') }}"><i class="fa fa-angle-right"></i>Penerimaan Hutang Piutang</a></li>
                    <li @if(\Request::segment(2) == 'hutang-supplier') class="active" @endif><a href="{{ route('hutang-supplier.index') }}"><i class="fa fa-angle-right"></i>AP (Production & Purchasing)</a></li>
                    <li @if(\Request::segment(2) == 'piutang-pelanggan') class="active" @endif><a href="{{ route('piutang-pelanggan.index') }}"><i class="fa fa-angle-right"></i>AR (Sales)</a></li>
                    <li @if(\Request::segment(2) == 'hutang-lain') class="active" @endif><a href="{{ route('hutang-lain.index') }}"><i class="fa fa-angle-right"></i>AP (Others)</a></li>
                    <li @if(\Request::segment(2) == 'piutang-lain') class="active" @endif><a href="{{ route('piutang-lain.index') }}"><i class="fa fa-angle-right"></i>AR (Others)</a></li>
                    
                </ul>
            </li>
            <li class="treeview @if(\Request::segment(1) == 'accounting') active @endif">
                <a href="#">
                    <i class="fa fa-balance-scale"></i> 
                    <span>Accounting</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right text-yellow"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li @if(\Request::segment(2) == 'kodePerkiraan' || \Request::segment(2) == 'saldoAwal') class="active" @endif><a href="{{ route('kodePerkiraan.index') }}"><i class="fa fa-angle-right"></i> Kode Akun (COA)</a></li>
                    <li @if(\Request::segment(2) == 'jurnalHarian') class="active" @endif><a href="{{ route('jurnalHarian.index') }}"><i class="fa fa-angle-right"></i> Jurnal Harian</a></li>
                    <li @if(\Request::segment(2) == 'bukuBesar') class="active" @endif><a href="{{ route('bukuBesar.index') }}"><i class="fa fa-angle-right"></i> Buku Besar</a></li>
                    <li @if(\Request::segment(2) == 'rugiLaba') class="active" @endif><a href="{{ route('rugiLaba.index') }}"><i class="fa fa-angle-right"></i> Rugi Laba</a></li>
                    <li @if(\Request::segment(2) == 'neraca') class="active" @endif><a href="{{ route('neraca.index') }}"><i class="fa fa-angle-right"></i> Neraca</a></li>
                    <li @if(\Request::segment(2) == 'arus-kas') class="active" @endif><a href="{{ route('arusKas.index') }}"><i class="fa fa-angle-right"></i> Arus Kas</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>