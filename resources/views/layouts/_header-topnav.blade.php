<header class="main-header">
    <nav class="navbar navbar-fixed-top">
        <div class="">
            <div class="navbar-header">
                <a href="#" class="navbar-brand"><b>SAKA </b>App</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    @if(\Gate::allows('as_sales') || \Gate::allows('as_accounting'))
                        <li><a href="#">Home</a></li>
                    @endif

                    @if(\Gate::allows('as_production'))
                        <li @if(\Request::segment(2) == 'produksi') class="active" @endif><a href="{{ route('produksi.index') }}"> Produksi</a></li>
                        {{--<li @if(\Request::segment(2) == 'saran-produksi') class="active" @endif><a href="{{ route('saran-produksi.index') }}"> Saran Produksi</a></li>--}}
                    @endif

                    @if(\Gate::allows('as_accounting'))

                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ auth()->user()->user_profile }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">My Account</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ auth()->user()->user_profile }}" class="img-circle" alt="User Image">
                                <p>
                                    {{ auth()->user()->name }}
                                    <small>{{ auth()->user()->human_role }}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('myprofile.index') }}" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                    <li>
                        <a href="{{ route('dashboard') }}" data-toggle="control-sidebar"><i class="fa fa-home" style="font-size: 20px;"></i></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>