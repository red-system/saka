@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbKategoriMaterial').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 2,
                    orderable: false
                } ]
            });

            $('#tbKategoriProduk').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 2,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Kategori - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Kategori
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Category</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#material" data-toggle="tab">Material</a></li>
                            <li><a href="#produk" data-toggle="tab">Product</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="material">
                                <a class="btn btn-social btn-google" data-toggle="modal" data-target="#addKategoriMaterial">
                                    <i class="fa fa-plus"></i> Add Material Category
                                </a>
                                <hr>
                                <table id="tbKategoriMaterial" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Category</th>
                                        <th class="text-center">Act</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($kategorimaterials as $km)
                                        <tr>
                                            <td>{{ $km->kd_kat }}</td>
                                            <td>{{ $km->kategori }}</td>
                                            <td class="text-center">
                                                {!! Form::model($km, ['route' => ['kategori-material.destroy', $km->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <a href="{{ route('kategori-material.edit', $km->id)}}" data-target="#editKategoriMaterial" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                                    <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                                </span>
                                                {!! Form::close()!!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!-- MODALS -->
                                <div class="modal fade" id="addKategoriMaterial">
                                    <div class="modal-dialog modal-center">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Add Material Category</h4>
                                            </div>
                                            {!! Form::open(['route' => 'kategori-material.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                                                            {!! Form::label('kd_kat', 'Code', ['class' => 'control-label']) !!}
                                                            {{ Form::text('kd_kat', $nokatmaterial, ['class' => 'form-control','required']) }}
                                                            {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                        <div class="form-group {!! $errors->has('kategori') ? 'has-error' : '' !!}">
                                                            {!! Form::label('kategori', 'Category') !!}
                                                            {{ Form::text('kategori', null, ['class' => 'form-control','required']) }}
                                                            {!! $errors->first('kategori', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                                <div id="editKategoriMaterial" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-center">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                                <span> &nbsp;&nbsp;Loading... </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="produk">
                                <a class="btn btn-social btn-google" data-toggle="modal" data-target="#addKategoriProduk">
                                    <i class="fa fa-plus"></i> Add Product Category
                                </a>
                                <hr>
                                <table id="tbKategoriProduk" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Category</th>
                                        <th class="text-center">Act</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($kategoriproduks as $kp)
                                        <tr>
                                            <td>{{ $kp->kd_kat }}</td>
                                            <td>{{ $kp->kategori }}</td>
                                            <td class="text-center">
                                                {!! Form::model($kp, ['route' => ['kategori-produk.destroy', $kp->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <a href="{{ route('kategori-produk.edit', $kp->id)}}" data-target="#editKategoriProduk" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                </span>
                                                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                                    <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                                </span>
                                                {!! Form::close()!!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!-- MODALS -->
                                <div class="modal fade" id="addKategoriProduk">
                                    <div class="modal-dialog modal-center">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Add Produk Category</h4>
                                            </div>
                                            {!! Form::open(['route' => 'kategori-produk.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                                                            {!! Form::label('kd_kat', 'Code', ['class' => 'control-label']) !!}
                                                            {{ Form::text('kd_kat', $nokatproduk, ['class' => 'form-control','required']) }}
                                                            {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                        <div class="form-group {!! $errors->has('kategori') ? 'has-error' : '' !!}">
                                                            {!! Form::label('kategori', 'Category') !!}
                                                            {{ Form::text('kategori', null, ['class' => 'form-control','required']) }}
                                                            {!! $errors->first('kategori', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                                <div id="editKategoriProduk" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-center">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                                <span> &nbsp;&nbsp;Loading... </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection