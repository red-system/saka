<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Currency</h4>
</div>
{!! Form::model($currency, ['route' => ['currency.update', $currency->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="col-md-12">
                    <div class="form-group {!! $errors->has('kd_currency') ? 'has-error' : '' !!}">
                        {!! Form::label('kd_currency', 'Kode Currency') !!}
                        {{ Form::text('kd_currency', null, ['class' => 'form-control','required']) }}
                        {!! $errors->first('kd_currency', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {!! $errors->has('currency') ? 'has-error' : '' !!}">
                        {!! Form::label('currency', 'Currency') !!}
                        {{ Form::text('currency', null, ['class' => 'form-control','required']) }}
                        {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {!! $errors->has('remark') ? 'has-error' : '' !!}">
                        {!! Form::label('remark', 'Remark') !!}
                        {{ Form::text('remark', null, ['class' => 'form-control','required']) }}
                        {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>