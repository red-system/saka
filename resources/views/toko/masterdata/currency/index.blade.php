@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbCurrency').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 1,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Currency - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Currency
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Currency</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Currency</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbCurrency" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="col-md-2 text-center">Kode Currency</th>
                            <th>Currency</th>
                            <th>Kurs</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($currencies as $cr)
                            <tr>
                                <td class="col-md-2 text-center">{{ $cr->kd_currency }}</td>
                                <td>{{ $cr->currency }}</td>
                                <td>{{ $cr->kurs }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
