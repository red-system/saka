@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbLocation').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 7,
                    orderable: false
                } ]
            });

            $('.select2').select2();
        })
    </script>
@endsection

@section('title')
    Location - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Sales Location
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Sales Location</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addLocation">
                <i class="fa fa-plus"></i> Add Sales Location
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Sales Location</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbLocation" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Location Name</th>
                            <th>PIC</th>
                            <th class="none">Adress</th>
                            <th>Tlp</th>
                            <th class="text-center">Consignment fee</th>
                            <th class="none">Area</th>
                            <th class="all">Type</th>
                            <th class="text-center all">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($locations as $location)
                            <tr>
                                <td>{{ $location->kd_location }}</td>
                                <td>{{ $location->location_name }}</td>
                                <td>{{ $location->pic }}</td>
                                <td>{{ $location->alamat }}</td>
                                <td>{{ $location->telp }}</td>
                                <td class="text-center">{{ $location->potongan.'%' }}</td>
                                <td>{{ $location->provinsi->provinsi }}</td>
                                <td>{{ $location->human_type }}</td>
                                <td class="text-center">
                                    {!! Form::model($location, ['route' => ['location.destroy', $location->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('location.edit', $location->id)}}" data-target="#editLocation" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addLocation">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Sales Location</h4>
                </div>
                {!! Form::open(['route' => 'location.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_location') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_location', 'Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_location', $nolocation, ['class' => 'form-control','required']) }}
                                {!! $errors->first('kd_location', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('location_name') ? 'has-error' : '' !!}">
                                {!! Form::label('location_name', 'Location Name') !!}
                                {{ Form::text('location_name', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('location_name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('pic') ? 'has-error' : '' !!}">
                                {!! Form::label('pic', 'PIC') !!}
                                {{ Form::text('pic', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('pic', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                                {!! Form::label('alamat', 'Adress') !!}
                                {{ Form::text('alamat', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('telp') ? 'has-error' : '' !!}">
                                {!! Form::label('telp', 'Tlp') !!}
                                {{ Form::text('telp', null, ['class' => 'form-control']) }}
                                {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('potongan') ? 'has-error' : '' !!}">
                                {!! Form::label('potongan', 'Consignment fee (%)') !!}
                                {{ Form::number('potongan', null, ['class' => 'form-control','required', 'max' => 100]) }}
                                {!! $errors->first('potongan', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('kd_provinsi') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_provinsi', 'Area') !!}
                                {{ Form::select('kd_provinsi', ['' => '']+App\Models\Provinsi::pluck('provinsi', 'kd_provinsi')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_provinsi', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('type') ? 'has-error' : '' !!}">
                                {!! Form::label('type', 'Type') !!}
                                {{ Form::select('type', ['' => '']+App\Models\Location::typeList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editLocation" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection