<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Sales Location</h4>
</div>
{!! Form::model($location, ['route' => ['location.update', $location->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="form-group {!! $errors->has('kd_location') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_location', 'Code', ['class' => 'control-label']) !!}
                    {{ Form::text('kd_location', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('kd_location', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('location_name') ? 'has-error' : '' !!}">
                    {!! Form::label('location_name', 'Location Name') !!}
                    {{ Form::text('location_name', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('location_name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('pic') ? 'has-error' : '' !!}">
                    {!! Form::label('pic', 'PIC') !!}
                    {{ Form::text('pic', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('pic', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                    {!! Form::label('alamat', 'Adress') !!}
                    {{ Form::text('alamat', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('telp') ? 'has-error' : '' !!}">
                    {!! Form::label('telp', 'Tlp') !!}
                    {{ Form::text('telp', null, ['class' => 'form-control']) }}
                    {!! $errors->first('telp', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('potongan') ? 'has-error' : '' !!}">
                    {!! Form::label('potongan', 'Consignment fee (%)') !!}
                    {{ Form::number('potongan', null, ['class' => 'form-control','required', 'max' => 100]) }}
                    {!! $errors->first('potongan', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('kd_provinsi') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_provinsi', 'Area') !!}
                    {{ Form::select('kd_provinsi', ['' => '']+App\Models\Provinsi::pluck('provinsi', 'kd_provinsi')->all(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('kd_provinsi', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('type') ? 'has-error' : '' !!}">
                    {!! Form::label('type', 'Type') !!}
                    {{ Form::select('type', ['' => '']+App\Models\Location::typeList(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $('.select2u').select2();
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>