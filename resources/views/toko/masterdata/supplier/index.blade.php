@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbSupplier').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Data Supplier - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Supplier
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Supplier</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addSupplier">
                <i class="fa fa-plus"></i> Add Supplier
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Supplier</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbSupplier" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Adress</th>
                            <th>Contacts</th>
                            <th class="text-center">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($suppliers as $supplier)
                        <tr>
                            <td>{{ $supplier->kd_supplier }}</td>
                            <td>{{ $supplier->nama_supplier }}</td>
                            <td>{{ $supplier->alamat_supplier }}</td>
                            <td>{{ $supplier->kontak_supplier }}</td>
                            <td class="text-center">
                                {!! Form::model($supplier, ['route' => ['supplier.destroy', $supplier->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <span data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="{{ route('supplier.edit', $supplier->id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                    <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                </span>
                                {!! Form::close()!!}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addSupplier">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tambah Supplier</h4>
                </div>
                {!! Form::open(['route' => 'supplier.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_supplier', 'Supplier Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_supplier', $nosupplier, ['class' => 'form-control','required']) }}
                                {!! $errors->first('kd_supplier', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('nama_supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('nama_supplier', 'Supplier Name') !!}
                                {{ Form::text('nama_supplier', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('nama_supplier', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('alamat_supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('alamat_supplier', 'Supplier Adress') !!}
                                {{ Form::text('alamat_supplier', null, ['class' => 'form-control']) }}
                                {!! $errors->first('alamat_supplier', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('kontak_supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('kontak_supplier', 'Supplier Contacts') !!}
                                {{ Form::text('kontak_supplier', null, ['class' => 'form-control']) }}
                                {!! $errors->first('kontak_supplier', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editSupplier" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection