@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbCollection').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 1,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Collection - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Collection
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Collection</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Collection</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <a class="btn btn-social btn-google" data-toggle="modal" data-target="#addCollection">
                        <i class="fa fa-plus"></i> Add Collection
                    </a>
                    <hr>
                    <table id="tbCollection" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Collection</th>
                            <th class="text-center col-md-2">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($collections as $cl)
                            <tr>
                                <td>{{ $cl->collection }}</td>
                                <td class="text-center">
                                    {!! Form::model($cl, ['route' => ['collection.destroy', $cl->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                        <span data-toggle="tooltip" data-placement="top" title="Edit">
                                            <a href="{{ route('collection.edit', $cl->id)}}" data-target="#editCollection" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                        </span>
                                        <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                            <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                        </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <!-- MODALS -->
                    <div class="modal fade" id="addCollection">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Tambah Collection</h4>
                                </div>
                                {!! Form::open(['route' => 'collection.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {!! $errors->has('collection') ? 'has-error' : '' !!}">
                                                {!! Form::label('collection', 'Collection') !!}
                                                {{ Form::text('collection', null, ['class' => 'form-control','required']) }}
                                                {!! $errors->first('collection', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                    <div id="editCollection" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                    <span> &nbsp;&nbsp;Loading... </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
