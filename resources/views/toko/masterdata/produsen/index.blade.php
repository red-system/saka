@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbProdusen').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Data Produsen - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Produsen
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Produsen</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addProdusen">
                <i class="fa fa-plus"></i> Add Produsen
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Produsen</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbProdusen" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Adress</th>
                            <th>No Tlp</th>
                            <th class="text-center">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($produsens as $produsen)
                            <tr>
                                <td>{{ $produsen->kd_produsen }}</td>
                                <td>{{ $produsen->nama_produsen }}</td>
                                <td>{{ $produsen->alamat_produsen }}</td>
                                <td>{{ $produsen->telp_produsen }}</td>
                                <td class="text-center">
                                    {!! Form::model($produsen, ['route' => ['produsen.destroy', $produsen->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                    <a href="{{ route('produsen.edit', $produsen->id)}}" data-target="#editProdusen" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                    <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addProdusen">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Produsen</h4>
                </div>
                {!! Form::open(['route' => 'produsen.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produsen', 'Produsen Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_produsen', $noprodusen, ['class' => 'form-control','required']) }}
                                {!! $errors->first('kd_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('nama_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('nama_produsen', 'Produsen Name') !!}
                                {{ Form::text('nama_produsen', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('nama_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('alamat_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('alamat_produsen', 'Produsen Adress') !!}
                                {{ Form::text('alamat_produsen', null, ['class' => 'form-control']) }}
                                {!! $errors->first('alamat_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('telp_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('telp_produsen', 'Produsen Phone               ') !!}
                                {{ Form::text('telp_produsen', null, ['class' => 'form-control']) }}
                                {!! $errors->first('telp_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editProdusen" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection