<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Produsen</h4>
</div>
{!! Form::model($produsen, ['route' => ['produsen.update', $produsen->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="form-group {!! $errors->has('kd_produsen') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_produsen', 'Produsen Code', ['class' => 'control-label']) !!}
                    {{ Form::text('kd_produsen', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('kd_produsen', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('nama_produsen') ? 'has-error' : '' !!}">
                    {!! Form::label('nama_produsen', 'Produsen Nama') !!}
                    {{ Form::text('nama_produsen', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('nama_produsen', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('alamat_produsen') ? 'has-error' : '' !!}">
                    {!! Form::label('alamat_produsen', 'Produsen Adress') !!}
                    {{ Form::text('alamat_produsen', null, ['class' => 'form-control']) }}
                    {!! $errors->first('alamat_produsen', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('telp_produsen') ? 'has-error' : '' !!}">
                    {!! Form::label('telp_produsen', 'Produsen phone') !!}
                    {{ Form::text('telp_produsen', null, ['class' => 'form-control']) }}
                    {!! $errors->first('telp_produsen', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>