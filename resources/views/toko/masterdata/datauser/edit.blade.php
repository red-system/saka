<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit User</h4>
</div>
{!! Form::model($user, ['route' => ['data-user.update', $user->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    {{ Form::text('name', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
                    {!! Form::label('username', 'Username') !!}
                    {{ Form::text('username', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                    {!! Form::label('email', 'Email') !!}
                    {{ Form::email('email', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
                    {!! Form::label('password', 'Password') !!}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
                    {!! Form::label('password_confirmation', 'Confirm Password') !!}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('role') ? 'has-error' : '' !!}">
                    {!! Form::label('role', 'Role') !!}
                    {{ Form::select('role', ['' => '']+App\Models\User::roleList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'roleU']) }}
                    {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
                </div>
                <div id="locationFormU">
                    <div class="form-group {!! $errors->has('kd_location') ? 'has-error' : '' !!}">
                        {!! Form::label('kd_location', 'Location') !!}
                        {{ Form::select('kd_location', ['' => '']+App\Models\Location::pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                        {!! $errors->first('kd_location', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>