@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbUser').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });
        })
    </script>
@endsection

@section('title')
    Data User - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Data User
        <small>Master Data</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Master Data</li>
        <li class="active">Data User</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addUser">
                <i class="fa fa-plus"></i> Add User
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data User</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbUser" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th class="text-center">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    {{ $user->human_role }}
                                    <br>{{ $user->location ?  $user->location->location_name : '-' }}
                                </td>
                                <td class="text-center">
                                    {!! Form::model($user, ['route' => ['data-user.destroy', $user->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('data-user.edit', $user->id)}}" data-target="#editUser" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete User">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addUser">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                {!! Form::open(['route' => 'data-user.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                {!! Form::label('name', 'Nama', ['class' => 'control-label']) !!}
                                {{ Form::text('name', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
                                {!! Form::label('username', 'Username') !!}
                                {{ Form::text('username', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                {!! Form::label('email', 'Email') !!}
                                {{ Form::email('email', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
                                {!! Form::label('password', 'Password') !!}
                                {{ Form::password('password', ['class' => 'form-control','required']) }}
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
                                {!! Form::label('password_confirmation', 'Confirm Password') !!}
                                {{ Form::password('password_confirmation', ['class' => 'form-control','required']) }}
                                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('role') ? 'has-error' : '' !!}">
                                {!! Form::label('role', 'Role') !!}
                                {{ Form::select('role', ['' => '']+App\Models\User::roleList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'role']) }}
                                {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div id="locationForm">
                                <div class="form-group {!! $errors->has('kd_location') ? 'has-error' : '' !!}">
                                    {!! Form::label('kd_location', 'Location') !!}
                                    {{ Form::select('kd_location', ['' => '']+App\Models\Location::pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                    {!! $errors->first('kd_location', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editUser" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection