<tr>
                <td class="master_id hide">
                    {{ Form::select('master_id[]', App\Models\MasterCoa::pluck('mst_nama_rekening', 'master_id')->all(), null, ['class' => 'form-control select2']) }}
                </td>
                <td class="text-center debet">

                    {{ Form::text('debet[]', 0, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotal()']) }}
                </td>
                <td class="text-center">
                    {{ Form::textarea('catatan[]', null, ['class' => 'form-control', 'rows' => 2]) }}
                </td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                        <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </span>
                </td>
            </tr>