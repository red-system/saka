@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        // $(document).ready(function () {
            function addItem() {
                $.get("{{ route('hutang-piutang.addform') }}", function(formMaterial){
                    $('#tbItemProduksi tbody').append(formMaterial);

                    $('#tbItemProduksi tbody tr:last').find('.select2').select2();
                    $('#tbItemProduksi tbody tr:last').find('input[name="debet[]"]').number(true, 2);
                });

                // debet_ju_up();
            }


            $('.select2').select2();

            $('[name="jenis"]').change(function() {
                var jenis = $(this).val();

                if(jenis=='ap'){
                    var no_ap = $('[name="no_ap"]').val();
                    $('[name="kode_bukti"]').val(no_ap);
                    
                }else{
                    var no_ar = $('[name="no_ar"]').val();
                    $('[name="kode_bukti"]').val(no_ar);                
                    
                }
            });

            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            hitungSubTotal();

            function removeItem(element) {
                $(element).parents('tr').remove();
                hitungSubTotal();
            }

            function hitungSubTotal() {
                var subtotal = 0;
                $('[name="debet[]"]').each(function () {
                    var price = $(this).val();
                    // var qty = $(this).parent().siblings('.qtyMaterial').find('input[name="qtyMaterial[]"]').val();

                    // var subTotalItem = parseInt(price)*parseInt(qty);
                    subtotal = parseFloat(subtotal) + parseFloat(price);
                });
                $('[name="total_debet"]').val(subtotal.toFixed(2)).trigger('change');
            }
        // });
    </script>
@endsection

@section('title')
    {{$title}} - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        {{$title}}
        <small>Hutang Piutang</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Hutang Piutang</li>
        <li><a href="{{ route('produksi.index') }}">Penerimaan Hutang Piutang</a></li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => 'hutangPiutang.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('jenis', 'Jenis', ['class' => 'control-label']) !!}
                                {{ Form::select('jenis', array(''=>'', 'ap' => 'Hutang', 'ar' => 'Piutang'), null, ['class' => 'form-control select2','required']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                            

                            <div class="form-group {!! $errors->has('tgl_produksi') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_transaksi', 'Tanggal', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('tgl_transaksi', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tgl_transaksi', '<p class="help-block">:message</p>') !!}
                            </div>
                            <!-- select -->

                            
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('kode_bukti', 'No Bukti', ['class' => 'control-label']) !!}
                                {{ Form::text('kode_bukti', null, ['class' => 'form-control','required','readonly', 'id' => 'no_bukti']) }}
                                {!! $errors->first('kode_bukti', '<p class="help-block">:message</p>') !!}
                                <input type="hidden" name="no_ap" value="{{$no_ap}}">
                                <input type="hidden" name="no_ar" value="{{$no_ar}}">
                            </div>
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('total', 'Keterangan Jurnal', ['class' => 'control-label']) !!}
                                {{ Form::text('keterangan', null, ['class' => 'form-control','required', 'id' => 'keterangan']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {!! $errors->has('atas_nama') ? 'has-error' : '' !!}">
                                {!! Form::label('atas_nama', 'Atas Nama', ['class' => 'control-label']) !!}
                                {{ Form::text('atas_nama', null, ['class' => 'form-control','required', 'id' => 'atas_nama']) }}
                                {{ Form::hidden('nama', null, ['class' => 'form-control','required', 'id' => 'atas_nama']) }}
                                {!! $errors->first('atas_nama', '<p class="help-block">:message</p>') !!}
                            </div>
                            <!-- select -->
                            <!-- <div id='akun-ar' class="form-group {!! $errors->has('akun_ar') ? 'has-error' : '' !!}">
                                {!! Form::label('nama_akun', 'Akun', ['class' => 'control-label']) !!}

                                {{ Form::select('nama_akun', $akun->pluck('mst_nama_rekening', 'master_id')->all(), null, ['class' => 'form-control select2','required','id'=>'akun_ar']) }}

                                {!! $errors->first('nama_akun', '<p class="help-block">:message</p>') !!}
                            </div> -->
                            <input type="hidden" name="nama_akun" value="16">

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <button type="button" class="btn btn-social btn-google btn-add-trs" onclick="addItem()">
                <i class="fa fa-plus"></i> Item
            </button>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbItemProduksi" width="100%">
                        <thead>
                            <tr>
                                <!-- <th class="text-center">Kode</th> -->
                                <th class="text-center">Nominal</th>
                                <th class="text-center">Catatan</th>
                                <th width="5%" class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- /.col -->
    
    <!-- /.col -->

    <div class="row">
        <div class="col-md-offset-6 col-md-6 col-xs-12">
            <div class="col-xs-6" style="padding: 3px;">
                <input type="number" name="total_debet" value="0">
                <a href="{{ route('jurnalHarian.index') }}" class="btn btn-block btn-default">Cancel</a>
            </div>
            <div class="col-xs-6" style="padding: 3px;">
                <button type="submit" class="btn btn-block btn-success">Simpan</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection