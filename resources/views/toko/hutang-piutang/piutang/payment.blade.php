
<div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="{{route('piutang-pelanggan.insert-pembayaran')}}" method="post" id="form-payment-hutang-supplier">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3">Tgl Transaksi</label>
                            <div class="col-md-4">
                              <input class="form-control date-picker" size="16" type="text" name="tgl_transaksi" data-date-format="yyyy-mm-dd" required="required" value="{{date('Y-m-d')}}" />
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">No Transaksi</label>
                            <div class="col-md-4">
                              <input type="text" name="no_transaksi" class="form-control" value="{{$no_transaksi}}" readonly="readonly">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">No Hutang</label>
                            <div class="col-md-4">
                              <input type="text" name="no_faktur_hutang" class="form-control" value="{{$hutang->no_faktur}}" readonly="readonly">
                              <input type="hidden" name="pp_invoice" class="form-control" value="{{$hutang->id}}" readonly="readonly">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Atas Nama</label>
                            <div class="col-md-4">
                              <input type="text" name="cus_nama" class="form-control" value="{{$hutang->nama}}">
                              <input type="hidden" name="cus_kode" class="form-control" value="{{$hutang->cus_kode}}">
                              <input type="hidden" name="kode_perkiraan" class="form-control" value="{{$hutang->kode_perkiraan}}">
                            <!-- </td> -->
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-4" style="margin-top: -20px">
                            <br>
                              <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal" href="#modal-tambah"> 
                                <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                              </button>
                          </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                        <thead>
                            <tr>
                                <th>Tipe Pembayaran</th>
                                <th>Payment</th>
                                <th>Total</th>
                                <th>Keterangan</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr>
                                <td colspan="14">
                                    <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                                    </button>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-xs-3">
                            <h2>Total</h2>
                        </div>
                        <div class="col-xs-9">
                            <h2 class="nominal-grand-total" name="nominal-grand-total" id="nominal-grand-total">{{number_format($hutang->sisa_piutang,2)}}</h2>
                            <input type="hidden" name="amount" step=".01" value="{{$hutang->sisa_piutang}}">
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <h2>Sisa</h2>
                            </div>
                            <div class="col-xs-9">
                                <h2 class="nominal-sisa">{{number_format($hutang->sisa_piutang,2)}}</h2>
                                <input type="hidden" name="amount_sisa" value="{{$hutang->sisa_piutang}}" step=".01">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success btn-lg" id="submit-payment-hutang-supplier">SAVE</button>
                                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
<table class="table-row-payment hide">
        <tbody>
          <tr>
            <td class="hide">
              <select name="master_id[]" class="form-control select2" data-live-search="true" data-palceholder="Kode Akunting">
                @foreach($perkiraan as $r)
                  <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                  @endforeach
              </select>
            </td>
            <td>
              <select name="tipe_pembayaran[]" class="form-control select2" data-live-search="true" data-palceholder="Tipe Pembayaran">
                <option value="tunai">Tunai</option>
                <option value="cc">CC</option>
                <option value="debet">Debet</option>
                <option value="transfer">Transfer</option>
              </select>
            </td>
            <td class="payment">
              <input type="number" name="payment[]" class="form-control" value="0" step=".01">
            </td>
            <td class="payment_total">
              <input type="number" name="payment_total[]" class="form-control" value="0" readonly step=".01">
            </td>
            <td>
              <input type="text" name="keterangan[]" class="form-control">
            </td>
            <td>
              <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
            </td>
          </tr>
        </tbody>
    </table>
<script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
      $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('#form-payment-hutang-supplier').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#submit-payment-hutang-supplier').attr('disabled', true);
                var sisa = $('[name="sisa"]').val();
                // if(sisa > 0) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Belum Balance',
                //         type: 'error'
                //     });
                //     $('#submit-payment-hutang-supplier').attr('disabled', false);
                // }
                // else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        $('#submit-payment-hutang-supplier').attr('disabled', false);
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                // }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->