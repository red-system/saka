@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbSupplier').DataTable({
                responsive: true,
                pageLength: 20,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });

            $('.select2').select2();
        })
    </script>
@endsection

@section('title')
    {{$title}} - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        {{$title}}
        <small>{{$menu}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>{{$menu}}</li>
        <li class="active">{{$title}}</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{$title}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbSupplier" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th width="15%"> No Faktur </th>
                            <th> Tanggal</th>
                            <th> Atas Nama </th>
                            <th> Total Piutang Piutang </th>
                            <th> Sisa Piutang </th>                            
                            <th> Jatuh Tempo </th>
                            <th> Status </th>
                            <th> Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;?>
                        @foreach($data_piutang as $hutang)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$hutang->no_faktur}}</td>
                                <td>{{$hutang->tgl_piutang}}</td>
                                <td>{{$hutang->nama}}</td>
                                <td>{{number_format($hutang->total_piutang,2)}}</td>
                                <td>{{number_format($hutang->sisa_piutang,2)}}</td>
                                <td>{{$hutang->due_date}}</td>                             
                                <td>
                                    @if($hutang->pp_status=='belum_lunas')
                                    <span style="font-size: 12px" class="label label-danger">  Belum Lunas  </span>
                                    @elseif($hutang->pp_status=='lunas')
                                    <span style="font-size: 12px" class="label label-success">  Lunas  </span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    
                                    @if($hutang->pp_status=='belum_lunas')
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('piutang-pelanggan.get-data', $hutang->id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-primary"><i class="fa fa-money"></i></a>
                                    </span>
                                    <!-- <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span> -->
                                    @endif                                   
                                    
                                </td>
                            </tr>
                            <?php $no++;?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <!-- /.modal -->

    <div id="editSupplier" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>

    <div id="addSaldoAwal" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection