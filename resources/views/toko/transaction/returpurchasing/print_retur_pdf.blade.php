<html>
<head>
    <title>Nota Retur {{ $returpurchasing->no_retur }}</title>
    <style>
        @page { margin: 5px 5px; }

        body {
            font-family: "Times New Roman", Times, serif;
        }

        .lr-none {
            border-right: none;
            border-left: none;
        }

        .tb-grey {
            border-top: 0.5px solid #888;
            border-bottom: 0.5px solid #888;
        }

        .pd10 {
            padding-bottom: 4px;
            padding-top: 4px;
        }

        .pd8 {
            padding-bottom: 3px;
            padding-top: 3px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .left {
            text-align: left;
        }

        .right {
            text-align: right;
        }

        .vat {
            vertical-align: top;
        }

        .text-muted {
            color: #000000;
        }

        table > tr > td {
            font-family: "Times New Roman", Times, serif !important;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td align="center" style="font-size: 13px;" colspan="2"><strong>Retur Purchasing</strong></td>
    </tr>
    <tr>
        <td width="40%" style="vertical-align: top;">
            <table width="100%" style="margin-top: 5px; font-size: 10px;">
                <tr>
                    <td>Saka Karya Bali</td>
                </tr>
                <tr>
                    <td>{{ $location->alamat }}</td>
                </tr>
                <tr>
                    <td>{{ $location->telp }}</td>
                </tr>
            </table>
        </td>
        <td width="60%" style="vertical-align: top;">
            <table width="100%" style="margin-top: 5px; font-size: 10px;">
                <tr>
                    <td colspan="3">
                        Yth. {{ $returpurchasing->purchasing->supplier->nama_supplier }}
                        <br> {{ $returpurchasing->purchasing->supplier->alamat_supplier }}
                        <br> {{ $returpurchasing->purchasing->supplier->kontak_supplier }}
                    </td>
                </tr>
                <tr>
                    <td width="37%">Retur number</td>
                    <td width="5%">:</td>
                    <td width="53%" style="text-align: right;">{{ $returpurchasing->no_retur }}</td>
                </tr>
                <tr>
                    <td>Invoice Number</td>
                    <td>:</td>
                    <td style="text-align: right;">{{ $returpurchasing->no_faktur }}</td>
                </tr>
                <tr>
                    <td>Retur Date</td>
                    <td>:</td>
                    <td style="text-align: right;">{{ date('d F Y', strtotime($returpurchasing->tgl_pengembalian)) }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" class="table" border="1" style="margin-top: 5px; font-size: 11px; border-collapse: collapse;">
    <tr>
        <th class="lr-none tb-grey pd10 center">No</th>
        <th class="lr-none tb-grey pd10 left" width="54%">Material</th>
        <th class="lr-none tb-grey pd10 center" width="10%">Qty Retur</th>
        <th class="lr-none tb-grey pd10 center" width="30%">Price</th>
    </tr>
    <?php $no = 1; ?>
    @foreach($detail_retur as $dr)<tr>
        <td class="lr-none tb-grey pd8 center vat">
            <span class="bold">{{ $no }}</span>
        </td>
        <td class="lr-none tb-grey pd8 vat">
            <span class="bold">{{ $dr->material->name }}</span><br>
        </td>
        <td class="lr-none tb-grey pd8 center vat">
            <span class="bold">{{ $dr->qty_retur }}</span>
        </td>
        <td class="lr-none tb-grey pd8 center vat">
            <span class="bold">{{ number_format($dr->price, 2, ',', '.') }}</span><br>
        </td>
    </tr>
    <?php $no++; ?>
    @endforeach
</table>

<div style="margin-left: 45%; margin-top: 10px;">
    <table width="100%" style="font-size: 11px; border-collapse: collapse;">
        <tr>
            <td class="lr-none tb-grey pd8 left">Total Retur</td>
            <td class="lr-none tb-grey pd8 right" style="font-size: 13px; color: #000000;">{{ number_format($returpurchasing->total_retur, 2, ',', '.') }}</td>
        </tr>

        <tr>
            <td class="lr-none tb-grey pd8 left">Additional Cost</td>
            <td class="lr-none tb-grey pd8 right" style="font-size: 13px; color: #000000;">{{ number_format($returpurchasing->tambahan_biaya, 2, ',', '.') }}</td>
        </tr>
    </table>
</div>
</body>
</html>