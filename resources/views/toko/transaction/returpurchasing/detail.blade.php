@extends('layouts.master-pembelian')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .panel-heading {
            padding: 15px 20px;
            border-bottom: 1px solid #f4f4f4 !important;
        }

        .list-condensed > li, .list-condensed > li .list > li {
            margin-top: 3px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 15px 20px;
        }

        .table.pd8 > thead > tr > th, .table.pd8 > tbody > tr > th, .table.pd8 > tfoot > tr > th, .table.pd8 > thead > tr > td, .table.pd8 > tbody > tr > td, .table.pd8 > tfoot > tr > td {
            padding: 8px !important;
        }

        .text-semibold {
            font-weight: 500;
        }

        .modal-full {
            width: calc(100% - 30px);
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
@endsection

@section('custom-script')

@endsection

@section('title')
    SAKA | Purchasing
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Retur Purchasing Detail</h4>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-sm-6 content-group">
                            <ul class="list-condensed list-unstyled" style="padding-left: 5px;">
                                <li>Retur Number: {{ $returpurchasing->no_retur }}</li>
                                <li>Invoice Number: {{ $returpurchasing->no_faktur }}</li>
                                <li>Retur Date: {{ date('d F Y', strtotime($returpurchasing->tgl_pengembalian)) }}</li>
                                <li>Doer: {{ $returpurchasing->pelaksana }}</li>
                                <li>Reason: {{ $returpurchasing->human_alasan }}</li>
                                @if($returpurchasing->alasan_lain != '')
                                    <li>Other reason: {{ $returpurchasing->alasan_lain }}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <?php $subTotal = 0;?>
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th class="col-sm-2 text-center">Qty Retur</th>
                            <th class="col-sm-3">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($returpurchasing->detail_retur as $dr)
                            <tr>
                                <td>
                                    <h5 class="no-margin">{{ $dr->material->name }}</h5>
                                </td>
                                <td class="text-center">
                                    <h5 class="no-margin">{{ $dr->qty_retur }}</h5>
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ number_format($dr->total_retur, 2, ',', '.') }}</h5>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase">Total</h4>
                </div>
                <div class="table-responsive no-border">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Total Retur <br> <span class="text-muted" style="font-size: 10px;">(Include Ppn & Global Disc)</span></th>
                            <td class="text-right">{{ number_format($returpurchasing->total_retur, 2, ',', '.') }}</td>
                        </tr>

                        <tr>
                            <th>Additional Cost</th>
                            <td class="text-right">{{ number_format($returpurchasing->tambahan_biaya, 2, ',', '.') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="{{ route('retur-purchasing.print', $returpurchasing->no_retur) }}" class="btn btn-success" target="_blank">Print</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection