@extends('layouts.master-pembelian')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/css/buttons.bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterKategori {
            display: inline;
        }

        .filterTanggal {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            var tablePurchasing = $('#tbPurchasing').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 6,
                    orderable: false
                }],
                order: [[1, 'desc']],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            var allowFilter = ['tbHistoryRetur'];
            $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        if ($.inArray(settings.nTable.getAttribute('id'), allowFilter ) == -1 )
                        {
                            return true;
                        }
                        var from = $('#fltFrom').datepicker("getDate");
                        var to = $('#fltTo').datepicker("getDate");
                        var startDate = new Date(data[1]).setHours(0,0,0,0);
                        if (from == null && to == null) { return true; }
                        if (from == null && startDate <= to) { return true;}
                        if(to == null && startDate >= from) {return true;}
                        if (startDate <= to && startDate >= from) { return true; }
                        return false;
                    }
            );

            var tbHistoryRetur = $('#tbHistoryRetur').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 8,
                    orderable: false
                }],
                order: [[2, 'desc']],
                dom: '<"datatable-header"<"row"<"col-md-9 col-sm-8 col-xs-12"f<"filterTanggal">B><"col-md-3 col-sm-4 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
                buttons: [
                    'excel',
                    {
                        extend: 'pdf',
                        customize: function (doc) {
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'print'
                ]
            });

            $("#filterTanggal").appendTo(".filterTanggal");
            $(".dt-buttons").find('.btn').addClass('btn-sm');

            $('#fltFrom, #fltTo').change(function () {
                tbHistoryRetur.draw();
            });
        })
    </script>
@endsection

@section('title')
    Retur Purchasing - Saka Karya Bali
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#datapurchasing" data-toggle="tab">Data Purchasing</a></li>
                    <li><a href="#historyretur" data-toggle="tab">History Retur</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="datapurchasing">
                        <table id="tbPurchasing" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Invoice number</th>
                                <th>Date</th>
                                <th>Supplier</th>
                                <th>Additional Cost</th>
                                <th>Disc</th>
                                <th>Tax</th>
                                <th>Grand Total</th>
                                <th class="text-center all">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datapurchasing as $purchasing)
                                <tr>
                                    <td>{{ $purchasing->no_faktur }}</td>
                                    <td>{!! date('Y-m-d', strtotime($purchasing->tgl)) !!}</td>
                                    <td>{{ $purchasing->supplier->nama_supplier }}</td>
                                    <td>{{ number_format($purchasing->biaya_tambahan, 2, ',', '.') }}</td>
                                    <td>{{ number_format($purchasing->disc_nominal, 2, ',', '.') }}</td>
                                    <td>{{ number_format($purchasing->ppn_nominal, 2, ',', '.') }}</td>
                                    <td>{{ number_format($purchasing->grand_total, 2, ',', '.') }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('retur-purchasing.create', $purchasing->no_faktur) }}" class="btn btn-sm btn-danger"> Retur</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="historyretur">
                        <div id="filterTanggal" for="filterTanggal" style="display: inherit;">
                            <span style="margin: 8px 5px 8px 15px;">Date:</span>
                            <div class="input-group" style="width: 220px;">
                                <input name="fltFrom" id="fltFrom" class="form-control input-sm input-inline datepicker" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Until</button>
                                </span>
                                <input name="fltTo" id="fltTo" class="form-control input-sm input-inline datepicker" type="text">
                            </div>
                        </div>
                        <table id="tbHistoryRetur" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Retur number</th>
                                <th>Retur date</th>
                                <th>Reason</th>
                                <th>Total Retur</th>
                                <th>Additional Cost</th>
                                <th>Invoice Number</th>
                                <th class="none">Doer</th>
                                <th class="none">Other Reason</th>
                                <th class="all text-center">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historyretur as $hrt)
                                <tr>
                                    <td>{{ $hrt->no_retur }}</td>
                                    <td>{{ date('Y-m-d', strtotime($hrt->tgl_pengembalian)) }}</td>
                                    <td>{{ $hrt->human_alasan }}</td>
                                    <td>{{ number_format($hrt->total_retur, 2, ',', '.') }}</td>
                                    <td>{{ number_format($hrt->tambahan_biaya, 2, ',', '.') }}</td>
                                    <td>{{ $hrt->no_faktur }}</td>
                                    <td>{!! $hrt->pelaksana !!}</td>
                                    <td>{!! $hrt->alasan_lain !!}</td>
                                    <td class="text-center">
                                        <a href="{{ route('retur-purchasing.detail', $hrt->no_retur) }}" class="btn btn-sm btn-default" target="_blank"> Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection