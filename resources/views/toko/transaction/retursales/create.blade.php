@extends('layouts.master-pos')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }

        #tbItemSales tr td {
            vertical-align: middle;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function formatMoney(n, c, d, t) {
            var c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "," : d,
                    t = t == undefined ? "." : t,
                    s = n < 0 ? "-" : "",
                    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                    j = (j = i.length) > 3 ? j % 3 : 0;

            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('.currency').number(true, 2);
        })
        
        function returItem(elem) {
            var qtyRetur = $(elem).val();
            var mprice = $(elem).attr('data-price');
            var diskon = $(elem).parent().siblings('.tdDiscPersen').find('input:hidden[class="disc_persen"]').val();

            var hargaRetur = parseFloat(mprice - ((mprice*diskon)/100));
            var subTotalRetur = parseFloat(qtyRetur*hargaRetur);
            var elemHarga = $(elem).parent().siblings('.tdSubTotal');
            elemHarga.find('input:hidden[class="subtotal"]').val(subTotalRetur);
            elemHarga.find('span').html(formatMoney(subTotalRetur, 2));
            hitungTotalRetur();
        }
        
        function hitungTotalRetur() {
            var totalRetur = 0;
            $('input:hidden[class="subtotal"]').each(function () {
                var subtotal = parseInt($(this).val());
                totalRetur = totalRetur + subtotal;
            });

            var disc = parseInt($('#disc').val());
            if (disc > 0) {
                totalRetur = totalRetur - ((totalRetur*disc)/100);
            }
            $('#totalRetur').val(totalRetur);
        }
    </script>
@endsection

@section('title')
    Retur Sales - Saka Karya Bali
@endsection

@section('content')
    {!! Form::open(['route' => ['retur-sales.store', $sales->no_faktur], 'class' => 'jq-validate form-horizontal', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('no_faktur') ? 'has-error' : '' !!}">
                                {!! Form::label('no_faktur', 'Invoice Number', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('no_faktur', $sales->no_faktur, ['class' => 'form-control','required', 'readonly']) }}
                                    {!! $errors->first('no_faktur', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('tgl_faktur') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_faktur', 'Invoice Date', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('tgl_faktur', date('Y-m-d', strtotime($sales->tgl)), ['class' => 'form-control','required', 'readonly']) }}
                                    {!! $errors->first('tgl_faktur', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('tgl_pengembalian') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_pengembalian', 'Retur Date', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('tgl_pengembalian', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly']) }}
                                    {!! $errors->first('tgl_pengembalian', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('pelaksana') ? 'has-error' : '' !!}">
                                {!! Form::label('pelaksana', 'Doer', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('pelaksana', null, ['class' => 'form-control','required']) }}
                                    {!! $errors->first('pelaksana', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group {!! $errors->has('alasan') ? 'has-error' : '' !!}">
                                {!! Form::label('alasan', 'Reason', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::select('alasan', []+App\Models\ReturSales::alasanList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                    {!! $errors->first('alasan', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('alasan_lain') ? 'has-error' : '' !!}">
                                {!! Form::label('alasan_lain', 'Other Reason', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::textarea('alasan_lain', null, ['class' => 'form-control','rows' => 5]) }}
                                    {!! $errors->first('alasan_lain', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="tbItemSales">
                                    <thead>
                                    <tr>
                                        <th>Product Code</th>
                                        <th>Desc</th>
                                        <th class="text-center col-md-1">Qty</th>
                                        <th class="text-center col-md-1">Qty Retur</th>
                                        <th class="col-md-2">Price</th>
                                        <th class="text-center col-md-1">Disc</th>
                                        <th class="col-md-2">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sales->detail_sales as $dsale)
                                        @if($dsale->qty > 0)
                                        <tr>
                                            <td>{{ $dsale->kd_produk }}</td>
                                            <td>{{ $dsale->produk->deskripsi }}</td>
                                            <td class="text-center col-md-1">{{ $dsale->qty }}</td>
                                            <td class="text-center col-md-1 tdQtyRetur">
                                                {!! Form::number('qty_retur['.$dsale->id.']', 0, ['class' => 'form-control qtyRetur', 'min' => 0, 'max' => $dsale->qty, 'data-price' => $dsale->price, 'onchange' => 'returItem(this)']) !!}
                                            </td>
                                            <td class="col-md-2 tdPrice">
                                                <span>{{ number_format($dsale->price, 2, ',', '.') }}</span>
                                                {!! Form::hidden('price['.$dsale->id.']', $dsale->price, ['class' => 'price']) !!}
                                            </td>
                                            <td class="text-center col-md-1 tdDiscPersen">
                                                <span>{{ $dsale->disc_persen.'%' }}</span>
                                                {!! Form::hidden('disc_persen['.$dsale->id.']', $dsale->disc_persen, ['class' => 'disc_persen']) !!}
                                            </td>
                                            <td class="col-md-2 tdSubTotal">
                                                <span>0</span>
                                                {!! Form::hidden('subtotal['.$dsale->id.']', 0, ['class' => 'subtotal', 'data-msubtotal' => $dsale->subtotal]) !!}
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8 col-sm-offset-6">
                            <div class="form-group {!! $errors->has('disc') ? 'has-error' : '' !!}">
                                {!! Form::label('disc', 'Global Disc (%)', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::number('disc', $sales->disc_persen, ['class' => 'form-control','required', 'readonly', 'id' => 'disc']) }}
                                    {!! $errors->first('disc', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('total_retur') ? 'has-error' : '' !!}">
                                {!! Form::label('total_retur', 'Total Retur', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::text('total_retur', 0, ['class' => 'form-control currency','required', 'readonly', 'id' => 'totalRetur']) }}
                                    {!! $errors->first('total_retur', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('tambahan_biaya') ? 'has-error' : '' !!}">
                                {!! Form::label('tambahan_biaya', 'Additional Cost', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::text('tambahan_biaya', 0, ['class' => 'form-control currency','required', 'id' => 'tambahanBiaya']) }}
                                    {!! $errors->first('tambahan_biaya', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-offset-8 col-md-4 col-xs-12">
                            <div class="col-xs-6" style="padding: 3px;">
                                <a href="{{ route('retur-sales.index') }}" class="btn btn-block btn-default">Cancel</a>
                            </div>
                            <div class="col-xs-6" style="padding: 3px;">
                                <button type="submit" class="btn btn-block btn-success">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection