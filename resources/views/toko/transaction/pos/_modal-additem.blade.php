{!! Form::open(['route' => ['pos.additem.storeitem', $stockproduk->id], 'class' => 'form-horizontal jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formAddItem']) !!}
<div class="modal-body" style="padding-bottom: 0;">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-8">
                        <label style="margin-bottom: 10px;">{{ $produk->deskripsi }}</label>
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::select('price_type', []+App\Models\Product::priceTypeList(), 'retail_price', ['class' => 'form-control input-sm','required', 'style' => 'width: 100%;', 'id' => 'priceType', 'onkeypress' => 'return event.keyCode != 13;']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <label class="text-danger" id="txtProductPrice" style="font-weight: bold; font-size: 17px;">{{ $currency['used_currency'].' '.number_format($produk->price*$currency['idr1_to_usecurr'], 4, ',', '.') }}</label>
                        {!! Form::hidden('retail_price', $produk->price*$currency['idr1_to_usecurr'], ['id' => 'retailPrice']) !!}
                        {!! Form::hidden('wholesale_price', $produk->wholesale_price*$currency['usd1_to_usecurr'], ['id' => 'wholesalePrice']) !!}
                        {!! Form::hidden('price', $produk->price*$currency['idr1_to_usecurr'], ['id' => 'priceProduk']) !!}
                        <div class="row" style="margin: 0;">
                            <div class="col-md-7 col-md-offset-5">
                                <div class="form-group {!! $errors->has('qty') ? 'has-error' : '' !!}" style="margin-bottom: 0;">
                                    {{ Form::text('qty',null,['class' => 'input-sm touchspin', 'id' => 'qtyProduk', 'onkeypress' => 'return event.keyCode != 13;']) }}
                                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group" style="margin-bottom: 5px;">
                    {!! Form::label('stock', 'Stock', ['class' => 'col-md-3', 'style' => 'font-weight: 400;']) !!}
                    <div class="col-md-9">
                        <p style="font-weight: 700;">{{ $stockproduk->qty }}</p>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    {!! Form::label('size', 'Size', ['class' => 'col-md-3', 'style' => 'font-weight: 400;']) !!}
                    <div class="col-md-9">
                        <p style="font-weight: 700;">{{ $stockproduk->size_product }}</p>
                        {!! Form::text('size_product', $stockproduk->size_product, ['id' => 'size_product']) !!}
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    {!! Form::label('diskon', 'Discount', ['class' => 'col-md-3', 'style' => 'font-weight: 400;']) !!}
                    <div class="col-md-9">
                        {!! Form::hidden('discount', $produk->disc_nominal*$currency['idr1_to_usecurr'], ['id' => 'discountProduk']) !!}
                        {!! Form::hidden('discount_persen', $produk->disc_persen, ['id' => 'discountPersen']) !!}
                        <p style="font-weight: 700;">{{ $produk->disc_persen.'%' }}</p>
                    </div>
                </div>
                <div class="form-group {!! $errors->has('catatan') ? 'has-error' : '' !!}">
                    {!! Form::label('catatan', 'Notes', ['class' => 'col-md-3', 'style' => 'font-weight: 400;']) !!}
                    <div class="col-md-9">
                        {{ Form::textarea('catatan',null,['class' => 'form-control','rows' => '2']) }}
                        {!! $errors->first('catatan', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                {!! Form::hidden('totalHargaProduk', null, ['id' => 'totalHargaProduk']) !!}
                <div class="form-group {!! $errors->has('harga_total') ? 'has-error' : '' !!}">
                    {!! Form::label('harga_total', 'Total Price', ['class' => 'col-md-3', 'style' => 'font-weight: 400; padding-top: 10px;']) !!}
                    <div class="col-md-9">
                        <p class="text-success" style="padding-top: 8px; font-weight: bold; font-size: 17px;" id="txtTotalHargaProduk">{{ $currency['used_currency'] }} 0,00</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
    {!! Form::button('Add Product', ['type'=> 'submit', 'class'=>'btn bg-teal-400']) !!}
</div>
{!! Form::close() !!}

<script>
    $(".touchspin").TouchSpin({
        initval: 1,
        step: 1,
        min: 1,
        max: {{ $stockproduk->qty }},
    });

    function hitungTotalHargaProduk() {
        var hargaProduk = $('#priceProduk').val();
        var discountProduk = $('#discountProduk').val();
        var qtyProduk = $('#qtyProduk').val();
        var totalHarga = ((parseFloat(hargaProduk) - parseFloat(discountProduk)) * parseInt(qtyProduk));

        $('#totalHargaProduk').val(totalHarga);
        $('#txtTotalHargaProduk').html('{{ $currency['used_currency'] }} '+totalHarga.formatMoney(4, ',', '.'));
    }

    $('#qtyProduk').on('change', function () {
        hitungTotalHargaProduk();
    });

    $(function () {
        hitungTotalHargaProduk();

        $('#priceType').on('change', function(e){
            var priceType = e.target.value;
            if (priceType == 'wholesale_price') {
                var usedPrice = parseFloat($('#wholesalePrice').val());
            } else {
                var usedPrice = parseFloat($('#retailPrice').val());
            }
            $('#priceProduk').val(usedPrice);
            $('#txtProductPrice').html('{{ $currency['used_currency'] }} '+usedPrice.formatMoney(4, ',', '.'));

            var discPersen = parseInt($('#discountPersen').val());
            var discNominal = parseFloat((usedPrice*discPersen)/100);
            $('#discountProduk').val(discNominal);

            hitungTotalHargaProduk();
        });

        $('#qtyProduk').on('keyup', function () {
            var maxQty = {{ $stockproduk->qty }};
            if($(this).val() > maxQty) {
                swal('Stock Less', 'Sorry, the amount of product sales entered exceeds stock!', 'error');
            }
        })
    })

    $("#formAddItem").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                success: function(response) {
                    $('#modalDetailItem').modal('hide');

                    document.getElementById('formAddItem').reset();
                    $("#formAddItem .selectsearch").select2("val", "");

                    new PNotify({
                        title: 'Success',
                        text: 'Product added successfully',
                        addclass: 'alert-styled-left',
                        type: 'success'
                    });

                    getListItemProduk();
                }
            });
        }
    });
</script>