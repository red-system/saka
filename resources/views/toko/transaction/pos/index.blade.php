@extends('layouts.master-pos')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .slimScrollBar {
            right: -5px !important;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $(function () {
            $('#salesTodayScroll').slimScroll({
                height: '500px',
                railColor: '#d2d6de',
                railOpacity: 0.9,
            });

            $('.select2').select2({
                dropdownParent: $('#form-add-pl')
            });

            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
            $('.select22').select2({
                dropdownParent: $('#form-add-pk')
            });
            @endif

            if ($(document.getElementById("tokoPK")).length > 0) {
                var k = document.getElementById("tokoPK");
                var tokoPK = k.options[k.selectedIndex].value;

                $.post("{{ route('pos.get-detail-konsinyasi') }}", {toko: tokoPK,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#komisiPK').val(data.potongan);
                });
            }

            $('#tokoPK').on('change', function(e){
                var tokoPK = e.target.value;

                $.post("{{ route('pos.get-detail-konsinyasi') }}", {toko: tokoPK,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#komisiPK').val(data.potongan);
                });
            });

            if ($(document.getElementById("currLangsung")).length > 0) {
                var k = document.getElementById("currLangsung");
                var currLangsung = k.options[k.selectedIndex].value;

                $('.lblUseCurrLg').html(currLangsung);

                if(currLangsung == 'IDR') {
                    $('#priceRtLg').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/IDR/"+currLangsung+"/1", function(data) {
                        $('#priceRtLg').val(data.result);
                        if (data.success) {
                            $('#priceRtLg').attr('readonly', true);
                        } else {
                            $('#priceRtLg').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceRtLg').attr('readonly', false);
                    });
                }

                if(currLangsung == 'USD') {
                    $('#priceWhLg').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/USD/"+currLangsung+"/1", function(data) {
                        $('#priceWhLg').val(data.result);
                        if (data.success) {
                            $('#priceWhLg').attr('readonly', true);
                        } else {
                            $('#priceWhLg').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceWhLg').attr('readonly', false);
                    });
                }
            }

            $('#currLangsung').on('change', function(e){
                var currLangsung = e.target.value;
                $('.lblUseCurrLg').html(currLangsung);

                if(currLangsung == 'IDR') {
                    $('#priceRtLg').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/IDR/"+currLangsung+"/1", function(data) {
                        $('#priceRtLg').val(data.result);
                        if (data.success) {
                            $('#priceRtLg').attr('readonly', true);
                        } else {
                            $('#priceRtLg').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceRtLg').attr('readonly', false);
                    });
                }

                if(currLangsung == 'USD') {
                    $('#priceWhLg').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/USD/"+currLangsung+"/1", function(data) {
                        $('#priceWhLg').val(data.result);
                        if (data.success) {
                            $('#priceWhLg').attr('readonly', true);
                        } else {
                            $('#priceWhLg').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceWhLg').attr('readonly', false);
                    });
                }
            });

            if ($(document.getElementById("currKonsinyasi")).length > 0) {
                var k = document.getElementById("currKonsinyasi");
                var currKonsinyasi = k.options[k.selectedIndex].value;

                $('.lblUseCurrKs').html(currKonsinyasi);

                if(currKonsinyasi == 'IDR') {
                    $('#priceRtKs').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/IDR/"+currKonsinyasi+"/1", function(data) {
                        $('#priceRtKs').val(data.result);
                        if (data.success) {
                            $('#priceRtKs').attr('readonly', true);
                        } else {
                            $('#priceRtKs').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceRtKs').attr('readonly', false);
                    });
                }

                if(currKonsinyasi == 'USD') {
                    $('#priceWhKs').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/USD/"+currKonsinyasi+"/1", function(data) {
                        $('#priceWhKs').val(data.result);
                        if (data.success) {
                            $('#priceWhKs').attr('readonly', true);
                        } else {
                            $('#priceWhKs').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceWhKs').attr('readonly', false);
                    });
                }
            }

            $('#currKonsinyasi').on('change', function(e){
                var currKonsinyasi = e.target.value;
                $('.lblUseCurrKs').html(currKonsinyasi);

                if(currKonsinyasi == 'IDR') {
                    $('#priceRtKs').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/IDR/"+currKonsinyasi+"/1", function(data) {
                        $('#priceRtKs').val(data.result);
                        if (data.success) {
                            $('#priceRtKs').attr('readonly', true);
                        } else {
                            $('#priceRtKs').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceRtKs').attr('readonly', false);
                    });
                }

                if(currKonsinyasi == 'USD') {
                    $('#priceWhKs').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/USD/"+currKonsinyasi+"/1", function(data) {
                        $('#priceWhKs').val(data.result);
                        if (data.success) {
                            $('#priceWhKs').attr('readonly', true);
                        } else {
                            $('#priceWhKs').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceWhKs').attr('readonly', false);
                    });
                }
            });

            $(".currency").inputFilter(function(value) {
                return /^-?\d*[,]?\d*$/.test(value);
            });
        })
    </script>
@endsection

@section('title')
    SAKA | Point Of Sales
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">POS Menu</h3>
                </div>
                <div class="box-body" style="min-height: 200px">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#" data-toggle="modal" data-target="#form-add-pl">
                            <div class="info-box bg-orange">
                                <span class="info-box-icon"><i class="fa fa-cart-plus"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-pos text-center">Retail</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a>
                        <!-- /.info-box -->
                    </div>
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="#" data-toggle="modal" data-target="#form-add-pk">
                            <div class="info-box bg-teal">
                                <span class="info-box-icon"><i class="fa fa-cart-plus"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-pos text-center">Consignment</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </a>
                        <!-- /.info-box -->
                    </div>
                    @endif
                    <br><br><br>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Sales History Today</h3>
                </div>
                <div class="box-body">
                    <div id="salesTodayScroll">
                        @if($salestoday->count() > 0)
                            @foreach($salestoday as $st)
                            <!-- LIST ITEM SALES -->
                            <div class="box box-default">
                                <div class="box-body">
                                    <ul class="products-list product-list-in-box">
                                        <li class="item">
                                            <div class="product-img">
                                                <img src="{{ asset('assets/dist/img/cart.png') }}" alt="Product Image">
                                            </div>
                                            <div class="product-info">
                                                <a href="{{ route('pos.saved-order', $st->no_faktur) }}" class="product-title">{{ '#'.$st->no_faktur }}
                                                    <span class="label label-success pull-right">IDR {{ number_format($st->grand_total, 2, ',', '.') }}</span><br>
                                                    <span class="label label-info pull-right">{{ $st->used_currency.' '.number_format($st->grand_total/$st->usercurr1_to_idr, 2, ',', '.') }}</span>
                                                </a>
                                                <span class="product-description">{{ date('d/m/Y', strtotime($st->tgl)) }} | {{ $st->nama_pelanggan }}</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <p>Sales data is not available</p>
                        @endif
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="form-add-pl">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Retail</h4>
                </div>
                {!! Form::open(['route' => 'pos.store-customerpl', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('tgl') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl', 'Sales Date', ['class' => 'control-label']) !!}
                                {{ Form::text('tgl', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tgl', 'style' => 'background-color: #fff;']) }}
                                {!! $errors->first('Sales Date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('customer_type') ? 'has-error' : '' !!}">
                                {!! Form::label('customer_type', 'Customer Type', ['class' => 'control-label']) !!}
                                {{ Form::select('customer_type', ['retail' => 'Retail', 'wholesale' => 'Wholesale'], 'retail', ['class' => 'form-control']) }}
                                {!! $errors->first('customer_type', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('pelanggan') ? 'has-error' : '' !!}">
                                {!! Form::label('pelanggan', 'Customer', ['class' => 'control-label']) !!}
                                {{ Form::text('pelanggan', null, ['class' => 'form-control']) }}
                                {!! $errors->first('pelanggan', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                                {!! Form::label('alamat', 'Address', ['class' => 'control-label']) !!}
                                {{ Form::text('alamat', null, ['class' => 'form-control']) }}
                                {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('no_tlp') ? 'has-error' : '' !!}">
                                        {!! Form::label('no_tlp', 'Telp', ['class' => 'control-label']) !!}
                                        {{ Form::text('no_tlp', null, ['class' => 'form-control']) }}
                                        {!! $errors->first('no_tlp', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                        {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                                        {{ Form::text('email', null, ['class' => 'form-control']) }}
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('currency') ? 'has-error' : '' !!}">
                                {!! Form::label('currency', 'Currency', ['class' => 'control-label']) !!}
                                {{ Form::select('currency', ['' => '']+App\Models\Currency::selectRaw('kd_currency, concat(kd_currency," - ", currency) as nama')->pluck('nama', 'kd_currency')->all(), 'IDR', ['class' => 'form-control select2','required', 'style' => 'width: 100%;' , 'id' => 'currLangsung']) }}
                                {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div id="formCurrencyConversionLg" class="row">
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('usd_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="usd_to_usecurr" class="control-label">1 USD to <span class="lblUseCurrLg"></span></label>
                                        {{ Form::text('usd_to_usecurr', null, ['class' => 'form-control currency', 'readonly', 'id' => 'priceWhLg']) }}
                                        {!! $errors->first('usd_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('idr_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="idr_to_usecurr" class="control-label">1 IDR to <span class="lblUseCurrLg"></span></label>
                                        {{ Form::text('idr_to_usecurr', null, ['class' => 'form-control currency', 'readonly', 'id' => 'priceRtLg']) }}
                                        {!! $errors->first('idr_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
    <!-- MODALS -->
    <div class="modal fade" id="form-add-pk">
        <div class="modal-dialog modal-center ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Consigment</h4>
                </div>
                {!! Form::open(['route' => 'pos.store-customerpk', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('tgl') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl', 'Sales Date', ['class' => 'control-label']) !!}
                                {{ Form::text('tgl', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tgl', 'style' => 'background-color: #fff;']) }}
                                {!! $errors->first('Sales Date', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('toko') ? 'has-error' : '' !!}">
                                {!! Form::label('toko', 'Store') !!}
                                {{ Form::select('toko', ['' => '']+App\Models\Location::where('type', 'konsinyasi')->pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control select22','required', 'style' => 'width: 100%;', 'id' => 'tokoPK']) }}
                                {!! $errors->first('toko', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('komisi') ? 'has-error' : '' !!}">
                                {!! Form::label('komisi', 'Consignment Fee (%)') !!}
                                {{ Form::text('komisi', null, ['class' => 'form-control','id' => 'komisiPK']) }}
                                {!! $errors->first('komisi', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('currency') ? 'has-error' : '' !!}">
                                {!! Form::label('currency', 'Currency', ['class' => 'control-label']) !!}
                                {{ Form::select('currency', ['' => '']+App\Models\Currency::selectRaw('kd_currency, concat(kd_currency," - ", currency) as nama')->pluck('nama', 'kd_currency')->all(), 'IDR', ['class' => 'form-control select22','required', 'style' => 'width: 100%;' , 'id' => 'currKonsinyasi']) }}
                                {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div id="formCurrencyConversionKs" class="row">
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('usd_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="usd_to_usecurr" class="control-label">1 USD to <span class="lblUseCurrKs"></span></label>
                                        {{ Form::text('usd_to_usecurr', null, ['class' => 'form-control currency', 'readonly', 'id' => 'priceWhKs']) }}
                                        {!! $errors->first('usd_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('idr_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="idr_to_usecurr" class="control-label">1 IDR to <span class="lblUseCurrKs"></span></label>
                                        {{ Form::text('idr_to_usecurr', null, ['class' => 'form-control currency', 'readonly', 'id' => 'priceRtKs']) }}
                                        {!! $errors->first('idr_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endif
@endsection

@section('footer')
    <footer class="main-footer">
        <strong>&copy; 2018 - Saka Karya Bali</strong> powered by <a href="https://ganeshcomstudio.com">Ganeshcom Studio</a>
    </footer>
@endsection