<html>
<head>
    <title>Receipt {{ $sales->no_faktur }}</title>
    <style>
        @page { margin: 5px 5px; }

        body {
            font-family: Tahoma,Verdana,Segoe,sans-serif;
        }

        .lr-none {
            border-right: none;
            border-left: none;
        }

        .tb-grey {
            border-top: 0.5px solid #888;
            border-bottom: 0.5px solid #888;
        }

        .pd10 {
            padding-bottom: 4px;
            padding-top: 4px;
        }

        .pd8 {
            padding-bottom: 3px;
            padding-top: 3px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .left {
            text-align: left;
        }

        .right {
            text-align: right;
        }

        .vat {
            vertical-align: top;
        }

        .text-muted {
            color: #000000;
        }

        table > tr > td {
            font-family: Tahoma,Verdana,Segoe,sans-serif !important;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="15%" style="vertical-align: middle;">
              <img style="margin-top: 3px;" src="{{ asset('img/logo-saka-2.png') }}" alt="">
              {{--  <img style="margin-top: 3px;" src="img/logo-saka-2.png" alt="">  --}}
        </td>
        <td width="85%" style="vertical-align: top;">
            <table width="100%" style="margin-top: 5px; font-size: 9px;">
                <tr>
                    <td colspan="3"><strong style="font-size: 12px;">Saka Jewelry</strong></td>
                </tr>
                <tr>
                    <td width="15%"><strong>Receipt</strong></td>
                    <td width="1%">:</td>
                    <td width="84%" style="text-align: left;">{{ $sales->no_faktur }}</td>
                </tr>
                <tr>
                    <td><strong>Date</strong></td>
                    <td>:</td>
                    <td style="text-align: left;">{{ date('d F Y', strtotime($sales->tgl)) }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" class="table" border="1" style="margin-top: 10px; font-size: 9px; border-collapse: collapse; border-bottom: none; border-top: none; border-left: none; border-right: none;">
    <tr>
        <th class="lr-none tb-grey pd10 left" width="34%">Product</th>
        <th class="lr-none tb-grey pd10 center" width="8%">Qty</th>
        <th class="lr-none tb-grey pd10 center" width="20%">Price</th>
        <th class="lr-none tb-grey pd10 center" width="10%">Type</th>
        <th class="lr-none tb-grey pd10 right" width="28%">Sub Total</th>
    </tr>
    <?php $subTotal = 0; $totalDiskon = 0; ?>
    @foreach($detailsales as $ds)
        <tr>
            <td class="lr-none tb-grey pd8 vat">
                <span class="bold">{{ $ds->produk->kd_produk }}</span><br>
                <span class="bold">{{ $ds->produk->deskripsi }}</span><br>

                @if($ds->disc_persen > 0)
                    <span class="text-muted">Discount  </span><span class="text-muted">({{ $ds->disc_persen.'%' }})</span><br>
                @endif

                @if($ds->catatan != '')
                    <span class="text-muted">{{ $ds->catatan }}</span><br>
                @endif
            </td>
            <td class="lr-none tb-grey pd8 center vat">
                <span class="bold">{{ $ds->qty }}</span><br>

                {{--  @if($ds->disc_persen > 0)
                    <span class="text-muted">{{ $ds->disc_persen.'%' }}</span><br>
                @endif  --}}
            </td>
            <td class="lr-none tb-grey pd8 right vat">
                <span class="bold center">{{ number_format($ds->price/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>

                @if($ds->disc_persen > 0)
                    <span class="text-muted">{{ number_format((($ds->disc_persen*$ds->price)/100)/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>
                @endif
            </td>
            <td class="lr-none tb-grey pd8 center vat">
                {{ $ds->price_type == 'wholesale_price' ? 'Wholesale' : 'Retail' }}
            </td>
            <td class="lr-none tb-grey pd8 right vat">
                <span class="bold">{{ number_format(($ds->price*$ds->qty)/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>
                <?php $subTotal = $subTotal + ($ds->price*$ds->qty); ?>

                @if($ds->disc_persen > 0)
                    <span class="text-muted">{{ number_format(((($ds->disc_persen*$ds->price)/100)*$ds->qty)/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>
                    <?php $totalDiskon = $totalDiskon + (($ds->disc_persen*$ds->price)/100)*$ds->qty; ?>
                @endif
            </td>
        </tr>
    @endforeach
</table>

<div style="margin-left: 50%; margin-top: 10px;">
    <?php $semuaHarga = 0; ?>
    <table width="100%" style="font-size: 9px; border-collapse: collapse;">
        <tr>
            <td class="lr-none tb-grey pd8 left" width="50%">Sub Total</td>
            <td class="lr-none tb-grey pd8 right" width="50%">{{ number_format($subTotal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
            <?php $semuaHarga = $semuaHarga + $subTotal; ?>
        </tr>

        @if($sales->disc_persen > 0)
            <tr>
                <td class="lr-none tb-grey pd8 left">Global Diskon ({{ $sales->disc_persen.'%' }})</td>
                <td class="lr-none tb-grey pd8 right">{{ number_format($sales->disc_nominal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
            </tr>
        @endif
        
        @if($sales->additional_disc > 0)
            <tr>
                <td class="lr-none tb-grey pd8 left">Additional Disc</td>
                <td class="lr-none tb-grey pd8 right">{{ number_format($sales->additional_disc/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
            </tr>
        @endif

        @if($sales->type_sales == 'konsinyasi')
            @if($sales->komisi_persen > 0)
                <tr>
                    <td class="lr-none tb-grey pd8 left">Consignment Fee ({{ $sales->komisi_persen.'%' }})</td>
                    <td class="lr-none tb-grey pd8 right">{{ number_format($sales->komisi_nominal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
                </tr>
            @endif
        @endif

        <tr>
            <td class="lr-none tb-grey pd8 left">Grand Total</td>
            <td class="lr-none tb-grey pd8 right" style="color: #000000;"><strong>{{ $sales->used_currency }} {{ number_format($sales->grand_total/$sales->usercurr1_to_idr, 2, ',', '.') }}</strong></td>
        </tr>
    </table>
</div>


<div class="center" style="margin-top: 220px;">
    <span style="font-weight: bold; font-size: 10px;">Thank you</span><br>
    <span style="font-weight: bold; font-size: 9px;">Saka Jewelry</span><br>
    <span style="font-size: 8px;">Jalan Raya Lod Tunduh - Banjar Mawang Kelod, UBUD 80571</span><br>
    <img src="{{ asset('img/web3.png') }}" alt="" style="margin-top:4px;margin-right:4px;"><span style="font-size: 8px;">www.sakajewelry.com</span>
    <img src="{{ asset('img/insta1.jpg') }}" alt="" style="margin-top:4px;margin-right:4px;"><span style="font-size: 8px;">saka_jewelry</span>
    <img src="{{ asset('img/fb.jpg') }}" alt="" style="margin-top:4px;margin-right:4px;"><span style="font-size: 8px;">@sakajewelry</span>
    {{--  src="img/web3.png"
    src="img/insta1.jpg"
    src="img/fb.jpg"  --}}
</div>
</body>
</html>