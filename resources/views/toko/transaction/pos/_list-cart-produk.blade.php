<ul class="media-list media-list-linked media-list-bordered">
    <?php $grandTotal = 0; $komisi = 0; $subTotalProduk = 0; ?>
    @if(!empty($orderProduk))
        @if(count($orderProduk) > 0)
            @foreach($orderProduk as $index => $op)
            <li class="media">
                <div class="list-group-item">
                    <div class="row" style="font-weight: bold;">
                        <span class="col-md-5">{{ $op['deskripsi'] }}</span>
                        <span class="col-md-2">{{ 'x '.$op['qty']  }}</span>
                        <span class="text-right text-danger-400 col-md-5">{{ ($op['price_type'] == "wholesale_price" ? "(W)" : "(R)").'  '.number_format($op['harga_produk'], 4, ',', '.') }}</span>
                    </div>
                    @if($op['disc_persen'] > 0)
                        <div style="margin: 8px 0;">
                            <div class="row" style="font-size: 12px;">
                                <span class="col-md-5">Discount</span>
                                <span class="col-md-2">{{ $op['disc_persen'].'%' }}</span>
                                <span class="text-right col-md-5">{{ number_format($op['totalDiscountProduk'], 4, ',', '.') }}</span>
                            </div>
                        </div>
                    @endif
                    @if($op['catatan'] != '')
                        <div class="row">
                            <div class="col-md-12">
                                <p style="font-size: 11px; text-align: justify; color: #b3b3b3;">{{ $op['catatan'] }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="row" style="font-weight: bold;">
                        <span class="col-md-6 text-success">{{ number_format($op['totalHargaProduk'], 4, ',', '.') }}</span>
                        <?php
                            $grandTotal = $grandTotal + $op['totalHargaProduk'];
                            $subTotalProduk = $subTotalProduk + $op['totalHargaProduk'];
                            if (session()->get('order.type') == 'konsinyasi') {
                                if (floatval(session()->get('order.pelanggan.komisi')) > 0) {
                                    $komisi = $komisi + ($op['totalHargaProduk']*session()->get('order.pelanggan.komisi')/100);
                                }
                            }
                        ?>
                        <span class="text-right col-md-6">
                            <a href="{{ route('pos.additem.editdetailitem', $index) }}" data-target="#modalDetailItem" data-toggle="modal" class="btn btn-link" data-popup="tooltip" data-original-title="Edit" style="padding: 0 3px; color: inherit;"><i class="fa fa-pencil" style="font-size: 13px;"></i></a>
                            <button class="btn btn-link btnDeleteItem" data-index="{{ $index }}" data-popup="tooltip" data-original-title="Delete" style="padding: 0 3px; color: inherit;"><i class="fa fa-trash" style="font-size: 13px;"></i></button>
                        </span>
                    </div>
                </div>
            </li>
            @endforeach

            <?php session()->put('order.sub_total_produk', $subTotalProduk); ?>
        @else
            <li class="media">
                <div class="list-group-item">
                    <span>
                        No product items added
                    </span>
                </div>
            </li>
        @endif
    @else
        <li class="media">
            <div class="list-group-item">
                <span>
                    No product items added
                </span>
            </div>
        </li>
    @endif

    @if(count($surcharges) > 0)
        @if(isset($surcharges['global_discount']))
            <?php
            $totalGlobalDiscount = ($grandTotal*$surcharges['global_discount_persen'])/100;
            $additionalGlobalDisc=$surcharges['additional_discount_nominal'];
            session()->put('order.total_global_discount', $totalGlobalDiscount);
            session()->put('order.additional_global_discount', $additionalGlobalDisc);
            $grandTotal = $grandTotal - $totalGlobalDiscount-$additionalGlobalDisc;
            ?>
            <li class="media">
                <div class="list-group-item">
                    <div class="row" style="font-weight: bold;">
                        <span class="text-danger-400 col-md-7">Global Discount ({{ $surcharges['global_discount_persen'].'%' }})</span>
                        <span class="text-right text-danger-400 col-md-5">{{ number_format($totalGlobalDiscount, 4, ',', '.') }}</span>
                    </div>
                    <div class="row" style="font-weight: bold;">
                        <span class="text-danger-400 col-md-7">Additional Discount </span>
                        <span class="text-right text-danger-400 col-md-5">{{ number_format($additionalGlobalDisc, 4, ',', '.') }}</span>
                    </div>
                    <div class="row">
                        <span class="col-md-12 text-right"><button class="btn btn-link btnDeleteSurcharge" data-index="global-disc" data-popup="tooltip" data-original-title="Delete" style="padding: 0 3px; color: inherit;"><i class="fa fa-trash" style="font-size: 13px;"></i></button></span>
                    </div>
                </div>
            </li>
        @endif
    @endif

    <?php
        session()->put('order.total_komisi', $komisi);
        $grandTotal = $grandTotal - $komisi;
    ?>

    @if(session()->get('order.type') == 'konsinyasi')
        @if($komisi > 0)
            <li class="media">
                <div class="list-group-item">
                    <div class="row" style="font-weight: bold;">
                        <span class="text-danger-400 col-md-7">Consignment Fee ({{ session()->get('order.pelanggan.komisi').'%' }})</span>
                        <span class="text-right text-danger-400 col-md-5">{{ number_format($komisi, 4, ',', '.') }}</span>
                    </div>
                </div>
            </li>
        @endif
    @endif

    <?php session()->put('order.grand_total', $grandTotal); ?>
</ul>

<script>
    $('[data-popup="tooltip"]').tooltip();

    $('#grandTotalVal').html('{{ number_format($grandTotal, 4, ',', '.') }}');

    $('.btnDeleteItem').click(function(){
        var index = $(this).data("index");
        $.ajax({
            url: "{{ url('transaction/pos/delete-item/') }}"+"/"+index,
            type: "post",
            data: {
                "index": index,
                "_token": $('meta[name="_token"]').attr('content'),
            },
            success: function(response) {
                getListItemProduk();

                new PNotify({
                    title: 'Success',
                    text: 'Produk berhasil dihapus',
                    addclass: 'alert-styled-left',
                    type: 'success'
                });
            }
        });
    });

    $('.btnDeleteSurcharge').click(function(){
        var index = $(this).data("index");
        $.ajax({
            url: "{{ url('transaction/pos/delete-surcharge/') }}"+"/"+index,
            type: "post",
            data: {
                "index": index,
                "_token": $('meta[name="_token"]').attr('content'),
            },
            success: function(response) {
                getListItemProduk();

                new PNotify({
                    title: 'Success',
                    text: 'Biaya tambahan berhasil dihapus',
                    addclass: 'alert-styled-left',
                    type: 'success'
                });
            }
        });
    });
</script>