@extends('layouts.master-pos')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style>
        .slimScrollBar {
            right: -5px !important;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multifield/jquery.multifield.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script type="text/javascript">
        function batalkanOrder() {
            swal({
                title: 'Are you sure?',
                text: "You will not be able to return this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Cancel!'
            }, function () {
                $('#formBatalkan').submit();
            });
        }

        function getDataListProduk(kategoriproduk) {
            var search = $('#search').val();
            $.post("{{ route('pos.additem.getlistproduk') }}", {kp: kategoriproduk, search: search, _token: $('meta[name="_token"]').attr('content') }, function(dataProduk) {
                $('#dataListProduk').html(dataProduk);
            });
        }

        function getListItemProduk() {
            $.get("{{ route('pos.additem.getcartitem') }}", function(dataCartProduk) {
                $('#listCartItem').html(dataCartProduk);
            });
        }

        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        $(function () {
            getDataListProduk('all');
            getListItemProduk();

            $('#navProduk > li').on('click', function (e) {
                var linkKp = $(this).find('a');
                var kp = linkKp.attr('data-kategori');
                $('#search').val('');

                getDataListProduk(kp);

                $('#navProduk li.active').removeClass('active')
                $(this).addClass('active');
            });

            $('.select2').select2({
                dropdownParent: $('#changeCurrency')
            });

            $('#search').on('keyup', function () {
                var kp = $('#navProduk li.active a').attr('data-kategori');
                getDataListProduk(kp);
            });

            $('#currTransaction').on('change', function(e){
                var currTransaction = e.target.value;
                $('.lblUseCurr').html(currTransaction);

                if(currTransaction == 'IDR') {
                    $('#priceRt').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/IDR/"+currTransaction+"/1", function(data) {
                        $('#priceRt').val(data.result);
                        if (data.success) {
                            $('#priceRt').attr('readonly', true);
                        } else {
                            $('#priceRt').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceRt').attr('readonly', false);
                    });
                }

                if(currTransaction == 'USD') {
                    $('#priceWh').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/USD/"+currTransaction+"/1", function(data) {
                        console.log(data.status);
                        $('#priceWh').val(data.result);
                        if (data.success) {
                            $('#priceWh').attr('readonly', true);
                        } else {
                            $('#priceWh').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#priceWh').attr('readonly', false);
                    });
                }
            });

            $(".currency").inputFilter(function(value) {
                return /^-?\d*[,]?\d*$/.test(value);
            });

            $('#listTambahan').multifield({
                section: '.group',
                btnAdd:'#btnAddSurcharge',
                btnRemove:'.btnRemove'
            });

            $("#formSurcharge").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: $(form).serialize(),
                        success: function(response) {
                            $('#addSurcharge').modal('hide');

                            document.getElementById('formSurcharge').reset();

                            new PNotify({
                                title: 'Success',
                                text: 'Additional fees have been saved',
                                addclass: 'alert-styled-left',
                                type: 'success'
                            });

                            getListItemProduk();
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    SAKA | Point Of Sales
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Category</h3>
                </div>
                <div class="box-body no-padding" style="min-height: 475px">
                    <ul class="nav nav-pills nav-stacked" id="navProduk">
                        <li class="active">
                            <a href="#" data-kategori="all">
                                <i class="fa fa-tags"></i> All
                                {{--  <span class="label label-warning pull-right">{{ $kategoriproduk->count() > 0 ? $kategoriproduk->sum('stock') : 0 }}</span>  --}}
                                <span class="label label-warning pull-right">{{ $allstockproduk->allstock }}</span> 
                            </a>
                        </li>

                        @foreach($kategoriproduk as $kp)
                        <li>
                            <a href="#" data-kategori="{{ $kp->kd_kat }}">
                                <i class="fa fa-tags"></i> {{ $kp->kategori }}
                                {{--  <span class="label label-warning pull-right">{{ $kp->stock }}</span>  --}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input class="form-control" id="search" name="search" type="text" placeholder="Search...">
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-body" style="padding: 10px;">
                        <div id="dataListProduk"></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="detail-sticky-wrapper" class="sticky-wrapper">
                        <div id="detail" class="panel" style="border: none;">
                            <div class="panel-heading" style="border-bottom: 1px solid #eeeeee;">
                                <div style="display: inline-block;">
                                    <h6 class="panel-title">{{ $nofaktur }}</h6>
                                    <p style="margin-bottom: 0px;">{{ $pelanggan }}</p>
                                </div>
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger btn-sm">{{ session()->get('order.currency.used_currency') }}</button>
                                    <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#changeCurrency" data-toggle="modal">Change Currency</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body panel-fixed" style="max-height: 500px; padding: 0;">
                                <div id="listCartItem"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <div id="modalDetailItem" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
    <div id="changeCurrency" class="modal fade" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change Currency</h4>
                </div>
                {!! Form::open(['route' => 'pos.additem.changecurrency', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('currency') ? 'has-error' : '' !!}">
                                {!! Form::label('currency', 'Currency', ['class' => 'control-label']) !!}
                                {{ Form::select('currency', ['' => '']+App\Models\Currency::selectRaw('kd_currency, concat(kd_currency," - ", currency) as nama')->pluck('nama', 'kd_currency')->all(), session()->get('order.currency.used_currency'), ['class' => 'form-control select2','required', 'style' => 'width: 100%;' , 'id' => 'currTransaction']) }}
                                {!! $errors->first('currency', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div id="formCurrencyConversionLg" class="row">
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('usd_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="usd_to_usecurr" class="control-label">1 USD to <span class="lblUseCurr">{{ session()->get('order.currency.used_currency') }}</span></label>
                                        {{ Form::text('usd_to_usecurr', str_replace('.', ',', session()->get('order.currency.usd1_to_usecurr')), ['class' => 'form-control currency', 'readonly', 'id' => 'priceWh']) }}
                                        {!! $errors->first('usd_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors->has('idr_to_usecurr') ? 'has-error' : '' !!}">
                                        <label for="idr_to_usecurr" class="control-label">1 IDR to <span class="lblUseCurr">{{ session()->get('order.currency.used_currency') }}</span></label>
                                        {{ Form::text('idr_to_usecurr', str_replace('.', ',', session()->get('order.currency.idr1_to_usecurr')), ['class' => 'form-control currency', 'readonly', 'id' => 'priceRt']) }}
                                        {!! $errors->first('idr_to_usecurr', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
@endsection

@section('footer')
    <footer class="sticky-footer">
        <ul class="nav nav-tabs nav-lg nav-justified no-margin no-border-radius text-center">
            <li class="bg-danger">
                <div data-toggle="tab" aria-expanded="true" class="text-uppercase">
                    <h5>Total: {{ session()->get('order.currency.used_currency') }} <span id="grandTotalVal">0</span></h5>
                </div>
            </li>
            <li class="bg-grey" id="batal">
                <a href="#" onclick="batalkanOrder()" class="text-uppercase no-hover" style="color: #fff; border-bottom: none;">
                    <h5 style="margin: 0;">Cancel</h5>
                </a>
            </li>
            <li class="bg-info" id="surcharge">
                <a href="#addSurcharge" data-toggle="modal" class="text-uppercase no-hover" style="color: #fff; border-bottom: none;">
                    <h5 style="margin: 0;">Discount</h5>
                </a>
            </li>
            <li class="bg-success" id="simpan">
                <a href="{{ route('pos.orderdetail') }}" class="text-uppercase no-hover" style="color: #fff; border-bottom: none;">
                    <h5 style="margin: 0;">Send Order</h5>
                </a>
            </li>
        </ul>
    </footer>

    <div style="display: none;">
        {!! Form::open(['route' => 'pos.batalkanorder', 'method' => 'post', 'novalidate', 'id' => 'formBatalkan']) !!}
        {!! Form::close() !!}
    </div>

    <div id="addSurcharge" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route' => 'pos.storesurcharge', 'id' => 'formSurcharge', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            {!! Form::checkbox('global_disc', '1', true, ['id' => 'globalDisc']) !!} Global Discount(%)
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    {{ Form::number('global_disc_persen', 0,['class' => 'form-control', 'id' => 'globalDiscPersen', 'min' => 0, 'max' => 100]) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="checkbox-inline">
                                            {!! Form::checkbox('additional_disc', '1', true, ['id' => 'additionalDisc']) !!} Additional Discount(Nominal)
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    {{ Form::number('additional_disc_nominal', 0,['class' => 'form-control', 'id' => 'additionalDiscNominal']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn bg-teal-400']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection