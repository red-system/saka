@extends('layouts.master-pos')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <style>
        .panel-heading {
            padding: 15px 20px;
            border-bottom: 1px solid #f4f4f4 !important;
        }

        .list-condensed > li, .list-condensed > li .list > li {
            margin-top: 3px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 15px 20px;
        }

        .table.pd8 > thead > tr > th, .table.pd8 > tbody > tr > th, .table.pd8 > tfoot > tr > th, .table.pd8 > thead > tr > td, .table.pd8 > tbody > tr > td, .table.pd8 > tfoot > tr > td {
            padding: 8px !important;
        }

        .text-semibold {
            font-weight: 500;
        }

        .modal-full {
            width: calc(100% - 30px);
        }

        .nav-tabs-custom {
            margin-bottom: 20px;
            background: #e3e3e3;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
            border-radius: 2px;
        }

        .nav-tabs {
            border-bottom: none;
        }

        .nav-tabs-custom > .nav-tabs > li {
            border-top: none !important;
            margin-bottom: 0px;
        }

        .nav-tabs-custom > .nav-tabs > li.active > a, .nav-tabs-custom > .nav-tabs > li.active:hover > a {
            background-color: #00a7d0;
            color: #fff;
        }


    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            };
        }(jQuery));

        function addPayment() {
            var row = $('#rowAddPayment tbody').html();
            $('#tbDataPayment tbody').append(row);
            $('#tbDataPayment tbody tr:last').find('input[name="paymentPrice[]"]').number(true, 2);
            $('#tbDataPayment tbody tr:last').find('input[name="dueDate[]"]').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
        }

        function removePayment(element) {
            $(element).parents('tr').remove();
        }

        function hitungSisaPembayaran() {
            var totalPayment = 0;
            var grandTotal = {!! $order['grand_total'] !!};
            $('input[name="paymentPrice[]"]').each(function() {
                var payment = parseInt($(this).val());
                if(payment > 0) {
                    totalPayment = totalPayment + payment;
                }
            });
            var sisaPayment = Math.abs(parseInt(totalPayment - grandTotal));
            $('#sisaPembayaran').html(sisaPayment.formatMoney(2, ',', '.'));
        }

        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('.select2').select2({
                dropdownParent: $('#finishOrder')
            });

            if ($(document.getElementById("usercurr1_to_idr")).length > 0) {
                var usedCurrency = '{{ $currency->kd_currency }}';

                if(usedCurrency == 'IDR') {
                    $('#usercurr1_to_idr').val(1);
                } else {
                    $.get("{{ url('currency-conversion') }}/"+usedCurrency+"/IDR/1", function(data) {
                        $('#usercurr1_to_idr').val(data.result);
                        if (data.success) {
                            $('#usercurr1_to_idr').attr('readonly', true);
                        } else {
                            $('#usercurr1_to_idr').attr('readonly', false);
                        }
                    }).fail(function () {
                        $('#usercurr1_to_idr').attr('readonly', false);
                    });
                }
            }
        })

        $(function () {
            $('.currency').number(true, 2);
        })
    </script>
@endsection

@section('title')
    SAKA | Point Of Sales
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-warning">
                <p>All price in <strong>{{ $currency->kd_currency.' - '.$currency->currency }}</strong></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Order Detail</h4>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-sm-6 content-group">
                            <ul class="list-condensed list-unstyled" style="padding-left: 5px;">
                                <li>Invoice Number: {{ $order['no_faktur'] }}</li>
                                <li>Customer: {{ $order['pelanggan']['pelanggan'] }}</li>
                                @if($order['type'] == 'langsung')
                                    <li>Telp: {{ $order['pelanggan']['no_tlp'] != '' ? $order['pelanggan']['no_tlp'] : '-' }}</li>
                                    <li>Email: {{ $order['pelanggan']['email'] != '' ? $order['pelanggan']['email'] : '-' }}</li>
                                    <li><Address></Address>: {{ $order['pelanggan']['alamat'] != '' ? $order['pelanggan']['alamat'] : '-' }}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <?php $subTotal = 0;?>
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>Description</th>
                            <th class="col-sm-1">Qty</th>
                            <th class="col-sm-2">Price</th>
                            <th class="col-sm-2">Remark</th>
                            <th class="col-sm-2">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order['produk'] as $ip => $produk)
                            <tr>
                                <td>
                                    <h5 class="no-margin">{{ $produk['deskripsi'] }}</h5>

                                    @if($produk['disc_persen'] > 0)
                                        <span class="text-muted">Discount</span><br>
                                    @endif

                                    @if($produk['catatan'] != '')
                                        <span class="text-muted">{{ $produk['catatan'] }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ $produk['qty'] }}</h5>

                                    @if($produk['disc_persen'] > 0)
                                        <span class="text-muted">{{ $produk['disc_persen'].'%' }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ number_format($produk['harga_produk'], 4, ',', '.') }}</h5>

                                    @if($produk['disc_persen'] > 0)
                                        <span class="text-muted">{{ number_format($produk['disc_nominal'], 4, ',', '.') }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ $produk['price_type'] == 'wholesale_price' ? 'Wholesale Price' : 'Retail Price' }}</h5>
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ number_format($produk['harga_produk']*$produk['qty'], 4, ',', '.') }}</h5>

                                    @if($produk['disc_persen'] > 0)
                                        <span class="text-muted">{{ number_format($produk['totalDiscountProduk'], 4, ',', '.') }}</span><br>
                                    @endif

                                    <?php $subTotal = $subTotal + (($produk['harga_produk']*$produk['qty']) - ($produk['disc_nominal']*$produk['qty'])); ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase">Total</h4>
                </div>
                <div class="table-responsive no-border">
                    <?php $semuaHarga = 0; ?>
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Subtotal</th>
                            <td class="text-right">{{ number_format($subTotal, 4, ',', '.') }}</td>
                        </tr>
                        <?php $semuaHarga = $semuaHarga + $subTotal; ?>

                        @if(isset($order['surcharge']))
                            @if(count($order['surcharge']) > 0)
                                @if(isset($order['surcharge']['global_discount']))
                                    <tr>
                                        <th>Global Discount ({{ $order['surcharge']['global_discount_persen'].'%' }})</th>
                                        <td class="text-right">{{ number_format($order['total_global_discount'], 4, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Additional Discount</th>
                                        <td class="text-right">{{ number_format($order['surcharge']['additional_discount_nominal'], 4, ',', '.') }}</td>
                                    </tr>
                                    <?php $semuaHarga = $semuaHarga - $order['total_global_discount']; ?>
                                @endif
                            @endif
                        @endif

                        @if(session()->get('order.type') == 'konsinyasi')
                            @if($order['total_komisi'] > 0)
                                <?php $semuaHarga = $semuaHarga - $order['total_komisi']; ?>
                                <tr>
                                    <th>Consignment Fee ({{ session()->get('order.pelanggan.komisi').'%' }})</th>
                                    <td class="text-right">{{ number_format($order['total_komisi'], 4, ',', '.') }}</td>
                                </tr>
                            @endif
                        @endif

                        <tr>
                            <th>Grand Total</th>
                            <td class="text-right text-primary"><p class="text-semibold" style="font-size: 15px;">{{ number_format($order['grand_total'], 4, ',', '.') }}</p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button type="button" data-toggle="modal" data-target="#finishOrder" class="btn btn-success">Finish Order</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="finishOrder" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Finish Order</h4>
                </div>
                {!! Form::open(['route' => 'pos.storeallorder', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    @if($order['type'] == 'langsung')
                    <div class="nav-tabs-custom" style="margin-bottom: 0;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#payment" data-toggle="tab">Payment</a></li>
                            <li><a href="#dataCustomer" data-toggle="tab">Data Customer</a></li>
                        </ul>
                    </div>
                    <div class="tab-content" style="padding: 15px 0 0 0;">
                        <div class="tab-pane active" id="payment">
                    @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-body">
                                        <div class="form-group {!! $errors->has('kode_bukti') ? 'has-error' : '' !!}">
                                            {!! Form::label('kode_bukti', 'Invoice Number') !!}
                                            {{ Form::text('kode_bukti', $order['no_faktur'],['class' => 'form-control', 'readonly', 'id' => 'totalBayar']) }}
                                            {!! $errors->first('kode_bukti', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-body">
                                        <div class="form-group {!! $errors->has('usercurr1_to_idr') ? 'has-error' : '' !!}">
                                            {!! Form::label('usercurr1_to_idr', '1 '.$currency->kd_currency.' to IDR') !!}
                                            {{ Form::text('usercurr1_to_idr', null,['class' => 'form-control', 'readonly', 'id' => 'usercurr1_to_idr']) }}
                                            {!! $errors->first('usercurr1_to_idr', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="form-group {!! $errors->has('salesuser_id') ? 'has-error' : '' !!}">
                                            {!! Form::label('salesuser_id', 'SPG/SPB') !!}
                                            {{ Form::select('salesuser_id', ['' => '']+\App\Models\User::pluck('name', 'id')->all(), null, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                                            {!! $errors->first('salesuser_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="margin-top: 0px;">
                            <button type="button" class="btn btn-social btn-info" onclick="addPayment();">
                                <i class="fa fa-plus"></i> Add Payment
                            </button>
                            <table class="table table-hover table-bordered pd8" id="tbDataPayment" style="margin-top: 10px;">
                                <thead>
                                <tr>
                                    <th>Payment Type</th>
                                    <th>Bank</th>
                                    <th>Due Date</th>
                                    <th>Amount</th>
                                    <th class="text-center col-md-1">#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        {{ Form::select('paymentType[]', []+\App\Models\Payment::paymentTypeList(), 'tunai', ['class' => 'form-control']) }}
                                    </td>
                                    <td>
                                        {{ Form::select('bank[]', ['' => '']+\App\Models\Payment::bankList(), null, ['class' => 'form-control']) }}
                                    </td>
                                    <td class="form-group">
                                        {{ Form::text('dueDate[]', null, ['class' => 'form-control date-picker','required', 'id' => 'tglawal']) }}
                                    </td>
                                    <td>
                                        {{ Form::text('paymentPrice[]', number_format($order['grand_total'], 2, ',', '.'), ['class' => 'form-control currency', 'min' => 0, 'onchange' => 'hitungSisaPembayaran()']) }}
                                    </td>
                                    <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Payment">
                                        <button type="button" class="btn btn-danger" onclick="removePayment(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Grand Total</h3>
                                </div>
                                <div class="col-md-6">
                                    <h3>{{ number_format($order['grand_total'], 2, ',', '.') }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h3 style="margin-top: 0px;">Balance</h3>
                                </div>
                                <div class="col-md-6">
                                    <h3 style="margin-top: 0px;" id="sisaPembayaran">0</h3>
                                </div>
                            </div>
                        @if($order['type'] == 'langsung')
                        </div>
                        <div class="tab-pane" id="dataCustomer">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {!! $errors->has('customer_type') ? 'has-error' : '' !!}">
                                        {!! Form::label('customer_type', 'Customer Type', ['class' => 'control-label']) !!}
                                        {{ Form::select('customer_type', ['retail' => 'Retail', 'wholesale' => 'Wholesale'], session()->get('order.pelanggan.customer_type'), ['class' => 'form-control']) }}
                                        {!! $errors->first('customer_type', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group {!! $errors->has('pelanggan') ? 'has-error' : '' !!}">
                                        {!! Form::label('pelanggan', 'Customer', ['class' => 'control-label']) !!}
                                        {{ Form::text('pelanggan', session()->get('order.pelanggan.pelanggan'), ['class' => 'form-control']) }}
                                        {!! $errors->first('pelanggan', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group {!! $errors->has('alamat') ? 'has-error' : '' !!}">
                                        {!! Form::label('alamat', 'Address', ['class' => 'control-label']) !!}
                                        {{ Form::text('alamat', session()->get('order.pelanggan.alamat'), ['class' => 'form-control']) }}
                                        {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group {!! $errors->has('no_tlp') ? 'has-error' : '' !!}">
                                                {!! Form::label('no_tlp', 'Telp', ['class' => 'control-label']) !!}
                                                {{ Form::text('no_tlp', session()->get('order.pelanggan.no_tlp'), ['class' => 'form-control']) }}
                                                {!! $errors->first('no_tlp', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                                {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                                                {{ Form::text('email', session()->get('order.pelanggan.email'), ['class' => 'form-control']) }}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    {!! Form::button('Checkout', ['type'=> 'submit', 'class'=>'btn bg-teal-400']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <table class="hide" id="rowAddPayment">
        <tbody>
        <tr>
            <td>
                {{ Form::select('paymentType[]', []+\App\Models\Payment::paymentTypeList(), null, ['class' => 'form-control']) }}
            </td>
            <td>
                {{ Form::select('bank[]', ['' => '']+\App\Models\Payment::bankList(), null, ['class' => 'form-control']) }}
            </td>
            <td class="form-group">
                {{ Form::text('dueDate[]', null, ['class' => 'form-control date-picker','required', 'id' => 'jatuhTempo']) }}
            </td>
            <td>
                {{ Form::text('paymentPrice[]', null, ['class' => 'form-control currency', 'min' => 0, 'onchange' => 'hitungSisaPembayaran()']) }}
            </td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="Delete Payment">
                    <button type="button" class="btn btn-danger" onclick="removePayment(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
@endsection