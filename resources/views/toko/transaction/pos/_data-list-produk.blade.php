<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Code</th>
            <th>Product</th>
            <th>Size</th>
            <th class="text-center col-md-2">Stock</th>
            <th class="text-center col-md-2">Retail</th>
            <th class="text-center col-md-2">Wholesale</th>
            <th class="text-center col-md-1">Act</th>
        </tr>
        </thead>
        <tbody>
        @if($dataproduk->count() > 0)
            @foreach($dataproduk as $dp)
            <tr>
                <td>{{ $dp->kd_produk }}</td>
                <td>{!! $dp->deskripsi !!}</td>
                <td>{!! $dp->size_product !!}</td>
                <td class="text-center">{{ $dp->qty }}</td>
                <td class="text-right"><strong>{!! number_format($dp->price*$currency['idr1_to_usecurr'], 4, ',', '.') !!}</strong></td>
                <td class="text-right"><strong>{!! number_format($dp->wholesale_price*$currency['usd1_to_usecurr'], 4, ',', '.') !!}</strong></td>
                <td class="text-center">
                    <a href="{{ route('pos.additem.detailitem', $dp->id_stock) }}" data-target="#modalDetailItem" data-toggle="modal" class="btn btn-info btn-labeled btn-xs" style="font-size: 10px; ">Add</a>
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" class="text-center">There is no product data available</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>