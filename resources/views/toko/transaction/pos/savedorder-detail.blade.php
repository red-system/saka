@extends('layouts.master-pos')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .panel-heading {
            padding: 15px 20px;
            border-bottom: 1px solid #f4f4f4 !important;
        }

        .list-condensed > li, .list-condensed > li .list > li {
            margin-top: 3px;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 15px 20px;
        }

        .table.pd8 > thead > tr > th, .table.pd8 > tbody > tr > th, .table.pd8 > tfoot > tr > th, .table.pd8 > thead > tr > td, .table.pd8 > tbody > tr > td, .table.pd8 > tfoot > tr > td {
            padding: 8px !important;
        }

        .text-semibold {
            font-weight: 500;
        }

        .modal-full {
            width: calc(100% - 30px);
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
@endsection

@section('custom-script')

@endsection

@section('title')
    SAKA | Point Of Sales
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-warning">
                <p>All price in <strong>{{ $currency->kd_currency.' - '.$currency->currency }}</strong></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Order Detail</h4>
                </div>

                <div class="panel-body no-padding-bottom">
                    <div class="row">
                        <div class="col-sm-6 content-group">
                            <ul class="list-condensed list-unstyled" style="padding-left: 5px;">
                                <li>Invoice Number: {{ $sales->no_faktur }}</li>
                                <li>Customer: {{ $sales->nama_pelanggan }}</li>
                                <li>Telp: {{ $sales->telp }}</li>
                                <li>Email: {{ $sales->email }}</li>
                                <li>Address: {{ $sales->alamat }}</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <?php $subTotal = 0;?>
                    <table class="table table-lg">
                        <thead>
                        <tr>
                            <th>Deskripsi</th>
                            <th class="col-sm-1">Qty</th>
                            <th class="col-sm-2">Price</th>
                            <th class="col-sm-2">Remark</th>
                            <th class="col-sm-2">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($detailsales as $ds)
                            <tr>
                                <td>
                                    <h5 class="no-margin">{{ $ds->produk->deskripsi }}</h5>

                                    @if($ds->disc_persen > 0)
                                        <span class="text-muted">Discount</span><br>
                                    @endif

                                    @if($ds->catatan != '')
                                        <span class="text-muted">{{ $ds->catatan }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ $ds->qty }}</h5>

                                    @if($ds->disc_persen > 0)
                                        <span class="text-muted">{{ $ds->disc_persen.'%' }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ number_format($ds->price/$sales->usercurr1_to_idr, 2, ',', '.') }}</h5>

                                    @if($ds->disc_persen > 0)
                                        <span class="text-muted">{{ number_format($ds->disc_nominal/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>
                                    @endif
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ $ds->price_type == 'wholesale_price' ? 'Wholesale Price' : 'Retail Price' }}</h5>
                                </td>
                                <td>
                                    <h5 class="no-margin">{{ number_format(($ds->price*$ds->qty)/$sales->usercurr1_to_idr, 2, ',', '.') }}</h5>

                                    @if($ds->disc_persen > 0)
                                        <span class="text-muted">{{ number_format(($ds->disc_nominal*$ds->qty)/$sales->usercurr1_to_idr, 2, ',', '.') }}</span><br>
                                    @endif

                                    <?php $subTotal = $subTotal + (($ds->price*$ds->qty) - ($ds->disc_nominal*$ds->qty)); ?>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase">Total</h4>
                </div>
                <div class="table-responsive no-border">
                    <?php $semuaHarga = 0; ?>
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Subtotal</th>
                            <td class="text-right">{{ number_format($subTotal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
                        </tr>
                        <?php $semuaHarga = $semuaHarga + $subTotal; ?>

                        @if($sales->disc_persen > 0)
                            <tr>
                                <th>Global Discount ({{ $sales->disc_persen.'%' }})</th>
                                <td class="text-right">{{ number_format($sales->disc_nominal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
                            </tr>
                            <?php $semuaHarga = $semuaHarga - $sales->disc_nominal; ?>
                        @endif

                        @if($sales->type_sales == 'konsinyasi')
                            <tr>
                                <th>Consignment Fee ({{ $sales->komisi_persen.'%' }})</th>
                                <td class="text-right">{{ number_format($sales->komisi_nominal/$sales->usercurr1_to_idr, 2, ',', '.') }}</td>
                            </tr>
                            <?php $semuaHarga = $semuaHarga - $sales->komisi_nominal; ?>
                        @endif

                        <tr>
                            <th>Grand Total</th>
                            <td class="text-right text-primary"><p class="text-semibold" style="font-size: 15px;">{{ number_format($sales->grand_total/$sales->usercurr1_to_idr, 2, ',', '.') }}</p></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="{{ route('pos.print-order', $sales->no_faktur) }}" class="btn btn-success" target="_blank">Print</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection