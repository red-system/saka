@extends('layouts.master-pembelian')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <style>
        .control-label {
            text-align: left !important;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function addMaterial() {
            $.get("{{ route('pembelian.addform') }}", function(formMaterial){
                $('#tbPembelianMaterial tbody').append(formMaterial);

                $('#tbPembelianMaterial tbody tr:last').find('.select2').select2();
                $('#tbPembelianMaterial tbody tr:last').find('input[name="priceMaterial[]"]').number(true, 2);
            });
        }

        function removeMaterial(element) {
            $(element).parents('tr').remove();
            hitungSubTotal();
        }

        function searchMaterial(elem) {
            $('button.lastBtnSearchClicked').removeClass('lastBtnSearchClicked');
            $(elem).addClass('lastBtnSearchClicked');
            $('#listSearchMaterial').modal("show");
        }

        function selectMaterial(kdMaterial) {
            $.post("{{ route('pembelian.detailMaterial') }}", {kdMaterial: kdMaterial, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                if (result.success) {
                    var inputKdMaterial = $('.lastBtnSearchClicked').parent().siblings('input[name="kdMaterial[]"]');
                    var inputNamaMaterial = inputKdMaterial.parent().parent().siblings('.namaMaterial').find('input[name="namaMaterial[]"]');
                    var inputKatMaterial = inputKdMaterial.parent().parent().siblings('.katMaterial').find('select[name="kategoriMaterial[]"]');
                    var inputTypeMaterial = inputKdMaterial.parent().parent().siblings('.typeMaterial').find('select[name="typeMaterial[]"]');
                    var inputMinStockMaterial = inputKdMaterial.parent().parent().siblings('.minStockMaterial').find('input[name="minStockMaterial[]"]');
                    var inputSatuanMaterial = inputKdMaterial.parent().parent().siblings('.satuanMaterial').find('select[name="satuanMaterial[]"]');

                    var material = result.material;
                    inputKdMaterial.val(material.kd_material);
                    inputNamaMaterial.val(material.name);
                    inputKatMaterial.val(material.kd_kat);
                    inputTypeMaterial.val(material.type_material);
                    inputMinStockMaterial.val(material.minimal_stock);
                    inputSatuanMaterial.val(material.kd_satuan);

                    inputNamaMaterial.prop('readonly', true);
                    inputKatMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputTypeMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSatuanMaterial.find('option:not(:selected)').prop('disabled', true);

                    $('#listSearchMaterial').modal("hide");
                    $('button.lastBtnSearchClicked').removeClass('lastBtnSearchClicked');
                    $('.select2').select2();
                }
            });
        }
        
        function handleKeyupKdMaterial(elem) {
            var kdMaterial = $(elem).val();
            $.post("{{ route('pembelian.detailMaterial') }}", {kdMaterial: kdMaterial, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                var inputNamaMaterial = $(elem).parent().parent().siblings('.namaMaterial').find('input[name="namaMaterial[]"]');
                var inputKatMaterial = $(elem).parent().parent().siblings('.katMaterial').find('select[name="kategoriMaterial[]"]');
                var inputTypeMaterial = $(elem).parent().parent().siblings('.typeMaterial').find('select[name="typeMaterial[]"]');
                var inputMinStockMaterial = $(elem).parent().parent().siblings('.minStockMaterial').find('input[name="minStockMaterial[]"]');
                var inputSatuanMaterial = $(elem).parent().parent().siblings('.satuanMaterial').find('select[name="satuanMaterial[]"]');

                if (result.success) {
                    var material = result.material;
                    inputNamaMaterial.val(material.name);
                    inputKatMaterial.val(material.kd_kat).trigger('change');
                    inputTypeMaterial.val(material.type_material).trigger('change');
                    inputMinStockMaterial.val(material.minimal_stock);
                    inputSatuanMaterial.val(material.kd_satuan).trigger('change');

                    inputNamaMaterial.prop('readonly', true);
                    inputKatMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputTypeMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSatuanMaterial.find('option:not(:selected)').prop('disabled', true);
                } else {
                    inputNamaMaterial.prop('readonly', false);
                    inputKatMaterial.find('option').prop('disabled', false);
                    inputTypeMaterial.find('option').prop('disabled', false);
                    inputSatuanMaterial.find('option').prop('disabled', false);
                }
                $('.select2').select2();
            });
        }
        
        function hitungSubTotal() {
            var subtotal = 0;
            $('.inputPriceMaterial').each(function () {
                var price = $(this).val();
                var qty = $(this).parent().siblings('.qtyMaterial').find('input[name="qtyMaterial[]"]').val();

                var subTotalItem = parseInt(price)*parseInt(qty);
                subtotal = subtotal + subTotalItem;
            });
            $('#subTotal').val(subtotal).trigger('change');
        }
        
        function hitungGrandTotal() {
            var subtotal = $('#subTotal').val();
            var grandtotal = parseInt(subtotal);

            var bTambahan = parseInt($('#biayaTambahan').val());
            if (bTambahan > 0) {
                grandtotal = parseInt(grandtotal + bTambahan);
            }

            var disc = parseInt($('#discount').val());
            if (disc > 0) {
                grandtotal = parseInt(grandtotal - ((grandtotal*disc)/100));
            }

            var ppn = parseInt($('#ppn').val());
            if (ppn > 0) {
                grandtotal = parseInt(grandtotal + ((grandtotal*ppn)/100));
            }

            $('#grandTotal').val(grandtotal).trigger('change');
        }

        $(function () {
            $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('#tbListSearchMaterial').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });

            hitungSubTotal();
            hitungGrandTotal();

            $('#subTotal').on('change', function () {
                hitungGrandTotal();
            });

            $('#biayaTambahan').on('change', function () {
               hitungGrandTotal();
            });

            $('#discount').on('change', function () {
                hitungGrandTotal();
            });

            $('#ppn').on('change', function () {
                hitungGrandTotal();
            });

            $('#tbPembelianMaterial tbody tr').find('.select2').select2();
            $('#tbPembelianMaterial tbody tr').find('input[name="priceMaterial[]"]').number(true, 2);

            $('.currency').number(true, 2);
        })
    </script>
@endsection

@section('title')
    SAKA | Purchasing
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Purchasing</h3>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'pembelian.store', 'class' => 'form-horizontal jq-validate', 'method' => 'post', 'novalidate']) !!}
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group {!! $errors->has('kode_pembelian') ? 'has-error' : '' !!}">
                                {!! Form::label('kode_pembelian', 'Purchasing Number', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('kode_pembelian', $kdpembelian, ['class' => 'form-control','required', 'readonly']) }}
                                    {!! $errors->first('kode_pembelian', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('no_faktur') ? 'has-error' : '' !!}">
                                {!! Form::label('no_faktur', 'Invoice Number', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('no_faktur', null, ['class' => 'form-control','required']) }}
                                    {!! $errors->first('no_faktur', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group {!! $errors->has('supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('supplier', 'Supplier', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::select('supplier', ['' => '']+App\Models\Supplier::pluck('nama_supplier', 'kd_supplier')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                    {!! $errors->first('supplier', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('tanggal') ? 'has-error' : '' !!}">
                                {!! Form::label('tanggal', 'Date', ['class' => 'control-label col-md-3']) !!}
                                <div class="col-md-9">
                                    {{ Form::text('tanggal', null, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                    {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 30px;">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <button type="button" class="btn btn-social btn-google" onclick="addMaterial()">
                                <i class="fa fa-plus"></i> Material
                            </button>
                            <br>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="tbPembelianMaterial">
                                    <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Type Material</th>
                                        <th width="100px">Min Stock</th>
                                        <th>Price</th>
                                        <th width="100px">Qty</th>
                                        <th>Unit</th>
                                        <th class="text-center">#</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="kdMaterial">
                                            <div class="input-group">
                                                {{ Form::text('kdMaterial[]', null, ['class' => 'form-control', 'onkeyup' => 'handleKeyupKdMaterial(this)']) }}
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-flat" onclick="searchMaterial(this)"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td class="namaMaterial">
                                            {{ Form::text('namaMaterial[]', null, ['class' => 'form-control']) }}
                                        </td>
                                        <td class="katMaterial">
                                            {{ Form::select('kategoriMaterial[]', ['' => '']+App\Models\Kategori::where('type_kat', 'material')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2']) }}
                                        </td>
                                        <td class="typeMaterial">
                                            {{ Form::select('typeMaterial[]', ['' => '']+App\Models\Material::typeList(), null, ['class' => 'form-control select2']) }}
                                        </td>
                                        <td class="minStockMaterial">
                                            {{ Form::number('minStockMaterial[]', 0, ['class' => 'form-control', 'min' => 0]) }}
                                        </td>
                                        <td class="priceMaterial">
                                            {{ Form::text('priceMaterial[]', 0, ['class' => 'form-control inputPriceMaterial', 'onchange' => 'hitungSubTotal()']) }}
                                        </td>
                                        <td class="qtyMaterial">
                                            {{ Form::number('qtyMaterial[]', 0, ['class' => 'form-control inputQtyMaterial', 'min' => 0, 'onchange' => 'hitungSubTotal()']) }}
                                        </td>
                                        <td class="satuanMaterial">
                                            {{ Form::select('satuanMaterial[]', ['' => '']+App\Models\Satuan::pluck('satuan', 'kd_satuan')->all(), null, ['class' => 'form-control select2']) }}
                                        </td>
                                        <td>
                                            <span data-toggle="tooltip" data-placement="top" title="Delete Material">
                                                <button type="button" class="btn btn-danger" onclick="removeMaterial(this)">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group {!! $errors->has('sub_total') ? 'has-error' : '' !!}">
                                {!! Form::label('sub_total', 'Sub Total', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::text('sub_total', null, ['class' => 'form-control currency','readonly', 'id' => 'subTotal']) }}
                                    {!! $errors->first('sub_total', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('biaya_tambahan') ? 'has-error' : '' !!}">
                                {!! Form::label('biaya_tambahan', 'additional cost', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::text('biaya_tambahan', null, ['class' => 'form-control currency', 'id' => 'biayaTambahan']) }}
                                    {!! $errors->first('biaya_tambahan', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('disc') ? 'has-error' : '' !!}">
                                {!! Form::label('disc', 'Disc', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::number('disc', null, ['class' => 'form-control','min' => 0, 'id' => 'discount']) }}
                                    {!! $errors->first('disc', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group {!! $errors->has('ppn') ? 'has-error' : '' !!}">
                                {!! Form::label('ppn', 'Tax (%)', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::select('ppn', ['0' => '0', '10' => '10'], 0, ['class' => 'form-control', 'id' => 'ppn']) }}
                                    {!! $errors->first('ppn', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group {!! $errors->has('grand_total') ? 'has-error' : '' !!}">
                                {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label col-md-4']) !!}
                                <div class="col-md-8">
                                    {{ Form::text('grand_total', null, ['class' => 'form-control currency', 'readonly', 'id' => 'grandTotal']) }}
                                    {!! $errors->first('grand_total', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-block btn-success">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="listSearchMaterial">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Search Material</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tbListSearchMaterial" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Type Material</th>
                                    <th class="text-center">Act</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datamaterial as $dmt)
                                    <tr>
                                        <td>{{ $dmt->kd_material }}</td>
                                        <td>{{ $dmt->name }}</td>
                                        <td>{{ $dmt->kategori->kategori }}</td>
                                        <td>{{ $dmt->human_type }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="selectMaterial('{{ $dmt->kd_material }}')">Pilih</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('footer')
    <footer class="main-footer">
        <strong>&copy; 2018 - Saka Karya Bali</strong> powered by <a href="https://ganeshcomstudio.com">Ganeshcom Studio</a>
    </footer>
@endsection