<tr>
    <td class="kdMaterial">
        <div class="input-group">
            {{ Form::text('kdMaterial[]', null, ['class' => 'form-control', 'onkeyup' => 'handleKeyupKdMaterial(this)']) }}
            <span class="input-group-btn">
                <button type="button" class="btn btn-flat" onclick="searchMaterial(this)"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </td>
    <td class="namaMaterial">
        {{ Form::text('namaMaterial[]', null, ['class' => 'form-control']) }}
    </td>
    <td class="katMaterial">
        {{ Form::select('kategoriMaterial[]', ['' => '']+App\Models\Kategori::where('type_kat', 'material')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2']) }}
    </td>
    <td class="typeMaterial">
        {{ Form::select('typeMaterial[]', ['' => '']+App\Models\Material::typeList(), null, ['class' => 'form-control select2']) }}
    </td>
    <td class="minStockMaterial">
        {{ Form::number('minStockMaterial[]', 0, ['class' => 'form-control', 'min' => 0]) }}
    </td>
    <td class="priceMaterial">
        {{ Form::text('priceMaterial[]', 0, ['class' => 'form-control inputPriceMaterial', 'onchange' => 'hitungSubTotal()']) }}
    </td>
    <td class="qtyMaterial">
        {{ Form::number('qtyMaterial[]', 0, ['class' => 'form-control inputQtyMaterial', 'min' => 0, 'onchange' => 'hitungSubTotal()']) }}
    </td>
    <td class="satuanMaterial">
        {{ Form::select('satuanMaterial[]', ['' => '']+App\Models\Satuan::pluck('satuan', 'kd_satuan')->all(), null, ['class' => 'form-control select2']) }}
    </td>
    <td>
        <span data-toggle="tooltip" data-placement="top" title="Delete Material">
            <button type="button" class="btn btn-danger" onclick="removeMaterial(this)">
                <i class="fa fa-trash"></i>
            </button>
        </span>
    </td>
</tr>