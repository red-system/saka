@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();

        $('#table_asset, #table_liabilitas, #table_ekuitas, #table_aktiva').dataTable( {
            "aLengthMenu": [[-1], ["All"]],
            "pageLength": -1,
            responsive: true,
        } );
        
        function printPembelian() {
            var tgl_awal = $('#tglawal').val();
            var tgl_akhir = $('#tglakhir').val();
            // var master_id = $('#master_id').val();

            var urlPrint = '{{ route('neraca.index') }}'+'/'+tgl_awal+'/'+tgl_akhir;
            if(tgl_awal !='' && tgl_akhir !=''){
                window.open(urlPrint);
            }else{
                swal({
                    title: 'Perhatian',
                    text: 'Pilih Salah Satu Kode COA',
                    type: 'warning'
                });
            }
        }

        $('[name="master_id"]').change(function () {
            var master_id = $(this).val();
            $('#tglawal').val(master_id);
        });

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('neraca.getdata') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Laporan Neraca - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Neraca
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Neraca</li>
    </ol>
@endsection

@section('content')
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
    .tg td{font-family:Tahoma;font-size:12px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
    .tg th{font-family:Tahoma;font-size:14px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;text-align: center;}
</style>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- <a href="{{ route('jurnalHarian.create') }}" class="btn btn-block btn-social btn-google">
                <i class="fa fa-plus"></i> Tambah Jurnal
            </a> -->
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Neraca</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Tanggal') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', $start_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', $end_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-3">
                            <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataPembelian">
                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr class="success">
                                            <th width="25%"><center>ASET</center></th>
                                            <th width="25%"><center>LIABILITAS</center></th>
                                            <th width="25%"><center>EKUITAS</center></th>
                                            <th width="25%"><center>STATUS</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{number_format($total_asset,2)}}</td>
                                            <td>{{number_format($total_liabilitas,2)}}</td>
                                            <td>{{number_format($total_ekuitas,2)}}</td>
                                            
                                            @if($status=='BALANCE')
                                            <td><font color="green"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                            @else
                                            <td><font color="red"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                                    $total_assets = 0;
                                    $total_liabilitass = 0;
                                    $total_ekuitass = 0;
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                        <table class="tg" width="100%">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Activa</th>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody><?php $activa = 0;?>
                                                @foreach($data_asset as $detail)
                                                    @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP')
                                                    <tr>
                                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                            <?php
                                                                
                                                                $total_assets = $total_assets+$asset[$detail->mst_kode_rekening];
                                                            ?>
                                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                                    </tr><?php $activa++;?>
                                                    @foreach($detail->childs as $det_child)
                                                    <tr>
                                                        <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                        <?php
                                                            $total_assets = $total_assets+$asset[$det_child->mst_kode_rekening];
                                                        ?>
                                                        <td align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                                    </tr><?php $activa++;?>
                                                    @endforeach
                                                    @endif
                                                @endforeach
                                                <?php
                                                    $total_aktiva = 0;
                                                ?>
                                                @foreach($data_asset as $detail)
                                                    @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP')
                                                    <tr>
                                                        <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                                    </tr><?php $activa++;?>
                                                    @foreach($detail->childs as $det_child)
                                                    <tr>
                                                        <td style="font-weight: bold;">{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                        <td style="font-weight: bold;" align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                                    </tr><?php $activa++;?>
                                                    @foreach($det_child->childs as $child)
                                                    <tr>
                                                        <td>{!!$space3!!}{{$child->mst_kode_rekening}} {{$child->mst_nama_rekening}}</td>
                                                        <td align="right">{{number_format($asset[$child->mst_kode_rekening],2)}}</td>
                                                    </tr><?php $activa++;?>
                                                    @endforeach
                                                    @endforeach
                                                    @endif
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Total Aktiva</th>
                                                    <th>{{number_format($total_asset,2)}}</th>
                                                </tr>
                                            </tfoot>                                    
                                        </table>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <table class="tg" width="100%">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Pasiva</th>
                                            </tr>
                                            <tr>
                                                <th>Description</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody><?php $passiva = 0;?>
                                        @foreach($data_liabilitas as $detail)
                                            @if($detail->mst_neraca_tipe == 'liabilitas' && $detail->mst_master_id == '0')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                            
                                                <td style="font-weight: bold;" align="right">{{number_format($liabilitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                <td align="right">{{number_format($liabilitas[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        @foreach($data_ekuitas as $detail)
                                            @if($detail->mst_neraca_tipe == 'ekuitas' && $detail->mst_master_id == '0')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                <td style="font-weight: bold;" align="right">{{number_format($ekuitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                <td align="right">{{number_format($ekuitas[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr><?php $passiva++;?>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        <?php $selisih = $activa-$passiva;?>
                                        @for($i=1;$i<=$selisih;$i++)
                                        <tr>
                                            <td></td>
                                            <td align="right">0</td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Pasiva</th>
                                            <th>{{number_format($hitung,2)}}</th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- /.box-body -->
    </div>
            <!-- /.box -->

    <div id="detailPembelian" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                        <span> &nbsp;&nbsp;Loading... </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection