<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}"> --}}
    <title></title>
</head>
<body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:10px;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial;font-size:12px;font-weight:normal;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <table width="100%">
        <tbody>
            <tr>
                <td style="font-family: Tahoma;font-size: 14px;font-weight: bold;"><center>Laporan Rugi Laba</center></td>
            </tr>
            <tr>
                <td style="font-family: Tahoma;font-size: 14px;font-weight: bold;"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="tg" width="100%">
        <thead>
            <tr class="success" >
                <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
                <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
                <th width="20%"><center> Nominal </center></th>
            </tr>
            <tr class="success">
                <th colspan="3"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></th>
            </tr>
        </thead>
                                            
        <tbody>    
            @foreach($data_laba_kotor as $laba_kotor_1)
            <tr >
                <td align="center"><strong>{{$laba_kotor_1->mst_kode_rekening}}</strong></td>
                <td><strong>{!!$space1!!}{{$laba_kotor_1->mst_nama_rekening}}</strong></td>
                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_1->mst_kode_rekening])}} </td>
            </tr>
            @foreach($laba_kotor_1->childs as $laba_kotor_2)
            <tr >
                <td align="center">{{$laba_kotor_2->mst_kode_rekening}}</td>
                <td>{!!$space2!!}{{$laba_kotor_2->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_2->mst_kode_rekening])}}</td>
            </tr>
            @foreach($laba_kotor_2->childs as $laba_kotor_3)
            <tr >
                <td align="center">{{$laba_kotor_3->mst_kode_rekening}}</td>
                <td>{!!$space3!!}{{$laba_kotor_3->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_3->mst_kode_rekening])}}</td>
            </tr>
            @foreach($laba_kotor_3->childs as $laba_kotor_4)
            <tr >
                <td align="center">{{$laba_kotor_4->mst_kode_rekening}}</td>
                <td>{!!$space4!!}{{$laba_kotor_4->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_4->mst_kode_rekening])}}</td>
            </tr>
            @endforeach
            @endforeach
            @endforeach
            @endforeach
            <tr class="">
                <td colspan="2" align="center"><h4><strong> LABA KOTOR </strong></h4></td>
                <td align="right"><h4><strong> {{number_format($total_laba_kotor)}} </strong></h4></td>
            </tr>
            <tr class="">
                <td colspan="3" align="center"><h5></h5></td>
            </tr>

            @foreach($data_laba_bersih as $laba_bersih_1)
            <tr >
                <td align="center"><strong>{{$laba_bersih_1->mst_kode_rekening}}</strong></td>
                <td><strong>{!!$space1!!}{{$laba_bersih_1->mst_nama_rekening}}</strong></td>                                            
                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_1->mst_kode_rekening])}} </td>
            </tr>
            @foreach($laba_bersih_1->childs as $laba_bersih_2)
            <tr >
                <td align="center">{{$laba_bersih_2->mst_kode_rekening}}</td>
                <td>{!!$space2!!}{{$laba_bersih_2->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_2->mst_kode_rekening])}}</td>                                                                                           
            </tr>
            @foreach($laba_bersih_2->childs as $laba_bersih_3)
            <tr >
                <td align="center">{{$laba_bersih_3->mst_kode_rekening}}</td>
                <td>{!!$space3!!}{{$laba_bersih_3->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_3->mst_kode_rekening])}}</td>                                                                                           
            </tr>
            @foreach($laba_bersih_3->childs as $laba_bersih_4)
            <tr >
                <td align="center">{{$laba_bersih_4->mst_kode_rekening}}</td>
                <td>{!!$space4!!}{{$laba_bersih_4->mst_nama_rekening}}</td>
                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_4->mst_kode_rekening])}}</td>
            </tr>
            @endforeach
            @endforeach
            @endforeach
            @endforeach
            <tr class="">
                <td colspan="2" align="center"><h4><strong> LABA BERSIH </strong></h4></td>
                <td align="right"><h4><strong> {{number_format($total_laba_bersih+$total_laba_kotor)}} </strong></h4></td>
            </tr>
            <tr class="">
                <td colspan="3" align="center"><h5></h5></td>
            </tr>
        </tbody>
    </table>
<script>
		window.print();
	</script>
</body>
</html>
