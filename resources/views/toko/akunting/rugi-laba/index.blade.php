@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        function printPembelian() {
            var tgl_awal = $('#tglawal').val();
            var tgl_akhir = $('#tglakhir').val();
            // var master_id = $('#master_id').val();

            var urlPrint = '{{ route('rugiLaba.index') }}'+'/'+tgl_awal+'/'+tgl_akhir;
            if(tgl_awal !='' && tgl_akhir !=''){
                window.open(urlPrint);
            }else{
                swal({
                    title: 'Perhatian',
                    text: 'Pilih Salah Satu Kode COA',
                    type: 'warning'
                });
            }
        }

        $('[name="master_id"]').change(function () {
            var master_id = $(this).val();
            $('#tglawal').val(master_id);
        });

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('rugiLaba.getdata') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Rugi Laba - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Rugi Laba
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Rugi Laba</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- <a href="{{ route('jurnalHarian.create') }}" class="btn btn-block btn-social btn-google">
                <i class="fa fa-plus"></i> Tambah Jurnal
            </a> -->
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">Rugi Laba</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Tanggal') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', $start_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', $end_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-3">
                            <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div id="dataPembelian">
                                <hr>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success" >
                                                <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
                                                <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                            <tr class="success">
                                                <th colspan="3"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        <?php
                                            $total_pendapatan=0;
                                        ?>
                                        @foreach($data_laba_kotor as $laba_kotor_1)
                                            <tr >
                                                <td align="center"><strong>{{$laba_kotor_1->mst_kode_rekening}}</strong></td>
                                                <td><strong>{!!$space1!!}{{$laba_kotor_1->mst_nama_rekening}}</strong></td>                                            
                                                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_1->mst_kode_rekening])}} </td>
                                            </tr>
                                            @foreach($laba_kotor_1->childs as $laba_kotor_2)
                                            <tr >
                                                <td align="center">{{$laba_kotor_2->mst_kode_rekening}}</td>
                                                <td>{!!$space2!!}{{$laba_kotor_2->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_2->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($laba_kotor_2->childs as $laba_kotor_3)
                                            <tr >
                                                <td align="center">{{$laba_kotor_3->mst_kode_rekening}}</td>
                                                <td>{!!$space3!!}{{$laba_kotor_3->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_3->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($laba_kotor_3->childs as $laba_kotor_4)
                                            <tr >
                                                <td align="center">{{$laba_kotor_4->mst_kode_rekening}}</td>
                                                <td>{!!$space4!!}{{$laba_kotor_4->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_4->mst_kode_rekening])}}</td>
                                                                                                                                             
                                            </tr>
                                            @endforeach
                                            @endforeach
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> LABA KOTOR </strong></h4></td>
                                                <td align="right"><h4><strong> {{number_format($total_laba_kotor)}} </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>

                                        @foreach($data_laba_bersih as $laba_bersih_1)
                                            <tr >
                                                <td align="center"><strong>{{$laba_bersih_1->mst_kode_rekening}}</strong></td>
                                                <td><strong>{!!$space1!!}{{$laba_bersih_1->mst_nama_rekening}}</strong></td>                                            
                                                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_1->mst_kode_rekening])}} </td>
                                            </tr>
                                            @foreach($laba_bersih_1->childs as $laba_bersih_2)
                                            <tr >
                                                <td align="center">{{$laba_bersih_2->mst_kode_rekening}}</td>
                                                <td>{!!$space2!!}{{$laba_bersih_2->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_2->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($laba_bersih_2->childs as $laba_bersih_3)
                                            <tr >
                                                <td align="center">{{$laba_bersih_3->mst_kode_rekening}}</td>
                                                <td>{!!$space3!!}{{$laba_bersih_3->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_3->mst_kode_rekening])}}</td>                                                                                           
                                            </tr>
                                            @foreach($laba_bersih_3->childs as $laba_bersih_4)
                                            <tr >
                                                <td align="center">{{$laba_bersih_4->mst_kode_rekening}}</td>
                                                <td>{!!$space4!!}{{$laba_bersih_4->mst_nama_rekening}}</td>
                                                <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_4->mst_kode_rekening])}}</td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                            @endforeach
                                        @endforeach
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> LABA BERSIH </strong></h4></td>
                                                <td align="right"><h4><strong> {{number_format($total_laba_bersih+$total_laba_kotor)}} </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <div id="detailPembelian" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                <span> &nbsp;&nbsp;Loading... </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection