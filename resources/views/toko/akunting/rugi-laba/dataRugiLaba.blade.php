<table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
    <thead>
        <tr class="success" >
            <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
            <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
            <th width="20%"><center> Nominal </center></th>
        </tr>
        <tr class="success">
            <th colspan="3"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></th>
        </tr>
    </thead>
                                        
    <tbody>    
        @foreach($data_laba_kotor as $laba_kotor_1)
        <tr >
            <td align="center"><strong>{{$laba_kotor_1->mst_kode_rekening}}</strong></td>
            <td><strong>{!!$space1!!}{{$laba_kotor_1->mst_nama_rekening}}</strong></td>
            <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_1->mst_kode_rekening])}} </td>
        </tr>
        @foreach($laba_kotor_1->childs as $laba_kotor_2)
        <tr >
            <td align="center">{{$laba_kotor_2->mst_kode_rekening}}</td>
            <td>{!!$space2!!}{{$laba_kotor_2->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_2->mst_kode_rekening])}}</td>
        </tr>
        @foreach($laba_kotor_2->childs as $laba_kotor_3)
        <tr >
            <td align="center">{{$laba_kotor_3->mst_kode_rekening}}</td>
            <td>{!!$space3!!}{{$laba_kotor_3->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_3->mst_kode_rekening])}}</td>
        </tr>
        @foreach($laba_kotor_3->childs as $laba_kotor_4)
        <tr >
            <td align="center">{{$laba_kotor_4->mst_kode_rekening}}</td>
            <td>{!!$space4!!}{{$laba_kotor_4->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_kotor[$laba_kotor_4->mst_kode_rekening])}}</td>
        </tr>
        @endforeach
        @endforeach
        @endforeach
        @endforeach
        <tr class="">
            <td colspan="2" align="center"><h4><strong> LABA KOTOR </strong></h4></td>
            <td align="right"><h4><strong> {{number_format($total_laba_kotor)}} </strong></h4></td>
        </tr>
        <tr class="">
            <td colspan="3" align="center"><h5></h5></td>
        </tr>

        @foreach($data_laba_bersih as $laba_bersih_1)
        <tr >
            <td align="center"><strong>{{$laba_bersih_1->mst_kode_rekening}}</strong></td>
            <td><strong>{!!$space1!!}{{$laba_bersih_1->mst_nama_rekening}}</strong></td>                                            
            <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_1->mst_kode_rekening])}} </td>
        </tr>
        @foreach($laba_bersih_1->childs as $laba_bersih_2)
        <tr >
            <td align="center">{{$laba_bersih_2->mst_kode_rekening}}</td>
            <td>{!!$space2!!}{{$laba_bersih_2->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_2->mst_kode_rekening])}}</td>                                                                                           
        </tr>
        @foreach($laba_bersih_2->childs as $laba_bersih_3)
        <tr >
            <td align="center">{{$laba_bersih_3->mst_kode_rekening}}</td>
            <td>{!!$space3!!}{{$laba_bersih_3->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_3->mst_kode_rekening])}}</td>                                                                                           
        </tr>
        @foreach($laba_bersih_3->childs as $laba_bersih_4)
        <tr >
            <td align="center">{{$laba_bersih_4->mst_kode_rekening}}</td>
            <td>{!!$space4!!}{{$laba_bersih_4->mst_nama_rekening}}</td>
            <td colspan="3" align="right">{{number_format($laba_bersih[$laba_bersih_4->mst_kode_rekening])}}</td>
        </tr>
        @endforeach
        @endforeach
        @endforeach
        @endforeach
        <tr class="">
            <td colspan="2" align="center"><h4><strong> LABA BERSIH </strong></h4></td>
            <td align="right"><h4><strong> {{number_format($total_laba_bersih+$total_laba_kotor)}} </strong></h4></td>
        </tr>
        <tr class="">
            <td colspan="3" align="center"><h5></h5></td>
        </tr>
    </tbody>
</table>
<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>