@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        function printPembelian() {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            // var master_id = $('#master_id').val();

            var urlPrint = '{{ route('arusKas.index') }}'+'/'+start_date+'/'+end_date;
            if(start_date !='' && end_date !=''){
                window.open(urlPrint);
            }else{
                swal({
                    title: 'Perhatian',
                    text: 'Pilih Salah Satu Kode COA',
                    type: 'warning'
                });
            }
        }


        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('arus-kas.getdata') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    {{$title}} - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        {{$title}}
        <small>{{$menu}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>{{$menu}}</li>
        <li class="active">{{$title}}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- <a href="{{ route('jurnalHarian.create') }}" class="btn btn-block btn-social btn-google">
                <i class="fa fa-plus"></i> Tambah Jurnal
            </a> -->
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h4 class="box-title">{{$title}}</h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Tanggal') !!}
                                <div class="input-group">
                                    {{ Form::text('start_date', $start_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'start_date']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('end_date', $end_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'end_date']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-3">
                            <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div id="dataPembelian">
                                <hr>
                                    <table class="table table-striped table-bordered table-hover tg" width="100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>I. ARUS KAS DARI KEGIATAN OPERASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            <?php
                                                $total_operasi=0;
                                                $total_pendanaan=0;
                                                $total_investasi=0;
                                            ?>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach                                
                                            
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                                <?php
                                                    $total_operasi=$total_operasi+$trs->trs_debet-$trs->trs_kredit;
                                                ?>
                                            @endforeach 
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN OPERASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_operasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>II. ARUS KAS DARI KEGIATAN PENDANAAN</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            <?php
                                                $total_pendanaan=$total_pendanaan+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN PENDANAAN</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_pendanaan)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2"><strong>III. ARUS KAS DARI KEGIATAN INVESTASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','Investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            
                                            <?php
                                                $total_investasi=$total_investasi+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN INVESTASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_investasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>TOTAL<strong></h4></td> 
                                                <td><h4><strong>{{number_format($total_investasi+$total_operasi+$total_pendanaan)}}<strong></h4></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>Saldo Awal<strong></h4></td> 
                                                <td><h4><strong>{{number_format($kas_awal)}}<strong></h4></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>Saldo Akhir<strong></h4></td> 
                                                <td><h4><strong>{{number_format($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}<strong></h4></td>                                  
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <div id="detailPembelian" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                <span> &nbsp;&nbsp;Loading... </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection