<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}"> --}}
    <title></title>
</head>
<body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:10px;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial;font-size:12px;font-weight:normal;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <table width="100%">
        <tbody>
            <tr>
                <td style="font-family: Tahoma;font-size: 14px;font-weight: bold;"><center>Laporan {{$title}}</center></td>
            </tr>
            <tr>
                <td style="font-family: Tahoma;font-size: 14px;font-weight: bold;"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></td>
            </tr>
        </tbody>
    </table>
    <br>
    <table class="table table-striped table-bordered table-hover tg" width="100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>I. ARUS KAS DARI KEGIATAN OPERASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            <?php
                                                $total_operasi=0;
                                                $total_pendanaan=0;
                                                $total_investasi=0;
                                            ?>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach                                
                                            
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                                <?php
                                                    $total_operasi=$total_operasi+$trs->trs_debet-$trs->trs_kredit;
                                                ?>
                                            @endforeach 
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN OPERASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_operasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>II. ARUS KAS DARI KEGIATAN PENDANAAN</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            <?php
                                                $total_pendanaan=$total_pendanaan+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN PENDANAAN</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_pendanaan)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2"><strong>III. ARUS KAS DARI KEGIATAN INVESTASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            
                                            <?php
                                                $total_investasi=$total_investasi+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN INVESTASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_investasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h5><strong>TOTAL<strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_investasi+$total_operasi+$total_pendanaan)}}<strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h5><strong>Saldo Awal<strong></h5></td> 
                                                <td><h5><strong>{{number_format($kas_awal)}}<strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h5><strong>Saldo Akhir<strong></h5></td> 
                                                <td><h5><strong>{{number_format($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}<strong></h4></td>                                  
                                            </tr>
                                        </tbody>
                                    </table>
<script>
		window.print();
	</script>
</body>
</html>
