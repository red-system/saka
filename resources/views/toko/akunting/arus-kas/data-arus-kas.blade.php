<hr>
<table class="table table-striped table-bordered table-hover tg" width="100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2"><strong>I. ARUS KAS DARI KEGIATAN OPERASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            <?php
                                                $total_operasi=0;
                                                $total_pendanaan=0;
                                                $total_investasi=0;
                                            ?>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach                                
                                            
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','operasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                                <?php
                                                    $total_operasi=$total_operasi+$trs->trs_debet-$trs->trs_kredit;
                                                ?>
                                            @endforeach 
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN OPERASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_operasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>II. ARUS KAS DARI KEGIATAN PENDANAAN</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','pendanaan') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            <?php
                                                $total_pendanaan=$total_pendanaan+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN PENDANAAN</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_pendanaan)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            
                                            <tr>
                                                <td colspan="2"><strong>III. ARUS KAS DARI KEGIATAN INVESTASI</strong></td>                                   
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Masuk :</strong></td>                                   
                                            </tr>
                                            
                                            @foreach($transaksi->where('trs_tipe_arus_kas','investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='debet')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_debet)}}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td colspan="2"><strong>&nbsp;&nbsp;&nbsp;- Arus Kas Keluar :</strong></td>                                   
                                            </tr>
                                            @foreach($transaksi->where('trs_tipe_arus_kas','investasi') as $trs)
                                                @if($trs->trs_jenis_transaksi=='kredit')
                                                <tr>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$trs->jurnalUmum->jmu_keterangan}}</td>  
                                                    <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($trs->trs_kredit)}}</td>
                                                </tr>
                                                @endif
                                            
                                            <?php
                                                $total_investasi=$total_investasi+$trs->trs_debet-$trs->trs_kredit;
                                            ?>
                                            @endforeach
                                            <tr>
                                                <td><h5><strong>TOTAL KEGIATAN INVESTASI</strong></h5></td> 
                                                <td><h5><strong>{{number_format($total_investasi)}}</strong></h5></td>                                  
                                            </tr>
                                            <tr>
                                                <td></td> 
                                                <td></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>TOTAL<strong></h4></td> 
                                                <td><h4><strong>{{number_format($total_investasi+$total_operasi+$total_pendanaan)}}<strong></h4></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>Saldo Awal<strong></h4></td> 
                                                <td><h4><strong>{{number_format($kas_awal)}}<strong></h4></td>                                  
                                            </tr>
                                            <tr>
                                                <td><h4><strong>Saldo Akhir<strong></h4></td> 
                                                <td><h4><strong>{{number_format($kas_awal+($total_investasi+$total_operasi+$total_pendanaan))}}<strong></h4></td>                                  
                                            </tr>
                                        </tbody>
                                    </table>