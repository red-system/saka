<h3 class="box-title">Buku Besar {{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</h3>
@foreach($perkiraan as $pkr)
                                    <hr>
                                    <br>
                                    <table width="100%" id="{{$pkr->mst_nama_rekening}}">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 12px;" width="50%"><strong>Perkiraan : {{$pkr->mst_nama_rekening}}</strong></td>
                                            <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : {{$pkr->mst_kode_rekening}}</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size: 12px;"><center> Tanggal </center></th>
                                                <th style="font-size: 12px;"><center> No Bukti </center></th>
                                                <th style="font-size: 12px;"><center> Keterangan </center></th>
                                                <th style="font-size: 12px;"><center> Debet </center></th>
                                                <th style="font-size: 12px;"><center> Kredit </center></th>
                                                @if($pkr->mst_normal == 'kredit')
                                                <th style="font-size: 12px;"><center> Saldo(Kredit)</center></th>
                                                @endif
                                                @if($pkr->mst_normal == 'debet')
                                                <th style="font-size: 12px;"><center> Saldo(Debet)</center></th>
                                                @endif
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <?php $kode = $pkr->mst_kode_rekening;?>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($tgl_saldo_awal)) }}</td>
                                                <td style="font-size: 11px;"></td>
                                                <td style="font-size: 11px;">Saldo awal</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo_awal[$pkr->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php
                                                $saldo = $saldo_awal[$pkr->mst_kode_rekening];
                                            ?>
                                            @foreach($pkr->transaksi->where('tgl_transaksi','>=',$start_date)->where('tgl_transaksi','<=',$end_date) as $transaksi)
                                            <?php
                                                $total=0;
                                                if($pkr->mst_normal == 'kredit'){
                                                    // $saldo_awal=$pkr->msd_awal_kredit;
                                                    $saldo=$saldo+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                }
                                                if($pkr->mst_normal == 'debet'){
                                                    // $saldo_awal=$pkr->msd_awal_debet;
                                                    $saldo=$saldo-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                }
                                                $total=$total+$saldo;                                                
                                            ?>
                                            <tr>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal)) }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->jurnalUmum->no_jurnal }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->trs_catatan }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_debet,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_kredit,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo,2)}}</td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td style="font-size: 12px;" colspan="5" align="right"><strong> TOTAL </strong></td>
                                                <td style="font-size: 12px;" align="right"><strong>{{number_format($saldo,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @endforeach
<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>