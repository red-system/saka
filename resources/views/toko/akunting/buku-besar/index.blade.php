@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        function printPembelian() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var master_id = $('#master_id').val();

            var urlPrint = '{{ route('bukuBesar.index') }}'+'/'+tglawal+'/'+tglakhir+'/'+master_id;
            if(tglawal != '' && tglakhir !=''){
                window.open(urlPrint);
            }
            // else{
            //     swal({
            //         title: 'Perhatian',
            //         text: 'Pilih Salah Satu Kode COA',
            //         type: 'warning'
            //     });
            // }
        }

        // $('[name="master_id"]').change(function () {
        //     var master_id = $(this).val();
        //     $('#tglawal').val(master_id);
        // });

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('bukuBesar.getdata') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Buku Besar - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Buku Besar
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Buku Besar</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- <a href="{{ route('jurnalHarian.create') }}" class="btn btn-block btn-social btn-google">
                <i class="fa fa-plus"></i> Tambah Jurnal
            </a> -->
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Buku Besar</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">
                        
                        <div class="col-md-8">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('tgl', 'Tanggal') !!}
                                    <div class="input-group">
                                        {{ Form::text('tgl_awal', $start_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default">S/D</button>
                                        </div>
                                        {{ Form::text('tgl_akhir', $end_date, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('master', 'COA') !!}
                                <div class="input-group col-md-12">
                                    {{ Form::select('master_id', ['0'=>'All']+App\Models\MasterCoa::pluck('mst_nama_rekening', 'master_id')->all(), $master_id, ['class' => 'form-control select2','required','id'=>'master_id']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-2">
                                <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div id="dataPembelian">
                                    <h3 class="box-title">Buku Besar {{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</h3>
                                    @foreach($perkiraan as $pkr)
                                    <hr>
                                    <br>
                                    <table width="100%" id="{{$pkr->mst_nama_rekening}}">
                                        <tbody>
                                        <tr>
                                            <td style="font-size: 12px;" width="50%"><strong>Perkiraan : {{$pkr->mst_nama_rekening}}</strong></td>
                                            <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : {{$pkr->mst_kode_rekening}}</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-striped table-bordered table-hover table-header-fixed">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size: 12px;"><center> Tanggal </center></th>
                                                <th style="font-size: 12px;"><center> No Bukti </center></th>
                                                <th style="font-size: 12px;"><center> Keterangan </center></th>
                                                <th style="font-size: 12px;"><center> Debet </center></th>
                                                <th style="font-size: 12px;"><center> Kredit </center></th>
                                                @if($pkr->mst_normal == 'kredit')
                                                <th style="font-size: 12px;"><center> Saldo(Kredit)</center></th>
                                                @endif
                                                @if($pkr->mst_normal == 'debet')
                                                <th style="font-size: 12px;"><center> Saldo(Debet)</center></th>
                                                @endif
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <?php $kode = $pkr->mst_kode_rekening;?>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($tgl_saldo_awal)) }}</td>
                                                <td style="font-size: 11px;"></td>
                                                <td style="font-size: 11px;">Saldo awal</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format(0,2)}}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo_awal[$pkr->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php
                                                $saldo = $saldo_awal[$pkr->mst_kode_rekening];
                                            ?>
                                            @foreach($pkr->transaksi->where('tgl_transaksi','>=',$start_date)->where('tgl_transaksi','<=',$end_date) as $transaksi)
                                            <?php
                                                $total=0;
                                                if($pkr->mst_normal == 'kredit'){
                                                    // $saldo_awal=$pkr->msd_awal_kredit;
                                                    $saldo=$saldo+$transaksi->trs_kredit-$transaksi->trs_debet;
                                                }
                                                if($pkr->mst_normal == 'debet'){
                                                    // $saldo_awal=$pkr->msd_awal_debet;
                                                    $saldo=$saldo-$transaksi->trs_kredit+$transaksi->trs_debet;
                                                }
                                                $total=$total+$saldo;                                                
                                            ?>
                                            <tr>
                                                <td style="font-size: 11px;">{{ date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal)) }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->jurnalUmum->no_jurnal }}</td>
                                                <td style="font-size: 11px;">{{ $transaksi->jurnalUmum->jmu_keterangan }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_debet,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{ number_format($transaksi->trs_kredit,2) }}</td>
                                                <td style="font-size: 11px;" align="right">{{number_format($saldo,2)}}</td>
                                            </tr>
                                            @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr class="">
                                                <td style="font-size: 12px;" colspan="5" align="right"><strong> TOTAL </strong></td>
                                                <td style="font-size: 12px;" align="right"><strong>{{number_format($saldo,2)}}</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <div id="detailPembelian" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                <span> &nbsp;&nbsp;Loading... </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection