@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        function printPembelian() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();

            var urlPrint = '{{ route('jurnalHarian.index') }}'+'/'+tglawal+'/'+tglakhir;
            if (tglawal != '' && tglakhir != '') {
                window.open(urlPrint);
            }
        }

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd',
                orientation: "bottom auto"
            })
            
            $('.select2').select2();

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('jurnalHarian.getdata') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Jurnal Harian - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Jurnal Harian
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Jurnal Harian</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('jurnalHarian.create') }}" class="btn btn-block btn-social btn-google">
                <i class="fa fa-plus"></i> Tambah Jurnal
            </a>
            <br>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <!-- <a href="{{ route('jurnalHarian.posting') }}" class="btn btn-block btn-social btn-facebook">
                <i class="fa fa-download"></i> Posting Jurnal {{$month[$periode_month+1]}} Ke Buku Besar
            </a> -->
            
            <br>
        </div>
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
            <h4>Bulan Aktif : {{$month[$periode_month]}}</h4>
            <br>
        </div> -->
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Jurnal Harian</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Tanggal') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-3">
                            <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataPembelian"></div>
                        </div>
                    </div>
                </div>
                    <!-- /.box-body -->
            </div>
                <!-- /.box -->
            <div id="detailPembelian" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                            <span> &nbsp;&nbsp;Loading... </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection