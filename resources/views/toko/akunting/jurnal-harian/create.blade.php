@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>

        function btn_selectpickerx(el) {
            el.children('td:nth-child(1)').children('').select2();
            // el.parents('td').siblings('td:nth-child(1)').children('select').select2();
        }

        function hitungGrandTotal() {
            var total = parseInt($('#total').val());
            var ppnpersen = parseInt($('#ppn_persen').val());

            var grandtotal = total + ((total * ppnpersen) / 100);
            if (isNaN(grandtotal)) {
                grandtotal = 0;
            }
            $('#grandtotal').val(grandtotal);
        }

        $('.btn-add-trs').click(function() {
            var row = $('#rowAddItem tbody').html();
            var el          = $(row);
            $('#tbItemProduksi tbody').append(row);
            btn_selectpickerx(el);
        });

        $(function () {
            // $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('#total').on('change', function () {
                hitungGrandTotal()
            });

            $('#ppn_persen').on('change', function () {
                hitungGrandTotal()
            });
        })

        function addItem() {
            var row = $('#rowAddItem tbody').html();
            var el          = $(row);
            $('#tbItemProduksi tbody').append(row);
            btn_selectpickerx(el);
        }

        function removeItem(element) {
            $(element).parents('tr').remove();
        }

        function addMaterial() {
            var row = $('#rowAddMaterial tbody').html();
            $('#tbMaterialProduksi tbody').append(row);
        }

        function removeMaterial(element) {
            $(element).parents('tr').remove();
        }

        function selectKdMaterial(element) {
            var kdmaterial = $(element).val();
            var nama = $(element).parents('td').siblings('td.mt_name').children('input[name="materialName[]"]');
            var satuan = $(element).parents('td').siblings('td.mt_satuan').children('input[name="materialSatuan[]"]');

            $.ajax({
                url: "{{ route('produksi.detailmaterial') }}",
                type: "POST",
                data: {'kdmaterial': kdmaterial, '_token': '{{ csrf_token() }}'},
                success: function(result) {
                    nama.val(result.namaMaterial);
                    satuan.val(result.satuanMaterial);
                }
            });
        }
    </script>
@endsection

@section('title')
    Jurnal Harian - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Jurnal Harian
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li><a href="{{ route('produksi.index') }}">Jurnal Harian</a></li>
        <li class="active">Tambah Jurnal</li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => 'jurnalHarian.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- text input -->

                            <div class="form-group {!! $errors->has('tgl_produksi') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_produksi', 'Tanggal Jurnal', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('tgl_jmu', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tgl_jmu', '<p class="help-block">:message</p>') !!}
                            </div>
                            <!-- select -->
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('total', 'No Bukti', ['class' => 'control-label']) !!}
                                {{ Form::text('no_bukti', $no_jurnal, ['class' => 'form-control','required', 'id' => 'no_bukti']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('total', 'Keterangan Jurnal', ['class' => 'control-label']) !!}
                                {{ Form::text('keterangan', null, ['class' => 'form-control','required', 'id' => 'keterangan']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <button type="button" class="btn btn-block btn-social btn-google btn-add-trs">
                <i class="fa fa-plus"></i> Item
            </button>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Item List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbItemProduksi" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Kode Perkiraan</th>
                                <th class="text-center">Jenis Transaksi</th>
                                <th class="text-center">Debet</th>
                                <th class="text-center">Kredit</th>
                                <th class="text-center">Tipe Arus Kas</th>
                                <th class="text-center">Catatan</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- /.col -->
    
    <!-- /.col -->

    <div class="row">
        <div class="col-md-offset-6 col-md-6 col-xs-12">
            <div class="col-xs-6" style="padding: 3px;">
                <a href="{{ route('jurnalHarian.index') }}" class="btn btn-block btn-default">Cancel</a>
            </div>
            <div class="col-xs-6" style="padding: 3px;">
                <button type="submit" class="btn btn-block btn-success">Simpan</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <table class="hide" id="rowAddItem">
        <tbody>
            <tr>
                <td width="25%">
                    {{ Form::select('master_id[]', App\Models\MasterCoa::pluck('mst_nama_rekening', 'master_id')->all(), null, ['class' => 'form-control','required']) }}
                </td>
                <td width="10%">
                    {{ Form::select('jenis_transaksi[]', array('debet' => 'Debet', 'kredit' => 'Kredit'), null, ['class' => 'form-control']) }}
                </td>
                <td class="text-center"  width="15%">
                    {{ Form::number('debet[]', 0, ['class' => 'form-control', 'min' => 0]) }}
                </td>
                <td width="15%">
                    {{ Form::number('kredit[]', 0, ['class' => 'form-control', 'min' => 0]) }}
                </td>
                <td width="10%">
                    {{ Form::select('tipe[]', array('operasi' => 'Operasi', 'pendanaan' => 'Pendanaan','investasi' => 'Investasi'), null, ['class' => 'form-control']) }}
                </td>
                <td class="text-center" width="20%">
                    {{ Form::textarea('catatan[]', null, ['class' => 'form-control', 'rows' => 2]) }}
                </td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                        <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
@endsection