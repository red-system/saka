<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}"> --}}
    <title></title>
</head>
<body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:10px;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-weight:bold;font-family:Arial;font-size:12px;font-weight:normal;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h4 align="center">Jurnal Harian</h4>
                    <h5 align="center">{{$start}} S/D {{$end}}</h5>
                </div>
                <br>
                <div class="portlet light ">
                    <table class="tg table table-bordered table-hover table-header-fixed">
                        <thead>
                        <tr class="">
                            <th width="10" style="font-size:12px"><center> No </center></th>
                            <th style="font-size:12px"><center> Tanggal </center></th>
                            <th style="font-size:12px"><center> No Bukti </center></th>
                            <th style="font-size:12px"><center> Keterangan </center></th>
                            <th style="font-size:12px"><center> No Akun </center></th>
                            <th style="font-size:12px"><center> Debet </center></th>
                            <th style="font-size:12px"><center> Kredit </center></th>
                            <th style="font-size:12px"><center> Catatan </center></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $total_debet = 0;
                            $total_kredit = 0;
                            $no = 1;
                        ?>
                        @foreach($dataJurnal as $jmu)
                            <tr>
                                <td align="center" style="font-size:11px">{{$no}}</td>
                                <td style="font-size:11px"> {{ date('d M Y', strtotime($jmu->jmu_tanggal)) }} </td>
                                <td style="font-size:11px"><center> {{ $jmu->no_invoice }} </center></td>
                                <td style="font-size:11px" colspan="5"> {{ $jmu->jmu_keterangan }}</td>
                            </tr>
                            @foreach($jmu->transaksi as $trs)
                            <tr>                                
                                <td style="font-size:11px"></td>
                                <td style="font-size:11px"></td>
                                <td style="font-size:11px"></td>                                
                                <td style="font-size:11px" <?php if($trs->trs_jenis_transaksi=='kredit') echo 'align="right"';?>>   {{ $trs->trs_nama_rekening }}</td>
                                <td style="font-size:11px"><center>   {{ $trs->trs_kode_rekening }}</center></td>
                                <td style="font-size:11px" align="right">   {{ number_format($trs->trs_debet,2) }} </td>
                                <td style="font-size:11px" align="right">   {{ number_format($trs->trs_kredit,2) }} </td>
                                <td style="font-size:11px">   {{ $trs->trs_catatan }} </td>                               
                            </tr>
                            <?php
                                $total_debet += $trs->trs_debet;
                                $total_kredit += $trs->trs_kredit;
                            ?>
                            @endforeach
                            <?php $no++;?>
                        @endforeach
                            <tr class="">
                                <th width="10"></th>
                                <th colspan="4" align="center" style="font-size:12px"><center><strong> TOTAL </strong></center></th>
                                <th style="font-size:12px"><center><strong>{{number_format($total_debet,2)}} </strong></center></th>
                                <th style="font-size:12px"><center><strong>{{number_format($total_kredit,2)}} </th>
                                @if($total_debet==$total_kredit && $total_debet>0 && $total_kredit>0)
                                <th style="font-size:12px">
                                    <strong>Status : <font color="green">Balance</font></strong>
                                </th>
                                @endif
                                @if($total_debet!=$total_kredit)
                                <th style="font-size:12px">
                                    <strong>Status : <font color="red">Not Balance</font></strong>
                                </th>
                                @endif
                                @if($total_debet==0 && $total_kredit==0)
                                <th style="font-size:12px">
                                    <strong>Status : <font color="red"></font></strong>
                                </th>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>
