@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbSupplier').DataTable({
                responsive: true,
                pageLength: 20,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });

            $('.select2').select2();
        })
    </script>
@endsection

@section('title')
    Kode Perkiraan - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Kode Perkiraan
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Kode Akun</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addKodePerkiraan">
                <i class="fa fa-plus"></i> Tambah Kode Akun
            </a>
            <br>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <!-- <a href="{{route('kodePerkiraan.saldo-awal')}}" class="btn btn-block btn-social btn-facebook">
                <i class=""></i> Lihat Saldo Awal
            </a> -->
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kode Akun</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbSupplier" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th width="15%"> Kode Rekening </th>
                            <th> Nama Rekening</th>
                            <th> Posisi </th>
                            <th> Normal </th>
                            {{-- <th style="font-size:10px"> Kredit </th>
                            <th> Debet </th> --}}
                            <th> Aksi </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;?>
                        @foreach($kodePerkiraan as $coa)
                            <tr>
                                <td>{{$no}}</td>
                                <td><strong>{{$coa->mst_kode_rekening}}</strong></td>
                                <td><strong>{{$coa->mst_nama_rekening}}</strong></td>
                                <td><strong>{{$coa->mst_posisi}}</strong></td>
                                <td><strong>{{$coa->mst_normal}}</strong></td>
                                <td class="text-center">
                                    {!! Form::model($coa, ['route' => ['kodePerkiraan.destroy', $coa->master_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.edit', $coa->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    <!-- <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.addSaldoAwal', $coa->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i></a>
                                    </span> -->
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @foreach($coa->childs as $coaChild)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$coaChild->mst_kode_rekening}}</td>
                                <td>{{$coaChild->mst_nama_rekening}}</td>
                                <td>{{$coaChild->mst_posisi}}</td>
                                <td>{{$coaChild->mst_normal}}</td>
                                <td class="text-center">
                                    {!! Form::model($coaChild, ['route' => ['kodePerkiraan.destroy', $coaChild->master_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.edit', $coaChild->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    <!-- <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.addSaldoAwal', $coaChild->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i></a>
                                    </span> -->
                                    {!! Form::close()!!}
                                </td>
                            </tr><?php $no++; ?>
                            @foreach($coaChild->childs as $coaChild2)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$coaChild2->mst_kode_rekening}}</td>
                                <td>{{$coaChild2->mst_nama_rekening}}</td>
                                <td>{{$coaChild2->mst_posisi}}</td>
                                <td>{{$coaChild2->mst_normal}}</td>
                                <td class="text-center">
                                    {!! Form::model($coaChild2, ['route' => ['kodePerkiraan.destroy', $coaChild2->master_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.edit', $coaChild2->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    <!-- <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.addSaldoAwal', $coaChild2->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i></a>
                                    </span> -->
                                    {!! Form::close()!!}
                                </td>
                            </tr><?php $no++; ?>
                            @foreach($coaChild2->childs as $coaChild3)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$coaChild3->mst_kode_rekening}}</td>
                                <td>{{$coaChild3->mst_nama_rekening}}</td>
                                <td>{{$coaChild3->mst_posisi}}</td>
                                <td>{{$coaChild3->mst_normal}}</td>
                                <td class="text-center">
                                    {!! Form::model($coaChild3, ['route' => ['kodePerkiraan.destroy', $coaChild3->master_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.edit', $coaChild3->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    <!-- <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('kodePerkiraan.addSaldoAwal', $coaChild3->master_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i></a>
                                    </span> -->
                                    {!! Form::close()!!}
                                </td>
                            </tr><?php $no++; ?>
                            @endforeach
                            @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addKodePerkiraan">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tambah Kode Akunting</h4>
                </div>
                {!! Form::open(['route' => 'kodePerkiraan.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-3 col-lg-3 col-sm-3 control-label">Parent Data</label>
                            <div class="col-md-9 col-lg-9 col-sm-9">
                                <select name="mst_master_id" class="form-control select2" style="width: 100%" required>
                                    <option value="0">Parent Data</option>
                                    <?php foreach($kodePerkiraan as $coa) { ?>
                                    <option value="<?php echo $coa['master_id'] ?>">---<?php echo $coa['mst_kode_rekening'].' - '.$coa['mst_nama_rekening'] ?></option>
                                    <?php foreach($coa->childs as $r1) { ?>
                                        <option value="<?php echo $r1['master_id'] ?>">------<?php echo $r1['mst_kode_rekening'].' - '.$r1['mst_nama_rekening'] ?></option>
                                        <?php foreach($r1->childs as $r2) { ?>
                                        <option value="<?php echo $r2['master_id'] ?>">---------<?php echo $r2['mst_kode_rekening'].' - '.$r2['mst_nama_rekening'] ?></option>
                                        <?php foreach($r2->childs as $r3) { ?>
                                            <option value="<?php echo $r3['master_id'] ?>">------------<?php echo $r3['mst_kode_rekening'].' - '.$r3['mst_nama_rekening'] ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Kode Rekening</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mst_kode_rekening">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Nama Rekening</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mst_nama_rekening">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Posisi</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_posisi" style="width: 100%">
                                    <option value="asset">Asset</option>
                                    <option value="liabilitas">Liabilitas</option>
                                    <option value="ekuitas">Ekuitas</option>
                                    <option value="laba_bersih">Laba Bersih</option>
                                    <option value="laba_kotor">Laba Kotor</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Normal Balance</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_normal" style="width: 100%">
                                    <option value="debet">Debet</option>
                                    <option value="kredit">Kredit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Termasuk Arus Kas</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_kas_status" style="width: 100%">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editSupplier" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>

    <div id="addSaldoAwal" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection