<h3 align="center">Saldo Awal {{$month[$periode_month]}} {{$periode_year}}</h3>
                            <table id="tbSupplier" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th width="15%"> Kode Rekening </th>
                                    <th> Nama Rekening</th>
                                    <th> Posisi </th>
                                    <th> Normal </th>
                                    <th> Saldo Awal</th>
                                    <th> Aksi </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no=1;?>
                                @foreach($detailPerkiraan as $coa)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$coa->master->mst_kode_rekening}}</td>
                                        <td>{{$coa->master->mst_nama_rekening}}</td>
                                        <td>{{$coa->master->mst_posisi}}</td>
                                        <td>{{$coa->master->mst_normal}}</td>
                                        <?php
                                            if($coa->master->mst_normal=='debet'){
                                                echo '<td>'.number_format($coa->msd_awal_debet).'</td>';
                                            }else{
                                                echo '<td>'.number_format($coa->msd_awal_kredit).'</td>';
                                            }
                                        ?>
                                        <td class="text-center">
                                            {!! Form::model($coa, ['route' => ['kodePerkiraan.destroy', $coa->master_detail_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                <a href="{{ route('kodePerkiraan.edit', $coa->master_detail_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                            </span>
                                            {!! Form::close()!!}
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                                </tbody>
                            </table>
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $('#tbSupplier').DataTable({
                responsive: true,
                pageLength: 10,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });
</script>