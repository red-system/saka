@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbSupplier').DataTable({
                responsive: true,
                pageLength: 10,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });

            $("#formPembelian").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('kodePerkiraan.get-saldo-awal') }}",
                        type: "POST",
                        data: $("#formPembelian").serialize(),
                        success: function(htmlCode) {
                            $('#dataPembelian').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Kode Perkiraan - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Kode Perkiraan
        <small>Accounting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Accounting</li>
        <li class="active">Kode Perkiraan (COA)</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <!-- <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addKodePerkiraan">
                <i class="fa fa-plus"></i> Tambah Kode Perkiraan
            </a>
            <br>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-facebook" data-toggle="modal" data-target="#addKodePerkiraan">
                <i class=""></i> Lihat Saldo Awal
            </a>
            <br>
        </div> -->
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    
                </div>
                <!-- /.box-header -->
                
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPembelian']) !!}
                    <div class="row">                        
                        <div class="col-md-4">
                            <div class="form-group col-md-6">
                                {!! Form::label('bln', 'Bulan') !!}
                                <div class="input-group col-md-12">
                                {{ Form::hidden('tgl_awal', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    {{ Form::select('bulan', $month, $periode_month, ['class' => 'form-control select2','required','id'=>'bulan']) }}                                    
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('thn', 'Tahun') !!}
                                <div class="input-group col-md-12">
                                    {{ Form::select('tahun', $year, $periode_year, ['class' => 'form-control select2','required','id'=>'tahun']) }}
                                </div>
                            </div>                            
                        </div>
                        <div class="col-md-2">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-6">
                                <!-- <button type="button" class="btn btn-danger pull-right" onclick="printPembelian()" style="margin-top: 25px;">Print</button> -->
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataPembelian">
                            <h3 align="center">Saldo Awal {{$month[$periode_month]}} {{$periode_year}}</h3>
                            <table id="tbSupplier" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th width="15%"> Kode Rekening </th>
                                    <th> Nama Rekening</th>
                                    <th> Posisi </th>
                                    <th> Normal </th>
                                    <th> Saldo Awal</th>
                                    <th> Aksi </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $no=1;?>
                                @foreach($detailPerkiraan as $coa)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$coa->master->mst_kode_rekening}}</td>
                                        <td>{{$coa->master->mst_nama_rekening}}</td>
                                        <td>{{$coa->master->mst_posisi}}</td>
                                        <td>{{$coa->master->mst_normal}}</td>
                                        <?php
                                            if($coa->master->mst_normal=='debet'){
                                                echo '<td>'.number_format($coa->msd_awal_debet).'</td>';
                                            }else{
                                                echo '<td>'.number_format($coa->msd_awal_kredit).'</td>';
                                            }
                                        ?>
                                        <td class="text-center">
                                            {!! Form::model($coa, ['route' => ['kodePerkiraan.destroy', $coa->master_detail_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                <a href="{{ route('kodePerkiraan.edit', $coa->master_detail_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                            </span>
                                            <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                                <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                            </span>
                                            <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                <a href="{{ route('kodePerkiraan.addSaldoAwal', $coa->master_detail_id)}}" data-target="#editSupplier" data-toggle="modal" class="btn btn-sm btn-success"><i class="fa fa-money"></i></a>
                                            </span>
                                            {!! Form::close()!!}
                                        </td>
                                    </tr>
                                    <?php $no++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                                
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>


    <div id="addSaldoAwal" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection