<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Supplier</h4>
</div>
{!! Form::model($kodePerkiraan, ['route' => ['kodePerkiraan.update', $id], 'method' => 'post', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label" style="padding-top: 5px;">Parent Data</label>
            <div class="col-md-9">
                <select name="mst_master_id" class="form-control select2" required data-live-search="true">
                    <option value="0" <?php if($kodePerkiraan->mst_master_id==0) echo 'selected';?>>Parent Data</option>
                    <?php foreach($dataPerkiraan as $coa) { ?>
                    <option value="<?php echo $coa['master_id'] ?>" <?php if($kodePerkiraan->mst_master_id==$coa['master_id']) echo 'selected';?>>---<?php echo $coa['mst_kode_rekening'].' - '.$coa['mst_nama_rekening'] ?></option>
                    <?php foreach($coa->childs as $r1) { ?>
                    <option value="<?php echo $r1['master_id'] ?>" <?php if($kodePerkiraan->mst_master_id==$r1['master_id']) echo 'selected';?>>------<?php echo $r1['mst_kode_rekening'].' - '.$r1['mst_nama_rekening'] ?></option>
                    <?php foreach($r1->childs as $r2) { ?>
                    <option value="<?php echo $r2['master_id'] ?>" <?php if($kodePerkiraan->mst_master_id==$r2['master_id']) echo 'selected';?>>---------<?php echo $r2['mst_kode_rekening'].' - '.$r2['mst_nama_rekening'] ?></option>
                    <?php foreach($r2->childs as $r3) { ?>
                    <option value="<?php echo $r3['master_id'] ?>" <?php if($kodePerkiraan->mst_master_id==$r3['master_id']) echo 'selected';?>>------------<?php echo $r3['mst_kode_rekening'].' - '.$r3['mst_nama_rekening'] ?></option>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 10px;">
        <div class="form-group">
            <label class="col-md-3 control-label" style="padding-top: 5px;">Kode Rekening</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="mst_kode_rekening" value="{{$kodePerkiraan->mst_kode_rekening}}">
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 10px;">
        <div class="form-group">
            <label class="col-md-3 control-label" style="padding-top: 5px;">Nama Rekening</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mst_nama_rekening" value="{{$kodePerkiraan->mst_nama_rekening}}">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Posisi</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_posisi">                                    
                                    <option value="asset" <?php if($kodePerkiraan->mst_posisi=='asset') echo 'selected';?>>Asset</option>
                                    <option value="liabilitas" <?php if($kodePerkiraan->mst_posisi=='liabilitas') echo 'selected';?>>Liabilitas</option>
                                    <option value="ekuitas" <?php if($kodePerkiraan->mst_posisi=='ekuitas') echo 'selected';?>>Ekuitas</option>
                                    <option value="laba_bersih" <?php if($kodePerkiraan->mst_posisi=='laba_bersih') echo 'selected';?>>Laba Bersih</option>
                                    <option value="laba_kotor" <?php if($kodePerkiraan->mst_posisi=='laba_kotor') echo 'selected';?>>Laba Kotor</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Normal Balance</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_normal">
                                    <option value="debet" <?php if($kodePerkiraan->mst_normal=='debet') echo 'selected';?>>Debet</option>
                                    <option value="kredit" <?php if($kodePerkiraan->mst_normal=='kredit') echo 'selected';?>>Kredit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row" style="padding-top: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-top: 5px;">Termasuk Arus Kas</label>
                            <div class="col-md-9">
                                <select class="form-control select2" name="mst_kas_status">
                                    <option value="yes" <?php if($kodePerkiraan->mst_kas_status=='yes') echo 'selected';?>>Yes</option>
                                    <option value="no" <?php if($kodePerkiraan->mst_kas_status=='no') echo 'selected';?>>No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Simpan', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $('.select2').select2();
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>