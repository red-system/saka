<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Summary Stock {{$start}} s/d {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12" style="margin-top: 25px;">
            <div class="text-center">
                <h4>Summary Stock</h4>
                <h4>{{$start}} S/D {{$end}}</h4>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Date</th>
                        <th>Code</th>
                        @if($type == 'produk')
                            <th>No Seri</th>
                        @endif
                        <th>Nama</th>
                        <th style="text-align: center;">Qty In</th>
                        <th style="text-align: center;">Unit In</th>
                        <th style="text-align: center;">Qty Out</th>
                        <th style="text-align: center;">Unit Out</th>
                        <th>Remark</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($datastocks->count() > 0)
                        @foreach($datastocks as $ds)
                            <tr>
                                <td>{{ $ds->tgl }}</td>
                                <td>{{ $ds->kode }}</td>
                                @if($type == 'produk')
                                    <td>{{ $ds->no_seri }}</td>
                                @endif
                                <td>{{ $ds->nama }}</td>
                                <td style="text-align: center;">{{ $ds->total_qty_in }}</td>
                                <td style="text-align: center;">{{ $ds->satuan_in }}</td>
                                <td style="text-align: center;">{{ $ds->total_qty_out }}</td>
                                <td style="text-align: center;">{{ $ds->satuan_out }}</td>
                                <td>
                                    {{ $ds->keterangan }}
                                    @if($ds->remark != '')
                                        <br>{{ $ds->remark }}
                                    @endif
                                </td>
                                <td>{{ $ds->location->location_name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td style="text-align: center;" colspan="@if($type == 'produk') 10 @else 9 @endif">There is no stock data available</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
