<hr style="margin-top: 0">
<div class="table-responsive">
    <table class="table table-bordered table-stripped" id="dtSummaryStock">
        <thead>
        <tr>
            <th style="vertical-align: middle;">Code Product</th>
            <th class="text-center">Stock</th>
            <th class="text-center">Sales</th>
            <th class="text-center">Production</th>
            <th>Detail PO</th>
        </tr>
        </thead>
        <tbody>
        @if($products->count() > 0)
            @foreach($products as $prd)
                <tr>
                    <td>{{ $prd->kd_produk }}</td>
                    <?php
                    $stock = $stocks->where('kd_produk', $prd->kd_produk)->sum('qty');
                    $sales = $detailsales->where('kd_produk', $prd->kd_produk)->sum('saleQty');
                    ?>
                    <th class="text-center">@if($stock <= $prd->minimal_stock) <strong>{{ $stock }}</strong> @else {{ $stock }} @endif</th>
                    <th class="text-center">{{ $sales }}</th>
                    <?php
                        $produksiProduk = $detailproduction->where('kd_produk', $prd->kd_produk);
                    ?>
                    <td class="text-center">{{ $produksiProduk->sum('qty') }}</td>
                    <td>
                        @foreach($produksiProduk as $pp)
                            - {{ $pp->nama_produsen.'/Qty:'.$pp->qty.'/Keterangan: '.$pp->remark != '' ? $pp->remark : '-' }}<br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="5">There is no stock data available</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<script>
    $('#dtSummaryStock').DataTable();
</script>