@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <link href="{{ asset('assets/plugins/loading/waitMe.css') }}" rel="stylesheet">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/loading/waitMe.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        function runLoader() {
            $('#boxData').waitMe({
                effect : 'stretch',
                text : 'Please wait...',
                bg : 'rgba(255,255,255,0.7)',
                color : '#000',
                maxSize : 80,
                waitTime : -1,
                textPos : 'vertical',
                fontSize : '',
                source : '',
            });
        }

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formSummaryStock").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(form) {
                    if($('#tglawal').val() != '' && $('#tglakhir').val() != '') {
                        runLoader();
                        $.ajax({
                            url: "{{ route('summary-stock-global.getdata') }}",
                            type: "POST",
                            data: $("#formSummaryStock").serialize(),
                            success: function(htmlCode) {
                                $('#dataSummaryStock').html(htmlCode);
                                $('#boxData').waitMe("hide");
                            }
                        });
                    }
                }
            });
        })
    </script>
@endsection

@section('title')
    Summary Stock Global - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Summary Stock Global
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Summary Stock Global</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box" id="boxData">
                <div class="box-header">
                    <h3 class="box-title">Summary Stock Global</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '#', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formSummaryStock']) !!}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Date') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::button('View', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataSummaryStock"></div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
@endsection