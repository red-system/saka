@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            $('#tbStockAllert').DataTable({
                responsive: true,
                "columnDefs": [{
                    'orderable': false,
                    'targets': [6]
                }]
            });

            $("#checkAll").click(function() {
                var checked = $(this).prop('checked');
                $('.produk').each(function(){
                    $(this).prop("checked", checked).trigger("change");
                });
            });

            $('.produk').change(function() {
                if ($('.produk:checked').length == $('.produk').length) {
                    $("#checkAll").prop("checked", true);
                } else {
                    $("#checkAll").prop("checked", false);
                }
            });
        })
    </script>
@endsection

@section('title')
    Stock Allert - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Allert
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Stock Allert</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Stock Allert</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['route' => 'stock-allert.create-costing', 'class' => 'form-horizontal', 'id' => 'formProduk'])!!}
                    <table id="tbStockAllert" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Location</th>
                            <th class="text-center">Minimal Stock</th>
                            <th class="text-center">Available Stock By Location</th>
                            //<th class="text-center">Stock All Location</th>
                            <th class="text-center col-md-1 all">
                                <input type="checkbox" id="checkAll" data-set="#tbProduct .produk">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stockallerts->filter(function ($stck) { return $stck->totalQty <= $stck->minimal_stock; })->all() as $sa)
                            <tr>
                                <td>{{ $sa->kd_produk }}</td>
                                <td>{{ $sa->deskripsi }}</td>
                                <td>{{ $sa->location->location_name }}</td>
                                <td class="text-center">{{ number_format($sa->minimal_stock, 0, ',', '.') }}</td>
                                <td class="text-center">
                                    {{ number_format($sa->totalQty, 0, ',', '.') }}
                                    {{ Form::hidden('kebutuhan['.$sa->kd_produk.']', $sa->minimal_stock - $sa->totalQty) }}
                                </td>
                                <td class="text-center">{{ number_format($sa->globalStock, 0, ',', '.') }}</td>
                                <td class="text-center col-md-1">
                                    {!! Form::checkbox('listproduk['.$sa->kd_produk.']', $sa->kd_produk, false, ['class' => 'produk']) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr style="margin-bottom: 10px;">
                    <div class="text-right">
                        {!! Form::button('Create Costing', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
