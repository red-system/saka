@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/css/buttons.bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterKategori {
            display: inline;
        }

        .filterTanggal {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function rejectMaterial(elem) {
            var kdMaterial = $(elem).attr('data-material');
            $('#kdMaterial').val(kdMaterial);
            $('#mdRejectMaterial').modal('show');
        }

        $(function () {
            var tableMaterial = $('#tbMaterial').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 6,
                    orderable: false
                }],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            var allowFilter = ['tbHistoryReject'];
            $.fn.dataTable.ext.search.push(
                    function (settings, data, dataIndex) {
                        if ($.inArray(settings.nTable.getAttribute('id'), allowFilter ) == -1 )
                        {
                            return true;
                        }
                        var from = $('#fltFrom').datepicker("getDate");
                        var to = $('#fltTo').datepicker("getDate");
                        var startDate = new Date(data[0]).setHours(0,0,0,0);
                        if (from == null && to == null) { return true; }
                        if (from == null && startDate <= to) { return true;}
                        if(to == null && startDate >= from) {return true;}
                        if (startDate <= to && startDate >= from) { return true; }
                        return false;
                    }
            );
            var tbHistoryReject = $('#tbHistoryReject').DataTable({
                responsive: true,
                order: [0, 'desc'],
                dom: '<"datatable-header"<"row"<"col-md-9 col-sm-8 col-xs-12"f<"filterTanggal">B><"col-md-3 col-sm-4 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
                buttons: [
                    'excel',
                    {
                        extend: 'pdf',
                        customize: function (doc) {
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'print'
                ]
            });
            $("#filterTanggal").appendTo(".filterTanggal");
            $(".dt-buttons").find('.btn').addClass('btn-sm');

            $('#fltFrom, #fltTo').change(function () {
                tbHistoryReject.draw();
            });
        })
    </script>
@endsection

@section('title')
    Material Reject - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Material Reject
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Material Reject</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#materialreject" data-toggle="tab">Material Reject</a></li>
                    <li><a href="#historyreject" data-toggle="tab">History Reject</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="materialreject">
                        <table id="tbMaterial" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Type Material</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Unit</th>
                                <th class="text-center all">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($materials as $mt)
                                <tr>
                                    <td>{{ $mt->kd_material }}</td>
                                    <td>{!! $mt->name !!}</td>
                                    <td>{{ $mt->kategori->kategori }}</td>
                                    <td>{{ $mt->human_type }}</td>
                                    <td class="text-center">{{ number_format($mt->qty, 0, ',', '.') }}</td>
                                    <td>{{ $mt->satuan->satuan }}</td>
                                    <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Reject">
                                        <button type="button" class="btn btn-sm btn-danger" data-material="{{ $mt->kd_material }}" onclick="rejectMaterial(this)"> Reject</button>
                                    </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="modal fade" id="mdRejectMaterial">
                            <div class="modal-dialog modal-center modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">Reject Material</h4>
                                    </div>
                                    {!! Form::open(['route' => 'material-reject.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group {!! $errors->has('kd_material') ? 'has-error' : '' !!}">
                                                    {!! Form::label('kd_material', 'Material Code') !!}
                                                    {{ Form::text('kd_material', null, ['class' => 'form-control','readonly', 'id' => 'kdMaterial']) }}
                                                    {!! $errors->first('kd_material', '<p class="help-block">:message</p>') !!}
                                                </div>
                                                <div class="form-group qtyMaterial {!! $errors->has('qty') ? 'has-error' : '' !!}">
                                                    {!! Form::label('qty', 'Qty Reject') !!}
                                                    {{ Form::number('qty', null, ['class' => 'form-control','required', 'min' => 0]) }}
                                                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                                                </div>
                                                <div class="form-group {!! $errors->has('keterangan') ? 'has-error' : '' !!}">
                                                    {!! Form::label('keterangan', '*Remark') !!}
                                                    {{ Form::textarea('keterangan', null, ['class' => 'form-control', 'required', 'rows' => 3]) }}
                                                    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                        {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class="tab-pane" id="historyreject">
                        <div id="filterTanggal" for="filterTanggal" style="display: inherit;">
                            <span style="margin: 8px 5px 8px 15px;">Tgl:</span>
                            <div class="input-group" style="width: 220px;">
                                <input name="fltFrom" id="fltFrom" class="form-control input-sm input-inline datepicker" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">S/D</button>
                                </span>
                                <input name="fltTo" id="fltTo" class="form-control input-sm input-inline datepicker" type="text">
                            </div>
                        </div>
                        <table id="tbHistoryReject" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Type Material</th>
                                <th class="text-center">Qty</th>
                                <th class="none">Remark</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($materialReject as $hrj)
                                <tr>
                                    <td>{{ date('Y-m-d', strtotime($hrj->tgl_reject_material)) }}</td>
                                    <td>{{ $hrj->kd_material }}</td>
                                    <td>{{ $hrj->name }}</td>
                                    <td>{{ $hrj->human_type }}</td>
                                    <td class="text-center">{{ $hrj->qty }}</td>
                                    <td>{!! $hrj->keterangan !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection