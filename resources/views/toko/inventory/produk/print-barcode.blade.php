<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <style>
        @page {
            margin: 5px;
            size: 148mm 210mm;
        }
        body { margin: 10px 8px; }
    </style>
</head>
<body>
<table width="535px;">
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
        <td style="padding: 10px 12px;">
            <table width="100%" style="font-size: 7px;" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2">{!! DNS1D::getBarcodeHTML(strtoupper($produk->kd_produk), "C128A", 0.62, 30) !!}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 3px 0 0 0; margin: 0; text-align: center; text-transform: uppercase;">{{ $produk->kd_produk }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; margin: 0; text-align: center;">{{ $produk->deskripsi }}</td>
                </tr>
                <tr>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: left;"><b>Rp {{ number_format($produk->price, 2, "," ,".") }}</b></td>
                    <td style="padding: 3px 0 0 0; margin: 0; text-align: right;"><b>SK</b></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
