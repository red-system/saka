<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail No Seri</h4>
</div>
<div class="modal-body">
    <table class="table table-bordered table-stripped">
        <thead>
        <tr>
            <th>Produk Code</th>
            <th>No Seri</th>
            <th>Size</th>
            <th class="text-center">Qty</th>
            <th class="text-right">Production Cost</th>
            <th class="text-right">Product Value</th>
            <th class="text-center">Location</th>
            
        </tr>
        </thead>
        <tbody>
        @if($dataStock->count() > 0)
            <?php $totalQty = 0; $totalProductionCost = 0; $totalProductValue = 0; ?>
            @foreach($dataStock as $ds)
                <tr>
                    <td>{{ $ds->kd_produk }}</td>
                    <td>{{ $ds->no_seri_produk }}</td>
                    <td>{{ $ds->size_product }}</td>
                   <td class="text-center">{{ number_format($ds->qty, 0, ',', '.') }}</td>
                    <td class="text-right">{{ number_format($ds->hpp, 0, ',', '.') }}</td>
                    <td class="text-right">{{ number_format($ds->qty*$ds->hpp, 0, ',', '.') }}</td>
                    <td>{{ $ds->location_name }}</td>
                </tr>
                <?php
                    $totalQty = $totalQty + $ds->qty;
                    $totalProductionCost = $totalProductionCost + $ds->hpp;
                    $totalProductValue = $totalProductValue + ($ds->qty*$ds->hpp);
                ?>
            @endforeach
            <tr>
                <td colspan="2" class="text-right"><strong>Total</strong></td>
                <td class="text-center"><strong>{{ number_format($totalQty, 0, ',', '.') }}</strong></td>
                <td class="text-right"><strong>{{ number_format($totalProductionCost, 0, ',', '.') }}</strong></td>
                <td class="text-right"><strong>{{ number_format($totalProductValue, 0, ',', '.') }}</strong></td>
            </tr>
        @else
            <tr>
                <td class="text-center" colspan="5">There is no stock data available</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
