<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Product Code</th>
        <th>Desc</th>
        <th>Kategori</th>
        <th>Material</th>
        <th class="text-center">Min Stock</th>
        <th class="text-center">Stock</th>
        <th class="text-right">Product Value</th>
        <th>Stok Location</th>
        <th class="text-center">#</th>
    </tr>
    </thead>
    <tbody>
    @if($dataStock->count() > 0)
        <?php $totalQty = 0; $totalProductValue = 0; ?>
        @foreach($dataStock as $ds)
            <tr>
                <td>{{ $ds->kd_produk }}</td>
                <td>{{ $ds->deskripsi }}</td>
                <td>{{ $ds->kategori }}</td>
                <td>{{ $ds->product->material }}</td>
                <td class="text-center">{{ $ds->product->minimal_stock }}</td>
                <td class="text-center">{{ $ds->totalQty }}</td>
                <td class="text-right">{{ number_format($ds->totalPrdValue, 0, ',', '.') }}</td>
                <td>{{ $ds->globalLocation }}</td>
                <td class="text-center">
                    <span data-toggle="tooltip" data-placement="top" title="Detail Seri">
                        <a href="{{ route('stok-bylocation.detailseri', [$ds->kd_produk, $ds->globalLocation])}}" data-target="#detailNoSeri" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                    </span>
                </td>
            </tr>
            <?php
                $totalQty = $totalQty + $ds->totalQty;
                $totalProductValue = $totalProductValue + $ds->totalPrdValue;
            ?>
        @endforeach
        <tr>
            <td colspan="5" class="text-right"><strong>Total</strong></td>
            <td class="text-center"><strong>{{ number_format($totalQty, 0, ',', '.') }}</strong></td>
            <td class="text-right"><strong>{{ number_format($totalProductValue, 0, ',', '.') }}</strong></td>
        </tr>
    @else
        <tr>
            <td class="text-center" colspan="7">There is no stock data available</td>
        </tr>
    @endif
    </tbody>
</table>