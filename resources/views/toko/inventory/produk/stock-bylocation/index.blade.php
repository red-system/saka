@extends($mlayout)

@section('custom-css')

@endsection

@section('plugin-js')

@endsection

@section('custom-script')
    <script>
        function printStock() {
            var sllocation = $('#location').val();
            var searchTxt = $('#search').val();

            if(sllocation != '') {
                var urlPrint = '{{ route('stok-bylocation.index') }}'+'/print/'+sllocation+'?search='+searchTxt;
                window.open(urlPrint);
            }
        }

        function exportStock() {
            var sllocation = $('#location').val();
            var searchTxt = $('#search').val();

            if(sllocation != '') {
                var urlExport = '{{ route('stok-bylocation.index') }}'+'/export/'+sllocation+'?search='+searchTxt;
                location.href = urlExport;
            }
        }

        $(function () {
            if ($(document.getElementById("location")).length > 0) {
                var k = document.getElementById("location");
                var location = k.options[k.selectedIndex].value;
                var searchTxt = $('#search').val();

                $.post("{{ route('stok-bylocation.getstock') }}", {location: location, search: searchTxt, _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#listDataStock').html(data);
                });
            }

            $('#location').on('change', function(e){
                var location = e.target.value;
                var searchTxt = $('#search').val();

                $.post("{{ route('stok-bylocation.getstock') }}", {location: location, search: searchTxt, _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#listDataStock').html(data);
                });
            });

            $('#search').on('keyup', function(e){
                var location = $('#location').val();
                var searchTxt = $('#search').val();

                $.post("{{ route('stok-bylocation.getstock') }}", {location: location, search: searchTxt, _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#listDataStock').html(data);
                });
            });
        })
    </script>
@endsection

@section('title')
    Product Stock by Location - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Product Stock by Location
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li class="active">Product Stock by Location</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Stock Data</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('location', 'Location', ['class' => 'control-label', 'style' => 'text-align:left;']) !!}
                                {{ Form::select('location', ['all' => 'Semua Lokasi']+App\Models\Location::pluck('location_name', 'kd_location')->all(), auth()->user()->kd_location, ['class' => 'form-control select2','required', 'id' => 'location']) }}
                            </div>
                        </div>
                        <div class="col-md-3">
                            {!! Form::label('search', 'Search', ['class' => 'control-label', 'style' => 'text-align:left;']) !!}
                            {{ Form::text('search', null, ['class' => 'form-control', 'id' => 'search']) }}
                        </div>
                        <div class="col-md-5 text-right">
                            <button type="button" class="btn btn-danger" onclick="printStock()" style="margin-top: 25px;">Print</button>
                            <button type="button" class="btn btn-success" onclick="exportStock()" style="margin-top: 25px;">Print Excel</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <div id="listDataStock"></div>
                    </div>
                    <div id="detailNoSeri" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-center modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                                    <span> &nbsp;&nbsp;Loading... </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection