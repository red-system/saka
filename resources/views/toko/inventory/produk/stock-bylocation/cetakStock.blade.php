<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    @if ($location==NULL)
        <title>Stok All Location</title>
    @else
        <title>Stok {{ $location->location_name }}</title>
    @endif
    
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12" style="margin-top: 25px;">
            <div class="text-center">
                @if ($location==NULL)
                <h4>Stok All Location</h4>
                @else
                    <h4>Stok {{ $location->location_name }}</h4>
                @endif
                
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Product Code</th>
                        <th>Desc</th>
                        <th>Material</th>
                        <th style="text-align: center;">Min Stock</th>
                        <th style="text-align: center;">Stock</th>
                        <th style="text-align: right;">Product Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $totalQty = 0; $totalProductValue = 0; ?>
                    @foreach($datastock as $ds)
                        <tr>
                            <td>{{ $ds->kd_produk }}</td>
                            <td>{{ $ds->deskripsi }}</td>
                            <td>{{ $ds->product->material }}</td>
                            <td style="text-align: center;">{{ $ds->product->minimal_stock }}</td>
                            <td style="text-align: center;">{{ $ds->totalQty }}</td>
                            <td style="text-align: right;">{{ 'Rp '.number_format($ds->totalPrdValue, 2, ',', '.') }}</td>
                        </tr>
                    <?php
                        $totalQty = $totalQty + $ds->totalQty;
                        $totalProductValue = $totalProductValue + $ds->totalPrdValue;
                    ?>
                    @endforeach
                    <tfoot>
                    <tr>
                        <td colspan="4" style="font-weight:bold; text-align: right;">Total</td>
                        <td style="font-weight:bold; text-align: center;"><strong>{{ number_format($totalQty, 0, ',', '.') }}</strong></td>
                        <td style="font-weight:bold; text-align: right;"><strong>{{ 'Rp '.number_format($totalProductValue, 2, ',', '.') }}</strong></td>
                    </tr>
                    </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
