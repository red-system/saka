{!! Form::model($produk, ['route' => ['produk.destroy', $produk->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
    <span data-toggle="tooltip" data-placement="top" title="Edit">
        <a href="{{ route('produk.edit', $produk->id)}}" data-target="#editProduct" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
    </span>
    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
    </span>
    <span data-toggle="tooltip" data-placement="top" title="View Stock">
        <a href="{{ route('stok-produk.index', $produk->id) }}" class="btn btn-sm btn-warning" target="blank"><i class="fa fa-th-list"></i></a>
    </span>
    <span data-toggle="tooltip" data-placement="top" title="Product Composition">
        <a href="{{ route('komposisi-produk.index', $produk->id) }}" class="btn btn-sm btn-success"><i class="fa fa-cogs"></i></a>
    </span>
    <span data-toggle="tooltip" data-placement="top" title="Print Barcode">
        <a href="{{ route('produk.print-barcode', $produk->id) }}" class="btn btn-sm btn-default" target="_blank"><i class="fa fa-print"></i></a>
    </span>
{!! Form::close()!!}