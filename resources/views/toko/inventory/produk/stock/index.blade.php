@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.select2').select2();

            $(".selectLoc").select2({
                dropdownParent: $("#addStock")
            });

            if ($(document.getElementById("kdlocation")).length > 0) {
                var k = document.getElementById("kdlocation");
                var kdlocation = k.options[k.selectedIndex].value;

                $.post("{{ route('stok-produk.getlocation') }}", {kd_location: kdlocation,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#pic').val(data.pic);
                });
            }

            $('#kdlocation').on('change', function(e){
                var kdlocation = e.target.value;
                $.post("{{ route('stok-produk.getlocation') }}", {kd_location: kdlocation,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#pic').val(data.pic);
                });
            });
        })
    </script>
@endsection

@section('title')
    Product Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Product Stock
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('produk.index') }}">Product</a></li>
        <li class="active">Product Stock</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addStock">
                <i class="fa fa-plus"></i> Add Stock
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Stock Data</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbStock" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>No Seri</th>
                            <th>Location</th>
                            <th>PIC</th>
                            <th>Size</th>
                            <th class="text-center">Qty</th>
                            <th class="text-right">Production Cost</th>
                            <th class="text-right">Product Value</th>
                            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                            <th class="text-center all">Act</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if($produkstocks->count() > 0)
                            <?php $totalQty = 0; $totalProductionCost = 0; $totalProductValue = 0; ?>
                            @foreach($produkstocks as $ps)
                                <tr>
                                    <td>{{ $ps->kd_produk }}</td>
                                    <td>{{ $ps->no_seri_produk }}</td>
                                    <td>{{ $ps->location->location_name }}</td>
                                    <td>{{ $ps->location->pic }}</td>
                                    <td class="text-center">{{ number_format($ps->size_product, 0, ',', '.') }}</td>
                                    <td class="text-center">{{ number_format($ps->qty, 0, ',', '.') }}</td>
                                    <td class="text-right">{{ number_format($ps->hpp, 0, ',', '.') }}</td>
                                    <td class="text-right">{{ number_format($ps->qty*$ps->hpp, 0, ',', '.') }}</td>
                                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                                        <td class="text-center">
                                            {!! Form::model($ps, ['route' => ['stok-produk.destroy', $ps->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                                <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        </td>
                                    @endif
                                </tr>
                                <?php
                                    $totalQty = $totalQty + $ps->qty;
                                    $totalProductionCost = $totalProductionCost + $ps->hpp;
                                    $totalProductValue = $totalProductValue + ($ps->qty*$ps->hpp);
                                ?>
                            @endforeach
                            <tr>
                                <td colspan="4" class="text-right"><strong>Total</strong></td>
                                <td class="text-center"><strong>{{ number_format($totalQty, 0, ',', '.') }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($totalProductionCost, 0, ',', '.') }}</strong></td>
                                <td class="text-right"><strong>{{ number_format($totalProductValue, 0, ',', '.') }}</strong></td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="@if(\Gate::allows('as_owner_or_manager_kantorpusat')) 8 @else 7 @endif"></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addStock">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tambah Stock</h4>
                </div>
                {!! Form::open(['route' => ['stok-produk.store', $produk->id], 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_produk') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produk', 'Product Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_produk', $produk->kd_produk, ['class' => 'form-control','disabled']) }}
                                {!! $errors->first('kd_produk', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('no_seri_produk') ? 'has-error' : '' !!}">
                                {!! Form::label('no_seri_produk', 'No Seri Product', ['class' => 'control-label']) !!}
                                {{ Form::text('no_seri_produk', $noseri, ['class' => 'form-control','readonly']) }}
                                {!! $errors->first('no_seri_produk', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('kd_location') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_location', 'Location') !!}
                                @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                                    {{ Form::select('kd_location', ['' => '']+App\Models\Location::pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control selectLoc','required', 'style' => 'width: 100%;', 'id' => 'kdlocation']) }}
                                @else
                                    {{ Form::select('kd_location', ['' => '']+App\Models\Location::where('kd_location', auth()->user()->kd_location)->pluck('location_name', 'kd_location')->all(), auth()->user()->kd_location, ['class' => 'form-control','disabled', 'style' => 'width: 100%;', 'id' => 'kdlocation']) }}
                                @endif
                                {!! $errors->first('kd_location', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('size_product') ? 'has-error' : '' !!}">
                                {!! Form::label('size_product', 'Size', ['class' => 'control-label']) !!}
                                {{ Form::text('size_product', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('size_product', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('pic') ? 'has-error' : '' !!}">
                                {!! Form::label('pic', 'PIC', ['class' => 'control-label']) !!}
                                {{ Form::text('pic', null, ['class' => 'form-control','readonly', 'id' => 'pic']) }}
                                {!! $errors->first('pic', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('qty') ? 'has-error' : '' !!}">
                                {!! Form::label('qty', 'Qty') !!}
                                {{ Form::number('qty', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('hpp') ? 'has-error' : '' !!}">
                                {!! Form::label('hpp', 'Production Cost') !!}
                                {{ Form::number('hpp', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('hpp', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection