@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            var table = $('#tbProduct').dataTable({
                "columnDefs": [{
                    'orderable': false,
                    'targets': [4]
                }]
            });

            $("#checkAll").click(function() {
                var checked = $(this).prop('checked');
                $('.produk').each(function(){
                    $(this).prop("checked", checked).trigger("change");
                });
            });

            $('.produk').change(function() {
                if ($('.produk:checked').length == $('.produk').length) {
                    $("#checkAll").prop("checked", true);
                } else {
                    $("#checkAll").prop("checked", false);
                }
            });
        })
    </script>
@endsection

@section('title')
    Discount Product - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Product
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li><a href="{{ route('produk.index') }}">Product</a></li>
        <li class="active">Discount</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Discount Product</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['route' => 'diskon-produk.set-diskon', 'class' => 'form-horizontal', 'id' => 'formProduk'])!!}
                    <table id="tbProduct" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Desc</th>
                            <th>Material</th>
                            <th class="text-center col-md-2">Discount (%)</th>
                            <th class="text-center col-md-1">
                                <input type="checkbox" id="checkAll" data-set="#tbProduct .produk">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dataproduk as $produk)
                            <tr>
                                <td>{{ $produk->kd_produk }}</td>
                                <td>{!! $produk->deskripsi !!}</td>
                                <td>{{ $produk->material }}</td>
                                <td class="text-center col-md-2">{{ $produk->disc_persen }}</td>
                                <td class="text-center col-md-1">
                                    {!! Form::checkbox('listproduk[]', $produk->id, false, ['class' => 'produk']) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr style="margin-bottom: 10px;">
                    <div class="text-right">
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addDiskon">Set Discount</button>
                    </div>
                    <!-- MODALS -->
                    <div class="modal fade" id="addDiskon">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">Discount</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="{!! $errors->has('discount') ? 'has-error' : '' !!}">
                                                {!! Form::label('discount', 'Discount (%)', ['class' => 'col-md-3']) !!}
                                                <div class="col-md-9">
                                                    {{ Form::number('discount', 0, ['class' => 'form-control','required', 'min' => 0, 'max' => 100]) }}
                                                    {!! $errors->first('discount', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                    {!! Form::close() !!}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection