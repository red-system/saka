<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Build Of Material</h4>
</div>
{!! Form::model($komposisi, ['route' => ['komposisi-produk.update', $komposisi->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="form-group {!! $errors->has('kd_material') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_material', 'Material Code') !!}
                    {{ Form::select('kd_material', ['' => '']+App\Models\Material::selectRaw('kd_material, concat(kd_material, " - ", name) as kode_nama')->pluck('kode_nama', 'kd_material')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'kdmaterialu']) }}
                    {!! $errors->first('kd_material', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                    {{ Form::text('name', null, ['class' => 'form-control','readonly', 'id' => 'nameu']) }}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('qty') ? 'has-error' : '' !!}">
                    {!! Form::label('qty', 'Qty') !!}
                    {{ Form::number('qty', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {!! $errors->has('namasatuan') ? 'has-error' : '' !!}">
                    {!! Form::label('namasatuan', 'Unit') !!}
                    {{ Form::text('namasatuan', null, ['class' => 'form-control','readonly', 'id' => 'satuanu']) }}
                    {!! $errors->first('namasatuan', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $(function () {
        $('.select2u').select2();

        if ($(document.getElementById("kdmaterialu")).length > 0) {
            var k = document.getElementById("kdmaterialu");
            var kdmaterial = k.options[k.selectedIndex].value;

            $.post("{{ route('komposisi-produk.getmaterial') }}", {kd_material: kdmaterial,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                $('#nameu').val(data.name);
                $('#satuanu').val(data.satuan);
            });
        }

        $('#kdmaterialu').on('change', function(e){
            var kdmaterial = e.target.value;
            $.post("{{ route('komposisi-produk.getmaterial') }}", {kd_material: kdmaterial,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                $('#nameu').val(data.name);
                $('#satuanu').val(data.satuan);
            });
        });

        $(".validate-form").validate({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo(element.parent().parent().parent());
                } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent());
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    })
</script>