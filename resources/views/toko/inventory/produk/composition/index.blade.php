@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('#tbKomposisi').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ]
            });

            $('.select2').select2();

            if ($(document.getElementById("kdmaterial")).length > 0) {
                var k = document.getElementById("kdmaterial");
                var kdmaterial = k.options[k.selectedIndex].value;

                $.post("{{ route('komposisi-produk.getmaterial') }}", {kd_material: kdmaterial,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#name').val(data.name);
                    $('#satuan').val(data.satuan);
                });
            }

            $('#kdmaterial').on('change', function(e){
                var kdmaterial = e.target.value;
                $.post("{{ route('komposisi-produk.getmaterial') }}", {kd_material: kdmaterial,  _token: $('meta[name="_token"]').attr('content') }, function(data) {
                    $('#name').val(data.name);
                    $('#satuan').val(data.satuan);
                });
            });
        })
    </script>
@endsection

@section('title')
    Product Build Of Material - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Product BOM
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('produk.index') }}">Product</a></li>
        <li class="active">Product BOM</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addKomposisi">
                <i class="fa fa-plus"></i> Add Material
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Composition Data - {{ $produk->kd_produk }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbKomposisi" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Material Code</th>
                            <th>Material Name</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center all">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($komposisiproduks as $kp)
                            <tr>
                                <td>{{ $kp->kd_produk }}</td>
                                <td>{{ $kp->material->name }}</td>
                                <td class="text-center">{{ number_format($kp->qty, 0, ',', '.') }}</td>
                                <td class="text-center">{{ $kp->satuan->satuan }}</td>
                                <td class="text-center">
                                    {!! Form::model($kp, ['route' => ['komposisi-produk.destroy', $kp->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Edit">
                                        <a href="{{ route('komposisi-produk.edit', $kp->id)}}" data-target="#editKomposisi" data-toggle="modal" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addKomposisi">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Material</h4>
                </div>
                {!! Form::open(['route' => ['komposisi-produk.store', $produk->id], 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_material') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_material', 'Material Code') !!}
                                {{ Form::select('kd_material', ['' => '']+App\Models\Material::selectRaw('kd_material, concat(kd_material, " - ", name) as kode_nama')->pluck('kode_nama', 'kd_material')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'kdmaterial']) }}
                                {!! $errors->first('kd_material', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                {{ Form::text('name', null, ['class' => 'form-control','readonly', 'id' => 'name']) }}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('qty') ? 'has-error' : '' !!}">
                                {!! Form::label('qty', 'Qty') !!}
                                {{ Form::number('qty', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('satuan') ? 'has-error' : '' !!}">
                                {!! Form::label('satuan', 'Unit') !!}
                                {{ Form::text('satuan', null, ['class' => 'form-control','readonly', 'id' => 'satuan']) }}
                                {!! $errors->first('satuan', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editKomposisi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection