@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#productimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            var l = window.location;
            var base_url = l.protocol + "//" + l.host + "/";

            $('#tbProduct').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('produk.getData') !!}',
                columns: [
                    { data: 'kd_produk', name: 'tb_product.kd_produk' },
                    { data: 'deskripsi', name: 'tb_product.deskripsi'},
                    { data: 'material', name: 'tb_product.material' },
                    { data: 'weight', name: 'tb_product.weight'},
                    { data: 'kategori', name: 'tb_kat.kategori' },
                    { data: 'wholesale_price', name: 'tb_product.wholesale_price', render: $.fn.dataTable.render.number('.', ',', 2) },
                    { data: 'price', name: 'tb_product.price', render: $.fn.dataTable.render.number('.', ',', 2) },
                    { data: 'collection', name: 'tb_collection.collection' },
                    { data: 'disc_persen', name: 'tb_product.disc_persen', 'class': 'text-center'},
                    { data: 'minimal_stock', name: 'tb_product.minimal_stock', render: $.fn.dataTable.render.number('.', ',', 0), 'class': 'text-center'},
                    { data: 'global_stock', name: 'global_stock', searchable: false, render: $.fn.dataTable.render.number('.', ',', 0), 'class': 'text-center'},
                    {data: 'action', name: 'action', orderable: false, searchable: false, 'class': 'text-center'},
                ]
            });

            $('.select2').select2();

            $("#imgproduct").change(function() {
                readURL(this);
            });

            $('.currency').number(true, 2);

            $('#kdproduk').inputmask({
                mask: "[A|9]",
                repeat: "12",
                greedy: true
            });

            $('#kdproduk').change(function () {
                var kdproduk = $('#kdproduk').val();
              // kode_akun.html('');
                $.ajax({
                    url: base_url+'/inventory/produk/cekkdproduk/'+kdproduk,
                    type: 'GET',
                    success: function(data) {
                        if(data.count>0){
                            $('#btn-save-produk').attr('disabled', true);
                            swal({
                                title: 'Perhatian',
                                text: 'Kode Produk Sudah ada!',
                                type: 'warning'
                            });
                        }else{
                            $('#btn-save-produk').attr('disabled', false);
                        }
                    }
                });
            });
        })
    </script>
@endsection

@section('title')
    Product - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Product
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Product</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-2 col-sm-6 col-xs-12" style="padding-right: 0px;">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addProduct">
                <i class="fa fa-plus"></i> Add Product
            </a>
            <br>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12">
            <a href="{{ route('diskon-produk.index') }}" class="btn btn-block btn-social btn-info">
                <i class="fa fa-credit-card"></i> Discount
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Product</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbProduct" class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Desc</th>
                            <th class="none">Material</th>
                            <th class="none">Weight</th>
                            <th class="text-center">Product Category</th>
                            <th class="none text-center">Wholesale Price</th>
                            <th class="text-center">Retail Price</th>
                            <th class="none">Collection</th>
                            <th>Disc</th>
                            <th class="text-center">Min Stock</th>
                            <th class="text-center">Glb Stock</th>
                            <th class="text-center all">Act</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addProduct">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Product</h4>
                </div>
                {!! Form::open(['route' => 'produk.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'files' => true]) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_kat', 'Product Category') !!}
                                {{ Form::select('kd_kat', ['' => '']+App\Models\Kategori::where('type_kat', 'produk')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'kategori']) }}
                                {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('style_number') ? 'has-error' : '' !!}">
                                {!! Form::label('style_number', 'Style Number') !!}
                                {{ Form::text('style_number', null, ['class' => 'form-control','required', 'id' => 'stylenumber']) }}
                                {!! $errors->first('style_number', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('material') ? 'has-error' : '' !!}">
                                {!! Form::label('material', 'Material', ['class' => 'control-label']) !!}
                                {{ Form::text('material', null, ['class' => 'form-control','required', 'id' => 'material']) }}
                                {!! $errors->first('material', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('stone_color') ? 'has-error' : '' !!}">
                                {!! Form::label('stone_color', 'Stone Color', ['class' => 'control-label']) !!}
                                {{ Form::text('stone_color', null, ['class' => 'form-control','required', 'id' => 'stonecolor']) }}
                                {!! $errors->first('stone_color', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('size') ? 'has-error' : '' !!}">
                                {!! Form::label('size', 'Size', ['class' => 'control-label']) !!}
                                {{ Form::text('size', null, ['class' => 'form-control','required', 'id' => 'size']) }}
                                {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('kd_produk') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produk', 'Product Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_produk', null, ['class' => 'form-control','required', 'id' => 'kdproduk']) }}
                                {!! $errors->first('kd_produk', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}">
                                {!! Form::label('deskripsi', 'Description') !!}
                                {{ Form::textarea('deskripsi', null, ['class' => 'form-control','required', 'rows' => 4]) }}
                                {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('weight') ? 'has-error' : '' !!}">
                                {!! Form::label('weight', 'Weight') !!}
                                {{ Form::number('weight', null, ['class' => 'form-control','required', 'step' => '0.1']) }}
                                {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                                {!! Form::label('price', 'Retail Price (IDR - Rupiah Indonesia)') !!}
                                {{ Form::text('price', null, ['class' => 'form-control currency','required']) }}
                                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('wholesale_price') ? 'has-error' : '' !!}">
                                {!! Form::label('wholesale_price', 'Wholesale Price (USD - Dolar Amerika Serikat)') !!}
                                {{ Form::text('wholesale_price', null, ['class' => 'form-control currency','required']) }}
                                {!! $errors->first('wholesale_price', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('minimal_stock') ? 'has-error' : '' !!}">
                                {!! Form::label('minimal_stock', 'Minimal Stock') !!}
                                {{ Form::number('minimal_stock', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('minimal_stock', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('collection_id') ? 'has-error' : '' !!}">
                                {!! Form::label('collection_id', 'Collection') !!}
                                {{ Form::select('collection_id', ['' => '']+App\Models\Collection::pluck('collection', 'id')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('collection_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <img id="productimg" class="img-responsive" src="http://via.placeholder.com/500x500" alt="User profile picture">
                            <hr>
                            <div class="form-group">
                                {!! Form::label('imgproduct', 'Browse File image') !!}
                                {{ Form::file('imgproduct', ['id' => 'imgproduct']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success','id'=>'btn-save-produk']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editProduct" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection