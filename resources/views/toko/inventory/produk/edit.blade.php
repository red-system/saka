<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Product</h4>
</div>
{!! Form::model($produk, ['route' => ['produk.update', $produk->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form', 'files' => true])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                {!! Form::label('kd_kat', 'Product Category') !!}
                {{ Form::select('kd_kat', ['' => '']+App\Models\Kategori::where('type_kat', 'produk')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'ukategori']) }}
                {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('style_number') ? 'has-error' : '' !!}">
                {!! Form::label('style_number', 'Style Number') !!}
                {{ Form::text('style_number', null, ['class' => 'form-control','required', 'id' => 'stylenumber']) }}
                {!! $errors->first('style_number', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('material') ? 'has-error' : '' !!}">
                {!! Form::label('material', 'Material', ['class' => 'control-label']) !!}
                {{ Form::text('material', null, ['class' => 'form-control','required', 'id' => 'material']) }}
                {!! $errors->first('material', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('stone_color') ? 'has-error' : '' !!}">
                {!! Form::label('stone_color', 'Stone Color', ['class' => 'control-label']) !!}
                {{ Form::text('stone_color', null, ['class' => 'form-control','required', 'id' => 'stonecolor']) }}
                {!! $errors->first('stone_color', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('size') ? 'has-error' : '' !!}">
                {!! Form::label('size', 'Size', ['class' => 'control-label']) !!}
                {{ Form::text('size', null, ['class' => 'form-control','required', 'id' => 'size']) }}
                {!! $errors->first('size', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('kd_produk') ? 'has-error' : '' !!}">
                {!! Form::label('kd_produk', 'Product Code', ['class' => 'control-label']) !!}
                {{ Form::text('kd_produk', null, ['class' => 'form-control','required','readonly', 'id' => 'ukdproduk']) }}
                {!! $errors->first('kd_produk', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}">
                {!! Form::label('deskripsi', 'Description') !!}
                {{ Form::textarea('deskripsi', null, ['class' => 'form-control','required', 'rows' => 4]) }}
                {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('weight') ? 'has-error' : '' !!}">
                {!! Form::label('weight', 'Weight') !!}
                {{ Form::number('weight', null, ['class' => 'form-control','required', 'step' => '0.1']) }}
                {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('price') ? 'has-error' : '' !!}">
                {!! Form::label('price', 'Retail Price (ID - Rp)') !!}
                {{ Form::text('price', \Konversi::database_to_localcurrency($produk->price), ['class' => 'form-control edcurrency','required']) }}
                {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('wholesale_price') ? 'has-error' : '' !!}">
                {!! Form::label('wholesale_price', 'Wholesale Price (US - $)') !!}
                {{ Form::text('wholesale_price', \Konversi::database_to_localcurrency($produk->wholesale_price), ['class' => 'form-control edcurrency','required']) }}
                {!! $errors->first('wholesale_price', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('minimal_stock') ? 'has-error' : '' !!}">
                {!! Form::label('minimal_stock', 'Minimal Stock') !!}
                {{ Form::number('minimal_stock', null, ['class' => 'form-control','required']) }}
                {!! $errors->first('minimal_stock', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group {!! $errors->has('collection_id') ? 'has-error' : '' !!}">
                {!! Form::label('collection_id', 'Collection') !!}
                {{ Form::select('collection_id', ['' => '']+App\Models\Collection::pluck('collection', 'id')->all(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                {!! $errors->first('collection_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-4">
            <img id="productimgu" class="img-responsive" src="{{ $produk->produk_image }}" alt="User profile picture">
            <hr>
            <div class="form-group">
                {!! Form::label('imgproduct', 'Browse File image') !!}
                {{ Form::file('imgproduct', ['id' => 'imgproductu']) }}
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    function readURLUpdate(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#productimgu').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgproductu").change(function() {
        readURLUpdate(this);
    });

    $('.edcurrency').number(true, 2);

    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#ukdproduk').inputmask({
        mask: "[A|9]",
        repeat: "12",
        greedy: true
    });
</script>