<hr style="margin-top: 0">
<div class="table-responsive">
    <table class="table table-bordered table-stripped" id="dtSummaryStock">
        <thead>
        <tr>
            <th style="vertical-align: middle;">Product Code</th>
            <th style="vertical-align: middle;">Product</th>
            <th class="text-center">Stock</th>
            <th class="text-center">Sales</th>
        </tr>
        </thead>
        <tbody>
        @if($products->count() > 0)
            @foreach($products as $prd)
                <tr>
                    <td>{{ $prd->kd_produk }}</td>
                    <?php
                        $stock = $stocks->where('kd_produk', $prd->kd_produk)->sum('qty');
                        $sales = $detailsales->where('kd_produk', $prd->kd_produk)->sum('saleQty');
                    ?>
                    <td>{{ $prd->deskripsi }}</td>
                    <th class="text-center">@if($stock <= $prd->minimal_stock) <strong>{{ $stock }}</strong> @else {{ $stock }} @endif</th>
                    <th class="text-center">{{ $sales }}</th>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="3">There is no stock data available</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<script>
    $('#dtSummaryStock').DataTable();
</script>