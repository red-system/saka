<hr style="margin-top: 0">
<div class="table-responsive">
    <table class="table table-bordered table-stripped" id="dtSummaryStock">
        <thead>
        <tr>
            <th rowspan="2" style="vertical-align: middle;">Product Code</th>
            <th rowspan="2" style="vertical-align: middle;">Product Name</th>
            @foreach($outlets as $loc)
                <th class="text-center" colspan="2" style="vertical-align: middle;">{{ $loc->location_name }}</th>
            @endforeach
        </tr>
        <tr>
            @foreach($outlets as $loc)
                <th class="text-center">Stock</th>
                <th class="text-center">Sales</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @if($products->count() > 0)
            @foreach($products as $prd)
                <tr>
                    <td>{{ $prd->kd_produk }}</td>
                    <td>{{ $prd->deskripsi }}</td>
                    @foreach($outlets as $loc)
                        <?php
                            $stock = $stocks->where('kd_produk', $prd->kd_produk)->where('kd_location', $loc->kd_location)->sum('qty');
                            $sales = $detailsales->where('kd_produk', $prd->kd_produk)->where('kd_location', $loc->kd_location)->sum('saleQty');
                        ?>
                        <th class="text-center">@if($stock <= $prd->minimal_stock) <strong style="color: red;">{{ $stock }}</strong> @else {{ $stock }} @endif</th>
                        <th class="text-center">{{ $sales }}</th>
                    @endforeach
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="{{ ($outlets->count()*2) + 2 }}">There is no stock data available</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
<script>
    $('#dtSummaryStock').DataTable();
</script>