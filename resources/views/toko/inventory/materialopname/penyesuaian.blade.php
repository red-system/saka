@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
        });
    </script>
@endsection

@section('title')
    Material Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Material Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('material-opname.index') }}">Material Opname</a></li>
        <li class="active">Adjustment</li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => ['material-opname.updatePenyesuaian', $mo->id], 'class' => 'jq-validate form-horizontal', 'method' => 'patch', 'novalidate']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Adjustment of Hospitalization Material</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                {!! Form::label('tgl_opname', 'Date', ['class' => 'col-xs-1']) !!}
                                <div class="col-xs-11">
                                    <p>{{ $mo->tgl_opname }}</p>
                                </div>
                            </div>

                            <div class="row">
                                {!! Form::label('pelaksana', 'Doer', ['class' => 'col-xs-1']) !!}
                                <div class="col-xs-11">
                                    <p>{{ $mo->pelaksana }}</p>
                                </div>
                            </div>

                            <div class="row">
                                {!! Form::label('status', 'Status', ['class' => 'col-xs-1']) !!}
                                <div class="col-xs-11">
                                    <p>{{ $mo->human_status }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="tbItemProduksi">
                                <thead>
                                <tr>
                                    <th>Material Code</th>
                                    <th>Material Name</th>
                                    <th>Category</th>
                                    <th>Type Material</th>
                                    <th>Unit</th>
                                    <th class="text-center">Data Qty</th>
                                    <th class="text-center">Qty Real</th>
                                    <th class="text-center col-md-2">Qty Adjustment</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($detailMO as $dmo)
                                    @if(!empty($dmo->material))
                                        <tr @if($dmo->material->qty != $dmo->qty_so)style="background-color: #F44336; color: #fff;"@endif>
                                            <td style="vertical-align: middle;">{{ $dmo->material->kd_material }}</td>
                                            <td style="vertical-align: middle;">{{ $dmo->material->name }}</td>
                                            <td style="vertical-align: middle;">{{ $dmo->material->kategori->kategori }}</td>
                                            <td style="vertical-align: middle;">{{ $dmo->material->human_type }}</td>
                                            <td style="vertical-align: middle;">{{ $dmo->material->satuan->satuan }}</td>
                                            <td style="vertical-align: middle;" class="text-center">{{ $dmo->material->qty }}</td>
                                            <td style="vertical-align: middle;" class="text-center">{{ $dmo->qty_so }}</td>
                                            <td class="text-center">
                                                {{ Form::number('stockPenyesuaian['.$dmo->id.']', $dmo->qty_so, ['class' => 'form-control', 'min' => 0]) }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <hr style="margin-top: 0px;">
                            <button type="submit" class="btn btn-success js-save-confirm">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection