@extends('layouts.master-toko')

@section('custom-css')
    @if($mo->status == 'unpublish')
        <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    @endif
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    @if($mo->status == 'unpublish')
        <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
        <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    @endif
@endsection

@section('custom-script')
    @if($mo->status == 'unpublish')
        <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
        <script>
            $(function () {
                $('.date-picker').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                });
            });
        </script>
    @endif
@endsection

@section('title')
    Material Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Material Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('material-opname.index') }}">Material Opname</a></li>
        <li class="active">Detail Opname</li>
    </ol>
@endsection

@section('content')
    @if($mo->status == 'unpublish')
        {!! Form::open(['route' => ['material-opname.update', $mo->id], 'class' => 'jq-validate form-horizontal', 'method' => 'patch', 'novalidate']) !!}
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Detail Material Opname</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {!! $errors->has('tgl_opname') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_opname', 'Date', ['class' => 'control-label col-xs-1']) !!}
                                @if($mo->status == 'unpublish')
                                    <div class="col-xs-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {{ Form::text('tgl_opname', $mo->tgl_opname, ['class' => 'form-control date-picker','required']) }}
                                        </div>
                                        {!! $errors->first('tgl_opname', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $mo->tgl_opname }}</p>
                                    </div>
                            @endif

                            <!-- /.input group -->
                            </div>

                            <div class="form-group {!! $errors->has('pelaksana') ? 'has-error' : '' !!}">
                                {!! Form::label('pelaksana', 'Doer', ['class' => 'control-label col-xs-1']) !!}
                                @if($mo->status == 'unpublish')
                                    <div class="col-xs-6">
                                        {{ Form::text('pelaksana', $mo->pelaksana, ['class' => 'form-control','required']) }}
                                        {!! $errors->first('pelaksana', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $mo->pelaksana }}</p>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group {!! $errors->has('status') ? 'has-error' : '' !!}">
                                {!! Form::label('status', 'Status', ['class' => 'control-label col-xs-1']) !!}
                                @if($mo->status == 'unpublish')
                                    <div class="col-xs-6">
                                        <label class="radio-inline">
                                            {!! Form::radio('status', 'unpublish', $mo->status == 'unpublish' ? true : false) !!} Unpublish
                                        </label>
                                        <label class="radio-inline">
                                            {!! Form::radio('status', 'publish', $mo->status == 'publish' ? true : false) !!} Publish
                                        </label>
                                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $mo->human_status }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Material Code</th>
                                    <th>Material Name</th>
                                    <th>Category</th>
                                    <th>Type Material</th>
                                    <th>Unit</th>
                                    <th class="text-center col-md-2">Qty</th>
                                    @if($mo->status == 'finish')
                                        <th class="text-center">Qty Adjustment</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($materials as $mt)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $mt->kd_material }}</td>
                                        <td style="vertical-align: middle;">{{ $mt->name }}</td>
                                        <td style="vertical-align: middle;">{{ $mt->kategori->kategori }}</td>
                                        <td style="vertical-align: middle;">{{ $mt->human_type }}</td>
                                        <td style="vertical-align: middle;">{{ $mt->satuan->satuan }}</td>
                                        <td style="vertical-align: middle;" class="text-center">
                                            <?php $dtlMo = \App\Models\DetailMaterialOpname::where('materialopname_id', $mo->id)->where('material_id', $mt->id)->first(); ?>
                                            @if($mo->status == 'unpublish')
                                                {{ Form::number('stockQty['.$mt->id.']', !empty($dtlMo) ? $dtlMo->qty_so : null, ['class' => 'form-control', 'min' => 0]) }}
                                            @else
                                                {{ !empty($dtlMo) ? $dtlMo->qty_so : '' }}
                                            @endif
                                        </td>
                                        @if($mo->status == 'finish')
                                            <td class="text-center">{{ !empty($dtlMo) ? $dtlMo->qty_penyesuaian : '' }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if($mo->status == 'unpublish')
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <hr style="margin-top: 0px;">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if($mo->status == 'unpublish')
        {!! Form::close() !!}
    @endif
@endsection