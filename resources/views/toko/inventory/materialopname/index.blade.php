@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/css/buttons.bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterKategori {
            display: inline;
        }

        .filterTanggal {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }

        .filterTgl {
            display: inline;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            var tableSO = $('#tbSO').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 3,
                    orderable: false
                }],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f<"filterTgl">><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            $('#fltTgl').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
            $("#filterTgl").appendTo(".filterTgl");

            $('#fltTgl').on('change', function () {
                tableSO.columns(0).search(this.value).draw();
            });

            var allowFilter = ['tbHistorySO'];
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    if ($.inArray(settings.nTable.getAttribute('id'), allowFilter ) == -1 )
                    {
                        return true;
                    }
                    var from = $('#fltFrom').datepicker("getDate");
                    var to = $('#fltTo').datepicker("getDate");
                    var startDate = new Date(data[0]).setHours(0,0,0,0);
                    if (from == null && to == null) { return true; }
                    if (from == null && startDate <= to) { return true;}
                    if(to == null && startDate >= from) {return true;}
                    if (startDate <= to && startDate >= from) { return true; }
                    return false;
                }
            );
            var tbHistorySO = $('#tbHistorySO').DataTable({
                responsive: true,
                order: [0, 'desc'],
                dom: '<"datatable-header"<"row"<"col-md-9 col-sm-8 col-xs-12"f<"filterTanggal">B><"col-md-3 col-sm-4 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
                buttons: [
                    'excel',
                    {
                        extend: 'pdf',
                        customize: function (doc) {
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'print'
                ]
            });
            $("#filterTanggal").appendTo(".filterTanggal");
            $(".dt-buttons").find('.btn').addClass('btn-sm');

            $('#fltFrom, #fltTo').change(function () {
                tbHistorySO.draw();
            });
        })
    </script>
@endsection

@section('title')
    Stock Opname Material - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Opname Material
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Stock Opname Material</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#materialopname" data-toggle="tab">Stock Opname Material</a></li>
                    <li><a href="#historyso" data-toggle="tab">History SO Material</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="materialopname">
                        <a class="btn btn-social btn-google" href="{{ route('material-opname.create') }}" style="margin-top: 10px;">
                            <i class="fa fa-plus"></i> Create Opname
                        </a>
                        <hr>
                        <span id="filterTgl" for="filterTgl" style="display: inherit;">
                            <span style="margin: 8px 5px 8px 15px;">Tgl:</span>
                            <span style="width: 220px;">
                                <input name="fltTgl" id="fltTgl" class="form-control input-sm input-inline" type="text">
                            </span>
                        </span>
                        <table id="tbSO" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="col-md-2">Opname Date</th>
                                <th>Doer</th>
                                <th>Status</th>
                                <th class="text-center all">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($materialopname as $so)
                                <tr>
                                    <td>{{ $so->tgl_opname }}</td>
                                    <td>{{ $so->pelaksana }}</td>
                                    <td>{!! $so->human_status !!}</td>
                                    <td class="text-center">
                                        <a href="{{ route('material-opname.detail', $so->id) }}" class="btn btn-sm btn-warning">Detail</a>

                                        @if($so->status == 'unpublish')
                                            {!! Form::model($so, ['route' => ['material-opname.cancel', $so->id], 'method' => 'delete', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <button type="button" class="btn btn-sm btn-danger js-cancel-confirm">Cancel</button>
                                            {!! Form::close()!!}
                                        @elseif($so->status == 'publish')
                                            <a href="{{ route('material-opname.penyesuaian', $so->id) }}" class="btn btn-sm btn-info">Adjustment</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="historyso">
                        <div id="filterTanggal" for="filterTanggal" style="display: inherit;">
                            <span style="margin: 8px 5px 8px 15px;">Tgl:</span>
                            <div class="input-group" style="width: 220px;">
                                <input name="fltFrom" id="fltFrom" class="form-control input-sm input-inline datepicker" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">S/D</button>
                                </span>
                                <input name="fltTo" id="fltTo" class="form-control input-sm input-inline datepicker" type="text">
                            </div>
                        </div>
                        <table id="tbHistorySO" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Code</th>
                                <th>Material name</th>
                                <th class="none">Category</th>
                                <th class="none">Type Material</th>
                                <th>Satuan</th>
                                <th class="none">Supplier</th>
                                <th class="text-center">First Stok</th>
                                <th class="text-center">Last Stock</th>
                                <th>Doer</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historymo as $hso)
                                <tr>
                                    <td>{{ date('Y-m-d', strtotime($hso->tgl)) }}</td>
                                    <td>{{ $hso->kode }}</td>
                                    <td>{{ $hso->nama_material }}</td>
                                    <td>{{ $hso->kategori }}</td>
                                    <td>{{ $hso->type_material }}</td>
                                    <td>{{ $hso->satuan }}</td>
                                    <td>{{ $hso->supplier }}</td>
                                    <td class="text-center">{{ $hso->qty_awal }}</td>
                                    <td class="text-center">{{ $hso->qty_akhir }}</td>
                                    <td>{!! $hso->pelaksana !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection