@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function searchMaterial(elem) {
            $('button.lastBtnSearchClicked').removeClass('lastBtnSearchClicked');
            $(elem).addClass('lastBtnSearchClicked');
            $('#listSearchMaterial').modal("show");
        }

        function selectMaterial(kdMaterial) {
            $.post("{{ route('material.detailmaterial') }}", {kdMaterial: kdMaterial, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                if (result.success) {
                    var inputKdMaterial = $('.lastBtnSearchClicked').parent().siblings('input[name="kd_material"]');
                    var inputNamaMaterial = inputKdMaterial.parent().parent().siblings('.namaMaterial').find('input[name="name"]');
                    var inputKatMaterial = inputKdMaterial.parent().parent().siblings('.katMaterial').find('select[name="kd_kat"]');
                    var inputTypeMaterial = inputKdMaterial.parent().parent().siblings('.typeMaterial').find('select[name="type_material"]');
                    var inputMinStockMaterial = inputKdMaterial.parent().parent().siblings('.minStockMaterial').find('input[name="minimal_stock"]');
                    var inputPriceMaterial = inputKdMaterial.parent().parent().siblings('.priceMaterial').find('input[name="harga"]');
                    var inputSatuanMaterial = inputKdMaterial.parent().parent().siblings('.satuanMaterial').find('select[name="kd_satuan"]');
                    var inputSupplierMaterial = inputKdMaterial.parent().parent().siblings('.supplierMaterial').find('select[name="kd_supplier"]');

                    var material = result.material;
                    inputKdMaterial.val(material.kd_material).trigger('change');
                    inputNamaMaterial.val(material.name).trigger('change');
                    inputKatMaterial.val(material.kd_kat).trigger('change');
                    inputTypeMaterial.val(material.type_material).trigger('change');
                    inputMinStockMaterial.val(material.minimal_stock).trigger('change');
                    inputPriceMaterial.val(material.harga).trigger('change');
                    inputSatuanMaterial.val(material.kd_satuan).trigger('change');
                    inputSupplierMaterial.val(material.kd_supplier).trigger('change');

                    inputNamaMaterial.prop('readonly', true);
                    inputKatMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputTypeMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSatuanMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSupplierMaterial.find('option:not(:selected)').prop('disabled', true);

                    $('#listSearchMaterial').modal("hide");
                    $('button.lastBtnSearchClicked').removeClass('lastBtnSearchClicked');
                }
            });
        }

        function handleKeyupKdMaterial(elem) {
            var kdMaterial = $(elem).val();
            $.post("{{ route('material.detailmaterial') }}", {kdMaterial: kdMaterial, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                var inputNamaMaterial = $(elem).parent().parent().siblings('.namaMaterial').find('input[name="name"]');
                var inputKatMaterial = $(elem).parent().parent().siblings('.katMaterial').find('select[name="kd_kat"]');
                var inputTypeMaterial = $(elem).parent().parent().siblings('.typeMaterial').find('select[name="type_material"]');
                var inputMinStockMaterial = $(elem).parent().parent().siblings('.minStockMaterial').find('input[name="minimal_stock"]');
                var inputPriceMaterial = $(elem).parent().parent().siblings('.priceMaterial').find('input[name="harga"]');
                var inputSatuanMaterial = $(elem).parent().parent().siblings('.satuanMaterial').find('select[name="kd_satuan"]');
                var inputSupplierMaterial = $(elem).parent().parent().siblings('.supplierMaterial').find('select[name="kd_supplier"]');

                if (result.success) {
                    var material = result.material;
                    inputNamaMaterial.val(material.name).trigger('change');
                    inputKatMaterial.val(material.kd_kat).trigger('change');
                    inputTypeMaterial.val(material.type_material).trigger('change');
                    inputMinStockMaterial.val(material.minimal_stock).trigger('change');
                    inputPriceMaterial.val(material.harga).trigger('change');
                    inputSatuanMaterial.val(material.kd_satuan).trigger('change');
                    inputSupplierMaterial.val(material.kd_supplier).trigger('change');

                    inputNamaMaterial.prop('readonly', true);
                    inputKatMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputTypeMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSatuanMaterial.find('option:not(:selected)').prop('disabled', true);
                    inputSupplierMaterial.find('option:not(:selected)').prop('disabled', true);
                } else {
                    inputNamaMaterial.prop('readonly', false);
                    inputKatMaterial.find('option').prop('disabled', false);
                    inputTypeMaterial.find('option').prop('disabled', false);
                    inputSatuanMaterial.find('option').prop('disabled', false);
                    inputSupplierMaterial.find('option').prop('disabled', false);
                }
            });
        }

        $(function () {
            $('#tbMaterial').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 8,
                    orderable: false
                } ]
            });

            $('.select2').select2();
            $('.currency').number(true, 2);
        })
    </script>
@endsection

@section('title')
    Material - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Material
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Material</li>
    </ol>
@endsection

@section('content')
    <!-- /.col -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a class="btn btn-block btn-social btn-google" data-toggle="modal" data-target="#addMaterial">
                <i class="fa fa-plus"></i> Add Material
            </a>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Material</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbMaterial" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th class="none">Category</th>
                            <th class="none">Type Material</th>
                            <th class="text-center">Minimum Stok</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Unit</th>
                            <th>Price</th>
                            <th class="none">Supplier</th>
                            <th class="text-center all">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($materials as $material)
                            <tr>
                                <td>{{ $material->kd_material }}</td>
                                <td>{{ $material->name }}</td>
                                <td>{{ $material->kategori->kategori }}</td>
                                <td>{{ $material->human_type }}</td>
                                <td class="text-center">{{ number_format($material->minimal_stock, 0, ',', '.') }}</td>
                                <td class="text-center">{{ number_format($material->qty, 0, ',', '.') }}</td>
                                <td class="text-center">{{ $material->satuan->satuan }}</td>
                                <td>IDR {{ \Konversi::database_to_localcurrency($material->harga) }}</td>
                                <td>{{ $material->supplier->nama_supplier }}</td>
                                <td class="text-center">
                                    {!! Form::model($material, ['route' => ['material.destroy', $material->id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-sm btn-danger js-submit-confirm"><i class="fa fa-trash"></i></button>
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- MODALS -->
    <div class="modal fade" id="addMaterial">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Tambah Material</h4>
                </div>
                {!! Form::open(['route' => 'material.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('kd_material') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_material', 'Material Code', ['class' => 'control-label']) !!}
                                <div class="input-group">
                                    {{ Form::text('kd_material', null, ['class' => 'form-control','required', 'onkeyup' => 'handleKeyupKdMaterial(this)']) }}
                                    <span class="input-group-btn">
                                      <button type="button" onclick="searchMaterial(this)" class="btn btn-flat"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                {!! $errors->first('kd_material', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group namaMaterial {!! $errors->has('name') ? 'has-error' : '' !!}">
                                {!! Form::label('name', 'Name') !!}
                                {{ Form::text('name', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group katMaterial {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_kat', 'Material Category') !!}
                                {{ Form::select('kd_kat', ['' => '']+App\Models\Kategori::where('type_kat', 'material')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group typeMaterial {!! $errors->has('type_material') ? 'has-error' : '' !!}">
                                {!! Form::label('type_material', 'Type Material') !!}
                                {{ Form::select('type_material', ['' => '']+App\Models\Material::typeList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('type_material', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group minStockMaterial {!! $errors->has('minimal_stock') ? 'has-error' : '' !!}">
                                {!! Form::label('minimal_stock', 'Minimal Stock') !!}
                                {{ Form::number('minimal_stock', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('minimal_stock', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group priceMaterial {!! $errors->has('harga') ? 'has-error' : '' !!}">
                                {!! Form::label('harga', 'Price') !!}
                                {{ Form::text('harga', null, ['class' => 'form-control currency','required']) }}
                                {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group qtyMaterial {!! $errors->has('qty') ? 'has-error' : '' !!}">
                                {!! Form::label('qty', 'Qty') !!}
                                {{ Form::number('qty', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group satuanMaterial {!! $errors->has('kd_satuan') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_satuan', 'Unit') !!}
                                {{ Form::select('kd_satuan', ['' => '']+App\Models\Satuan::pluck('satuan', 'kd_satuan')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_satuan', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group supplierMaterial {!! $errors->has('kd_supplier') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_supplier', 'Supplier') !!}
                                {{ Form::select('kd_supplier', ['' => '']+App\Models\Supplier::pluck('nama_supplier', 'kd_supplier')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_supplier', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div id="editMaterial" class="modal fade" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="listSearchMaterial">
        <div class="modal-dialog modal-center modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Search Material</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tbListSearchMaterial" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Type Material</th>
                                    <th>Unit</th>
                                    <th>Supplier</th>
                                    <th class="text-center">Act</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($materials as $dmt)
                                    <tr>
                                        <td>{{ $dmt->kd_material }}</td>
                                        <td>{{ $dmt->name }}</td>
                                        <td>{{ $dmt->kategori->kategori }}</td>
                                        <td>{{ $dmt->human_type }}</td>
                                        <td class="text-center">{{ $material->satuan->satuan }}</td>
                                        <td>{{ $material->supplier->nama_supplier }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="selectMaterial('{{ $dmt->kd_material }}')">Pilih</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection