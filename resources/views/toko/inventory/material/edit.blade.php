<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Material</h4>
</div>
{!! Form::model($material, ['route' => ['material.update', $material->id], 'method' => 'patch', 'novalidate', 'class' => 'validate-form'])!!}
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-body">
                <div class="form-group {!! $errors->has('kd_material') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_material', 'Material Code', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        {{ Form::text('kd_material', null, ['class' => 'form-control','required', 'onkeyup' => 'handleKeyupKdMaterial(this)']) }}
                        <span class="input-group-btn">
                          <button type="button" onclick="searchMaterial(this)" class="btn btn-flat"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    {!! $errors->first('kd_material', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group namaMaterial {!! $errors->has('name') ? 'has-error' : '' !!}">
                    {!! Form::label('name', 'Name') !!}
                    {{ Form::text('name', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group katMaterial {!! $errors->has('kd_kat') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_kat', 'Material Category') !!}
                    {{ Form::select('kd_kat', ['' => '']+App\Models\Kategori::where('type_kat', 'material')->pluck('kategori', 'kd_kat')->all(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('kd_kat', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group typeMaterial {!! $errors->has('type_material') ? 'has-error' : '' !!}">
                    {!! Form::label('type_material', 'Type Material') !!}
                    {{ Form::select('type_material', ['' => '']+App\Models\Material::typeList(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('type_material', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group minStockMaterial {!! $errors->has('minimal_stock') ? 'has-error' : '' !!}">
                    {!! Form::label('minimal_stock', 'Minimal Stock') !!}
                    {{ Form::number('minimal_stock', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('minimal_stock', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group priceMaterial {!! $errors->has('harga') ? 'has-error' : '' !!}">
                    {!! Form::label('harga', 'Price') !!}
                    {{ Form::text('harga', \Konversi::database_to_localcurrency($material->harga), ['class' => 'form-control currency2','required']) }}
                    {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group qtyMaterial {!! $errors->has('qty') ? 'has-error' : '' !!}">
                    {!! Form::label('qty', 'Qty') !!}
                    {{ Form::number('qty', null, ['class' => 'form-control','required']) }}
                    {!! $errors->first('qty', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group satuanMaterial {!! $errors->has('kd_satuan') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_satuan', 'Unit') !!}
                    {{ Form::select('kd_satuan', ['' => '']+App\Models\Satuan::pluck('satuan', 'kd_satuan')->all(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('kd_satuan', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group supplierMaterial {!! $errors->has('kd_supplier') ? 'has-error' : '' !!}">
                    {!! Form::label('kd_supplier', 'Supplier') !!}
                    {{ Form::select('kd_supplier', ['' => '']+App\Models\Supplier::pluck('nama_supplier', 'kd_supplier')->all(), null, ['class' => 'form-control select2u','required', 'style' => 'width: 100%;']) }}
                    {!! $errors->first('kd_supplier', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    {!! Form::button('Save', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!}

<script>
    $('.select2u').select2();
    $('.currency2').number(true, 2);
    $(".validate-form").validate({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>