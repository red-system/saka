@extends($mlayout)

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        $(function () {
            $("#checkAll").click(function() {
                var checked = $(this).prop('checked');
                $('.produk').each(function(){
                    $(this).prop("checked", checked).trigger("change");
                });
            });

            $('.produk').change(function() {
                if ($('.produk:checked').length == $('.produk').length) {
                    $("#checkAll").prop("checked", true);
                } else {
                    $("#checkAll").prop("checked", false);
                }
            });
        });
    </script>
@endsection

@section('title')
    Transfer Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Transfer Stock
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('transfer-stok.index') }}">Transfer Stock</a></li>
        <li class="active">Detail Approval</li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => ['transfer-stok.storeapproval', $transferstok->id], 'class' => 'jq-validate form-horizontal', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Approval Transfer Stok</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="col-md-4">Date</td>
                                        <td>: {{ date('d F Y', strtotime($transferstok->tanggal)) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-4">From</td>
                                        <td>: {{ $transferstok->sumber_transfer ? $transferstok->sumber_transfer->location_name : $transferstok->dari }}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-4">Destination</td>
                                        <td>: {{ $transferstok->tujuan_transfer ? $transferstok->tujuan_transfer->location_name : $transferstok->tujuan }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="col-md-4">Doer</td>
                                        <td>: {{ $transferstok->pelaksana }}</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-4">Remark</td>
                                        <td>: {{ $transferstok->keterangan != '' ? $transferstok->keterangan : '-' }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <h4>Item Transfer</h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Code</th>
                                    <th>No Seri</th>
                                    <th>Product</th>
                                    <th class="text-center col-md-2">Qty Transfer</th>
                                    <th class="text-center" width="50px">
                                        <input type="checkbox" id="checkAll" data-set="#tbProduct .produk">
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($detailTransfer as $dtf)
                                    <tr>
                                        <td>{{ $dtf->kd_produk }}</td>
                                        <td>{{ $dtf->no_seri }}</td>
                                        <td>{{ $dtf->deskripsi }}</td>
                                        <td class="text-center">{{ $dtf->qty_transfer }}</td>
                                        <td class="text-center">
                                            @if($dtf->status == 'waiting-approval')
                                                {!! Form::checkbox('listtransfer[]', $dtf->id, false, ['class' => 'produk']) !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr style="margin-bottom: 10px;">
                            <div class="text-right">
                                {!! Form::button('Approve', ['type'=> 'submit', 'class'=>'btn btn-sm btn-success js-approve-confirm']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection