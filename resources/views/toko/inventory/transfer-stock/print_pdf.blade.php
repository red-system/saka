<html>
<head>
    <title>{{ 'Transfer Stok '.$transferstok->tanggal.' '.$transferstok->dari.' to '.$transferstok->tujuan }}</title>
    <style>
        @page { margin: 15px 30px; }

        body {
            font-family: Tahoma,Verdana,Segoe,sans-serif;
        }

        .lr-none {
            border-right: none;
            border-left: none;
        }

        .tb-grey {
            border-top: 0.5px solid #888;
            border-bottom: 0.5px solid #888;
        }

        .pd10 {
            padding-bottom: 7px;
            padding-top: 7px;
        }

        .pd8 {
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .left {
            text-align: left;
        }

        .right {
            text-align: right;
        }

        .vat {
            vertical-align: top;
        }

        .text-muted {
            color: #000000;
        }

        table > tr > td {
            font-family: Tahoma,Verdana,Segoe,sans-serif !important;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="7%" style="vertical-align: middle;">
            <img style="margin-top: 3px;" height="90px;" src="{{ asset('img/logo-saka.png') }}" alt="">
        </td>
        <td width="93%" style="vertical-align: top;">
            <table width="100%" style="margin-top: 10px; font-size: 11px;">
                <tr>
                    <td colspan="3"><strong style="font-size: 15px;">Transfer Stock</strong></td>
                </tr>
                <tr>
                    <td width="10%"><strong>Date</strong></td>
                    <td width="1%">:</td>
                    <td width="89%" style="text-align: left;">{{ date('d F Y', strtotime($transferstok->tanggal)) }}</td>
                </tr>
                <tr>
                    <td><strong>From</strong></td>
                    <td>:</td>
                    <td style="text-align: left;">{{ $transferstok->sumber_transfer->location_name }}</td>
                </tr>
                <tr>
                    <td><strong>Destination</strong></td>
                    <td>:</td>
                    <td style="text-align: left;">{{ $transferstok->tujuan_transfer->location_name }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" class="table" border="1" style="margin-top: 10px; font-size: 11px; border-collapse: collapse; border-bottom: none; border-top: none; border-left: none; border-right: none;">
    <tr>
        <th class="lr-none tb-grey pd10 center" width="5%">No</th>
        <th class="lr-none tb-grey pd10 left" width="20%">Product Code</th>
        <th class="lr-none tb-grey pd10 left" width="20%">No Seri</th>
        <th class="lr-none tb-grey pd10 left" width="40%">Product</th>
        <th class="lr-none tb-grey pd10 left" width="40%">Price</th>
        <th class="lr-none tb-grey pd10 center" width="15%">Qty</th>
        <th class="lr-none tb-grey pd10 center" width="15%">Subtotal</th>
    </tr>
    <?php $no = 1; ?>
    <?php $totalQtyTransfer=0; $grandTotal=0;?>
    @foreach($detailTransfer as $dtf)
        <tr>
            <td class="lr-none tb-grey pd8 center vat">
                {{ $no }}
            </td>
            <td class="lr-none tb-grey pd8 vat">
                <span>{{ $dtf->kd_produk }}</span>
            </td>
            <td class="lr-none tb-grey pd8 vat">
                <span>{{ $dtf->no_seri }}</span>
            </td>
            <td class="lr-none tb-grey pd8 vat">
                <span>{{ $dtf->deskripsi }}</span>
            </td>
            <td class="lr-none tb-grey pd8 vat">
                <span>{{ number_format($dtf->price, 2, "," ,".") }}</span>
            </td>
            <td class="lr-none tb-grey pd8 center vat">
                {{ $dtf->qty_transfer }}
            </td>
            <td class="lr-none tb-grey pd8 center vat">
                {{ number_format(($dtf->qty_transfer *  $dtf->price), 2, "," ,".") }}
            </td>
        </tr>
        <?php $no++; ?>
        <?php
            $totalQtyTransfer=$totalQtyTransfer+$dtf->qty_transfer;
            $grandTotal=$grandTotal+($dtf->qty_transfer * $dtf->price);
        ?>
    @endforeach
    <tr>
        <td class="lr-none tb-grey pd8 center vat"></td>
        <td class="lr-none tb-grey pd8 center vat"></td>
        <td class="lr-none tb-grey pd8 center vat"></td>
        <td class="lr-none tb-grey pd8 center vat"></td>
        <td class="lr-none tb-grey pd8 center vat">Total Item</td>
        <td class="lr-none tb-grey pd8 center vat">
            {{$totalQtyTransfer}}
        </td>
        <td class="lr-none tb-grey pd8 center vat">
            {{number_format($grandTotal, 2, "," ,".")}}
        </td>
    </tr>
</table>

@if($transferstok->keterangan != '')
    <table width="100%" style="margin-top: 15px;">
        <tr>
            <td style="font-size: 12px;">Remark :</td>
        </tr>
        <tr>
            <td style="font-size: 12px;">{!! $transferstok->keterangan !!}</td>
        </tr>
    </table>
@endif

<table width="100%" style="margin-top: 20px;">
    <tr>
        <td style="font-size: 12px;" width="33%"></td>
        <td style="font-size: 12px; text-align: center;" width="33%">
            <p>Sender,</p>
            <br><br><br><br>
            ---------------------------------------
        </td>
        <td style="font-size: 12px; text-align: center;" width="33%">
            <p>Receiver,</p>
            <br><br><br><br>
            ---------------------------------------
        </td>
    </tr>
</table>
</body>
</html>