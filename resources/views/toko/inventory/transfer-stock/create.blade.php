@extends($mlayout)

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
        });
    </script>
@endsection

@section('title')
    Transfer Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Transfer Stock
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('transfer-stok.index') }}">Transfer Stock</a></li>
        <li class="active">Add Transfer</li>
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => 'transfer-stok.readimport', 'class' => 'jq-validate form-horizontal', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Transfer</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group">
                                {!! Form::label('dari', 'Form', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    <p>{{ auth()->user()->location->location_name }}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('data_product', 'Data Product', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    <a class="btn btn-info" href="{{ route('transfer-stok.downloadstock') }}">Download Stock</a>
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('file_import') ? 'has-error' : '' !!}">
                                {!! Form::label('file_import', 'File Import', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    {!! Form::file('file_import') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin-top: 0px;">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-6">
                            <button type="submit" class="btn btn-success">Read Excel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection