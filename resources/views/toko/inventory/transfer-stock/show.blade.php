<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail Transfer Stock</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Date</td>
                        <td>: {{ date('d F Y', strtotime($transferstok->tanggal)) }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">From</td>
                        <td>: {{ $transferstok->sumber_transfer ? $transferstok->sumber_transfer->location_name : $transferstok->dari }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Destination</td>
                        <td>: {{ $transferstok->tujuan_transfer ? $transferstok->tujuan_transfer->location_name : $transferstok->tujuan }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Doer</td>
                        <td>: {{ $transferstok->pelaksana }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Remark</td>
                        <td>: {{ $transferstok->keterangan != '' ? $transferstok->keterangan : '-' }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <h4>Item Transfer</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Product Code</th>
                    <th>No Seri</th>
                    <th>Product</th>
                    <th>Price</th>
                    <th class="text-center col-md-1">Qty Stock</th>
                    <th class="text-center col-md-1">Qty Transfer</th>
                    <th class="text-center col-md-1">Subtotal</th>
                    @if(in_array($transferstok->status, ['on_progress', 'finish']))
                        <th class="col-md-2">Status</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                <?php $totalQtyTransfer=0; $grandTotal=0;?>
                @foreach($detailTransfer as $dtf)
                    <tr>
                        <td>{{ $dtf->kd_produk }}</td>
                        <td>{{ $dtf->no_seri }}</td>
                        <td>{{ $dtf->deskripsi }}</td>
                        <td>{{ number_format($dtf->price , 2, "," ,".")}}</td>
                        <td class="text-center">{{ $dtf->qty_stock }}</td>
                        <td class="text-center">{{ $dtf->qty_transfer }}</td>
                        <td class="text-center">{{ number_format(($dtf->qty_transfer * $dtf->price) , 2, "," ,".")}}</td>
                        @if(in_array($transferstok->status, ['on_progress', 'finish']))
                            <td>{{ $dtf->human_status }}</td>
                        @endif
                    </tr>
                    
                    <?php
                    $totalQtyTransfer=$totalQtyTransfer+$dtf->qty_transfer;
                    $grandTotal=$grandTotal+($dtf->qty_transfer * $dtf->price);
                    ?>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-center">Total Item</td>
                    <td></td>
                    <td class="text-center">
                        {{$totalQtyTransfer}}
                    </td>
                    <td class="text-center">
                        {{number_format($grandTotal, 2, "," ,".")}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>
