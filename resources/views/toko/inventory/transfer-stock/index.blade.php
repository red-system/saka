@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/css/buttons.bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterKategori {
            display: inline;
        }

        .filterTanggal {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/Buttons-1.5.4/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/JSZip-2.5.0/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        $(function () {
            var tbTransfer = $('#tbTransfer').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 5,
                    orderable: false
                }],
                dom: '<"datatable-header"<"row"<"col-md-6 col-sm-6 col-xs-12"f><"col-md-6 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            var tbApproval = $('#tbApproval').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 5,
                    orderable: false
                }],
                dom: '<"datatable-header"<"row"<"col-md-6 col-sm-6 col-xs-12"f><"col-md-6 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            $('.select2').select2();

            var allowFilter = ['tbHistoryTransfer'];
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    if ($.inArray(settings.nTable.getAttribute('id'), allowFilter ) == -1 )
                    {
                        return true;
                    }
                    var from = $('#fltFrom').datepicker("getDate");
                    var to = $('#fltTo').datepicker("getDate");
                    var startDate = new Date(data[0]).setHours(0,0,0,0);
                    if (from == null && to == null) { return true; }
                    if (from == null && startDate <= to) { return true;}
                    if(to == null && startDate >= from) {return true;}
                    if (startDate <= to && startDate >= from) { return true; }
                    return false;
                }
            );

            var tbHistoryTransfer = $('#tbHistoryTransfer').DataTable({
                responsive: true,
                order: [0, 'desc'],
                dom: '<"datatable-header"<"row"<"col-md-9 col-sm-8 col-xs-12"f<"filterTanggal">B><"col-md-3 col-sm-4 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
                buttons: [
                    'excel',
                    {
                        extend: 'pdf',
                        customize: function (doc) {
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        }
                    },
                    'print'
                ]
            });
            $("#filterTanggal").appendTo(".filterTanggal");
            $(".dt-buttons").find('.btn').addClass('btn-sm');

            $('#fltFrom, #fltTo').change(function () {
                tbHistoryTransfer.draw();
            });
        })
    </script>
@endsection

@section('title')
    Transfer Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Transfer Stock
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Transfer Stock</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#transferstok" data-toggle="tab">Transfer Stok</a></li>
                    <li><a href="#approvaltransfer" data-toggle="tab">Approval Transfer</a></li>
                    <li><a href="#historytransfer" data-toggle="tab">History Transfer</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="transferstok">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <a class="btn btn-block btn-social btn-google" href="{{ route('transfer-stok.create') }}">
                                    <i class="fa fa-plus"></i> Add Transfer
                                </a>
                            </div>
                        </div>
                        <hr>
                        <table id="tbTransfer" class="table table-bordered table-striped" width="100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Destination</th>
                                <th>Doer</th>
                                <th class="text-center">Status</th>
                                <th class="none">Remark</th>
                                <th class="text-center all">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transferStock as $tf)
                                <tr>
                                    <td>{{ $tf->tanggal }}</td>
                                    <td>{!! $tf->tujuan_transfer ? $tf->tujuan_transfer->location_name : $tf->tujuan !!}</td>
                                    <td>{{ $tf->pelaksana }}</td>
                                    <td>{{ $tf->human_status }}</td>
                                    <td>{{ $tf->keterangan }}</td>
                                    <td class="text-center">
                                        @if($tf->status == 'draft')
                                            <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                <a href="{{ route('transfer-stok.edit', $tf->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                            </span>
                                            {!! Form::model($tf, ['route' => ['transfer-stok.publish', $tf->id], 'method' => 'post', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Publish">
                                                <button type="button" class="btn btn-sm btn-warning js-publish-confirm"><i class="fa fa-thumbs-o-up"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                        <span data-toggle="tooltip" data-placement="top" title="Detail Transfer">
                                            <a href="{{ route('transfer-stok.show', $tf->id) }}" data-target="#viewDetailTransfer" data-toggle="modal" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                        </span>
                                        @if($tf->status == 'waiting-approval')
                                            <span data-toggle="tooltip" data-placement="top" title="Print">
                                                <a href="{{ route('transfer-stok.print', $tf->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-print"></i></a>
                                            </span>
                                        @endif
                                        @if(in_array($tf->status, ['draft', 'waiting-approval']))
                                            {!! Form::model($tf, ['route' => ['transfer-stok.destroy', $tf->id], 'method' => 'delete', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Cancel">
                                                <button type="button" class="btn btn-sm btn-danger js-cancel-confirm"><i class="fa fa-times"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="approvaltransfer">
                        <table id="tbApproval" class="table table-bordered table-striped" width="100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>From</th>
                                <th>Doer</th>
                                <th class="text-center">Status</th>
                                <th class="none">Remark</th>
                                <th class="text-center all">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($approvalTransfer as $atf)
                                <tr>
                                    <td>{{ $atf->tanggal }}</td>
                                    <td>{!! $atf->sumber_transfer ? $atf->sumber_transfer->location_name : $atf->dari !!}</td>
                                    <td>{{ $atf->pelaksana }}</td>
                                    <td>{{ $atf->human_status }}</td>
                                    <td>{{ $atf->keterangan }}</td>
                                    <td class="text-center">
                                        <span data-toggle="tooltip" data-placement="top" title="Detail Transfer">
                                            <a href="{{ route('transfer-stok.detailapproval', $atf->id) }}" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                        </span>
                                        @if($atf->status == 'on_progress')
                                            {!! Form::model($atf, ['route' => ['transfer-stok.setfinish', $atf->id], 'method' => 'post', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Set Finish">
                                                <button type="button" class="btn btn-sm btn-success js-confirm-finish"><i class="fa fa-check"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="historytransfer">
                        <div id="filterTanggal" for="filterTanggal" style="display: inherit;">
                            <span style="margin: 8px 5px 8px 15px;">Date:</span>
                            <div class="input-group" style="width: 220px;">
                                <input name="fltFrom" id="fltFrom" class="form-control input-sm input-inline datepicker" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">S/D</button>
                                </span>
                                <input name="fltTo" id="fltTo" class="form-control input-sm input-inline datepicker" type="text">
                            </div>
                        </div>
                        <table id="tbHistoryTransfer" class="table table-bordered table-striped" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Product Code</th>
                                <th>No Seri</th>
                                <th>Description</th>
                                <th>Form</th>
                                <th>Destination</th>
                                <th class="text-center">Stock Transfer</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historytransfer as $ht)
                            <tr>
                                <td>{{ date('Y-m-d', strtotime($ht->tgl)) }}</td>
                                <td>{{ $ht->kode }}</td>
                                <td>{{ $ht->no_seri }}</td>
                                <td>{{ $ht->deskripsi }}</td>
                                <td>{{ $ht->dari }}</td>
                                <td>{{ $ht->tujuan }}</td>
                                <td class="text-center">{{ $ht->qty_transfer }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="viewDetailTransfer" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-full">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection