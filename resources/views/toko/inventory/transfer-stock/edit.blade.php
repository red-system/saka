@extends($mlayout)

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('.select2').select2();
        });
    </script>
@endsection

@section('title')
    Transfer Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Transfer Stock
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li>Inventory</li>
        <li><a href="{{ route('transfer-stok.index') }}">Transfer Stock</a></li>
        <li class="active">Edit Transfer</li>
    </ol>
@endsection

@section('content')
    {!! Form::model($transferstok, ['route' => ['transfer-stok.update', $transferstok->id], 'class' => 'jq-validate', 'method' => 'patch', 'novalidate']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group {!! $errors->has('tanggal') ? 'has-error' : '' !!}">
                                {!! Form::label('tanggal', 'Date', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('tanggal', $transferstok->tanggal, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('dari', 'From', ['class' => 'control-label']) !!}
                                {{ Form::text('dari', auth()->user()->location->location_name, ['class' => 'form-control','required', 'readonly']) }}
                            </div>
                            <div class="form-group {!! $errors->has('tujuan') ? 'has-error' : '' !!}">
                                {!! Form::label('tujuan', 'Destination', ['class' => 'control-label']) !!}
                                {{ Form::select('tujuan', ['' => '']+App\Models\Location::where('kd_location', '!=', auth()->user()->kd_location)->pluck('location_name', 'kd_location')->all(), $transferstok->tujuan, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('tujuan', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group {!! $errors->has('pelaksana') ? 'has-error' : '' !!}">
                                {!! Form::label('pelaksana', 'Doer', ['class' => 'control-label']) !!}
                                {{ Form::text('pelaksana', auth()->user()->name, ['class' => 'form-control','readonly', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('pelaksana', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('keterangan') ? 'has-error' : '' !!}">
                                {!! Form::label('keterangan', 'Remark', ['class' => 'control-label']) !!}
                                {{ Form::textarea('keterangan', $transferstok->keterangan, ['class' => 'form-control','rows' => 4]) }}
                                {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Product List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbProductStock">
                        <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>No Seri</th>
                            <th>Product</th>
                            <th class="text-center col-md-2">Qty Stock</th>
                            <th class="text-center col-md-2">Qty Transfer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($detailTransfer as $dtf)
                            <tr>
                                <td>
                                    {{ Form::text('kdProduk['.$dtf->id.']', $dtf->kd_produk, ['class' => 'form-control', 'readonly']) }}
                                </td>
                                <td>
                                    {{ Form::text('noSeriProduk['.$dtf->id.']', $dtf->no_seri, ['class' => 'form-control', 'readonly']) }}
                                </td>
                                <td>
                                    {{ Form::text('descProduk['.$dtf->id.']', $dtf->deskripsi, ['class' => 'form-control', 'readonly']) }}
                                </td>
                                <td class="text-center col-md-2">
                                    {{ Form::text('qtyStock['.$dtf->id.']', $dtf->qty_stock, ['class' => 'form-control', 'readonly']) }}
                                </td>
                                <td class="text-center col-md-2">
                                    {{ Form::number('qtyTransfer['.$dtf->id.']', $dtf->qty_transfer, ['class' => 'form-control', 'min' => 0]) }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-8 col-md-4 col-xs-12">
            <div class="col-xs-6" style="padding: 3px;">
                <a href="{{ route('transfer-stok.index') }}" class="btn btn-block btn-default">Cancel</a>
            </div>
            <div class="col-xs-6" style="padding: 3px;">
                <button type="submit" class="btn btn-block btn-success">Save</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection