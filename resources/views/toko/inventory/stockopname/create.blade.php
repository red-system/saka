@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
@endsection

@section('title')
    Stock Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li><a href="{{ route('stock-opname.location', $location->kd_location) }}">{{ $location->location_name }}</a></li>
            <li class="active">Create Opname</li>
        @else
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li class="active">Create Opname</li>
        @endif
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => 'stock-opname.readimport', 'class' => 'jq-validate form-horizontal', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Create Stock Opname ({{ $location->location_name }})</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group">
                                {!! Form::label('lokasi', 'Location', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    <p>{{ $location->location_name }}</p>
                                    {{ Form::hidden('location', $location->kd_location) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('data_product', 'Data Produk', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    <a class="btn btn-info" href="{{ route('stock-opname.downloadstock', $location->kd_location) }}">Download Stock</a>
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('file_import') ? 'has-error' : '' !!}">
                                {!! Form::label('file_import', 'File Import', ['class' => 'col-xs-2']) !!}
                                <div class="col-xs-6">
                                    {!! Form::file('file_import') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="margin-top: 0px;">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-6">
                            <button type="submit" class="btn btn-success">Read Excel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection