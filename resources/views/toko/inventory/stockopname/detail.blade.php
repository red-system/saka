@extends('layouts.master-toko')

@section('custom-css')
    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    @endif
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    @endif
@endsection

@section('custom-script')
    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
        });
    </script>
    @endif
@endsection

@section('title')
    Stock Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li><a href="{{ route('stock-opname.location', $so->kd_location) }}">{{ $so->location->location_name }}</a></li>
            <li class="active">Detail Opname</li>
        @else
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li class="active">Detail Opname</li>
        @endif
    </ol>
@endsection

@section('content')
    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
    {!! Form::open(['route' => ['stock-opname.update', $so->id], 'class' => 'jq-validate form-horizontal', 'method' => 'patch', 'novalidate']) !!}
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Detail Stock Opname ({{ $so->location->location_name }})</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group">
                                {!! Form::label('lokasi', 'Location', ['class' => 'col-xs-1']) !!}
                                <div class="col-xs-11">
                                    <p>{{ $so->location->location_name }}</p>
                                </div>
                            </div>

                            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                                {{ Form::hidden('location', $so->kd_location) }}
                            @endif

                            <div class="form-group {!! $errors->has('tgl_opname') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_opname', 'Date', ['class' => 'control-label col-xs-1']) !!}
                                @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
                                    <div class="col-xs-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {{ Form::text('tgl_opname', $so->tgl_opname, ['class' => 'form-control date-picker','required']) }}
                                        </div>
                                        {!! $errors->first('tgl_opname', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $so->tgl_opname }}</p>
                                    </div>
                                @endif

                                <!-- /.input group -->
                            </div>

                            <div class="form-group {!! $errors->has('pelaksana') ? 'has-error' : '' !!}">
                                {!! Form::label('pelaksana', 'Doer', ['class' => 'control-label col-xs-1']) !!}
                                @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
                                    <div class="col-xs-6">
                                        {{ Form::text('pelaksana', $so->pelaksana, ['class' => 'form-control','required']) }}
                                        {!! $errors->first('pelaksana', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $so->pelaksana }}</p>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group {!! $errors->has('status') ? 'has-error' : '' !!}">
                                {!! Form::label('status', 'Status', ['class' => 'control-label col-xs-1']) !!}
                                @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
                                    <div class="col-xs-6">
                                        <label class="radio-inline">
                                            {!! Form::radio('status', 'unpublish', $so->status == 'unpublish' ? true : false) !!} Unpublish
                                        </label>
                                        <label class="radio-inline">
                                            {!! Form::radio('status', 'publish', $so->status == 'publish' ? true : false) !!} Publish
                                        </label>
                                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                    </div>
                                @else
                                    <div class="col-xs-11">
                                        <p>{{ $so->human_status }}</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="tbItemProduksi">
                                <thead>
                                <tr>
                                    <th>Produk Code</th>
                                    <th>Nama Produk</th>
                                    <th>No Seri</th>
                                    <th class="text-center col-md-2">Qty</th>
                                    @if($so->status == 'finish')
                                        <th class="text-center">Qty Adjustment</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datastock as $ds)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $ds->kd_produk }}</td>
                                        <td style="vertical-align: middle;">{{ $ds->product->deskripsi }}</td>
                                        <td style="vertical-align: middle;">{{ $ds->no_seri_produk }}</td>
                                        <td style="vertical-align: middle;" class="text-center">
                                            <?php $dtlSo = \App\Models\DetailStockOpname::where('stockopname_id', $so->id)->where('stock_id', $ds->id)->first(); ?>
                                            @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
                                                {{ Form::number('stockQty['.$ds->id.']', !empty($dtlSo) ? $dtlSo->qty_so : null, ['class' => 'form-control', 'min' => 0]) }}
                                            @else
                                                {{ !empty($dtlSo) ? $dtlSo->qty_so : '' }}
                                            @endif
                                        </td>
                                        @if($so->status == 'finish')
                                            <td class="text-center">{{ !empty($dtlSo) ? $dtlSo->qty_penyesuaian : '' }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <hr style="margin-top: 0px;">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if($so->status == 'unpublish' && $so->kd_location == auth()->user()->kd_location)
    {!! Form::close() !!}
    @endif
@endsection