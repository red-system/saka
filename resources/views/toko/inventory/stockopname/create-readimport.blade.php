@extends('layouts.master-toko')

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        .form-horizontal .control-label {
            text-align: left;
        }
    </style>
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
        });
    </script>
@endsection

@section('title')
    Stock Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li><a href="{{ route('stock-opname.location', $location->kd_location) }}">{{ $location->location_name }}</a></li>
            <li class="active">Buat Opname</li>
        @else
            <li>Inventory</li>
            <li><a href="{{ route('stock-opname.index') }}">Stock Opname</a></li>
            <li class="active">Buat Opname</li>
        @endif
    </ol>
@endsection

@section('content')
    {!! Form::open(['route' => 'stock-opname.store', 'class' => 'jq-validate form-horizontal', 'method' => 'post', 'novalidate']) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Buat Stock Opname ({{ $location->location_name }})</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- text input -->
                            <div class="form-group">
                                {!! Form::label('lokasi', 'Lokasi', ['class' => 'col-xs-1']) !!}
                                <div class="col-xs-6">
                                    <p>{{ $location->location_name }}</p>
                                </div>
                            </div>

                            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                                {{ Form::hidden('location', $location->kd_location) }}
                            @endif

                            <div class="form-group {!! $errors->has('tgl_opname') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_opname', 'Tanggal', ['class' => 'control-label col-xs-1']) !!}
                                <div class="col-xs-6">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {{ Form::text('tgl_opname', null, ['class' => 'form-control date-picker','required']) }}
                                    </div>
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tgl_opname', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('pelaksana') ? 'has-error' : '' !!}">
                                {!! Form::label('pelaksana', 'Pelaksana', ['class' => 'control-label col-xs-1']) !!}
                                <div class="col-xs-6">
                                    {{ Form::text('pelaksana', null, ['class' => 'form-control','required']) }}
                                    {!! $errors->first('pelaksana', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {!! $errors->has('status') ? 'has-error' : '' !!}">
                                {!! Form::label('status', 'Status', ['class' => 'control-label col-xs-1']) !!}
                                <div class="col-xs-6">
                                    <label class="radio-inline">
                                        {!! Form::radio('status', 'unpublish', true) !!} Unpublish
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('status', 'publish', false) !!} Publish
                                    </label>
                                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered" id="tbItemProduksi">
                                <thead>
                                <tr>
                                    <th>Kode Produk</th>
                                    <th>Nama Produk</th>
                                    <th>No Seri</th>
                                    <th class="text-center col-md-2">Qty</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($excels as $excel)
                                    <?php
                                        if ($excel->no_seri == '') {
                                            $noseri = '';
                                        } else {
                                            $noseri = $excel->no_seri;
                                        }
                                        $ds = $datastock->where('kd_produk', $excel->kd_produk)->where('no_seri_produk', $noseri)->first();
                                    ?>
                                    @if(!empty($ds) && $excel->qty > 0)
                                    <tr>
                                        <td style="vertical-align: middle;">{{ $ds->kd_produk }}</td>
                                        <td style="vertical-align: middle;">{{ $ds->product->deskripsi }}</td>
                                        <td style="vertical-align: middle;">{{ $ds->no_seri_produk }}</td>
                                        <td style="vertical-align: middle;">
                                            {{ Form::number('stockQty['.$ds->id.']', $excel->qty, ['class' => 'form-control', 'min' => 0]) }}
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <hr style="margin-top: 0px;">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection