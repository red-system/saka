@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css') }}">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterKategori {
            display: inline;
        }

        .filterTanggal {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/bower_components/datatables.net-bs/extensions/responsive.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            var tableLocation = $('#tbLocation').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 3,
                    orderable: false
                }],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });
        })
    </script>
@endsection

@section('title')
    Stock Opname - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Stock Opname
        <small>Inventory</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Inventory</li>
        <li class="active">Stock Opname</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Location</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbLocation" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Location Code</th>
                            <th>Location</th>
                            <th>Type</th>
                            <th class="text-center all">Act</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($locations as $lc)
                            <tr>
                                <td>{{ $lc->kd_location }}</td>
                                <td>{{ $lc->location_name }}</td>
                                <td>{{ $lc->human_type }}</td>
                                <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Opname">
                                        <a href="{{ route('stock-opname.location', $lc->kd_location) }}" class="btn btn-sm btn-warning">Opname</a>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection