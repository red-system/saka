<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Date</th>
        <th>Code</th>
        @if($type == 'produk')
            <th>No Seri</th>
        @endif
        <th>Nama</th>
        <th class="text-center">Qty In</th>
        <th class="text-center">Unit In</th>
        <th class="text-center">Qty Out</th>
        <th class="text-center">Unit Out</th>
        <th class="text-center">Last Stock</th>
        <th>Remark</th>
        <th>Location</th>
    </tr>
    </thead>
    <tbody>
    @if($datastocks->count() > 0)
        @foreach($datastocks as $ds)
        <tr>
            <td>{{ $ds->tgl }}</td>
            <td>{{ $ds->kode }}</td>
            @if($type == 'produk')
                <td>{{ $ds->no_seri }}</td>
            @endif
            <td>{{ $ds->nama }}</td>
            <td class="text-center">{{ $ds->qty_in }}</td>
            <td class="text-center">{{ $ds->satuan_in }}</td>
            <td class="text-center">{{ $ds->qty_out }}</td>
            <td class="text-center">{{ $ds->satuan_out }}</td>
            <td class="text-center">{{ $ds->last_stock }}</td>
            <td>
                {{ $ds->keterangan }}
                @if($ds->remark != '')
                    <br>{{ $ds->remark }}
                @endif
            </td>
            <td>{{ $ds->location->location_name }}</td>
        </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="@if($type == 'produk') 11 @else 10 @endif">There is no stock data available</td>
        </tr>
    @endif
    </tbody>
</table>