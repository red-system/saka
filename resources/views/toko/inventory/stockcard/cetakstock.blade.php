<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Stock Card {{$start}} s/d {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12" style="margin-top: 25px;">
            <div class="text-center">
                <h5>Stock Card</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th style="font-size:12px;">Date</th>
                        <th style="font-size:12px;">Code</th>
                        @if($type == 'produk')
                            <th style="font-size:12px;">No Seri</th>
                        @endif
                        <th style="font-size:12px;">Name</th>
                        <th style="font-size:12px; text-align: center;">Qty In</th>
                        <th style="font-size:12px; text-align: center;">Unit In</th>
                        <th style="font-size:12px; text-align: center;">Qty Out</th>
                        <th style="font-size:12px; text-align: center;">Unit Out</th>
                        <th style="font-size:12px; text-align: center;">Last Stock</th>
                        <th style="font-size:12px;">Remark</th>
                        <th style="font-size:12px;">Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($datastocks->count() > 0)
                        @foreach($datastocks as $ds)
                            <tr>
                                <td style="font-size:12px;">{{ $ds->tgl }}</td>
                                <td style="font-size:12px;">{{ $ds->kode }}</td>
                                @if($type == 'produk')
                                    <td style="font-size:12px;">{{ $ds->no_seri }}</td>
                                @endif
                                <td style="font-size:12px;">{{ $ds->nama }}</td>
                                <td style="font-size:12px; text-align: center;">{{ $ds->qty_in }}</td>
                                <td style="font-size:12px; text-align: center;">{{ $ds->satuan_in }}</td>
                                <td style="font-size:12px; text-align: center;">{{ $ds->qty_out }}</td>
                                <td style="font-size:12px; text-align: center;">{{ $ds->satuan_out }}</td>
                                <td style="font-size:12px; text-align: center;">{{ $ds->last_stock }}</td>
                                <td style="font-size:12px;">
                                    {{ $ds->keterangan }}
                                    @if($ds->remark != '')
                                        <br>{{ $ds->remark }}
                                    @endif
                                </td>
                                <td style="font-size:12px;">{{ $ds->location->location_name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td style="font-size:12px; text-align: center;" colspan="@if($type == 'produk') 11 @else 10 @endif">Tidak ada data stock yang tersedia</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
