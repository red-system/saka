@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        function printProduksi() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var produsen = $('#produsen').val();

            var urlPrint = '{{ route('produksi-produk.index') }}'+'/'+tglawal+'/'+tglakhir;
            urlPrint = urlPrint + '?produsen='+produsen;
            if (tglawal != '' && tglakhir != '') {
                window.open(urlPrint);
            }
        }

        function exportProduksi() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var produsen = $('#produsen').val();

            var urlExport = '{{ route('produksi-produk.index') }}'+'/export/'+tglawal+'/'+tglakhir;
            urlExport = urlExport + '?produsen='+produsen;
            if (tglawal != '' && tglakhir != '') {
                location.href = urlExport;
            }
        }

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formProduksi").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('produksi-produk.getdata') }}",
                        type: "POST",
                        data: $("#formProduksi").serialize(),
                        success: function(htmlCode) {
                            $('#dataProduksi').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Production - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Production
        <small>Report</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Report</li>
        <li class="active">Production</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Production</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formProduksi']) !!}
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('produsen', 'Vendor') !!}
                                {{ Form::select('produsen', ['all' => 'Semua Vendor']+App\Models\Produsen::pluck('nama_produsen', 'kd_produsen')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'produsen']) }}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Date') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        <div class="col-md-3 text-right">
                            <button type="button" class="btn btn-danger" onclick="printProduksi()" style="margin-top: 25px;">Print</button>
                            <button type="button" class="btn btn-success" onclick="exportProduksi()" style="margin-top: 25px;">Export</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataProduksi"></div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection