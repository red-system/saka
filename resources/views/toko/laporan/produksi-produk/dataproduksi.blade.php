<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Date</th>
        <th>Desc</th>
        <th class="text-center">Qty</th>
        <th>Vendor</th>
    </tr>
    </thead>
    <tbody>
    @if($dataproduksi->count() > 0)
        @foreach($dataproduksi as $dp)
            <tr>
                <td>{{ $dp->tanggal }}</td>
                <td>{{ $dp->deskripsi }}</td>
                <td class="text-center">{{ $dp->totalQty }}</td>
                <td>{{ $dp->production ? ($dp->production->produsen ? $dp->production->produsen->nama_produsen : '') : '' }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="4">No production data available</td>
        </tr>
    @endif
    </tbody>
</table>