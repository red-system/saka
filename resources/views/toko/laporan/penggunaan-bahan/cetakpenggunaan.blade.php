<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Material Used {{ !empty($vendor) ? $vendor->nama_produsen : 'Semua Vendor' }} {{$start}} S/D {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center" style="margin-top: 25px;">
                <h5>Material Used</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
                <h5>{{ !empty($vendor) ? $vendor->nama_produsen : 'Semua Vendor' }}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Code</th>
                        <th>Name</th>
                        <th>Material Type</th>
                        <th>Supplier</th>
                        <th style="text-align: center;">Qty</th>
                        <th>Unit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($penggunaanbahan as $row)
                        <tr>
                            <td>{{ $row->kd_material }}</td>
                            <td>{{ $row->material->name }}</td>
                            <td>{{ $row->material->human_type }}</td>
                            <td>{{ $row->material->supplier->nama_supplier }}</td>
                            <td style="text-align: center;">{{ $row->totalQty }}</td>
                            <td>{{ $row->satuan->satuan }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
