<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Code</th>
        <th>Name</th>
        <th>Material Type</th>
        <th>Supplier</th>
        <th class="text-center">Qty</th>
        <th>Unit</th>
    </tr>
    </thead>
    <tbody>
    @if($datapenggunaanbahan->count() > 0)
        @foreach($datapenggunaanbahan as $dp)
            <tr>
                <td>{{ $dp->kd_material }}</td>
                <td>{{ $dp->material->name }}</td>
                <td>{{ $dp->material->human_type }}</td>
                <td>{{ $dp->material->supplier->nama_supplier }}</td>
                <td class="text-center">{{ $dp->totalQty }}</td>
                <td>{{ $dp->satuan->satuan }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="6">There is no data on the used of materials available</td>
        </tr>
    @endif
    </tbody>
</table>