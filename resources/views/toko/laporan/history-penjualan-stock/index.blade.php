@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formHstPenjualanStok").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(){
                    var tglawal = $('#tglawal').val();
                    var tglakhir = $('#tglakhir').val();

                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                    var location = $('#location').val();
                    @endif

                    var urlPrint = '{{ route('history-penjualan-stok.index') }}'+'/'+tglawal+'/'+tglakhir;
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        urlPrint = urlPrint + '?location='+location;
                    @endif
                    if (tglawal != '' && tglakhir != '') {
                        window.open(urlPrint);
                    }
                },
            });
        })
    </script>
@endsection

@section('title')
    History Sales Stock - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        History Sales Stock
        <small>Laporan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Report</li>
        <li class="active">History Sales Stock</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">History Sales Stock</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '#', 'class' => 'form-horizontal jq-validate', 'id' => 'formHstPenjualanStok']) !!}
                    @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                        <div class="form-group">
                            {!! Form::label('location', 'Location', ['class' => 'col-md-1 control-label']) !!}
                            <div class="col-md-6">
                                {{ Form::select('location', ['all' => 'Semua Lokasi']+$listLocation, null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'location']) }}
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        {!! Form::label('tgl_awal', 'From', ['class' => 'col-md-1 control-label']) !!}
                        <div class="col-md-6">
                            {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'id' => 'tglawal']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('tgl_akhir', 'Until', ['class' => 'col-md-1 control-label']) !!}
                        <div class="col-md-6">
                            {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'id' => 'tglakhir']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-1">
                            <button type="submit" class="btn btn-danger">Print</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
@endsection