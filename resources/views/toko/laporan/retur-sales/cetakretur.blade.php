<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Retur Sales {{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'All Sales Location' }} {{$start}} S/D {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center" style="margin-top: 25px;">
                <h5>Retur Sales</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
                <h5>{{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'All Sales Location' }}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Retur Number</th>
                        <th>Invoice Number</th>
                        <th>Date</th>
                        <th>Doer</th>
                        <th>Product</th>
                        <th style="text-align: center;">Qty Retur</th>
                        <th>Reason</th>
                        <th>Other Reason</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($dataretur->count() > 0)
                        @foreach($dataretur as $dr)
                            <tr>
                                <td>{{ $dr->no_retur }}</td>
                                <td>{{ $dr->no_faktur }}</td>
                                <td>{{ date('Y-m-d', strtotime($dr->tgl_pengembalian)) }}</td>
                                <td>{{ $dr->pelaksana }}</td>
                                <td>{{ $dr->deskripsi }}</td>
                                <td style="text-align: center;">{{ $dr->qty_retur }}</td>
                                <td>{{ $alasanRetur[$dr->alasan] }}</td>
                                <td>{{ $dr->alasan_lain }}</td>
                                <td>{{ $dr->location_name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="8">There is no data on retur sales</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
