<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Retur Number</th>
        <th>Invoice Number</th>
        <th>Date</th>
        <th>Doer</th>
        <th>Product</th>
        <th class="text-center">Qty Retur</th>
        <th>Reason</th>
        <th>Other Reason</th>
        <th>Location</th>
    </tr>
    </thead>
    <tbody>
    @if($dataretur->count() > 0)
        @foreach($dataretur as $dr)
            <tr>
                <td>{{ $dr->no_retur }}</td>
                <td>{{ $dr->no_faktur }}</td>
                <td>{{ date('Y-m-d', strtotime($dr->tgl_pengembalian)) }}</td>
                <td>{{ $dr->pelaksana }}</td>
                <td>{{ $dr->deskripsi }}</td>
                <td class="text-center">{{ $dr->qty_retur }}</td>
                <td>{{ $alasanRetur[$dr->alasan] }}</td>
                <td>{{ $dr->alasan_lain }}</td>
                <td>{{ $dr->location_name }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="8">There is no data on retur sales</td>
        </tr>
    @endif
    </tbody>
</table>