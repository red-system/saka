<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>History Sales by Customer {{$start}} s/d {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12" style="margin-top: 25px;">
            <div class="text-center">
                <h5>History Sales by Customer</h5>
                <h5>{{$start}} s/d {{$end}}</h5>
                <h5>{{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi' }}</h5>
            </div>
            <br>
            @if($penjualan->count() > 0)
                @foreach($penjualan->groupBy('nama_pelanggan') as $pcustomer)
                    <table class="table table-bordered table-hover table-header-fixed">
                        <thead>
                        <tr>
                            <th colspan="10">Nama Pelanggan: {{ $pcustomer->first()->nama_pelanggan }}</th>
                        </tr>
                        <tr class="">
                            <th style="font-size:11px; text-align: center;">No</th>
                            <th style="font-size:11px">Date</th>
                            <th style="font-size:11px">Invoice Number</th>
                            <th style="font-size:11px;">Product Code</th>
                            <th style="font-size:11px;">Product</th>
                            <th style="font-size:11px;">No Seri</th>
                            <th style="font-size:11px; text-align: center;">Qty</th>
                            <th style="font-size:11px; text-align: center;">Price</th>
                            <th style="font-size:11px;">Price Type</th>
                            <th style="font-size:11px; text-align: center;">Total Discount</th>
                            <th style="font-size:11px; text-align: center;">Sub Total</th>
                            <th style="font-size:11px; text-align: center;">Location</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $totalQty = 0; $totalPrice = 0; $totalDisc = 0; $totalSub = 0; $no = 1; ?>
                        @foreach($pcustomer as $row)
                            <tr>
                                <td style="font-size:10px; text-align: center;"> {{ $no }} </td>
                                <td style="font-size:10px">{{ date('Y-m-d', strtotime($row->tgl)) }}</td>
                                <td style="font-size:10px">{{ $row->no_faktur }}</td>
                                <td style="font-size:10px;">{{ $row->kd_produk }}</td>
                                <td style="font-size:10px;">{{ $row->produk->deskripsi }}</td>
                                <td style="font-size:10px;">{{ $row->no_seri }}</td>
                                <td align="center" style="font-size:10px"> {{ $row->qty }} </td>
                                <td style="font-size:10px; text-align: right;"> {{ 'Rp. '.number_format($row->price, 2, "," ,".") }} </td>
                                <td style="font-size:10px;">{{ $row->price_type == 'wholesale_price' ? 'Wholesale' : 'Retail' }}</td>
                                <td style="font-size:10px; text-align: right;"> {{ 'Rp. '.number_format($row->totalDisc, 2, "," ,".") }} </td>
                                <td style="font-size:10px; text-align: right;"> {{ 'Rp. '.number_format($row->subtotal, 2, "," ,".") }} </td>
                                <td style="font-size:10px;">{{ $row->location_name }}</td>
                            </tr>
                            <?php
                            $totalQty = $totalQty + $row->qty;
                            $totalPrice = $totalPrice + $row->price;
                            $totalDisc = $totalDisc + $row->totalDisc;
                            $totalSub = $totalSub + $row->subtotal;
                            $no++;
                            ?>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6" align="right" style="font-size:10px;font-weight:bold">Total</td>
                            <td align="center" style="font-size:10px;font-weight:bold">{{ $totalQty }}</td>
                            <td style="font-size:10px;font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalPrice, 2, "," ,".")}}</td>
                            <td></td>
                            <td style="font-size:10px;font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalDisc, 2, "," ,".")}}</td>
                            <td style="font-size:10px;font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalSub, 2, "," ,".")}}</td>
                        </tr>
                        </tfoot>
                    </table>
                @endforeach
            @else
                <h4>There are no sales data from {{$start}} s/d {{$end}}</h4>
            @endif
        </div>
    </div>
</div>
</body>
</html>
