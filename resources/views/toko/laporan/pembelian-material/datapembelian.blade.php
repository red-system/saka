<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Date</th>
        <th>Code</th>
        <th>Name</th>
        <th>Material Type</th>
        <th>Supplier</th>
        <th class="text-center">Qty</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    @if($datapembelian->count() > 0)
        @foreach($datapembelian as $dp)
            <tr>
                <td>{{ $dp->tanggal }}</td>
                <td>{{ $dp->kd_material }}</td>
                <td>{{ $dp->name }}</td>
                <td>{{ $dp->human_type }}</td>
                <td>{{ $dp->purchasing->supplier->nama_supplier }}</td>
                <td class="text-center">{{ $dp->tglQty }}</td>
                <td>{{ 'Rp. '.number_format($dp->price, 2, ',', '.') }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="7">There is no data on material purchases available</td>
        </tr>
    @endif
    </tbody>
</table>