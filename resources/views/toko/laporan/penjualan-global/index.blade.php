@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        function printPenjualanGlobal() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var type = $('#type').val();

            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                var location = $('#location').val();
            @endif

            var urlPrint = '{{ route('penjualan-global.index') }}'+'/'+tglawal+'/'+tglakhir+ '?type='+type;
            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                urlPrint = urlPrint + '&location='+location ;
            @endif
            if (tglawal != '' && tglakhir != '') {
                window.open(urlPrint);
            }
        }

        function exportPenjualanGlobal() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            var type = $('#type').val();

            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                var location = $('#location').val();
            @endif

            var urlExport = '{{ route('penjualan-global.index') }}'+'/export/'+tglawal+'/'+tglakhir;
            @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                    urlExport = urlExport + '?location='+location + '&type='+type;
            @endif
            if (tglawal != '' && tglakhir != '') {
                window.location.href = urlExport;
            }
        }

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })
            $('.select2').select2();

            $("#formPenjualanGlobal").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function() {
                    $.ajax({
                        url: "{{ route('penjualan-global.getdata') }}",
                        type: "POST",
                        data: $("#formPenjualanGlobal").serialize(),
                        success: function(htmlCode) {
                            $('#dataPenjualanGlobal').html(htmlCode);
                        }
                    });
                }
            });
        })
    </script>
@endsection

@section('title')
    Sales Global - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Sales Global
        <small>Report</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Report</li>
        <li class="active">Sales Global</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sales Global</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(['url' => '', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'id' => 'formPenjualanGlobal']) !!}
                    <div class="row">
                        @if(\Gate::allows('as_owner_or_manager_kantorpusat'))
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('location', 'Location') !!}
                                    {{ Form::select('location', ['all' => 'Semua Lokasi']+App\Models\Location::pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'location']) }}
                                </div>
                            </div>
                        @endif
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label('tgl', 'Date') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                {!! Form::label('type', 'Payment Type') !!}
                                {{ Form::select('type', ['all' => 'All Type']+App\Models\Payment::paymentTypeList(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'type']) }}
                            </div>
                        </div>
                        <div class="col-md-2">
                            {!! Form::button('Search', ['type'=> 'submit', 'class'=>'btn btn-success', 'style' => 'margin-top: 25px;']) !!}
                        </div>

                        @if(\Gate::allows('as_owner_or_manager_kantorpusat')) <div class="col-md-2" style="text-align: right;"> @else <div class="col-md-5" style="text-align: right;"> @endif
                            <button type="button" class="btn btn-danger" onclick="printPenjualanGlobal()" style="margin-top: 25px;">Print</button>
                            <button type="button" class="btn btn-success" onclick="exportPenjualanGlobal()" style="margin-top: 25px;">Export</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataPenjualanGlobal"></div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <div id="detailPenjualan" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                            <span> &nbsp;&nbsp;Loading... </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection