<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail Sales</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Invoice Number</td>
                    <td style="padding: 4px;">: {{ $penjualan->no_faktur }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($penjualan->total, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Discount</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($penjualan->disc_nominal, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Additional Discount</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($penjualan->additional_disc, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Consignment Fee</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($penjualan->komisi_nominal, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Grand Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($penjualan->grand_total, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">SPB/SPG</td>
                    <td style="padding: 4px;">: {{ $penjualan->sales_user ? $penjualan->sales_user->name : '-' }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <table class="table table-bordered table-stripped">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>No Seri</th>
                    <th>Desc</th>
                    <th>Size</th>
                    <th class="text-center">Qty</th>
                    <th>Price</th>
                    <th>Price Type</th>
                    <th>Discount</th>
                    <th>Sub Total</th>
                </tr>
                </thead>
                <tbody>
                @if($detailsales->count() > 0)
                    @foreach($detailsales as $ds)
                        <tr>
                            <td>{{ $ds->kd_produk }}</td>
                            <td>{{ $ds->no_seri }}</td>
                            <td>{{ $ds->produk->deskripsi }}</td>
                            <td>{{ $ds->size_product }}</td>
                            <td class="text-center">{{ $ds->qty }}</td>
                            <td>{{ 'Rp. '.number_format($ds->price, 2, ',', '.') }}</td>
                            <td>{{ $ds->price_type == 'wholesale_price' ? 'Wholesale' : 'Retail' }}</td>
                            <td>{{ 'Rp. '.number_format($ds->disc_nominal, 2, ',', '.') }}</td>
                            <td>{{ 'Rp. '.number_format($ds->subtotal, 2, ',', '.') }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="8">There is no detailed sales data available</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>