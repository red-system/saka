<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Sales Global {{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi' }} {{$start}} S/D {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center" style="margin-top: 25px;">
                <h5>Sales Global</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
                <h5>{{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi' }}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <p style="font-weight: bold; font-size: 15px;">Sales</p>
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Invoice Number</th>
                        <th>Date</th>
                        <th>Customer</th>
                        <th style="text-align: center;">Total</th>
                        <th style="text-align: center;">Disc</th>
                        <th style="text-align: center;">Additional Disc</th>
                        <th style="text-align: center;">Consignment Fee</th>
                        <th style="text-align: center;">Grand Total</th>
                        <th>Payment</th>
                        <th>Location</th>
                        <th>Status Terbayar</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($datapenjualan->count() > 0)
                        <?php $totalSub = 0; $totalDisc = 0; $totalGrand = 0; $totalFee = 0; $totalAddDisc= 0;?>
                        @foreach($datapenjualan as $row)
                        <tr>
                            <td> {{ $row->no_faktur }} </td>
                            <td> {{ date('Y-m-d', strtotime($row->tgl)) }} </td>
                            <td> {{ $row->nama_pelanggan }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->total, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->disc_nominal, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->additional_disc, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->komisi_nominal, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->grand_total, 2, "," ,".") }} </td>
                            <td>
                                @foreach($row->paymentSales as $payment)
                                    @if($payment == $row->paymentSales->last())
                                        {!! $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.') !!}
                                    @else
                                        {!! $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.').', ' !!}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $row->location_name }}</td>
                            <td>{{ $row->status_terbayar == '1' ? 'Sudah Terbayar' : 'Belum Terbayar' }}</td>
                        </tr>
                        <?php
                        $totalSub = $totalSub + $row->total;
                        $totalDisc = $totalDisc + $row->disc_nominal;
                        $totalAddDisc = $totalAddDisc + $row->additional_disc;
                        $totalFee = $totalFee + $row->komisi_nominal;
                        $totalGrand = $totalGrand + $row->grand_total;
                        ?>
                        @endforeach
                        <tfoot>
                        <tr>
                            <td colspan="3" style="font-weight:bold; text-align: right;">Total</td>
                            <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalSub, 2, "," ,".")}}</td>
                            <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalDisc, 2, "," ,".")}}</td>
                            <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalAddDisc, 2, "," ,".")}}</td>
                            <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalFee, 2, "," ,".")}}</td>
                            <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalGrand, 2, "," ,".")}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="font-weight:bold; text-align: right;">Items Sold</td>
                            <td style="font-weight:bold; text-align: right;">{{ number_format($totalQtyTerjual, 0, "," ,".").' Items'}}</td>
                            <td></td>
                        </tr>
                        </tfoot>
                    @else
                        <tr>
                            <td class="text-center" colspan="8">There are no global sales data available</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <p style="font-weight: bold; font-size: 15px; margin-top: 15px;">Retur Sales</p>
                <table class="table table-bordered table-stripped">
                    <thead>
                    <tr class="bg-aqua">
                        <th>Invoice Number</th>
                        <th>Retur Number</th>
                        <th>Retur Date</th>
                        <th>Total Retur</th>
                        <th>Additional Cost</th>
                        <th>Reason</th>
                        <th>Location</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($dataretur->count() > 0)
                        <?php $totalRetur = 0; $totalTambahan = 0; ?>
                        @foreach($dataretur as $dr)
                            <tr>
                                <td>{{ $dr->no_retur }}</td>
                                <td>{{ $dr->no_faktur }}</td>
                                <td>{{ date('Y-m-d', strtotime($dr->tgl_pengembalian)) }}</td>
                                <td>{{ 'Rp. '.number_format($dr->total_retur, 2, ',', '.') }}</td>
                                <td>{{ 'Rp. '.number_format($dr->tambahan_biaya, 2, ',', '.') }}</td>
                                <td>{{ $dr->human_alasan }}</td>
                                <td>{{ $dr->location_name }}</td>
                            </tr>
                            <?php
                            $totalRetur = $totalRetur + $dr->total_retur;
                            $totalTambahan = $totalTambahan + $dr->tambahan_biaya;
                            ?>
                        @endforeach
                        <tr>
                            <td colspan="3"><strong>Total</strong></td>
                            <td><strong>{{ 'Rp. '.number_format($totalRetur, 2, ',', '.') }}</strong></td>
                            <td><strong>{{ 'Rp. '.number_format($totalTambahan, 2, ',', '.') }}</strong></td>
                            <td></td>
                        </tr>
                    @else
                        <tr>
                            <td class="text-center" colspan="6">There are no retur sales data available</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <p style="font-weight: bold; font-size: 15px; margin-top: 15px;">Grand Total</p>
                <table class="table table-bordered table-stripped">
                    <thead>
                    <tr>
                        <th>Total Discount</th>
                        <th>Total Additional Discount</th>
                        <th>Total Cash</th>
                        <th>Total Transfer</th>
                        <th>Total Piutang</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totaldiskon = $datapenjualan->sum('disc_nominal');
                            } else {
                                $totaldiskon = 0;
                            }
                            ?>
                            {{ 'Rp. '.number_format($totaldiskon, 2, ',', '.') }}
                        </td>
                        <td>
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totaladddiskon = $datapenjualan->sum('additional_disc');
                            } else {
                                $totaladddiskon = 0;
                            }
                            ?>
                            {{ 'Rp. '.number_format($totaladddiskon, 2, ',', '.') }}
                        </td>
                        <td>
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totalcash = \App\Models\Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', 'tunai')->sum('payment');
                            } else {
                                $totalcash = 0;
                            }
                            ?>
                            {{ 'Rp. '.number_format($totalcash, 2, ',', '.') }}
                        </td>
                        <td>
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totaltransfer = \App\Models\Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', '!=', 'tunai')->sum('payment');
                            } else {
                                $totaltransfer = 0;
                            }
                            ?>
                            {{ 'Rp. '.number_format($totaltransfer, 2, ',', '.') }}
                        </td>
                        <td>
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totalpiutang = \App\Models\Piutang::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->sum('sisa_piutang');
                            } else {
                                $totalpiutang = 0;
                            }
                            ?>
                            {{ 'Rp. '.number_format($totalpiutang, 2, ',', '.') }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-center">
                            <?php
                            if ($datapenjualan->count() > 0) {
                                $totalPenjualan = $datapenjualan->sum('grand_total');
                            } else {
                                $totalPenjualan = 0;
                            }

                            if ($dataretur->count() > 0) {
                                $totalReturPenjualan = $dataretur->sum('total_retur');
                            } else {
                                $totalReturPenjualan = 0;
                            }
                            ?>
                            <strong>Grand Total Penjualan: {{ 'Rp. '.number_format($totalPenjualan - $totalReturPenjualan, 2, ',', '.') }}</strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
