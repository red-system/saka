<p style="font-weight: bold; font-size: 15px;">Sales</p>
<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Invoice Number</th>
        <th>Date</th>
        <th>Customer</th>
        <th>Grand Total</th>
        <th>Consignment Fee</th>
        <th>Payment</th>
        <th>Location</th>
        <th class="text-center col-md-1">Act</th>
        <th class="text-center col-md-1">Terbayar</th>
    </tr>
    </thead>
    <tbody>
    @if($datapenjualan->count() > 0)
        <?php $totalGrand = 0; $totalFee = 0; ?>
        @foreach($datapenjualan as $dp)
            <tr>
                <td>{{ $dp->no_faktur }}</td>
                <td>{{ date('Y-m-d', strtotime($dp->tgl)) }}</td>
                <td>{{ $dp->nama_pelanggan }}</td>
                <td>{{ 'Rp. '.number_format($dp->grand_total, 2, ',', '.') }}</td>
                <td>{{ 'Rp. '.number_format($dp->komisi_nominal, 2, ',', '.') }}</td>
                <td>
                    @foreach($dp->paymentSales as $payment)
                        @if($payment == $dp->paymentSales->last())
                            {!! $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.') !!}
                        @else
                            {!! $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.').', ' !!}
                        @endif
                    @endforeach
                </td>
                <td>{{ $dp->location_name }}</td>
                <td class="text-center col-md-1">
                    <span data-toggle="tooltip" data-placement="top" title="Detail">
                        <a href="{{ route('penjualan-global.show', $dp->id) }}" data-target="#detailPenjualan" data-toggle="modal" class="btn btn-sm btn-default">Detail</a>
                    </span>
                </td>
                <td class="text-center col-md-1">
                    {!! Form::checkbox('listSales[]', $dp->id, $dp->status_terbayar == '1' ? true : false, ['class' => 'chbkSales', 'onchange'=> 'setTerbayar(this)', 'data-location' => $dp->kd_location]) !!}
                </td>
            </tr>

            <?php
                $totalGrand = $totalGrand + $dp->grand_total;
                $totalFee = $totalFee + $dp->komisi_nominal;
            ?>
        @endforeach
        <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td>{{ 'Rp. '.number_format($totalGrand, 2, ',', '.') }}</td>
            <td>{{ 'Rp. '.number_format($totalFee, 2, ',', '.') }}</td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3"><strong>Item Sold</strong></td>
            <td>{{ number_format($totalQtyTerjual, 0, ',', '.').' Items' }}</td>
            <td colspan="3"></td>
        </tr>
    @else
        <tr>
            <td class="text-center" colspan="8">There are no global sales data available</td>
        </tr>
    @endif
    </tbody>
</table>

<p style="font-weight: bold; font-size: 15px;">Retur Sales</p>
<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Invoice Number</th>
        <th>Retur Number</th>
        <th>Retur Date</th>
        <th>Total Retur</th>
        <th>Additional Cost</th>
        <th>Reason</th>
        <th>Location</th>
        <th class="text-center col-md-1">Act</th>
    </tr>
    </thead>
    <tbody>
    @if($dataretur->count() > 0)
        <?php $totalRetur = 0; $totalTambahan = 0; ?>
        @foreach($dataretur as $dr)
            <tr>
                <td>{{ $dr->no_retur }}</td>
                <td>{{ $dr->no_faktur }}</td>
                <td>{{ date('Y-m-d', strtotime($dr->tgl_pengembalian)) }}</td>
                <td>{{ 'Rp. '.number_format($dr->total_retur, 2, ',', '.') }}</td>
                <td>{{ 'Rp. '.number_format($dr->tambahan_biaya, 2, ',', '.') }}</td>
                <td>{{ $dr->human_alasan }}</td>
                <td>{{ $dr->location_name }}</td>
                <td class="text-center col-md-1">
                    <span data-toggle="tooltip" data-placement="top" title="Detail">
                        <a href="{{ route('retur-sales.print', $dr->no_retur) }}" class="btn btn-sm btn-default" target="_blank"> Detail</a>
                    </span>
                </td>
            </tr>
            <?php
                $totalRetur = $totalRetur + $dr->total_retur;
                $totalTambahan = $totalTambahan + $dr->tambahan_biaya;
            ?>
        @endforeach
        <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td>{{ 'Rp. '.number_format($totalRetur, 2, ',', '.') }}</td>
            <td>{{ 'Rp. '.number_format($totalTambahan, 2, ',', '.') }}</td>
            <td colspan="2"></td>
        </tr>
    @else
        <tr>
            <td class="text-center" colspan="7">There are no retur sales data available</td>
        </tr>
    @endif
    </tbody>
</table>

<p style="font-weight: bold; font-size: 15px;">Grand Total</p>
<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Total Discount</th>
        <th>Total Additional Discount</th>
        <th>Total Cash</th>
        <th>Total Transfer</th>
        <th>Total Piutang</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php
                    if ($datapenjualan->count() > 0) {
                        $totaldiskon = $datapenjualan->sum('disc_nominal');
                    } else {
                        $totaldiskon = 0;
                    }
                ?>
                {{ 'Rp. '.number_format($totaldiskon, 2, ',', '.') }}
            </td>
            <td>
                <?php
                    if ($datapenjualan->count() > 0) {
                        $totaladddiskon = $datapenjualan->sum('additional_disc');
                    } else {
                        $totaladddiskon = 0;
                    }
                ?>
                {{ 'Rp. '.number_format($totaladddiskon, 2, ',', '.') }}
            </td>
            <td>
                <?php
                    if ($datapenjualan->count() > 0) {
                        $totalcash = \App\Models\Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', 'tunai')->sum('payment');
                    } else {
                        $totalcash = 0;
                    }
                ?>
                {{ 'Rp. '.number_format($totalcash, 2, ',', '.') }}
            </td>
            <td>
                <?php
                    if ($datapenjualan->count() > 0) {
                        $totaltransfer = \App\Models\Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', '!=', 'tunai')->sum('payment');
                    } else {
                        $totaltransfer = 0;
                    }
                ?>
                {{ 'Rp. '.number_format($totaltransfer, 2, ',', '.') }}
            </td>
            <td>
                <?php
                if ($datapenjualan->count() > 0) {
                    $totalpiutang = \App\Models\Piutang::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->sum('sisa_piutang');
                } else {
                    $totalpiutang = 0;
                }
                ?>
                {{ 'Rp. '.number_format($totalpiutang, 2, ',', '.') }}
            </td>
        </tr>
        <tr>
            <td colspan="4" class="text-center">
                <?php
                    if ($datapenjualan->count() > 0) {
                        $totalPenjualan = $datapenjualan->sum('grand_total');
                    } else {
                        $totalPenjualan = 0;
                    }

                    if ($dataretur->count() > 0) {
                        $totalReturPenjualan = $dataretur->sum('total_retur');
                    } else {
                        $totalReturPenjualan = 0;
                    }
                ?>
                <strong>Grand Total Penjualan: {{ 'Rp. '.number_format($totalPenjualan - $totalReturPenjualan, 2, ',', '.') }}</strong>
            </td>
        </tr>
    </tbody>
</table>
<script>
    $('[data-toggle="tooltip"]').tooltip();

    function setTerbayar(elem) {
        var salesId = $(elem).val();
        var chkStatus = $(elem).prop('checked');
        if(chkStatus) {
            var terbayar = 1;
        } else {
            var terbayar = 0;
        }
        var location = $(elem).attr('data-location');

        $.ajax({
            url: "{{ route('penjualan-global.setTerbayar') }}",
            type: "POST",
            data: {'id': salesId, 'terbayar': terbayar, 'location': location, '_token': '{{ csrf_token() }}'},
            success: function(result) {

            }
        });
    }
</script>