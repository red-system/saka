<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail Production</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Production Number</td>
                    <td style="padding: 4px;">: {{ $produksi->kd_produksi }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Vendor</td>
                    <td style="padding: 4px;">: {{ $produksi->produsen->nama_produsen }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($produksi->total, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Tax</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($produksi->ppn_nominal, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Grand Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($produksi->grand_total, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <table class="table table-bordered table-stripped">
                <thead>
                <tr>
                    <th>Desc</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Finish</th>
                </tr>
                </thead>
                <tbody>
                @if($detailproduksi->count() > 0)
                    @foreach($detailproduksi as $dp)
                        <tr>
                            <td>{{ $dp->deskripsi }}</td>
                            <td class="text-center">{{ $dp->qty }}</td>
                            <td class="text-center">{{ $dp->qty_progress }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="2">There is no detailed production data available</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>