<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Production Global {{$start}} S/D {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center" style="margin-top: 25px;">
                <h5>Production Global</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Prodution Code</th>
                        <th>Date</th>
                        <th>Vendor</th>
                        <th style="text-align: center;">Total</th>
                        <th style="text-align: center;">Tax</th>
                        <th style="text-align: center;">Grand Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $totalSub = 0; $totalPpn = 0; $totalGrand = 0; ?>
                    @foreach($dataproduksi as $row)
                        <tr>
                            <td> {{ $row->kd_produksi }} </td>
                            <td> {{ $row->tgl_produksi }} </td>
                            <td> {{ $row->produsen->nama_produsen }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->total, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->ppn_nominal, 2, "," ,".") }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->grand_total, 2, "," ,".") }} </td>
                        </tr>
                    <?php
                    $totalSub = $totalSub + $row->total;
                    $totalPpn = $totalPpn + $row->ppn_nominal;
                    $totalGrand = $totalGrand + $row->grand_total;
                    ?>
                    @endforeach
                    <tfoot>
                    <tr>
                        <td colspan="3" style="font-weight:bold; text-align: right;">Total</td>
                        <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalSub, 2, "," ,".")}}</td>
                        <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalPpn, 2, "," ,".")}}</td>
                        <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalGrand, 2, "," ,".")}}</td>
                    </tr>
                    </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
