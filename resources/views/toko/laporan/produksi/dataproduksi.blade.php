<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Production Code</th>
        <th>Date</th>
        <th>Vendor</th>
        <th>Grand Total</th>
        <th class="text-center col-md-1">Act</th>
    </tr>
    </thead>
    <tbody>
    @if($dataproduksi->count() > 0)
        @foreach($dataproduksi as $dp)
            <tr>
                <td>{{ $dp->kd_produksi }}</td>
                <td>{{ $dp->tgl_produksi }}</td>
                <td>{{ $dp->produsen->nama_produsen }}</td>
                <td>{{ 'Rp. '.number_format($dp->grand_total, 2, ',', '.') }}</td>
                <td class="text-center col-md-1">
                    <span data-toggle="tooltip" data-placement="top" title="Detail">
                        <a href="{{ route('produksi-global.show', $dp->id) }}" data-target="#detailProduksi" data-toggle="modal" class="btn btn-sm btn-default">Detail</a>
                    </span>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="5">No production data available</td>
        </tr>
    @endif
    </tbody>
</table>
<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>