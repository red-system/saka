<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Sales Stock {{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi' }} {{$start}} S/D {{$end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center" style="margin-top: 25px;">
                <h5>Sales Stock</h5>
                <h5>{{$start}} S/D {{$end}}</h5>
                <h5>{{ !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi' }}</h5>
            </div>
            <br>
            <div class="portlet light ">
                <table class="table table-bordered table-hover table-header-fixed">
                    <thead>
                    <tr class="">
                        <th>Code</th>
                        <th>Desc</th>
                        <th style="text-align: center;">Qty</th>
                        <th style="text-align: center;">Price</th>
                        <th style="text-align: center;">Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php  $totalGrand = 0; $totalQty = 0; $totalPrice = 0; $totalDisc = 0; $totalSub = 0;$totalDiscGlobal=0; $totalDiscAdd=0; ?>
                    @foreach($datapenjualan as $row)
                        <tr>
                            <td> {{ $row->kd_produk }} </td>
                            <td> {{ $row->produk->deskripsi }} </td>
                            <td style="text-align: center;"> {{ $row->totalQty }} </td>
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->price, 2, "," ,".") }} </td>
                            {{--  <td style="text-align: right;"> {{ 'Rp. '.number_format($row->tglDiscTotal, 2, "," ,".") }} </td>  --}}
                            <td style="text-align: right;"> {{ 'Rp. '.number_format($row->tglSubtotal, 2, "," ,".") }} </td>
                            {{--  <td> {{ $row->location_name }} </td>  --}}
                        </tr>
                    <?php
                    $totalQty = $totalQty + $row->totalQty;
                    $totalPrice = $totalPrice + $row->price;
                    $totalDisc = $totalDisc + $row->tglDiscTotal;
                    $totalSub = $totalSub + $row->tglSubtotal;
                    $totalGrand = $totalGrand + $row->tglSubtotal;
                    ?>
                    @endforeach
                    @foreach ($totalDc as $disc)
                    <?php
                        $totalDiscGlobal=$disc->totalDiscGlobal;
                        $totalDiscAdd=$disc->totalDiscAdditional;
                    ?>
                    @endforeach
                    <tfoot>
                    <tr>
                        <td colspan="2" style="text-align: right; font-weight:bold">Total</td>
                        <td style="text-align: center; font-weight:bold">{{ $totalQty }}</td>
                        <td style="font-weight:bold; text-align: right;"></td>
                        <td style="font-weight:bold; text-align: right;">{{ 'Rp. '.number_format($totalSub, 2, "," ,".")}}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><strong>Disc Global</strong></td>
                        <td>{{ 'Rp. '.number_format($totalDiscGlobal, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><strong>Additional Disc </strong></td>
                        <td>{{ 'Rp. '.number_format($totalDiscAdd, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td colspan="4"><strong>Grand Total </strong></td>
                        <td>{{ 'Rp. '.number_format($totalGrand-$totalDiscGlobal-$totalDiscAdd, 2, ',', '.') }}</td>
                    </tr>
                    </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
