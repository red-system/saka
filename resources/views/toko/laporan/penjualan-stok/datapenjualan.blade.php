<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Code</th>
        <th>Desc</th>
        <th class="text-center">Qty</th>
        <th>Price</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    @if($datapenjualan->count() > 0)
    <?php $totalGrand = 0; $totalQty = 0;$totalDiscGlobal=0; $totalDiscAdd=0; ?>
        @foreach($datapenjualan as $dp)
            <tr>
                <td>{{ $dp->kd_produk }}</td>
                <td>{{ $dp->produk->deskripsi }}</td>
                <td class="text-center">{{ $dp->totalQty }}</td>
                <td>{{ 'Rp. '.number_format($dp->price, 2, ',', '.') }}</td>
                {{--  <td>{{ 'Rp. '.number_format($dp->tglDiscTotal, 2, ',', '.') }}</td>  --}}
                <td>{{ 'Rp. '.number_format($dp->tglSubtotal, 2, ',', '.') }}</td>
                {{--  <td>{{ $dp->location_name }}</td>  --}}
            </tr>
            
            <?php
                $totalGrand = $totalGrand + $dp->tglSubtotal;
                $totalQty=$totalQty +$dp->totalQty;
            ?>
            
        @endforeach
        
        @foreach ($totalDisc as $disc)
            <?php
                $totalDiscGlobal=$disc->totalDiscGlobal;
                $totalDiscAdd=$disc->totalDiscAdditional;
            ?>
        @endforeach
        <tr>
            <td colspan="2"><strong>Total</strong></td>
            <td style="text-align: center;">{{ number_format($totalQty, 2, ',', '.') }}</td>
            <td style="text-align: center;"></td>
            <td>{{ 'Rp. '.number_format($totalGrand, 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td colspan="4"><strong>Disc Global</strong></td>
            <td>{{ 'Rp. '.number_format($totalDiscGlobal, 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td colspan="4"><strong>Additional Disc </strong></td>
            <td>{{ 'Rp. '.number_format($totalDiscAdd, 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td colspan="4"><strong>Grand Total </strong></td>
            <td>{{ 'Rp. '.number_format($totalGrand-$totalDiscGlobal-$totalDiscAdd, 2, ',', '.') }}</td>
        </tr>
    @else
        <tr>
            <td class="text-center" colspan="6">There is no data on available stock sales</td>
        </tr>
    @endif
    </tbody>
</table>