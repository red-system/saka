<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail Purchasing</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Invoice Number</td>
                    <td style="padding: 4px;">: {{ $pembelian->no_faktur }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Supplier</td>
                    <td style="padding: 4px;">: {{ $pembelian->no_faktur }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($pembelian->total, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Tax</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($pembelian->ppn_nominal, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <tr>
                    <td style="padding: 4px;">Discount</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($pembelian->disc_nominal, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Additional Cost</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($pembelian->biaya_tambahan, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">Grand Total</td>
                    <td style="padding: 4px;">: {{ 'Rp. '.number_format($pembelian->grand_total, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <table class="table table-bordered table-stripped">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Material Type</th>
                    <th class="text-center">Qty</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                @if($detailpurchasing->count() > 0)
                    @foreach($detailpurchasing as $dp)
                        <tr>
                            <td>{{ $dp->kd_material }}</td>
                            <td>{{ $dp->name }}</td>
                            <td>{{ $dp->human_type }}</td>
                            <td class="text-center">{{ $dp->qty }}</td>
                            <td>{{ 'Rp. '.number_format($dp->price, 2, ',', '.') }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="5">No purchase details data available</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>