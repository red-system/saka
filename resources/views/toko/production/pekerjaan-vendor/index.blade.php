@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            $('#tbVendor').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 2,
                    orderable: false
                } ]
            });

            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            $("#formCetakLaporan").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    } else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    } else if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(){
                    var tglawal = $('#tglawal').val();
                    var tglakhir = $('#tglakhir').val();

                    var urlPrint = '{{ route('pekerjaan-vendor.index') }}'+'/'+tglawal+'/'+tglakhir;
                    if (tglawal != '' && tglakhir != '') {
                        window.open(urlPrint);
                    }
                },
            });
        })
    </script>
@endsection

@section('title')
    Vendor Job - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
        <h1>
            Vendor Job
            <small>Production</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}">Home</a></li>
            <li>Production</li>
            <li class="active">Vendor Job</li>
        </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#vendor" data-toggle="tab">Vendor</a></li>
                    <li><a href="#cetaklaporan" data-toggle="tab">Print Report</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="vendor">
                        <table id="tbVendor" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Vendor Code</th>
                                <th>Vendor</th>
                                <th class="text-center">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($vendors as $vendor)
                                <tr>
                                    <td>{{ $vendor->kd_produsen }}</td>
                                    <td>{{ $vendor->nama_produsen }}</td>
                                    <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="View Detail">
                                        <a href="{{ route('pekerjaan-vendor.show', $vendor->kd_produsen) }}" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                    </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="cetaklaporan">
                        {!! Form::open(['url' => '#', 'class' => 'form-horizontal jq-validate', 'id' => 'formCetakLaporan']) !!}
                        <div class="form-group">
                            {!! Form::label('tgl_awal', 'From', ['class' => 'col-md-1 control-label']) !!}
                            <div class="col-md-6">
                                {{ Form::text('tgl_awal', null, ['class' => 'form-control date-picker','required', 'id' => 'tglawal']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('tgl_akhir', 'To', ['class' => 'col-md-1 control-label']) !!}
                            <div class="col-md-6">
                                {{ Form::text('tgl_akhir', null, ['class' => 'form-control date-picker','required', 'id' => 'tglakhir']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-1">
                                <button type="submit" class="btn btn-danger">Print</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection