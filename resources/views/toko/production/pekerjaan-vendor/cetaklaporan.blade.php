<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    {{--<link href="{{ public_path('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <style media="screen">
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
            z-index: 100000;
        }
        .my-float{
            margin-top:22px;
        }

        h4, .h4, h5, .h5, h6, .h6 {
            margin-top: 3px;
            margin-bottom: 3px;
        }
    </style>

    <script>
        function printDiv(divName){
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
    <title>Vendor Jobs {{ $start}} up to {{ $end}}</title>
</head>
<body>
<div class="container-fluid">
    <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="fa fa-print"></i>
    </button>
</div>
<div class="container-fluid" id='printMe'>
    <div class="row">
        <div class="col-xs-12" style="margin-top: 25px;">
            <div class="text-center">
                <h5>Vendor Jobs</h5>
                <h5>{{$start}} s/d {{$end}}</h5>
            </div>
            <br>
            @if($pekerjaanvendor->count() > 0)
                @foreach($pekerjaanvendor->groupBy('kd_produsen') as $pvendor)
                    <table class="table table-bordered table-hover table-header-fixed">
                        <thead>
                        <tr>
                            <th colspan="10">Vendor: {{ $pvendor->first()->production->produsen->nama_produsen }}</th>
                        </tr>
                        <tr class="">
                            <th style="text-align: center; width: 30px;">No</th>
                            <th>Product</th>
                            <th>invoice number</th>
                            <th style="text-align: center;">Qty</th>
                            <th style="text-align: center;">Finish</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $totalQty = 0; $totalFinish = 0; $no = 1; ?>
                        @foreach($pvendor as $row)
                            <tr>
                                <td style="text-align: center;"> {{ $no }} </td>
                                <td>{{ $row->deskripsi }}</td>
                                <td>{{ $row->kd_produksi }}</td>
                                <td style="text-align: center;">{{ $row->qty }}</td>
                                <td style="text-align: center;">{{ $row->qty_progress }}</td>
                            </tr>
                            <?php
                            $totalQty = $totalQty + $row->qty;
                            $totalFinish = $totalFinish + $row->qty_progress;
                            $no++;
                            ?>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3" align="right" style="font-weight:bold">Total</td>
                            <td align="center" style="font-weight:bold">{{ $totalQty }}</td>
                            <td align="center" style="font-weight:bold">{{ $totalFinish }}</td>
                        </tr>
                        </tfoot>
                    </table>
                @endforeach
            @else
                <h4>There are no vendor job data from {{ $start }} up to {{ $end }}</h4>
            @endif
        </div>
    </div>
</div>
</body>
</html>
