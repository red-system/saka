@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script>
        function getHistory() {
            var tglawal = $('#tglawal').val();
            var tglakhir = $('#tglakhir').val();
            if(tglawal != '' && tglakhir != '') {
                $.post("{{ route('pekerjaan-vendor.gethistory', $vendor->kd_produsen) }}", { tglawal:tglawal, tglakhir:tglakhir, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                    $('#dataHistoryPekerjaan').html(result);
                });
            }
        }

        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            if ($(document.getElementById("tglawal")).length > 0 && $(document.getElementById("tglakhir")).length > 0) {
                getHistory();
            }

            $("#tglawal").change(function () {
                getHistory();
            })

            $("#tglakhir").change(function () {
                getHistory();
            })
        })
    </script>
@endsection

@section('title')
    Vendor Job - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
        <h1>
            Vendor Job
            <small>Production</small>
        </h1>
        <ol class="breadcrumb">
            <li>Production</li>
            <li><a href="{{ route('pekerjaan-vendor.index') }}">Vendor Job</a></li>
            <li class="active">Job {{ $vendor->nama_produsen }}</li>
        </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Job {{ $vendor->nama_produsen }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#onprogress" data-toggle="tab">On Progress</a></li>
                            <li><a href="#historypekerjaan" data-toggle="tab">Job History</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="onprogress">
                                @if($onprogress->count() > 0)
                                    @foreach($onprogress as $op)
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-md-8">
                                                <p style="font-size: 16px;">Production code: <span style="font-weight: bold;">{{ $op->kd_produksi }}</span></p>
                                            </div>
                                            <div class="col-md-4">
                                                <p style="font-size: 16px;">{{ date('d/m/Y', strtotime($op->tgl_produksi)).' s/d '.date('d/m/Y', strtotime($op->deadline)) }}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-md-8">Production</th>
                                                        <th class="col-md-2 text-center">Qty</th>
                                                        <th class="col-md-2 text-center">Finish</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($op->detail_production as $dtlop)
                                                        <tr>
                                                            <td>{{ $dtlop->deskripsi }}</td>
                                                            <td class="text-center">{{ $dtlop->qty }}</td>
                                                            <td class="text-center">{{ $dtlop->qty_progress }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <h4>There are no jobs for vendors yet {{ $vendor->nama_produsen }}</h4>
                                @endif
                            </div>
                            <div class="tab-pane" id="historypekerjaan">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {!! Form::label('tgl', 'Tanggal') !!}
                                            <div class="input-group">
                                                {{ Form::text('tgl_awal', date('Y-m-1'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal', 'style' => 'background-color: #fff;']) }}
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default">S/D</button>
                                                </div>
                                                {{ Form::text('tgl_akhir', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir', 'style' => 'background-color: #fff;']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="dataHistoryPekerjaan"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection