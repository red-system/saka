@if($onprogress->count() > 0)
    @foreach($onprogress as $op)
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-8">
                <p style="font-size: 16px;">Production code: <span style="font-weight: bold;">{{ $op->kd_produksi }}</span></p>
            </div>
            <div class="col-md-4">
                <p style="font-size: 16px;">{{ date('d/m/Y', strtotime($op->tgl_produksi)).' s/d '.date('d/m/Y', strtotime($op->deadline)) }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-md-9">Product</th>
                        <th class="col-md-3 text-center">Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($op->detail_production as $dtlop)
                        <tr>
                            <td>{{ $dtlop->deskripsi }}</td>
                            <td class="text-center">{{ $dtlop->qty }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
@else
    <h4>There are no jobs for vendors yet {{ $vendor->nama_produsen }}</h4>
@endif