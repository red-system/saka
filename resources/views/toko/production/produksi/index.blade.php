@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
    <style>
        .dataTables_length {
            float: right !important;
            display: inline;
        }

        .dataTables_filter {
            text-align: left !important;
            display: inline;
        }

        .filterVendorOp {
            display: inline;
        }

        .filterVendorHst {
            display: inline;
        }

        .dt-buttons {
            margin-left: 10px;
        }
    </style>
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            var onProgressProduksi = $('#tbProduksi').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                } ],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f<"filterVendorOp">><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            $("#filterVendorOp").appendTo(".filterVendorOp");

            $('#selectVendorOp').on('change', function () {
                onProgressProduksi.columns(3).search(this.value).draw();
            });

            var historyProduksi = $('#tbHistoryProduksi').DataTable({
                responsive: true,
                columnDefs: [ {
                    targets: 4,
                    orderable: false
                }],
                order: [0, 'desc'],
                dom: '<"datatable-header"<"row"<"col-md-8 col-sm-6 col-xs-12"f<"filterVendorHst">><"col-md-4 col-sm-6 col-xs-12"l>>><"datatable-scroll-wrap"t><"datatable-footer"<"row"<"col-sm-5"i><"col-sm-7"p>>>',
            });

            $("#filterVendorHst").appendTo(".filterVendorHst");

            $('#selectVendorHst').on('change', function () {
                historyProduksi.columns(3).search(this.value).draw();
            });

            $('.select2').select2();
        })
    </script>
@endsection

@section('title')
    Produksi - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
    <h1>
        Production
        <small>Production</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Production</li>
        <li class="active">Production</li>
    </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#production" data-toggle="tab">Production Data</a></li>
                    <li><a href="#historyproduction" data-toggle="tab">Production History</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="production">
                        <!-- /.col -->
                        <div class="row">
                            <div class="@if(\Gate::allows('as_production')) col-md-2 col-sm-4 col-xs-12 @else col-md-3 col-sm-6 col-xs-12 @endif">
                                <a href="{{ route('produksi.create') }}" class="btn btn-block btn-social btn-google">
                                    <i class="fa fa-plus"></i>Add Production
                                </a>
                                <br>
                            </div>
                        </div>
                        <!-- /.col -->
                        <hr style="margin-top: 0;">
                        <div id="filterVendorOp" for="filterVendorOp" style="display: inline;">
                            <span style="margin: 8px 5px 8px 15px;">Vendor:</span>
                            {{ Form::select('vendor', ['' => 'All']+$productions->pluck('nama_produsen', 'nama_produsen')->all(), null, ['class' => 'select2', 'style' => 'width: 200px;', 'id' => 'selectVendorOp']) }}
                        </div>
                        <table id="tbProduksi" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Kode Production</th>
                                <th>Date</th>
                                <th>Deadline</th>
                                <th>Vendor</th>
                                <th>Status</th>
                                <th class="text-center">Progress</th>
                                <th class="text-center">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($productions as $produksi)
                                <tr>
                                    <td>{{ $produksi->kd_produksi }}</td>
                                    <td>{{ date('d M Y', strtotime($produksi->tgl_produksi)) }}</td>
                                    <td>{{ date('d M Y', strtotime($produksi->deadline)) }}</td>
                                    <td>{{ $produksi->nama_produsen }}</td>
                                    <td>{{ $produksi->human_status }}</td>
                                    <td class="text-center">
                                        <?php
                                        $qtyProgress = $produksi->detail_production()->sum('qty_progress');
                                        $qtyTotal = $produksi->detail_production()->sum('qty');
                                        ?>
                                        {{ round(($qtyProgress > 0 ? (($qtyProgress/$qtyTotal)*100) : 0), 2).'%' }}
                                    </td>
                                    <td class="text-center">
                                        @if($produksi->status == 'draft')
                                            <span data-toggle="tooltip" data-placement="top" title="Edit">
                                                <a href="{{ route('produksi.edit', $produksi->kd_produksi)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                            </span>
                                            {!! Form::model($produksi, ['route' => ['produksi.publish', $produksi->id], 'method' => 'post', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Publish">
                                                <button type="button" class="btn btn-sm btn-warning js-publish-confirm"><i class="fa fa-thumbs-o-up"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                        <span data-toggle="tooltip" data-placement="top" title="View Detail">
                                            <a href="{{ route('produksi.show', $produksi->kd_produksi) }}" data-target="#viewDetailProduksi" data-toggle="modal" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                        </span>
                                        @if($produksi->status != 'draft')
                                            <span data-toggle="tooltip" data-placement="top" title="Print">
                                                <a href="{{ route('produksi.print', $produksi->kd_produksi) }}" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i></a>
                                            </span>
                                        @endif
                                        @if($produksi->status == 'on_progress')
                                            <span data-toggle="tooltip" data-placement="top" title="Progress">
                                                <a href="{{ route('produksi.progress', $produksi->kd_produksi) }}" class="btn btn-sm btn-primary"><i class="fa fa-hourglass-half"></i></a>
                                            </span>
                                        @endif
                                        @if($qtyProgress == $qtyTotal && $produksi->status == 'on_progress')
                                            {!! Form::model($produksi, ['route' => ['produksi.setfinish', $produksi->id], 'method' => 'post', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Set Finish">
                                                <button type="button" class="btn btn-sm btn-success js-confirm-finish"><i class="fa fa-check"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                        @if(in_array($produksi->status, ['draft', 'on_progress']))
                                            {!! Form::model($produksi, ['route' => ['produksi.destroy', $produksi->id], 'method' => 'delete', 'class' => 'form-inline', 'style' => 'display: inline;'] ) !!}
                                            <span data-toggle="tooltip" data-placement="top" title="Batalkan">
                                                <button type="button" class="btn btn-sm btn-danger js-cancel-confirm"><i class="fa fa-times"></i></button>
                                            </span>
                                            {!! Form::close()!!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="historyproduction">
                        <div id="filterVendorHst" for="filterVendorHst" style="display: inline;">
                            <span style="margin: 8px 5px 8px 15px;">Vendor:</span>
                            {{ Form::select('vendor', ['' => 'All']+$historyproductions->pluck('nama_produsen', 'nama_produsen')->all(), null, ['class' => 'select2', 'style' => 'width: 200px;', 'id' => 'selectVendorHst']) }}
                        </div>
                        <table id="tbHistoryProduksi" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Production Code</th>
                                <th>Date</th>
                                <th>Deadline</th>
                                <th>Vendor</th>
                                <th>Status</th>
                                <th class="text-center">Progress</th>
                                <th class="text-center">Act</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($historyproductions as $produksi)
                                <tr>
                                    <td>{{ $produksi->kd_produksi }}</td>
                                    <td>{{ date('d M Y', strtotime($produksi->tgl_produksi)) }}</td>
                                    <td>{{ date('d M Y', strtotime($produksi->deadline)) }}</td>
                                    <td>{{ $produksi->nama_produsen }}</td>
                                    <td>{{ $produksi->human_status }}</td>
                                    <td class="text-center">
                                        <?php
                                        $qtyProgress = $produksi->detail_production()->sum('qty_progress');
                                        $qtyTotal = $produksi->detail_production()->sum('qty');
                                        ?>
                                        {{ round(($qtyProgress > 0 ? (($qtyProgress/$qtyTotal)*100) : 0), 2).'%' }}
                                    </td>
                                    <td class="text-center col-md-2">
                                        <span data-toggle="tooltip" data-placement="top" title="View Detail">
                                            <a href="{{ route('produksi.show', $produksi->kd_produksi) }}" data-target="#viewDetailProduksi" data-toggle="modal" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                                        </span>
                                        @if($produksi->status != 'cancel')
                                            <span data-toggle="tooltip" data-placement="top" title="Print">
                                                <a href="{{ route('produksi.print', $produksi->kd_produksi) }}" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i></a>
                                            </span>
                                        @endif
                                        <span data-toggle="tooltip" data-placement="top" title="Progress">
                                            <a href="{{ route('produksi.progress', $produksi->kd_produksi) }}" class="btn btn-sm btn-primary"><i class="fa fa-hourglass-half"></i></a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-center modal-full">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ asset('img/loading-spinner-grey.gif') }}" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
        </div>
    </div>
@endsection