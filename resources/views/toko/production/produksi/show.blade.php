<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Production Detail</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Production Code</td>
                        <td>: {{ $produksi->kd_produksi }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Production Date</td>
                        <td>: {{ date('d F Y', strtotime($produksi->tgl_produksi)) }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Deadline</td>
                        <td>: {{ date('d F Y', strtotime($produksi->deadline)) }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Vendor</td>
                        <td>: {{ $produksi->produsen ? $produksi->produsen->nama_produsen : $produksi->nama_produsen }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Total</td>
                        <td>: {{ number_format($produksi->total, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Tax</td>
                        <td>: {{ $produksi->ppn_persen.'%' }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Grand Total</td>
                        <td>: {{ number_format($produksi->grand_total, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Remark</td>
                        <td>: {{ $produksi->remark != '' ? $produksi->remark : '-' }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <h4>Production Item</h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Product Code</th>
                        <th>Product</th>
                        <th class="text-center col-md-1">Qty</th>
                        <th class="text-center col-md-1">Finish</th>
                        <th class="col-md-2">Design</th>
                        <th class="col-md-1">Production Cost</th>
                        <th class="col-md-1">Sub Total</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($itemProduksi as $ip)
                        <tr>
                            <td>{{ $ip->kd_produk }}</td>
                            <td>{{ $ip->product ? $ip->product->deskripsi : $ip->deskripsi }}</td>
                            <td class="text-center col-md-1">{{ $ip->qty }}</td>
                            <td class="text-center col-md-1">{{ $ip->qty_progress }}</td>
                            <td>
                                @if($ip->img_design != '')
                                    <div class="thumbnail">
                                        <img src="{{ asset('storage/item-produksi/'.$ip->img_design) }}" alt="">
                                    </div>
                                @endif
                            </td>
                            <td>{{ \Konversi::database_to_localcurrency($ip->biaya_produksi) }}</td>
                            <td>{{ \Konversi::database_to_localcurrency($ip->subtotal_produksi) }}</td>
                            <td>{{ $ip->remark }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <h4>Production Material</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="col-md-2">Material Code</th>
                    <th class="col-md-3">Material</th>
                    <th class="text-center col-md-1">Unit</th>
                    <th class="text-center col-md-2">Qty Requirement</th>
                    <th class="text-center col-md-2">Qty Available</th>
                    <th class="text-center col-md-2">Qty Deficiency</th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialProduksi as $mp)
                    <tr>
                        <td>{{ $mp->kd_material }}</td>
                        <td>{{ $mp->material ? $mp->material->name : $mp->nama_material }}</td>
                        <td>{{ $mp->satuan ? $mp->satuan->satuan : $mp->nama_satuan }}</td>
                        <td class="text-center">{{ $mp->qty_kebutuhan }}</td>
                        <td class="text-center">{{ $mp->qty_tersedia }}</td>
                        <td class="text-center">{{ $mp->qty_kurang }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>
