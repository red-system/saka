@extends($mlayout)

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function hitungSubTotalItem(element, jenis) {
            if(jenis == 'qty') {
                var qty = parseInt($(element).val());
                var biaya = parseFloat($(element).parents('div').siblings('div.itm_biaya').children('input[name="itemBiaya[]"]').val());
            } else {
                var qty = parseFloat($(element).parents('div').siblings('div.itm_qty').children('input[name="itemQty[]"]').val());
                var biaya = parseInt($(element).val());
            }

            if (isNaN(qty)) {
                qty = 0;
            }

            if (isNaN(biaya)) {
                biaya = 0;
            }

            var subtotal = parseFloat(qty*biaya);
            var elmSubTotal = $(element).parents('div').parents('div').siblings('div.row2').find('input[name="itemSubtotalBiaya[]"]');
            elmSubTotal.val(subtotal);

            hitungHargaTotal();

            if(jenis == 'qty') {
                getListMaterial();
            }
        }

        function hitungHargaTotal() {
            var hargaTotal = 0;
            $('#tbItemProduksi .itemSubtotalBiaya').each(function (item){
                var subTotal = parseFloat($(this).val());
                if (isNaN(subTotal)) {
                    subTotal = 0;
                }
                hargaTotal = hargaTotal + subTotal;
            });
            $('#total').val(hargaTotal);
            hitungGrandTotal();
        }

        function hitungGrandTotal() {
            var total = parseFloat($('#total').val());
            var ppnpersen = parseFloat($('#ppn_persen').val());

            var grandtotal = total + ((total * ppnpersen) / 100);
            if (isNaN(grandtotal)) {
                grandtotal = 0;
            }
            $('#grandtotal').val(grandtotal);
        }

        $(function () {
            $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('#tbItemProduksi tbody tr').find('select[name="productKode[]"]').select2();
            $('#tbItemProduksi tbody tr').find('input[name="itemBiaya[]"]').number(true, 2);
            $('#tbItemProduksi tbody tr').find('input[name="itemSubtotalBiaya[]"]').number(true, 2);
            $('.currency').number(true, 2);

            //getListMaterial();

            hitungHargaTotal();

            $('#ppn_persen').on('change', function () {
                hitungGrandTotal()
            });
        })

        function addItem() {
            var row = $('#rowAddItem tbody').html();
            $('#tbItemProduksi tbody').append(row);
            $('#tbItemProduksi tbody tr:last').find('select[name="productKode[]"]').select2();
            $('#tbItemProduksi tbody tr:last').find('input[name="itemBiaya[]"]').number(true, 2);
            $('#tbItemProduksi tbody tr:last').find('input[name="itemSubtotalBiaya[]"]').number(true, 2);
        }

        function removeItem(element) {
            $(element).parents('tr').remove();
            getListMaterial();
            hitungHargaTotal();
        }

        function getListMaterial() {
            var arrProduk = '';
            $('#tbItemProduksi .produkKode').each(function (){
                var kdproduk = $(this).val();
                var qtyproduk = $(this).parents('div').siblings('div.itm_qty').children('input[name="itemQty[]"]').val();

                if(kdproduk != '' && qtyproduk > 0) {
                    arrProduk += kdproduk+','+qtyproduk+'|';
                }
            });

            if(arrProduk != '') {
                $.ajax({
                    url: "{{ route('produksi.getlistmaterial') }}",
                    type: "POST",
                    data: {'produk': arrProduk, '_token': '{{ csrf_token() }}'},
                    success: function(result) {
                        if($('#tbMaterialProduksi tbody tr.material_additional').length == 0 && $('#tbMaterialProduksi tbody tr.material_generate').length == 0) {
                            $('#tbMaterialProduksi tbody').html(result);
                        } else {
                            $('#tbMaterialProduksi tbody tr.material_generate').remove();
                            $('#tbMaterialProduksi tbody').prepend(result);
                        }
                    }
                });
            }
        }

        function addMaterial() {
            var row = $('#rowAddMaterial tbody').html();
            if($('.material_generate').length == 0) {
                $('#tbMaterialProduksi tbody').html(row);
            } else {
                $('#tbMaterialProduksi tbody').append(row);
            }
            $('#tbMaterialProduksi tbody tr:last').find('select[name="materialKode[]"]').select2();
        }

        function removeMaterial(element) {
            $(element).parents('tr').remove();
        }

        function selectKdMaterial(element) {
            var kdmaterial = $(element).val();
            var satuan = $(element).parents('td').siblings('td.mt_satuan').children('input[name="materialSatuan[]"]');
            var qtytersedia = $(element).parents('td').siblings('td.mt_qty_tersedia').children('input[name="materialQtyTersedia[]"]');
            var qtykebutuhan = $(element).parents('td').siblings('td.mt_qty_kebutuhan').children('input[name="materialQtyKebutuhan[]"]');

            $.ajax({
                url: "{{ route('produksi.detailmaterial') }}",
                type: "POST",
                data: {'kdmaterial': kdmaterial, '_token': '{{ csrf_token() }}'},
                success: function(result) {
                    satuan.val(result.satuanMaterial);
                    qtytersedia.val(result.qtyStock);
                    cekQtyMaterial(qtykebutuhan);
                }
            });
        }

        function cekQtyMaterial(element) {
            var elm_qtytersedia = $(element).parents('td').siblings('td.mt_qty_tersedia').children('input[name="materialQtyTersedia[]"]');
            var elm_qtykurang = $(element).parents('td').siblings('td.mt_qty_kurang').children('input[name="materialQtyKurang[]"]');
            var qtytersedia = parseInt(elm_qtytersedia.val());
            var qtykebutuhan = parseInt($(element).val());
            if (qtykebutuhan > qtytersedia) {
                elm_qtykurang.val(qtykebutuhan - qtytersedia);
                elm_qtykurang.parents('td').addClass('has-error');
                swal({
                    title: 'Stock Less',
                    text: 'Sorry, there is not enough stock',
                    type: 'error',
                    html: true,
                });
            } else {
                elm_qtykurang.val(0);
                elm_qtykurang.parents('td').removeClass('has-error');
            }
        }
    </script>
@endsection

@section('title')
    Production - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
        <h1>
            Production
            <small>Production</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}">Home</a></li>
            <li>Production</li>
            <li><a href="{{ route('produksi.index') }}">Production</a></li>
            <li class="active">Add Production</li>
        </ol>
    @endif
@endsection

@section('content')
    {!! Form::open(['route' => 'produksi.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('kd_produksi') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produksi', 'Production Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_produksi', $kodeproduksi, ['class' => 'form-control','required', 'readonly']) }}
                                {!! $errors->first('kd_produksi', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('tgl_produksi') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_produksi', 'Production Date', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('tgl_produksi', !empty($costing) ? $costing->tgl_produksi : null, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tgl_produksi', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('deadline') ? 'has-error' : '' !!}">
                                {!! Form::label('deadline', 'Deadline', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('deadline', !empty($costing) ? $costing->deadline : null, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('deadline', '<p class="help-block">:message</p>') !!}
                            </div>

                            @if(!empty($costing))
                                <div class="form-group {!! $errors->has('kd_costing') ? 'has-error' : '' !!}">
                                    {!! Form::label('kd_costing', 'Costing Code', ['class' => 'control-label']) !!}
                                    {{ Form::text('kd_costing', $costing->kd_costing, ['class' => 'form-control','required', 'readonly']) }}
                                    {!! $errors->first('kd_costing', '<p class="help-block">:message</p>') !!}
                                </div>
                            @endif
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group {!! $errors->has('kd_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produsen', 'Vendor', ['class' => 'control-label']) !!}
                                {{ Form::select('kd_produsen', ['' => '']+App\Models\Produsen::pluck('nama_produsen', 'kd_produsen')->all(), !empty($costing) ? $costing->kd_produsen : null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('remark') ? 'has-error' : '' !!}">
                                {!! Form::label('remark', 'Remark', ['class' => 'control-label']) !!}
                                {{ Form::textarea('remark', !empty($costing) ? $costing->remark : null, ['class' => 'form-control','rows' => 4]) }}
                                {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.col -->
    <div class="row">
        <div class="@if(\Gate::allows('as_production')) col-md-2 col-sm-4 col-xs-12 @else col-md-3 col-sm-6 col-xs-12 @endif">
            <button type="button" class="btn btn-block btn-social btn-google" onclick="addItem()">
                <i class="fa fa-plus"></i> Item
            </button>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Item List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbItemProduksi">
                        <thead>
                        <tr>
                            <th>Item Detail</th>
                            <th class="col-md-2">Design</th>
                            <th class="text-center col-md-1">#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($costing))
                            @if($costing->detail_costing->count() > 0)
                                @foreach($costing->detail_costing as $dc)
                                    <tr>
                                        <td class="itm_detail">
                                            <div class="row row1">
                                                <div class="col-md-5 itm_kdproduk">
                                                    <label for="">Product</label>
                                                    {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), $dc->kd_produk, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                                                </div>
                                                <div class="col-md-3 itm_qty">
                                                    <label for="">Qty</label>
                                                    {{ Form::number('itemQty[]', $dc->qty, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                                                </div>
                                                <div class="col-md-4 itm_biaya">
                                                    <label for="">Production Cost</label>
                                                    {{ Form::text('itemBiaya[]', \Konversi::database_to_localcurrency($dc->biaya_produksi), ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                                                </div>
                                            </div>
                                            <div class="row row2" style="margin-top: 10px;">
                                                <div class="col-md-8 itm_remark">
                                                    <label for="">Remark</label>
                                                    {{ Form::text('itemRemark[]', null, ['class' => 'form-control']) }}
                                                </div>
                                                <div class="col-md-4 itm_subtotal">
                                                    <label for="">Sub Total</label><br>
                                                    {{ Form::text('itemSubtotalBiaya[]', \Konversi::database_to_localcurrency($dc->subtotal_produksi), ['class' => 'form-control itemSubtotalBiaya', 'readonly']) }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="itm_design">
                                            @if($dc->img_design != '')
                                                {{ Form::hidden('costingItemDesign['.$dc->kd_produk.']', $dc->img_design) }}
                                                <div class="thumbnail" style="margin-bottom: 5px;">
                                                    <img src="{{ asset('storage/item-costing/'.$dc->img_design) }}" alt="{{ $dc->img_design }}" style="height: 100px;">
                                                </div>
                                                {{ Form::file('itemDesign[]', null) }}
                                            @else
                                                <label for="">Add File</label>
                                                {{ Form::file('itemDesign[]', null) }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                                <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="itm_detail">
                                        <div class="row row1">
                                            <div class="col-md-5 itm_kdproduk">
                                                <label for="">Product</label>
                                                {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), null, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                                            </div>
                                            <div class="col-md-3 itm_qty">
                                                <label for="">Qty</label>
                                                {{ Form::number('itemQty[]', null, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                                            </div>
                                            <div class="col-md-4 itm_biaya">
                                                <label for="">Production Cost</label>
                                                {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                                            </div>
                                        </div>
                                        <div class="row row2" style="margin-top: 10px;">
                                            <div class="col-md-8 itm_remark">
                                                <label for="">Remark</label>
                                                {{ Form::text('itemRemark[]', null, ['class' => 'form-control']) }}
                                            </div>
                                            <div class="col-md-4 itm_subtotal">
                                                <label for="">Sub Total</label><br>
                                                {{ Form::text('itemSubtotalBiaya[]', 0.00, ['class' => 'form-control itemSubtotalBiaya', 'readonly']) }}
                                            </div>
                                        </div>
                                    </td>
                                    <td class="itm_design">
                                        <label for="">Add File</label>
                                        {{ Form::file('itemDesign[]', null) }}
                                    </td>
                                    <td class="text-center">
                                        <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                            <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td class="itm_detail">
                                    <div class="row row1">
                                        <div class="col-md-5 itm_kdproduk">
                                            <label for="">Product</label>
                                            {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), null, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                                        </div>
                                        <div class="col-md-3 itm_qty">
                                            <label for="">Qty</label>
                                            {{ Form::number('itemQty[]', null, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                                        </div>
                                        <div class="col-md-4 itm_biaya">
                                            <label for="">Production Cost</label>
                                            {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                                        </div>
                                    </div>
                                    <div class="row row2" style="margin-top: 10px;">
                                        <div class="col-md-8 itm_remark">
                                            <label for="">Remark</label>
                                            {{ Form::text('itemRemark[]', null, ['class' => 'form-control']) }}
                                        </div>
                                        <div class="col-md-4 itm_subtotal">
                                            <label for="">Sub Total</label><br>
                                            {{ Form::text('itemSubtotalBiaya[]', 0.00, ['class' => 'form-control itemSubtotalBiaya', 'readonly']) }}
                                        </div>
                                    </div>
                                </td>
                                <td class="itm_design">
                                    <label for="">Add File</label>
                                    {{ Form::file('itemDesign[]', null) }}
                                </td>
                                <td class="text-center">
                                    <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                        <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </span>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- /.col -->
    <div class="row">
        <div class="@if(\Gate::allows('as_production')) col-md-2 col-sm-4 col-xs-12 @else col-md-3 col-sm-6 col-xs-12 @endif">
            <button type="button" class="btn btn-block btn-social btn-facebook" onclick="addMaterial()">
                <i class="fa fa-plus"></i> Material
            </button>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Material List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbMaterialProduksi">
                        <thead>
                        <tr>
                            <th class="col-md-3">Material Code</th>
                            <th class="text-center col-md-2">Unit</th>
                            <th class="text-center col-md-2">Qty Requirement</th>
                            <th class="text-center col-md-2">Qty Available</th>
                            <th class="text-center col-md-2">Qty Deficiency</th>
                            <th class="text-center col-md-1">#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($costing))
                            @if($costing->costing_material->count() > 0)
                                <?php $materialKurang = 0; ?>
                                @foreach($costing->costing_material as $cm)
                                    <tr class="material_generate">
                                        <td class="mt_kode">
                                            {{ Form::text('materialName[]', $cm->kd_material.' - '.$cm->nama_material, ['class' => 'form-control materialName','readonly']) }}
                                            {{ Form::hidden('materialKode[]', $cm->kd_material, ['class' => 'materialKode']) }}
                                            {{ Form::hidden('materialKebutuhan[]', 'komposisi', ['class' => 'materialKebutuhan']) }}
                                        </td>
                                        <td class="text-center mt_satuan">
                                            {{ Form::text('materialSatuan[]', $cm->nama_satuan, ['class' => 'form-control materialSatuan','readonly']) }}
                                            {{ Form::hidden('materialKodeSatuan[]', $cm->kd_satuan, ['class' => 'materialKodeSatuan']) }}
                                        </td>
                                        <td class="text-center mt_qty_kebutuhan">
                                            {{ Form::text('materialQtyKebutuhan[]', $cm->qty_kebutuhan, ['class' => 'form-control materialQtyKebutuhan', 'readonly']) }}
                                        </td>
                                        <?php $qtytersedia = $cm->material ? $cm->material->qty : $cm->qty_tersedia; ?>
                                        <td class="text-center mt_qty_tersedia">
                                            {{ Form::text('materialQtyTersedia[]', $qtytersedia, ['class' => 'form-control materialQtyTersedia', 'readonly']) }}
                                        </td>
                                        <td class="text-center mt_qty_kurang @if($cm->qty_kebutuhan > $qtytersedia) has-error @endif">
                                            {{ Form::text('materialQtyKurang[]', $cm->qty_kebutuhan > $qtytersedia ? ($cm->qty_kebutuhan - $qtytersedia) : 0, ['class' => 'form-control materialQtyKurang', 'readonly']) }}
                                        </td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    if ($cm->qty_kebutuhan > $qtytersedia) {
                                        $materialKurang++;
                                    }
                                    ?>
                                @endforeach

                                @if($materialKurang > 0)
                                    <script>
                                        $(function () {
                                            swal({
                                                title: 'Stock Less',
                                                text: 'Sorry, there are {{ $materialKurang }} insufficient stock material.',
                                                type: 'error',
                                                html: true,
                                            });
                                        })
                                    </script>
                                @endif
                            @else
                                <tr>
                                    <td class="text-center" colspan="6">There is no material requirement, please add product items and make sure the quantity is greater than 0.</td>
                                </tr>
                            @endif
                        @else
                            <tr>
                                <td class="text-center" colspan="6">There is no material requirement, please add product items and make sure the quantity is greater than 0</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-6">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('total', 'Total', ['class' => 'control-label']) !!}
                                {{ Form::text('total', null, ['class' => 'form-control currency', 'readonly', 'id' => 'total']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('ppn_persen') ? 'has-error' : '' !!}">
                                {!! Form::label('ppn_persen', 'Tax', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    {{ Form::number('ppn_persen', 0, ['class' => 'form-control','required', 'id' => 'ppn_persen', 'max' => 100, 'min' => 0]) }}
                                    <div class="input-group-addon">
                                        <span>%</span>
                                    </div>
                                </div>
                                {!! $errors->first('ppn_persen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('grand_total') ? 'has-error' : '' !!}">
                                {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label']) !!}
                                {{ Form::text('grand_total', null, ['class' => 'form-control currency','required', 'readonly', 'id' => 'grandtotal']) }}
                                {!! $errors->first('grand_total', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="@if(\Gate::allows('as_production')) col-md-offset-8 col-md-4 col-xs-12 @else col-md-offset-6 col-md-6 col-xs-12 @endif">
            <div class="col-xs-6" style="padding: 3px;">
                <a href="{{ route('produksi.index') }}" class="btn btn-block btn-default">Cancel</a>
            </div>
            <div class="col-xs-6" style="padding: 3px;">
                <button type="submit" class="btn btn-block btn-success">Save</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <table class="hide" id="rowAddItem">
        <tbody>
        <tr>
            <td class="itm_detail">
                <div class="row row1">
                    <div class="col-md-5 itm_kdproduk">
                        <label for="">Product</label>
                        {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), null, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                    </div>
                    <div class="col-md-3 itm_qty">
                        <label for="">Qty</label>
                        {{ Form::number('itemQty[]', null, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                    </div>
                    <div class="col-md-4 itm_biaya">
                        <label for="">Production Cost</label>
                        {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                    </div>
                </div>
                <div class="row row2" style="margin-top: 10px;">
                    <div class="col-md-8 itm_biaya">
                        <label for="">Remark</label>
                        {{ Form::text('itemRemark[]', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-4 itm_subtotal">
                        <label for="">Sub Total</label><br>
                        {{ Form::text('itemSubtotalBiaya[]', 0.00, ['class' => 'form-control itemSubtotalBiaya', 'readonly']) }}
                    </div>
                </div>
            </td>
            <td class="itm_design">
                <label for="">Add File</label>
                {{ Form::file('itemDesign[]', null) }}
            </td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                    <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="hide" id="rowAddMaterial">
        <tbody>
        <tr class="material_additional">
            <td class="mt_kode">
                {{ Form::select('materialKode[]', ['' => '']+App\Models\Material::selectRaw('kd_material, concat(kd_material, " - ", name) as mtname')->pluck('mtname', 'kd_material')->all(), null, ['class' => 'form-control materialKode', 'style' => 'width: 100%;', 'onchange' => 'selectKdMaterial(this)']) }}
                {{ Form::hidden('materialKebutuhan[]', 'tambahan', ['class' => 'materialKebutuhan']) }}
            </td>
            <td class="text-center mt_satuan">
                {{ Form::text('materialSatuan[]', null, ['class' => 'form-control materialSatuan','readonly']) }}
                {{ Form::hidden('materialKodeSatuan[]', null, ['class' => 'materialKodeSatuan']) }}
            </td>
            <td class="text-center mt_qty_kebutuhan">
                {{ Form::number('materialQtyKebutuhan[]', 0, ['class' => 'form-control materialQtyKebutuhan', 'onchange' => 'cekQtyMaterial(this)', 'onkeyup' => 'cekQtyMaterial(this)']) }}
            </td>
            <td class="text-center mt_qty_tersedia">
                {{ Form::text('materialQtyTersedia[]', null, ['class' => 'form-control materialQtyTersedia', 'readonly']) }}
            </td>
            <td class="text-center mt_qty_kurang">
                {{ Form::text('materialQtyKurang[]', null, ['class' => 'form-control materialQtyKurang', 'readonly']) }}
            </td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                    <button type="button" class="btn btn-danger" onclick="removeMaterial(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
@endsection