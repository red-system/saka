@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        $(function () {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            $('#historyProgress').DataTable({
                responsive: true,
                order: [0, 'desc'],
            });
        })
    </script>
@endsection

@section('title')
    Production Progress - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
        <h1>
            Production
            <small>Production</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}">Home</a></li>
            <li>Production</li>
            <li><a href="{{ route('produksi.index') }}">Production</a></li>
            <li class="active">Production Progress</li>
        </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Production Progress ({{ $produksi->kd_produksi }})</h3>
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-info"><strong>{{ ($itemProduksi->sum('qty_progress') > 0 ? round((($itemProduksi->sum('qty_progress')/$itemProduksi->sum('qty'))*100), 2) : 0).'%' }}</strong></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if($itemProduksi->sum('qty') != $itemProduksi->sum('qty_progress') && $produksi->status == 'on_progress')
                    <div class="nav-tabs-custom" style="margin-bottom: 10px; box-shadow: none;">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#progress" data-toggle="tab">Progress</a></li>
                            <li><a href="#history" data-toggle="tab">History Progress</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="progress">
                                {!! Form::open(['route' => ['produksi.updateProgress', $produksi->kd_produksi], 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                <div class="form-group {!! $errors->has('tanggal') ? 'has-error' : '' !!}">
                                    {!! Form::label('tanggal', 'Progress Date', ['class' => 'control-label col-md-2', 'style' => 'padding-left: 0;padding-top: 8px;']) !!}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="input-group date">
                                                {{ Form::text('tanggal', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly']) }}
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                            {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Product Code</th>
                                            <th>Product</th>
                                            <th class="text-center col-md-1">Qty</th>
                                            <th class="text-center col-md-1">Finish</th>
                                            <th class="text-center col-md-2">New Progress</th>
                                            <th>Design</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($itemProduksi as $ip)
                                            <tr>
                                                <td>{{ $ip->kd_produk }}</td>
                                                <td>{{ $ip->deskripsi }}</td>
                                                <td class="text-center col-md-1">{{ $ip->qty }}</td>
                                                <td class="text-center col-md-1">{{ $ip->qty_progress }}</td>
                                                <td class="text-center col-md-2">
                                                    @if($ip->qty > $ip->qty_progress)
                                                        {{ Form::number('newProgress['.$ip->kd_detail_production.']', null, ['class' => 'form-control', 'min' => 0, 'max' => ($ip->qty - $ip->qty_progress)]) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td class="col-md-3">
                                                    @if($ip->img_design != '')
                                                        <img src="{{ asset('storage/item-produksi/'.$ip->img_design) }}" alt="">
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <hr style="margin-bottom: 10px; margin-top: 0px;">
                                <div class="row">
                                    <div class="@if(\Gate::allows('as_production')) col-md-offset-8 col-md-4 col-xs-12 @else col-md-offset-6 col-md-6 col-xs-12 @endif">
                                        <div class="col-xs-6" style="padding: 3px;">
                                            <a href="{{ route('produksi.index') }}" class="btn btn-block btn-default">Cancel</a>
                                        </div>
                                        <div class="col-xs-6" style="padding: 3px;">
                                            <button type="submit" class="btn btn-block btn-success">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="history">
                    @endif
                                <table id="historyProgress" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th class="col-md-3">Date</th>
                                        <th>Kode Product</th>
                                        <th>Product</th>
                                        <th class="text-center">Qty Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($historyProduction as $hp)
                                        <tr>
                                            <td>{{ $hp->tanggal }}</td>
                                            <td>{{ $hp->kd_produk }}</td>
                                            <td>{{ $hp->deskripsi }}</td>
                                            <td class="text-center">{{ $hp->qty_progress }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                    @if($itemProduksi->sum('qty') != $itemProduksi->sum('qty_progress') && $produksi->status == 'on_progress')
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection