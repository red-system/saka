<html>
<head>
    <title>Production {{ $produksi->kd_produksi }}</title>
    <style>
        @page { margin: 15px 30px; }

        body {
            font-family: Tahoma,Verdana,Segoe,sans-serif;
        }

        .lr-none {
            border-right: none;
            border-left: none;
        }

        .tb-grey {
            border-top: 0.5px solid #888;
            border-bottom: 0.5px solid #888;
        }

        .pd10 {
            padding-bottom: 7px;
            padding-top: 7px;
        }

        .pd8 {
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .left {
            text-align: left;
        }

        .right {
            text-align: right;
        }

        .vat {
            vertical-align: top;
        }

        .text-muted {
            color: #000000;
        }

        table > tr > td {
            font-family: Tahoma,Verdana,Segoe,sans-serif !important;
        }
    </style>
</head>
<body>
<table width="100%">
    <tr>
        <td width="7%" style="vertical-align: middle;">
            <img style="margin-top: 3px;" height="105px;" src="{{ asset('img/logo-saka.png') }}" alt="">
        </td>
        <td width="93%" style="vertical-align: top;">
            <table width="100%" style="margin-top: 10px; font-size: 11px;">
                <tr>
                    <td colspan="3"><strong style="font-size: 15px;">Production</strong></td>
                </tr>
                <tr>
                    <td width="17%"><strong>Production Code</strong></td>
                    <td width="1%">:</td>
                    <td width="82%" style="text-align: left;">{{ $produksi->kd_produksi }}</td>
                </tr>
                <tr>
                    <td><strong>Production Date</strong></td>
                    <td>:</td>
                    <td style="text-align: left;">{{ date('d F Y', strtotime($produksi->tgl_produksi)) }}</td>
                </tr>
                <tr>
                    <td><strong>Deadline</strong></td>
                    <td>:</td>
                    <td style="text-align: left;">{{ date('d F Y', strtotime($produksi->deadline)) }}</td>
                </tr>
                <tr>
                    <td><strong>Vendor</strong></td>
                    <td>:</td>
                    <td width="89%" style="text-align: left;">{{ $produksi->produsen ? $produksi->produsen->nama_produsen : $produksi->nama_produsen }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" class="table" border="1" style="margin-top: 10px; font-size: 11px; border-collapse: collapse; border-bottom: none; border-top: none; border-left: none; border-right: none;">
    <tr>
        <th class="lr-none tb-grey pd10 left" width="8%">Product Code</th>
        <th class="lr-none tb-grey pd10 left" width="17%">Product</th>
        <th class="lr-none tb-grey pd10 center" width="5%">Qty</th>
        {{--  <th class="lr-none tb-grey pd10 center" width="5%">Finish</th>  --}}
        <th class="lr-none tb-grey pd10 right" width="15%">Design</th>
        {{--  <th class="lr-none tb-grey pd10 right" width="15%">Production Cost</th>  --}}
        {{--  <th class="lr-none tb-grey pd10 right" width="15%">Sub Total</th>  --}}
        <th class="lr-none tb-grey pd10 center" width="20%">Remark</th>
    </tr>
    @foreach($itemProduksi as $ip)
        <tr>
            <td class="lr-none tb-grey pd8 vat">{{ $ip->kd_produk }}</td>
            <td class="lr-none tb-grey pd8 vat">{{ $ip->product ? $ip->product->deskripsi : $ip->deskripsi }}</td>
            <td class="lr-none tb-grey pd8 center vat">{{ $ip->qty }}</td>
            {{--  <td class="lr-none tb-grey pd8 center vat">{{ $ip->qty_progress }}</td>  --}}
            <td class="lr-none tb-grey pd8">
                @if($ip->img_design != '')
                    <div class="thumbnail" style="width: 100%;">
                        <img src="{{ asset('storage/item-produksi/'.$ip->img_design) }}" alt="">
                    </div>
                @endif
            </td>
            {{--  <td class="lr-none tb-grey pd8 right vat">{{ \Konversi::database_to_localcurrency($ip->biaya_produksi) }}</td>
            <td class="lr-none tb-grey pd8 right vat">{{ \Konversi::database_to_localcurrency($ip->subtotal_produksi) }}</td>  --}}
            <td class="lr-none tb-grey pd8 vat">{{ $ip->remark }}</td>
        </tr>
    @endforeach
</table>

<table width="100%" style="margin-top: 10px;">
    <tr>
        <td width="60%" style="font-size: 12px; padding-top: 8px; vertical-align: top;">
            <span style="font-weight: bold;">Remark:</span>
            @if($produksi->remark != '')
                <br>{!! $produksi->remark !!}
            @endif
        </td>
        {{--  <td width="40%">
            <table width="100%" style="font-size: 12px; border-collapse: collapse;">
                <tr>
                    <td class="lr-none tb-grey pd8 left" width="50%">Total</td>
                    <td class="lr-none tb-grey pd8 right" width="50%">{{ number_format($produksi->total, 2, ',', '.') }}</td>
                </tr>

                <tr>
                    <td class="lr-none tb-grey pd8 left">Tax ({{ $produksi->ppn_persen.'%' }})</td>
                    <td class="lr-none tb-grey pd8 right">{{ number_format($produksi->ppn_nominal, 2, ',', '.') }}</td>
                </tr>

                <tr>
                    <td class="lr-none tb-grey pd8 left">Grand Total</td>
                    <td class="lr-none tb-grey pd8 right" style="color: #000000;"><strong>{{ number_format($produksi->grand_total, 2, ',', '.') }}</strong></td>
                </tr>
            </table>
        </td>  --}}
    </tr>
</table>
</body>
</html>