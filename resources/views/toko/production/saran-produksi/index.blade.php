@extends($mlayout)

@section('custom-css')

@endsection

@section('plugin-js')

@endsection

@section('custom-script')

@endsection

@section('title')
    Production Advice - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
    <h1>
        Production Advice
        <small>Production</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li>Production</li>
        <li class="active">Production Advice</li>
    </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Production Advice</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="col-md-3">Product Code</th>
                            <th>Product Description</th>
                            <th class="text-center col-md-2">Qty</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($cekproduksi->filter(function ($mt) { return $mt->jml_produksi > 0; })->count() > 0)
                            @foreach($cekproduksi->filter(function ($mt) { return $mt->jml_produksi > 0; }) as $sproduk)
                                <tr>
                                    <td>{{ $sproduk->kd_produk }}</td>
                                    <td>{{ $sproduk->deskripsi }}</td>
                                    <td class="text-center col-md-2">{{ $sproduk->jml_produksi }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <th class="text-center" colspan="3"> No production advice data yet</th>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection