@if(count($allMaterial) > 0)
    <?php $materialKurang = 0; ?>
    @foreach($allMaterial as $material)
        <?php
            $cekmaterial = $oldCostingMaterial->where('kd_material', $material['kd_material'])->first();
        ?>
        <tr>
            <td class="mt_kode">
                @if(!empty($cekmaterial))
                {!! Form::hidden('materialId['.$cekmaterial->kd_material.']', $cekmaterial->id) !!}
                @endif
                {{ Form::text('materialName[]', $material['view_material'], ['class' => 'form-control materialName','readonly']) }}
                {{ Form::hidden('materialKode[]', $material['kd_material'], ['class' => 'materialKode']) }}
            </td>
            <td class="text-center mt_satuan">
                {{ Form::text('materialSatuan[]', $material['satuan'], ['class' => 'form-control materialSatuan','readonly']) }}
                {{ Form::hidden('materialKodeSatuan[]', $material['kd_satuan'], ['class' => 'materialKodeSatuan']) }}
            </td>
            <td class="text-center mt_qty_kebutuhan">
                {{ Form::text('materialQtyKebutuhan[]', $material['qty_kebutuhan'], ['class' => 'form-control materialQtyKebutuhan', 'readonly']) }}
            </td>
            <td class="text-center mt_qty_tersedia">
                {{ Form::text('materialQtyTersedia[]', $material['qty_tersedia'], ['class' => 'form-control materialQtyTersedia', 'readonly']) }}
            </td>
            <td class="text-center mt_qty_kurang @if($material['qty_kurang'] > 0) has-error @endif">
                {{ Form::text('materialQtyKurang[]', $material['qty_kurang'], ['class' => 'form-control materialQtyKurang', 'readonly']) }}
            </td>
        </tr>
        <?php
        if ($material['qty_kurang'] > 0) {
            $materialKurang++;
        }
        ?>
    @endforeach

    @if($materialKurang > 0)
        <script>
            $(function () {
                swal({
                    title: 'Lack of stock',
                    text: 'Sorry, there are {{ $materialKurang }} insufficient stock material.',
                    type: 'error',
                    html: true,
                });
            })
        </script>
    @endif
@else
    <tr>
        <td class="text-center" colspan="5">There is no material requirement, please add product items and make sure the quantity is greater than 0</td>
    </tr>
@endif

