<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Detail Costing</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Costing Code</td>
                        <td>: {{ $costing->kd_costing }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Production Date</td>
                        <td>: {{ date('d F Y', strtotime($costing->tgl_produksi)) }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Deadline</td>
                        <td>: {{ date('d F Y', strtotime($costing->deadline)) }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Vendor</td>
                        <td>: {{ $costing->produsen ? $costing->produsen->nama_produsen : $costing->nama_produsen }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Total</td>
                        <td>: {{ number_format($costing->total, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Tax</td>
                        <td>: {{ $costing->ppn_persen.'%' }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Grand Total</td>
                        <td>: {{ number_format($costing->grand_total, 2, ',', '.') }}</td>
                    </tr>
                    <tr>
                        <td class="col-md-4">Remark</td>
                        <td>: {{ $costing->remark != '' ? $costing->remark : '-' }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <h4>Item Produksi</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Product code</th>
                    <th>Product</th>
                    <th class="text-center col-md-1">Qty</th>
                    <th class="col-md-2">Design</th>
                    <th class="col-md-2">Production cost</th>
                    <th class="col-md-2">Sub Total</th>
                </tr>
                </thead>
                <tbody>
                @foreach($itemCosting as $ic)
                    <tr>
                        <td>{{ $ic->kd_produk }}</td>
                        <td>{{ $ic->product ? $ic->product->deskripsi : $ic->deskripsi }}</td>
                        <td class="text-center col-md-1">{{ $ic->qty }}</td>
                        <td>
                            @if($ic->img_design != '')
                                <div class="thumbnail">
                                    <img src="{{ asset('storage/item-costing/'.$ic->img_design) }}" alt="">
                                </div>
                            @endif
                        </td>
                        <td>{{ \Konversi::database_to_localcurrency($ic->biaya_produksi) }}</td>
                        <td>{{ \Konversi::database_to_localcurrency($ic->subtotal_produksi) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <h4>Material Produksi</h4>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="col-md-3">Material Code</th>
                    <th class="text-center col-md-2">Unit</th>
                    <th class="text-center col-md-2">Qty Requirement</th>
                    <th class="text-center col-md-2">Qty Available</th>
                    <th class="text-center col-md-2">Qty Deficiency</th>
                </tr>
                </thead>
                <tbody>
                @foreach($materialCosting as $mc)
                    <tr>
                        <td>{{ $mc->kd_material }}</td>
                        <td>{{ $mc->material ? $mc->material->name : $mc->nama_material }}</td>
                        <td>{{ $mc->satuan ? $mc->satuan->satuan : $mc->nama_satuan }}</td>
                        <td class="text-center">{{ $mc->qty_kebutuhan }}</td>
                        <td class="text-center">{{ $mc->qty_tersedia }}</td>
                        <td class="text-center">{{ $mc->qty_kurang }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
</div>
