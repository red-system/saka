@extends($mlayout)

@section('custom-css')
    <link href="{{ asset('sweet-alert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery.number.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function hitungSubTotalItem(element, jenis) {
            if(jenis == 'qty') {
                var qty = parseInt($(element).val());
                var biaya = parseFloat($(element).parents('td').siblings('td.itm_biaya').children('input[name="itemBiaya[]"]').val());
            } else {
                var qty = parseFloat($(element).parents('td').siblings('td.itm_qty').children('input[name="itemQty[]"]').val());
                var biaya = parseInt($(element).val());
            }

            if (isNaN(qty)) {
                qty = 0;
            }

            if (isNaN(biaya)) {
                biaya = 0;
            }

            var subtotal = parseFloat(qty*biaya);
            var elmSubTotal = $(element).parents('td').siblings('td.itm_subtotal').children('input[name="itemSubtotalBiaya[]"]');
            elmSubTotal.val(subtotal);

            var txtSubTotal = $(element).parents('td').siblings('td.itm_subtotal').children('span.txtItemSubtotalBiaya');
            txtSubTotal.html($.number(subtotal, 2, ',', '.' ));

            hitungHargaTotal();

            if(jenis == 'qty') {
                getListMaterial();
            }
        }

        function hitungHargaTotal() {
            var hargaTotal = 0;
            $('#tbItemCosting .itemSubtotalBiaya').each(function (item){
                var subTotal = parseFloat($(this).val());
                if (isNaN(subTotal)) {
                    subTotal = 0;
                }
                hargaTotal = hargaTotal + subTotal;
            });
            $('#total').val(hargaTotal);
            hitungGrandTotal();
        }
        
        function hitungGrandTotal() {
            var total = parseFloat($('#total').val());
            var ppnpersen = parseFloat($('#ppn_persen').val());

            var grandtotal = total + ((total * ppnpersen) / 100);
            if (isNaN(grandtotal)) {
                grandtotal = 0;
            }
            $('#grandtotal').val(grandtotal);
        }

        $(function () {
            $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });

            hitungHargaTotal();

            $('#ppn_persen').on('change', function () {
                hitungGrandTotal()
            });

            $('#tbItemCosting tbody tr').find('select[name="productKode[]"]').select2();
            $('#tbItemCosting tbody tr').find('input[name="itemBiaya[]"]').number(true, 2);
            $('.currency').number(true, 2);

            @if(count($listproduk) > 0)
                getListMaterial();
            @endif
        })

        function addItem() {
            var row = $('#rowAddItem tbody').html();
            $('#tbItemCosting tbody').append(row);
            $('#tbItemCosting tbody tr:last').find('select[name="productKode[]"]').select2();
            $('#tbItemCosting tbody tr:last').find('input[name="itemBiaya[]"]').number(true, 2);
        }

        function removeItem(element) {
            $(element).parents('tr').remove();
            getListMaterial();
            hitungHargaTotal();
        }

        function getListMaterial() {
            var arrProduk = '';
            $('#tbItemCosting .produkKode').each(function (){
                var kdproduk = $(this).val();
                var qtyproduk = $(this).parents('td').siblings('td.itm_qty').children('input[name="itemQty[]"]').val();

                if(kdproduk != '' && qtyproduk > 0) {
                    arrProduk += kdproduk+','+qtyproduk+'|';
                }
            });

            if(arrProduk != '') {
                $.ajax({
                    url: "{{ route('costing.getlistmaterial') }}",
                    type: "POST",
                    data: {'produk': arrProduk, '_token': '{{ csrf_token() }}'},
                    success: function(result) {
                        $('#tbMaterialCosting tbody').html(result);
                    }
                });
            }
        }
    </script>
@endsection

@section('title')
    Costing - Saka Karya Bali
@endsection

@section('pageheader')
    @if(\Gate::denies('as_production'))
        <h1>
            Costing
            <small>Production</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard') }}">Home</a></li>
            <li>Production</li>
            <li><a href="{{ route('costing.index') }}">Costing</a></li>
            <li class="active">Add Costing</li>
        </ol>
    @endif
@endsection

@section('content')
    {!! Form::open(['route' => 'costing.store', 'class' => 'jq-validate', 'method' => 'post', 'novalidate', 'files' => true]) !!}
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('kd_costing') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_costing', 'Costing Code', ['class' => 'control-label']) !!}
                                {{ Form::text('kd_costing', $kodecosting, ['class' => 'form-control','required', 'readonly']) }}
                                {!! $errors->first('kd_costing', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('tgl_produksi') ? 'has-error' : '' !!}">
                                {!! Form::label('tgl_produksi', 'Production Date', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('tgl_produksi', null, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('tgl_produksi', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('deadline') ? 'has-error' : '' !!}">
                                {!! Form::label('deadline', 'Deadline', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    {{ Form::text('deadline', null, ['class' => 'form-control date-picker','required', 'readonly']) }}
                                </div>
                                <!-- /.input group -->
                                {!! $errors->first('deadline', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group {!! $errors->has('kd_produsen') ? 'has-error' : '' !!}">
                                {!! Form::label('kd_produsen', 'Vendor', ['class' => 'control-label']) !!}
                                {{ Form::select('kd_produsen', ['' => '']+App\Models\Produsen::pluck('nama_produsen', 'kd_produsen')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;']) }}
                                {!! $errors->first('kd_produsen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('remark') ? 'has-error' : '' !!}">
                                {!! Form::label('remark', 'Remark', ['class' => 'control-label']) !!}
                                {{ Form::textarea('remark', null, ['class' => 'form-control','rows' => 4]) }}
                                {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.col -->
    <div class="row">
        <div class="@if(\Gate::allows('as_production')) col-md-2 col-sm-4 col-xs-12 @else col-md-3 col-sm-6 col-xs-12 @endif">
            <button type="button" class="btn btn-block btn-social btn-google" onclick="addItem()">
                <i class="fa fa-plus"></i> Item
            </button>
            <br>
        </div>
    </div>
    <!-- /.col -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Item List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbItemCosting">
                        <thead>
                        <tr>
                            <th>Produk</th>
                            <th class="text-center col-md-1">Qty</th>
                            <th class="col-md-2">Design</th>
                            <th class="col-md-2">Production Cost</th>
                            <th class="col-md-2">Sub Total</th>
                            <th class="text-center col-md-1">#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($listproduk) > 0)
                            @foreach($listproduk as $kprd)
                                <tr>
                                    <td class="itm_kdproduk">
                                        {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), $kprd['kd_produk'], ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                                    </td>
                                    <td class="text-center itm_qty">
                                        {{ Form::number('itemQty[]', $kprd['kebutuhan'], ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                                    </td>
                                    <td class="itm_design">
                                        {{ Form::file('itemDesign[]', null) }}
                                    </td>
                                    <td class="itm_biaya">
                                        {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                                    </td>
                                    <td style="padding-top: 15px;" class="itm_subtotal">
                                        <span class="txtItemSubtotalBiaya">0,00</span>
                                        {{ Form::hidden('itemSubtotalBiaya[]', 0.00, ['class' => 'itemSubtotalBiaya']) }}
                                    </td>
                                    <td class="text-center">
                                        <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                            <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="itm_kdproduk">
                                    {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), null, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
                                </td>
                                <td class="text-center itm_qty">
                                    {{ Form::number('itemQty[]', null, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
                                </td>
                                <td class="itm_design">
                                    {{ Form::file('itemDesign[]', null) }}
                                </td>
                                <td class="itm_biaya">
                                    {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
                                </td>
                                <td style="padding-top: 15px;" class="itm_subtotal">
                                    <span class="txtItemSubtotalBiaya">0,00</span>
                                    {{ Form::hidden('itemSubtotalBiaya[]', 0.00, ['class' => 'itemSubtotalBiaya']) }}
                                </td>
                                <td class="text-center">
                                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                                    <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </span>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Material List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" id="tbMaterialCosting">
                        <thead>
                        <tr>
                            <th class="col-md-3">Material Code</th>
                            <th class="text-center col-md-2">Unit</th>
                            <th class="text-center col-md-2">Qty Requirement</th>
                            <th class="text-center col-md-2">Qty Available</th>
                            <th class="text-center col-md-2">Qty Deficiency</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center" colspan="5">There is no material requirement, please add product items and make sure the quantity is greater than 0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-6">
                            <!-- text input -->
                            <div class="form-group {!! $errors->has('total') ? 'has-error' : '' !!}">
                                {!! Form::label('total', 'Total', ['class' => 'control-label']) !!}
                                {{ Form::text('total', null, ['class' => 'form-control currency', 'readonly', 'id' => 'total']) }}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('ppn_persen') ? 'has-error' : '' !!}">
                                {!! Form::label('ppn_persen', 'Tax', ['class' => 'control-label']) !!}
                                <div class="input-group date">
                                    {{ Form::number('ppn_persen', 0, ['class' => 'form-control','required', 'id' => 'ppn_persen', 'max' => 100, 'min' => 0]) }}
                                    <div class="input-group-addon">
                                        <span>%</span>
                                    </div>
                                </div>
                                {!! $errors->first('ppn_persen', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {!! $errors->has('grand_total') ? 'has-error' : '' !!}">
                                {!! Form::label('grand_total', 'Grand Total', ['class' => 'control-label']) !!}
                                {{ Form::text('grand_total', null, ['class' => 'form-control currency','required', 'readonly', 'id' => 'grandtotal']) }}
                                {!! $errors->first('grand_total', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="@if(\Gate::allows('as_production')) col-md-offset-8 col-md-4 col-xs-12 @else col-md-offset-6 col-md-6 col-xs-12 @endif">
            <div class="col-xs-6" style="padding: 3px;">
                <a href="{{ route('costing.index') }}" class="btn btn-block btn-default">Cancel</a>
            </div>
            <div class="col-xs-6" style="padding: 3px;">
                <button type="submit" class="btn btn-block btn-success">Simpan</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <table class="hide" id="rowAddItem">
        <tbody>
        <tr>
            <td class="itm_kdproduk">
                {{ Form::select('productKode[]', ['' => '']+App\Models\Product::selectRaw('kd_produk, concat(kd_produk, " - ", deskripsi) as prdname')->pluck('prdname', 'kd_produk')->all(), null, ['class' => 'form-control produkKode', 'style' => 'width: 100%;', 'onchange' => 'getListMaterial()']) }}
            </td>
            <td class="text-center itm_qty">
                {{ Form::number('itemQty[]', null, ['class' => 'form-control', 'min' => 0, 'onchange' => 'hitungSubTotalItem(this, "qty")']) }}
            </td>
            <td class="itm_design">
                {{ Form::file('itemDesign[]', null) }}
            </td>
            <td class="itm_biaya">
                {{ Form::text('itemBiaya[]', 0, ['class' => 'form-control', 'onchange' => 'hitungSubTotalItem(this, "biaya")']) }}
            </td>
            <td style="padding-top: 15px;" class="itm_subtotal">
                <span class="txtItemSubtotalBiaya">0,00</span>
                {{ Form::hidden('itemSubtotalBiaya[]', 0.00, ['class' => 'itemSubtotalBiaya']) }}
            </td>
            <td class="text-center">
                <span data-toggle="tooltip" data-placement="top" title="Delete Item">
                    <button type="button" class="btn btn-danger" onclick="removeItem(this)">
                        <i class="fa fa-trash"></i>
                    </button>
                </span>
            </td>
        </tr>
        </tbody>
    </table>
@endsection