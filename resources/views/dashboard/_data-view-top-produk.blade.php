<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Product Code</th>
        <th>Description</th>
        <th>Size</th>
        <th class="text-center">Qty</th>
    </tr>
    </thead>
    <tbody>
    <?php $totalQty=0; ?>
    @if($topproductsales->count() > 0)
        @foreach($topproductsales as $tps)
            <tr>
                <td>{{ $tps->kd_produk }}</td>
                <td>{{ $tps->produk ? $tps->produk->deskripsi : '' }}</td>
                <td>{{ $tps->size_product }}</td>
                <td class="text-center">{{ $tps->totalQty }}</td>
                <?php
                    $totalQty=$totalQty+$tps->totalQty;
                ?>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="3">There are no top data products available</td>
        </tr>
    @endif
    <tr>
        <td class="text-center" colspan="3" style="font-weight:bold;">Total Qty</td>
        <td class="text-center" colspan="3">{{$totalQty}}</td>
    </tr>
    </tbody>
</table>