<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>SPB/SPG</th>
        <th>Total Sales</th>
        <th>Location</th>
    </tr>
    </thead>
    <tbody>
    {{--  @if($topsalesusers->count() > 0)
        @foreach($topsalesusers as $tsr)
            <tr>
                <td>
                    {{ $tsr->sales_user ? $tsr->sales_user->name : '' }}
                </td>
                <td>{{ 'Rp '.number_format($tsr->totalSales, 0, ',', '.') }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="2">There are no top sales SPG/SPB data available</td>
        </tr>
    @endif  --}}
    {{--  @if($topsalesusers->count() > 0)
        @foreach($topsalesusers as $tsr)
            <tr>
                <td>
                    {{ $tsr->name }}
                </td>
                <td>{{ 'Rp '.number_format($tsr->totalSales, 0, ',', '.') }}</td>
                <td>
                    {{ $tsr->location_name }}
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="2">There are no top sales SPG/SPB data available</td>
        </tr>
    @endif  --}}
    @if($topsales->count() > 0)
        @foreach($topsales as $tsr)
            <tr>
                <td>
                    {{ $tsr->name }}
                </td>
                <td>{{ 'Rp '.number_format($tsr->totalSales, 0, ',', '.') }}</td>
                <td>
                    {{ $tsr->location_name }}
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="2">There are no top sales SPG/SPB data available</td>
        </tr>
    @endif
    </tbody>
</table>