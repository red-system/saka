{!! Form::open(['route' => 'view-all-customer.broadcast', 'class' => 'form-horizontal'])!!}
<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th width="15px"><input type="checkbox" id="checkAll"></th>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Total Shoping</th>
    </tr>
    </thead>
    <tbody>
    @if($topshopingcustomers->count() > 0)
        @foreach($topshopingcustomers as $tsc)
            <tr>
                <td>{!! Form::checkbox('listcustomer[]', $tsc->email, false, ['class' => 'customer']) !!}</td>
                <td>
                    {{ $tsc->nama_pelanggan }}
                    {!! Form::hidden('namapelanggan[]', $tsc->nama_pelanggan) !!}
                </td>
                <td>{{ $tsc->email }}</td>
                <td>{{ 'Rp '.number_format($tsc->totalShoping, 0, ',', '.') }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="4">There are no top shopping customer data available</td>
        </tr>
    @endif
    </tbody>
</table>
<hr style="margin-bottom: 10px;">
<div class="text-right">
    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addMessage">Broadcast Email</button>
</div>
<!-- MODALS -->
<div class="modal fade" id="addMessage">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Broadcast Email Message</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {!! $errors->has('subject') ? 'has-error' : '' !!}">
                            {!! Form::label('subject', 'Subject', ['class' => 'col-md-2']) !!}
                            <div class="col-md-10">
                                {{ Form::text('subject', null, ['class' => 'form-control','required']) }}
                                {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {!! $errors->has('message') ? 'has-error' : '' !!}">
                            {!! Form::label('message', 'Message', ['class' => 'col-md-2']) !!}
                            <div class="col-md-10">
                                {{ Form::textarea('message', null, ['class' => 'form-control editor-textarea','required', 'rows' => 5]) }}
                                {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                {!! Form::button('Send Email', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{!! Form::close() !!}
<script>
    $("#checkAll").click(function() {
        var checked = $(this).prop('checked');
        $('.customer').each(function(){
            $(this).prop("checked", checked).trigger("change");
        });
    });

    $('.customer').change(function() {
        if ($('.customer:checked').length == $('.customer').length) {
            $("#checkAll").prop("checked", true);
        } else {
            $("#checkAll").prop("checked", false);
        }
    });

    $('.editor-textarea').each(function(e){
        CKEDITOR.replace(this.id ,{
            filebrowserBrowseUrl : '{{url("ckeditor")}}/filemanager/dialog.php?type=2&editor=ckeditor&akey={{ md5('goestoe_ari_2905') }}&fldr=',
            filebrowserUploadUrl : '{{url("ckeditor")}}/filemanager/dialog.php?type=2&editor=ckeditor&akey={{ md5('goestoe_ari_2905') }}&fldr=',
            filebrowserImageBrowseUrl : '{{url("ckeditor")}}/filemanager/dialog.php?type=1&editor=ckeditor&akey={{ md5('goestoe_ari_2905') }}&fldr='
        });
    });
</script>