<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Category</th>
        <th class="col-md-2 text-center">Qty Sales</th>
    </tr>
    </thead>
    <tbody>
    @if($topcategorysales->count() > 0)
        @foreach($topcategorysales as $tcs)
            <tr>
                <td>{{ $tcs->kategori }}</td>
                <td class="text-center">{{ $tcs->totalQty }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="2">There is no top category sales data available</td>
        </tr>
    @endif
    </tbody>
</table>