@extends('layouts.master-toko')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            if ($(document.getElementById("tglawal")).length > 0 && $(document.getElementById("tglakhir")).length > 0) {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('view-all-customer.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir, location:location, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataTopShoppingCustomer').html(result);
                    });
                }
            }

            $("#tglawal").change(function () {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('view-all-customer.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir,location:location, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataTopShoppingCustomer').html(result);
                    });
                }
            })

            $("#tglakhir").change(function () {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('view-all-customer.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir,location:location, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataTopShoppingCustomer').html(result);
                    });
                }
            })
        })
    </script>
@endsection

@section('title')
    View Top Shoping Customer - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        View Top Shoping Customer
        <small>Dashboard</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li class="active">View Top Shoping Customer</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">View Top Shoping Customer</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            {{ Form::hidden('location', $location, ['class' => 'form-control','id' => 'location']) }}
                            <div class="form-group">
                                {!! Form::label('tgl', 'Date') !!}
                                <div class="input-group">
                                    {{ Form::text('tgl_awal', $from != '' ? $from : date('Y-m-1'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal']) }}
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default">S/D</button>
                                    </div>
                                    {{ Form::text('tgl_akhir', $to != '' ? $to : date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="row">
                        <div class="col-md-12">
                            <div id="dataTopShoppingCustomer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection