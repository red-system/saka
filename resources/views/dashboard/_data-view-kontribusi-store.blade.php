<table class="table table-bordered table-stripped">
    <thead>
    <tr class="bg-aqua">
        <th>Store</th>
        <th>Sales</th>
        <th>Total Qty</th>
        <th class="text-center">Percentage</th>
    </tr>
    </thead>
    <tbody>
    <?php $totalQty=0; ?>
    @if($dailysales->count() > 0)
        @foreach($dailysales as $ds)
            <tr>
                <td>{{ $ds->location_name }}</td>
                <td>{{ number_format($ds->totalSales, 2, ',', '.') }}</td>
                <td>{{ $ds->totalQty }}</td>
                <td class="text-center">{{ number_format(($ds->totalSales/$totalsales)*100, 2, ',', '.') }}</td>
                <?php
                    $totalQty=$totalQty+$ds->totalQty;
                ?>
            </tr>
        @endforeach
    @else
        <tr>
            <td class="text-center" colspan="3">There are no top data products available</td>
        </tr>
    @endif
    <tr>
        <td class="text-center" colspan="2" style="font-weight:bold;">Total Qty</td>
        <td >{{$totalQty}}</td>
    </tr>
    </tbody>
</table>