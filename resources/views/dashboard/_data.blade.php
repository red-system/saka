<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Gross Profit</span>
                <span class="info-box-number">{{ number_format($totalsales, 2, ',', '.') }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-tag"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Net Profit</span>
                <span class="info-box-number">{{ number_format($netprofit, 2, ',', '.') }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cart-plus"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Number Of Transaction</span>
                <span class="info-box-number">{{ number_format($numbertransaction, 0, ',', '.') }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                {{--  <span class="info-box-text">AVG Sales Transaction</span>
                <span class="info-box-number">{{ number_format($avgsalestransaction, 2, ',', '.') }}</span>  --}}
                <span class="info-box-text">AR Saka</span>
                <span class="info-box-number">{{ number_format($allPiutang, 2, ',', '.') }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-tag"></i></span>

            <div class="info-box-content">
                {{--  <span class="info-box-text">AVG Sales Transaction</span>
                <span class="info-box-number">{{ number_format($avgsalestransaction, 2, ',', '.') }}</span>  --}}
                <span class="info-box-text">Item Sold(Qty)</span>
                <span class="info-box-number">{{ $totalQtyTerjual }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Store Contribution</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <div class="btn-group">
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong>Store Contribution: {{ date('d M Y', strtotime($from)) }} - {{ date('d M Y', strtotime($to)) }}</strong>
                        </p>
                        <canvas id="salesChart"></canvas>
                        <!-- /.chart-responsive -->
                        <a href="{{ route('view-all-store-contribution.store').'?from='.$from.'&to='.$to.'&location='.$location }}" class="btn btn-sm btn-default btn-flat pull-right">View All Store Contribution</a>
                    </div>
                    <!-- /.col -->
                    {{--  <div class="col-md-5">
                        <p class="text-center">
                            <strong>Store Contribution</strong>
                        </p>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Store</th>
                                    <th>Sales</th>
                                    <th class="text-center">Percentage</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dailysales as $ds)
                                <tr>
                                    <td>{{ $ds->location_name }}</td>
                                    <td>{{ $ds->totalSales}}</td>
                                    <td class="text-center">{{ ($ds->totalSales/$totalsales)*100 }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ route('view-top-produk').'?from='.$from.'&to='.$to.'&location='.$location }}" class="btn btn-sm btn-default btn-flat pull-right">View All Top Product</a>
                    </div>z  --}}
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer clearfix">
                
                <!-- /.row -->
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>


<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Product Sales Per Category</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <div class="btn-group">
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-7">
                        <p class="text-center">
                            <strong>Sales Per Category: {{ date('d M Y', strtotime($from)) }} - {{ date('d M Y', strtotime($to)) }}</strong>
                        </p>
                        <canvas id="topCategorySales" height="150"></canvas>
                        {{--  <canvas id="salesChart" style="height: 200px;"></canvas>  --}}
                        <!-- /.chart-responsive -->
                        <a href="{{ route('view-all-category').'?from='.$from.'&to='.$to.'&location='.$location }}" class="btn btn-sm btn-default btn-flat pull-right">View All Category</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-5">
                        <p class="text-center">
                            <strong>Top Product Sales</strong>
                        </p>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Product Code</th>
                                    <th>Description</th>
                                    <th>Size</th>
                                    <th class="text-center">Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($topproductsales as $tps)
                                <tr>
                                    <td>{{ $tps->kd_produk }}</td>
                                    <td>{{ $tps->produk ? $tps->produk->deskripsi : '' }}</td>
                                    <td>{{ $tps->size_product }}</td>
                                    <td class="text-center">{{ $tps->totalQty }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ route('view-top-produk').'?from='.$from.'&to='.$to.'&location='.$location }}" class="btn btn-sm btn-default btn-flat pull-right">View All Top Product</a>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer clearfix">
                
                <!-- /.row -->
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Top Shoping Customer</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Total Shoping</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($topshopingcustomers as $tsc)
                            <tr>
                                <td>{{ $tsc->nama_pelanggan }}</td>
                                <td>{{ 'Rp '.number_format($tsc->totalShoping, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="{{ route('view-all-customer').'?from='.$from.'&to='.$to.'&location='.$location  }}" class="btn btn-sm btn-default btn-flat pull-right">View All Customer</a>
            </div>
            <!-- /.box-footer -->
        </div>

       

    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-6 connectedSortable">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Top Sales User</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sales</th>
                            <th>Total Sales</th>
                            <th>Location Sales Person</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--  @foreach($salesuser_total as $tsp)
                        
                            <tr>
                                <td>{{ $tsp['name'] }}</td>
                                <td>{{ 'Rp '.number_format($tsp['total'], 0, ',', '.') }}</td>
                            </tr>
                        @endforeach  --}}
                        @foreach($topsales as $tsp)
                        
                            <tr>
                                <td>{{ $tsp->name }}</td>
                                <td>{{ 'Rp '.number_format($tsp->totalSales, 0, ',', '.') }}</td>
                                <td>{{ $tsp->location_name }}</td>
                            </tr>
                        @endforeach 
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="{{ route('view-all-sales.user').'?from='.$from.'&to='.$to.'&location='.$location }}" class="btn btn-sm btn-default btn-flat pull-right">View All Sales</a>
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
        
    </section>
    <!-- right col -->
</div>

<script>
    new Chart(document.getElementById("salesChart"), {
        type: 'bar',
        data: {
            labels: [{!! $labelSales !!}],
            datasets: [{
                    data: [{!! $ammountSales !!}],
                    label: "Sales Ammount",
                    borderColor: "#3e95cd",
                    fill: false
                }
            ]
        },
        options: {
            title: {
                display: false,
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = data.datasets[tooltipItem.datasetIndex].label || '';
    
                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        return label;
                    }
                }
            }
        }
    });

    new Chart(document.getElementById("topCategorySales"), {
        type: 'pie',
        data: {
            labels: [{!! $labelCategory !!}],
            datasets: [
                {
                    label: "Category Sales",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: [{!! $ammountCategory !!}]
                }
            ]
        },
        options: {
            title: {
                display: false,
            },
            legend: {
                display: true
            },
        }
    });
</script>