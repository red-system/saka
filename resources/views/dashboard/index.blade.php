@extends($mlayout)

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('plugin-js')
    <script src="{{ asset('assets/chartjs/Chart.bundle.js') }}"></script>
    <script src="{{ asset('assets/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/bower_components/fastclick/lib/fastclick.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(function () {
            $('.date-picker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            if ($(document.getElementById("tglawal")).length > 0 && $(document.getElementById("tglakhir")).length > 0) {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('dashboard.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir, location:location , _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataDashboard').html(result);
                    });
                }
            }
            
            $("#location").change(function () {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('dashboard.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir, location:location , _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataDashboard').html(result);
                    });
                }
            })

            $("#tglawal").change(function () {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('dashboard.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir, location:location, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataDashboard').html(result);
                    });
                }
            })

            $("#tglakhir").change(function () {
                var tglawal = $('#tglawal').val();
                var tglakhir = $('#tglakhir').val();
                var location= $('#location').val();
                if(tglawal != '' && tglakhir != '') {
                    $.post("{{ route('dashboard.getdata') }}", { tglawal:tglawal, tglakhir:tglakhir, location:location, _token: $('meta[name="_token"]').attr('content') }, function(result) {
                        $('#dataDashboard').html(result);
                    });
                }
            })
        })
    </script>
@endsection

@section('title')
    Saka Karya Bali - Back Office
@endsection

@section('pageheader')
    @if(\Gate::denies('as_sales') && \Gate::denies('as_accounting'))
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
    @endif
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
               <div class="input-group">
                    {!! Form::label('location', 'Location') !!}
                    {{ Form::select('location', ['all' => 'Semua Lokasi']+App\Models\Location::pluck('location_name', 'kd_location')->all(), null, ['class' => 'form-control select2','required', 'style' => 'width: 100%;', 'id' => 'location']) }}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('tgl', 'Date') !!}
                <div class="input-group">
                    {{ Form::text('tgl_awal', date('Y-m-01'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglawal', 'style' => 'background-color: #fff;']) }}
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default">S/D</button>
                    </div>
                    {{ Form::text('tgl_akhir', date('Y-m-d'), ['class' => 'form-control date-picker','required', 'readonly', 'id' => 'tglakhir', 'style' => 'background-color: #fff;']) }}
                </div>
            </div>
        </div>
    </div>

    <div id="dataDashboard"></div>
@endsection