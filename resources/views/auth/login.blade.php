@extends('layouts.auth')

@section('content')
    <div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
        {{ Form::open(['route' => 'login.act', 'method' => 'post', 'class' => 'login100-form validate-form', 'novalidate']) }}
            <span class="login100-form-title p-b-35">
                Login
            </span>

            <div class="wrap-input100 validate-input m-b-16" data-validate = "Username/Email is required">
                <span class="symbol-input100">
                    <span class="lnr lnr-user"></span>
                </span>
                {{ Form::text('identity', null, ['class' => 'input100', 'placeholder' => 'Username/Email']) }}
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                <span class="symbol-input100">
                    <span class="lnr lnr-lock"></span>
                </span>
                {{ Form::password('password', ['class' => 'input100', 'autocomplete' => 'off', 'placeholder' => 'Password']) }}
                <span class="focus-input100"></span>
            </div>

            <div class="contact100-form-checkbox m-l-4">
                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember">
                <label class="label-checkbox100" for="ckb1">
                    Remember me
                </label>
            </div>

            <div class="container-login100-form-btn p-t-25">
                {!! Form::submit('Login', ['class' => 'login100-form-btn']) !!}
            </div>

            <div class="text-center w-full p-t-25">
                <a class="txt1 bo1 hov1" href="{{ route('password.reset') }}">
                    Lupa Password ?
                </a>
            </div>
        {{ Form::close() }}
    </div>
@endsection
