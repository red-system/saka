@extends('layouts.auth')

@section('content')
    <div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
        {{ Form::open(['url' => 'password/email', 'method' => 'post', 'class' => 'login100-form validate-form', 'novalidate']) }}
        <span class="login100-form-title p-b-35">
            Lupa Password
        </span>

        <div class="wrap-input100 validate-input m-b-16" data-validate = "Email is required">
            <span class="symbol-input100">
                <span class="lnr lnr-envelope"></span>
            </span>
            {{ Form::email('email', null, ['class' => 'input100', 'placeholder' => 'Email']) }}
            <span class="focus-input100"></span>
        </div>

        <div class="container-login100-form-btn p-t-25">
            {!! Form::submit('Submit', ['class' => 'login100-form-btn']) !!}
        </div>
        {{ Form::close() }}
    </div>
@endsection
