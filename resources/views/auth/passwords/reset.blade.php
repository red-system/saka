@extends('layouts.auth')

@section('content')
    <div class="wrap-login100 p-l-50 p-r-50 p-t-50 p-b-30">
        {{ Form::open(['url' => 'password/reset', 'method' => 'post', 'class' => 'login100-form validate-form', 'novalidate']) }}
        <span class="login100-form-title p-b-35">
            Reset Password
        </span>

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="wrap-input100 validate-input m-b-16 {{ $errors->has('email') ? ' has-error' : '' }}" data-validate = "Email is required">
            <span class="symbol-input100">
                <span class="lnr lnr-envelope"></span>
            </span>
            {{ Form::email('email', isset($email) ? $email : null, ['class' => 'input100', 'placeholder' => 'Email']) }}
            <span class="focus-input100"></span>
            {!! $errors->first('email', '<span class="help-block"><strong>:message</strong></span>') !!}
        </div>

        <div class="wrap-input100 validate-input m-b-16 {{ $errors->has('password') ? ' has-error' : '' }}" data-validate = "Password is required">
            <span class="symbol-input100">
                <span class="lnr lnr-lock"></span>
            </span>
            {{ Form::password('password', ['class' => 'input100', 'autocomplete' => 'off', 'placeholder' => 'Password']) }}
            <span class="focus-input100"></span>
            {!! $errors->first('password', '<span class="help-block"><strong>:message</strong></span>') !!}
        </div>

        <div class="wrap-input100 validate-input m-b-16 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" data-validate = "Password is required">
            <span class="symbol-input100">
                <span class="lnr lnr-lock"></span>
            </span>
            {{ Form::password('password_confirmation', ['class' => 'input100', 'autocomplete' => 'off', 'placeholder' => 'Password']) }}
            <span class="focus-input100"></span>
            {!! $errors->first('password_confirmation', '<span class="help-block"><strong>:message</strong></span>') !!}
        </div>

        <div class="container-login100-form-btn p-t-25">
            {!! Form::submit('Reset Password', ['class' => 'login100-form-btn']) !!}
        </div>
        {{ Form::close() }}
    </div>
@endsection
