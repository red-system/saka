@extends('layouts.master-toko')

@section('custom-css')

@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#profileimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function () {
            $("#imgprofile").change(function() {
                readURL(this);
            });
        })
    </script>
@endsection

@section('title')
    Profile - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Profile
        <small>Account Setting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li class="active">Profile</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('accountsetting._sidebar-profile')
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#myprofile" data-toggle="tab">My Profile</a></li>
                    <li><a href="#editprofile" data-toggle="tab">Edit Profile</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="myprofile">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td class="col-md-2">Name</td>
                                    <td class="col-md-10">: {{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Username</td>
                                    <td class="col-md-10">: {{ $user->username }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Email</td>
                                    <td class="col-md-10">: {{ $user->email }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="editprofile">
                        {!! Form::model($user, ['route' => 'myprofile.update', 'method' => 'post', 'novalidate', 'class' => 'validate-form', 'files' => true])!!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-body">
                                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                                {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                                {{ Form::text('name', null, ['class' => 'form-control','required']) }}
                                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="form-group {!! $errors->has('username') ? 'has-error' : '' !!}">
                                                {!! Form::label('username', 'Username') !!}
                                                {{ Form::text('username', null, ['class' => 'form-control','required']) }}
                                                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                                            </div>
                                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                                {!! Form::label('email', 'Email') !!}
                                                {{ Form::email('email', null, ['class' => 'form-control','required']) }}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <img id="profileimg" class="img-responsive" src="{{ auth()->user()->user_profile }}" alt="User profile">
                                        <hr>
                                        <div class="form-group">
                                            {!! Form::label('imgprofile', 'Browse image profile') !!}
                                            {{ Form::file('imgprofile', ['id' => 'imgprofile']) }}
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="text-right">
                                    {!! Form::button('Update Profile', ['type'=> 'submit', 'class'=>'btn btn-success']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection
