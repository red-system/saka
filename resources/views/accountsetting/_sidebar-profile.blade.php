<!-- Profile Image -->
<div class="box">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="{{ auth()->user()->user_profile }}" alt="User profile">
        <h3 class="profile-username text-center" style="margin-bottom: 0px;margin-top: 15px;">{{ auth()->user()->name }}</h3>
        <p class="text-muted text-center">{{ auth()->user()->human_role }}</p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li @if(\Request::segment(1) == 'my-profile') class="active" @endif><a href="{{ route('myprofile.index') }}">Profile</a></li>
            <li @if(\Request::segment(1) == 'change-password') class="active" @endif><a href="{{ route('change-password.index') }}">Change Password</a></li>
        </ul>
    </div>
</div>
<!-- /.box -->