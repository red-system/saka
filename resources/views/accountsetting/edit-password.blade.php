@extends('layouts.master-toko')

@section('custom-css')

@endsection

@section('plugin-js')
    <script type="text/javascript" src="{{ asset('assets/plugins/forms/validation/validate.min.js') }}"></script>
@endsection

@section('custom-script')
    <script type="text/javascript" src="{{ asset('js/jquery-form-validate.js') }}"></script>
@endsection

@section('title')
    Change Password - Saka Karya Bali
@endsection

@section('pageheader')
    <h1>
        Change Password
        <small>Account Setting</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}">Home</a></li>
        <li><a href="{{ route('myprofile.index') }}">Profile</a></li>
        <li class="active">Change Password</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('accountsetting._sidebar-profile')
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Change Password</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-body">
                                {!! Form::open(['route' => 'change-password.update', 'class' => 'jq-validate', 'method' => 'post', 'novalidate']) !!}
                                <div class="form-group {!! $errors->has('current_password') ? 'has-error' : '' !!}">
                                    {!! Form::label('current_password', 'Password Lama') !!}
                                    {{ Form::password('current_password', ['class' => 'form-control','required', 'minlength' => '8']) }}
                                    {!! $errors->first('current_password', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
                                    {!! Form::label('password', 'Password Baru') !!}
                                    {{ Form::password('password', ['class' => 'form-control','required', 'minlength' => '8']) }}
                                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {!! $errors->has('password_confirmation') ? 'has-error' : '' !!}">
                                    {!! Form::label('password_confirmation', 'Ulangi Password') !!}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control','required', 'minlength' => '8']) }}
                                    {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="text-right">
                                    {!! Form::submit('Ganti Password', ['class' =>  'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection
