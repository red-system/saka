<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReject extends Model
{
    protected $table = 'tb_product_reject';
    protected $guarded = [];

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }
}
