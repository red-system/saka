<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'tb_transaksi';
    protected $fillable = ['*'];
    protected $primaryKey = 'id_transaksi';

    public function jurnalUmum(){
        return $this->belongsTo(JurnalHarian::class,'id_jurnal_umum','id_jurnal');
    }
}
