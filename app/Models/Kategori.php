<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'tb_kat';
    protected $guarded = [];

    public static function typeList()
    {
        return [
            'produk' => 'Produk',
            'material' => 'Material',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type_kat];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }

    public function material() {
        return $this->hasMany(Material::class, 'kd_kat', 'kd_kat');
    }

    public function product() {
        return $this->hasMany(Product::class, 'kd_kat', 'kd_kat');
    }

    public function detail_purchasing() {
        return $this->hasMany(DetailPurchasing::class, 'kd_kat', 'kd_kat');
    }

    public function item_production() {
        return $this->hasMany(DetailProduction::class, 'kd_kat', 'kd_kat');
    }

    public function item_costing() {
        return $this->hasMany(DetailCosting::class, 'kd_kat', 'kd_kat');
    }
}
