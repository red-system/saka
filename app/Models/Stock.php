<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'tb_stock';
    protected $guarded = [];

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }

    public function detail_stock_opname() {
        return $this->hasMany(DetailStockOpname::class, 'stock_id');
    }
}
