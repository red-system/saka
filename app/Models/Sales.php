<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'tb_sales';
    protected $guarded = [];

    public function detail_sales() {
        return $this->hasMany(DetailSales::class, 'no_faktur', 'no_faktur');
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }

    public function retur_sales() {
        return $this->hasMany(ReturSales::class, 'no_faktur', 'no_faktur');
    }

    public function paymentSales() {
        return $this->hasMany(Payment::class, 'no_faktur', 'no_faktur');
    }

    public function piutang() {
        return $this->hasOne(Piutang::class, 'no_faktur', 'no_faktur');
    }

    public function sales_user() {
        return $this->belongsTo(User::class, 'salesuser_id', 'id');
    }
}
