<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSales extends Model
{
    protected $table = 'tb_detail_sales';
    protected $guarded = [];

    public function sales() {
        return $this->belongsTo(Sales::class, 'no_faktur', 'no_faktur');
    }

    public function produk() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }
}
