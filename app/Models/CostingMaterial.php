<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CostingMaterial extends Model
{
    protected $table = 'tb_costing_material';
    protected $guarded = [];

    public function costing() {
        return $this->belongsTo(Costing::class, 'kd_costing', 'kd_costing');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'kd_material', 'kd_material');
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'kd_satuan', 'kd_satuan');
    }
}
