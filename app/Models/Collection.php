<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'tb_collection';
    protected $guarded = [];

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'collection_id');
    }
}
