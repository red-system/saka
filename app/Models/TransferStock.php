<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferStock extends Model
{
    protected $table = 'tb_transfer_stock';
    protected $guarded = [];

    public static function statusList()
    {
        return [
            'draft' => 'Draft',
            'waiting-approval' => 'Waiting Approval',
            'on_progress' => 'On Progress',
            'finish' => 'Finish',
            'cancel' => 'Cancel',
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function sumber_transfer() {
        return $this->belongsTo(Location::class, 'dari', 'kd_location');
    }

    public function tujuan_transfer() {
        return $this->belongsTo(Location::class, 'tujuan', 'kd_location');
    }

    public function detail_transfer() {
        return $this->hasMany(DetailTransferStock::class, 'transferstock_id');
    }

}
