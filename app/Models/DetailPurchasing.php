<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPurchasing extends Model
{
    protected $table = 'tb_detail_purchasing';
    protected $guarded = [];

    public static function typeList()
    {
        return [
            'setengah_jadi' => 'Barang Setengah Jadi',
            'mentah' => 'Barang Mentah',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }

    public function purchasing() {
        return $this->belongsTo(Purchasing::class, 'kd_purchasing', 'kd_purchasing');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'kd_material', 'kd_material');
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kd_kat', 'kd_kat');
    }
}
