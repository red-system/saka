<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'tb_location';
    protected $guarded = [];

    public function provinsi() {
        return $this->belongsTo(Provinsi::class, 'kd_provinsi', 'kd_provinsi');
    }

    public static function typeList()
    {
        return [
            'konsinyasi' => 'Konsinyasi',
            'bukan_konsinyasi' => 'Bukan Konsinyasi',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }

    public function stock() {
        return $this->hasMany(Stock::class, 'kd_location', 'kd_location');
    }

    public function product_reject() {
        return $this->hasMany(ProductReject::class, 'kd_location', 'kd_location');
    }

    public function goods_flow()
    {
        return $this->hasMany(Stock::class, 'kd_location', 'kd_location');
    }

    public function user() {
        return $this->hasMany(User::class, 'kd_location', 'kd_location');
    }

    public function sales() {
        return $this->hasMany(Sales::class, 'kd_location', 'kd_location');
    }

    public function stock_opname() {
        return $this->hasMany(StockOpname::class, 'kd_location', 'kd_location');
    }

    public function history_stock_opname() {
        return $this->hasMany(HistorySO::class, 'kd_location', 'kd_location');
    }
}
