<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryMaterialOpname extends Model
{
    protected $table = 'tb_history_material_opname';
    protected $guarded = [];
}
