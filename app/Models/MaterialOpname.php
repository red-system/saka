<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialOpname extends Model
{
    protected $table = 'tb_material_opname';
    protected $guarded = [];

    public static function statusList()
    {
        return [
            'unpublish' => 'Unpublish',
            'publish' => 'Publish',
            'finish' => 'Finish',
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function detail_material_opname() {
        return $this->hasMany(DetailMaterialOpname::class, 'materialopname_id');
    }
}
