<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPurchasing extends Model
{
    protected $table = 'tb_retur_purchasing';
    protected $guarded = [];

    public function purchasing() {
        return $this->belongsTo(Purchasing::class, 'no_faktur', 'no_faktur');
    }

    public static function alasanList()
    {
        return [
            'rusak' => 'Rusak',
            'tukar' => 'Tukar',
        ];
    }

    public function getHumanAlasanAttribute()
    {
        return static::alasanList()[$this->alasan];
    }

    public static function allowedAlasan()
    {
        return array_keys(static::alasanList());
    }

    public function detail_retur() {
        return $this->hasMany(DetailReturPurchasing::class, 'no_retur', 'no_retur');
    }
}
