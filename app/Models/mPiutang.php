<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutang extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_piutang';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function dataList()
  {
    return static::all();
  }

  
}
