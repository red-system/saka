<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialReject extends Model
{
    protected $table = 'tb_material_reject';
    protected $guarded = [];

    public static function typeList()
    {
        return [
            'setengah_jadi' => 'Barang Setengah Jadi',
            'mentah' => 'Barang Mentah',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type_material];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }
}
