<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'tb_provinsi';
    protected $guarded = [];

    public function location() {
        return $this->hasMany(Location::class, 'kd_provinsi', 'kd_provinsi');
    }
}
