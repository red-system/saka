<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table = 'tb_satuan';
    protected $guarded = [];

    public function material() {
        return $this->hasMany(Material::class, 'kd_satuan', 'kd_satuan');
    }

    public function komposisi_product() {
        return $this->hasMany(KomposisiProduk::class, 'kd_satuan', 'kd_satuan');
    }

    public function production_material() {
        return $this->hasMany(ProductionMaterial::class, 'kd_satuan', 'kd_satuan');
    }

    public function costing_material() {
        return $this->hasMany(CostingMaterial::class, 'kd_satuan', 'kd_satuan');
    }
}
