<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailSendResetLink;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\UploadedFile;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'tb_user';
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserProfileAttribute()
    {
        if ($this->img_profile != '') {
            return asset('storage/user/' . $this->img_profile);
        } else {
            return asset('img/default-profile.png');
        }
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }

    public static function roleList()
    {
        return [
            'owner_or_manager_kantorpusat' => 'Owner/Manager Kantor Pusat',
            'manager_or_admin_toko' => 'Manager/Admin Toko',
            'production' => 'Production',
            'accounting' => 'Accounting',
            'sales' => 'Sales',
        ];
    }

    public function getHumanRoleAttribute()
    {
        return static::roleList()[$this->role];
    }

    public static function allowedRole()
    {
        return array_keys(static::roleList());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailSendResetLink($token, $this->email));
    }

    public static function saveAttachment(UploadedFile $file, $title)
    {
        $fileName = str_slug($title).'-'.date("YmdHis").'.'.$file->getClientOriginalExtension();
        \Storage::disk('user')->putFileAs('', $file, $fileName);
        return $fileName;
    }

    public static function deleteAttachment($filename)
    {
        return \Storage::disk('user')->delete($filename);
    }
}
