<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class DetailProduction extends Model
{
    protected $table = 'tb_detail_production';
    protected $primaryKey = 'kd_detail_production';
    protected $guarded = [];

    public function production() {
        return $this->belongsTo(Production::class, 'kd_produksi', 'kd_produksi');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kd_kat', 'kd_kat');
    }

    public static function saveAttachment(UploadedFile $file, $title)
    {
        $fileName = str_slug($title).'-'.date("YmdHis").'.'.$file->getClientOriginalExtension();
        \Storage::disk('item-produksi')->putFileAs('', $file, $fileName);
        return $fileName;
    }

    public static function deleteAttachment($filename)
    {
        return \Storage::disk('item-produksi')->delete($filename);
    }

    public static function saveFromCosting($title)
    {
        \Storage::copy('item-costing/'.$title, 'item-produksi/'.$title);
        return $title;
    }
}
