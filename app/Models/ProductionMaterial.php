<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionMaterial extends Model
{
    protected $table = 'tb_production_material';
    protected $guarded = [];

    public function production() {
        return $this->belongsTo(Production::class, 'kd_produksi', 'kd_produksi');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'kd_material', 'kd_material');
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'kd_satuan', 'kd_satuan');
    }
}
