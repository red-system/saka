<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'tb_payment';
    protected $guarded = [];

    public static function bankList()
    {
        return [
            'BNI' => 'BNI',
            'BRI' => 'BRI',
            'Mandiri' => 'Mandiri',
            'BCA' => 'BCA',
            'CIMB Niaga' => 'CIMB Niaga'
        ];
    }

    public static function paymentTypeList()
    {
        return [
            'tunai' => 'Tunai',
            'cc' => 'CC',
            'debet' => 'Debet',
            'transfer' => 'Transfer',
            'piutang' => 'Piutang',
            'retur' => 'Retur',
            'tambahanretur' => 'Pembayaran Tambahan Retur'
        ];
    }

    public function getHumanPaymentTypeAttribute()
    {
        return static::paymentTypeList()[$this->type_pembayaran];
    }

    public static function allowedPaymentType()
    {
        return array_keys(static::paymentTypeList());
    }

    public function sales() {
        return $this->belongsTo(Sales::class, 'no_faktur', 'no_faktur');
    }
}
