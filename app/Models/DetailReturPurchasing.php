<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailReturPurchasing extends Model
{
    protected $table = 'tb_detail_retur_purchasing';
    protected $guarded = [];

    public function purchasing() {
        return $this->belongsTo(Purchasing::class, 'no_faktur', 'no_faktur');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'kd_material', 'kd_material');
    }
}
