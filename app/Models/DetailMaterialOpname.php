<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailMaterialOpname extends Model
{
    protected $table = 'tb_detail_material_opname';
    protected $guarded = [];

    public function material_opname() {
        return $this->belongsTo(MaterialOpname::class, 'materialopname_id');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'material_id');
    }
}
