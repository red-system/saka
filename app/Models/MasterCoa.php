<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterCoa extends Model
{
    protected $table = 'tb_master_coa';
    protected $fillable = ['*'];
    protected $primaryKey = 'master_id';

    public function childs(){
        return $this->hasMany(MasterCoa::class,'mst_master_id');
    }
    
    public function parent(){
        return $this->belongsTo(MasterCoa::class,'mst_master_id');
    }

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class,'master_id','master_id');
    }

    public function master_detail()
    {
        return $this->hasMany(MasterDetail::class,'master_id','master_id');
    }
    
}
