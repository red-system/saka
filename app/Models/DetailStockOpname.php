<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailStockOpname extends Model
{
    protected $table = 'tb_detail_stock_opname';
    protected $guarded = [];

    public function stock_opname() {
        return $this->belongsTo(StockOpname::class, 'stockopname_id');
    }

    public function stock() {
        return $this->belongsTo(Stock::class, 'stock_id');
    }
}
