<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistorySO extends Model
{
    protected $table = 'tb_history_stock_opname';
    protected $guarded = [];

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }
}
