<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class DetailCosting extends Model
{
    protected $table = 'tb_detail_costing';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function costing() {
        return $this->belongsTo(Costing::class, 'kd_costing', 'kd_costing');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kd_kat', 'kd_kat');
    }

    public static function saveAttachment(UploadedFile $file, $title)
    {
        $fileName = str_slug($title).'-'.date("YmdHis").'.'.$file->getClientOriginalExtension();
        \Storage::disk('item-costing')->putFileAs('', $file, $fileName);
        return $fileName;
    }

    public static function deleteAttachment($filename)
    {
        return \Storage::disk('item-costing')->delete($filename);
    }
}
