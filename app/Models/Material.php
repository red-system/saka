<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'tb_material';
    protected $guarded = [];

    public static function typeList()
    {
        return [
            'setengah_jadi' => 'Barang Setengah Jadi',
            'mentah' => 'Barang Mentah',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type_material];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kd_kat', 'kd_kat');
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'kd_satuan', 'kd_satuan');
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'kd_supplier', 'kd_supplier');
    }

    public function komposisi_product() {
        return $this->hasMany(KomposisiProduk::class, 'kd_material', 'kd_kat');
    }

    public function production_material() {
        return $this->hasMany(ProductionMaterial::class, 'kd_material', 'kd_material');
    }

    public function costing_material() {
        return $this->hasMany(CostingMaterial::class, 'kd_material', 'kd_material');
    }

    public function detail_purchasing() {
        return $this->hasMany(DetailPurchasing::class, 'kd_material', 'kd_material');
    }
}
