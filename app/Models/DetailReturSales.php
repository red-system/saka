<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailReturSales extends Model
{
    protected $table = 'tb_detail_retur';
    protected $guarded = [];

    public function retur_sales() {
        return $this->belongsTo(ReturSales::class, 'no_retur', 'no_retur');
    }

    public function produk() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }
}
