<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryTransfer extends Model
{
    protected $table = 'tb_history_transfer';
    protected $guarded = [];
}
