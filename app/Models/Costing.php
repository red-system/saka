<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Costing extends Model
{
    protected $table = 'tb_costing';
    protected $guarded = [];

    public function produsen() {
        return $this->belongsTo(Produsen::class, 'kd_produsen', 'kd_produsen');
    }

    public static function statusList()
    {
        return [
            'draft' => 'Draft',
            'on_progress' => 'On Progress',
            'finish' => 'Finish',
            'cancel' => 'Cancel'
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function costing_material() {
        return $this->hasMany(CostingMaterial::class, 'kd_costing', 'kd_costing');
    }

    public function detail_costing() {
        return $this->hasMany(DetailCosting::class, 'kd_costing', 'kd_costing');
    }
}
