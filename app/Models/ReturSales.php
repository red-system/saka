<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturSales extends Model
{
    protected $table = 'tb_retur_sales';
    protected $guarded = [];

    public function sales() {
        return $this->belongsTo(Sales::class, 'no_faktur', 'no_faktur');
    }

    public static function alasanList()
    {
        return [
            'rusak' => 'Rusak',
            'tukar' => 'Tukar',
        ];
    }

    public function getHumanAlasanAttribute()
    {
        return static::alasanList()[$this->alasan];
    }

    public static function allowedAlasan()
    {
        return array_keys(static::alasanList());
    }

    public function detail_retur() {
        return $this->hasMany(DetailReturSales::class, 'no_retur', 'no_retur');
    }
}
