<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOpname extends Model
{
    protected $table = 'tb_stock_opname';
    protected $guarded = [];

    public static function statusList()
    {
        return [
            'unpublish' => 'Unpublish',
            'publish' => 'Publish',
            'finish' => 'Finish',
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }

    public function detail_stock_opname() {
        return $this->hasMany(DetailStockOpname::class, 'stockopname_id');
    }
}
