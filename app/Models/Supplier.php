<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'tb_supplier';
    protected $guarded = [];

    public function material() {
        return $this->hasMany(Material::class, 'kd_supplier', 'kd_supplier');
    }

    public function purchasing() {
        return $this->hasMany(Supplier::class, 'kd_supplier', 'kd_supplier');
    }
}
