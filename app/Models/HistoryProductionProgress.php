<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryProductionProgress extends Model
{
    protected $table = 'tb_history_production_progress';
    protected $guarded = [];
    protected $keyType = 'string';

    public function production() {
        return $this->belongsTo(Production::class, 'kd_produksi', 'kd_produksi');
    }
}
