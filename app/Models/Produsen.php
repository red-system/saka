<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produsen extends Model
{
    protected $table = 'tb_produsen';
    protected $guarded = [];

    public function production() {
        return $this->hasMany(Production::class, 'kd_produsen', 'kd_produsen');
    }

    public function costing() {
        return $this->hasMany(Costing::class, 'kd_produsen', 'kd_produsen');
    }
}
