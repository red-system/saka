<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurnalHarian extends Model
{
    protected $table = 'tb_jurnal_umum';
    protected $primaryKey = 'id_jurnal';
    protected $fillable = ['*'];

    public function transaksi(){
        return $this->hasMany(Transaksi::class,'id_jurnal_umum','id_jurnal')->orderBy('trs_jenis_transaksi','ASC');
      }

    
}
