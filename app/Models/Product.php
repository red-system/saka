<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Product extends Model
{
    protected $table = 'tb_product';
    protected $guarded = [];

    public static function priceTypeList()
    {
        return [
            'retail_price' => 'Retail Price',
            'wholesale_price' => 'Wholesale Price',
        ];
    }

    public static function styleNumberList()
    {
        return [
            '1' => ['display' => '1', 'kode' => '1'],
            '2' => ['display' => '2', 'kode' => '2'],
            '3' => ['display' => '3', 'kode' => '3'],
            '4' => ['display' => '4', 'kode' => '4'],
            '5' => ['display' => '5', 'kode' => '5'],
        ];
    }

    public static function selectStyleNumberList()
    {
        $result = [];
        foreach (static::styleNumberList() as $index => $val) {
            $result[$index] = $val['display'];
        }
        return $result;
    }

    public function getHumanStyleNumberAttribute()
    {
        return static::styleNumberList()[$this->style_number]['display'];
    }

    public static function allowedStyleNumber()
    {
        return array_keys(static::styleNumberList());
    }

    public static function materialList()
    {
        return [
            'silver' => ['display' => 'Silver', 'kode' => 'S'],
            'gold' => ['display' => 'Gold', 'kode' => 'G']
        ];
    }

    public static function selectMaterialList()
    {
        $result = [];
        foreach (static::materialList() as $index => $val) {
            $result[$index] = $val['display'];
        }
        return $result;
    }

    public function getHumanMaterialAttribute()
    {
        return static::materialList()[$this->material]['display'];
    }

    public static function allowedMaterial()
    {
        return array_keys(static::materialList());
    }

    public static function stoneColorList()
    {
        return [
            'onix' => ['display' => 'Onix', 'kode' => 'ONX'],
            'blue_topaz' => ['display' => 'Blue Topaz', 'kode' => 'BTP']
        ];
    }

    public static function selectStoneColorList()
    {
        $result = [];
        foreach (static::stoneColorList() as $index => $val) {
            $result[$index] = $val['display'];
        }
        return $result;
    }

    public function getHumanStoneColorAttribute()
    {
        return static::stoneColorList()[$this->stone_color]['display'];
    }

    public static function allowedStoneColor()
    {
        return array_keys(static::stoneColorList());
    }

    public static function sizeList()
    {
        return [
            '0' => ['display' => 'No Size', 'kode' => '00'],
            '5.5' => ['display' => '5.5', 'kode' => '55'],
            '6' => ['display' => '6', 'kode' => '06'],
            '7' => ['display' => '7', 'kode' => '07'],
            '8' => ['display' => '8', 'kode' => '08'],
            '16' => ['display' => '16 CM', 'kode' => '16'],
        ];
    }

    public static function selectSizeList()
    {
        $result = [];
        foreach (static::sizeList() as $index => $val) {
            $result[$index] = $val['display'];
        }
        return $result;
    }

    public function getHumanSizeAttribute()
    {
        return static::sizeList()[$this->size]['display'];
    }

    public static function allowedSize()
    {
        return array_keys(static::sizeList());
    }

    public function getProdukImageAttribute()
    {
        if ($this->img_produk != '') {
            return asset('storage/product/' . $this->img_produk);
        } else {
            return 'http://via.placeholder.com/500x500';
        }
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class, 'kd_kat', 'kd_kat');
    }

    public function stock() {
        return $this->hasMany(Stock::class, 'kd_produk', 'kd_produk');
    }

    public function product_reject() {
        return $this->hasMany(ProductReject::class, 'kd_produk', 'kd_produk');
    }

    public function komposisi_product() {
        return $this->hasMany(KomposisiProduk::class, 'kd_produk', 'kd_produk');
    }

    public function detail_sales() {
        return $this->hasMany(DetailSales::class, 'kd_produk', 'kd_produk');
    }

    public function collection()
    {
        return $this->belongsTo('App\Models\Collection', 'collection_id');
    }

    public function item_production() {
        return $this->hasMany(DetailProduction::class, 'kd_produk', 'kd_produk');
    }

    public function item_costing() {
        return $this->hasMany(DetailCosting::class, 'kd_produk', 'kd_produk');
    }

    public static function saveAttachment(UploadedFile $file, $title)
    {
        $fileName = str_slug($title).'-'.date("YmdHis").'.'.$file->getClientOriginalExtension();
        \Storage::disk('product')->putFileAs('', $file, $fileName);
        return $fileName;
    }

    public static function deleteAttachment($filename)
    {
        return \Storage::disk('product')->delete($filename);
    }
    
    public function detailtransferstok() {
        return $this->belongsTo(DetailTransferStock::class, 'kd_produk');
    }
}
