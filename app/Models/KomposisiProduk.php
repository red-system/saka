<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KomposisiProduk extends Model
{
    protected $table = 'tb_komposisi_produk';
    protected $guarded = [];

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function material() {
        return $this->belongsTo(Material::class, 'kd_material', 'kd_material');
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'kd_satuan', 'kd_satuan');
    }
}
