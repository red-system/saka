<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailTransferStock extends Model
{
    protected $table = 'tb_detail_transfer';
    protected $guarded = [];

    public static function statusList()
    {
        return [
            'waiting-approval' => 'Waiting Approval',
            'approved' => 'Approved',
            'rejected' => 'Rejected',
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function product() {
        return $this->belongsTo(Product::class, 'kd_produk', 'kd_produk');
    }

    public function transferstok() {
        return $this->belongsTo(TransferStock::class, 'transferstock_id');
    }
    
    public function productDetailTransfer() {
        return $this->hasOne(Product::class, 'kd_produk', 'kd_produk');
    }
    

}
