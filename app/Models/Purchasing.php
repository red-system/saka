<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchasing extends Model
{
    protected $table = 'tb_purchasing';
    protected $guarded = [];

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'kd_supplier', 'kd_supplier');
    }

    public function detail_purchasing() {
        return $this->hasMany(DetailPurchasing::class, 'kd_purchasing', 'kd_purchasing');
    }
}
