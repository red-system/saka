<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $table = 'tb_production';
    protected $guarded = [];

    public function produsen() {
        return $this->belongsTo(Produsen::class, 'kd_produsen', 'kd_produsen');
    }

    public static function statusList()
    {
        return [
            'draft' => 'Draft',
            'on_progress' => 'On Progress',
            'finish' => 'Finish',
            'cancel' => 'Cancel'
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function production_material() {
        return $this->hasMany(ProductionMaterial::class, 'kd_produksi', 'kd_produksi');
    }

    public function detail_production() {
        return $this->hasMany(DetailProduction::class, 'kd_produksi', 'kd_produksi');
    }

    public function history_production() {
        return $this->hasMany(HistoryProductionProgress::class, 'kd_produksi', 'kd_produksi');
    }
}
