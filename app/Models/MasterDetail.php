<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDetail extends Model
{
    protected $table = 'tb_ac_master_detail';
    protected $fillable = ['*'];
    protected $primaryKey = 'master_detail_id';
    
    public function master(){
        return $this->belongsTo(MasterCoa::class,'master_id','master_id');
    }
    
}
