<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Piutang extends Model
{
    protected $table = 'tb_piutang';
    protected $guarded = [];

    public function sales() {
        return $this->belongsTo(Sales::class, 'no_faktur', 'no_faktur');
    }
}
