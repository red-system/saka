<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHutang extends Model
{
  public $incrementing = false;
  protected $table = 'tb_hutang';
  protected $primaryKey = 'hs_kode';
  public $timestamps = false;



  public function dataList()
  {
    return static::all();
  }

  public function suppliers(){
    return $this->belongsTo(mSupplier::class,'spl_kode','spl_kode');
  }

  public function pembelianSupplier()
  {
    return $this->belongsTo('App\Models\mPembelianSupplier', 'ps_no_faktur','no_pembelian');
  }


}
