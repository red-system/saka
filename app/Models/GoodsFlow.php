<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsFlow extends Model
{
    protected $table = 'tb_goods_flow';
    protected $guarded = [];

    public static function typeList()
    {
        return [
            'produk' => 'Produk',
            'material' => 'Material',
        ];
    }

    public function getHumanTypeAttribute()
    {
        return static::typeList()[$this->type_flow];
    }

    public static function allowedType()
    {
        return array_keys(static::typeList());
    }

    public function location() {
        return $this->belongsTo(Location::class, 'kd_location', 'kd_location');
    }
}
