<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        $gate->define('as_owner_or_manager_kantorpusat', function($user) {
            return $user->role == 'owner_or_manager_kantorpusat';
        });

        $gate->define('as_manager_or_admin_toko', function($user) {
            return $user->role == 'manager_or_admin_toko';
        });

        $gate->define('as_production', function($user) {
            return $user->role == 'production';
        });

        $gate->define('as_accounting', function($user) {
            return $user->role == 'accounting';
        });

        $gate->define('as_sales', function($user) {
            return $user->role == 'sales';
        });
    }
}
