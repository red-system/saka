<?php

namespace App\Http\Middleware;

use Closure;

class ToAddItemIfOrderEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('order.produk')) {
            if (count(session()->get('order.produk')) > 0) {
                return $next($request);
            }
        }
        return redirect()->route('pos.additem.index');
    }
}
