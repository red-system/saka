<?php

namespace App\Http\Controllers\Akunting;

use App\Models\Periode;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use App\Models\MasterDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class KodePerkiraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kodePerkiraan = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        // return view('toko.masterdata.supplier.index', compact('suppliers', 'nosupplier'));
        return view('toko.akunting.kode-perkiraan.index', compact('kodePerkiraan'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    function rules($request) {
        $rules = [
            'mst_kode_rekening' => 'required',
            'mst_nama_rekening' => 'required',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        \Validator::make($request, $rules, $customeMessage)->validate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{
            $this->rules($request->all());
            $data = $request->except('_token', 'nominal');
            $data['mst_tanggal_awal'] = date('Y-m-d');
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = auth()->user()->name;
            $data['updated_by'] = auth()->user()->name;
            $perkiraan = MasterCoa::insertGetId($data);

            $month =  Carbon::now()->month;
            $year =  Carbon::now()->year;

            // $detail = new MasterDetail();
            // $detail->master_id = $perkiraan;
            // $detail->msd_year = $year;
            // $detail->msd_month = $month;
            // if ($request->mst_normal == 'debet') {
            //     $detail->msd_awal_kredit = 0;
            //     $detail->msd_awal_debet = $request->nominal;
            // }
            // else {
            //     $detail->msd_awal_kredit = $request->nominal;
            //     $detail->msd_awal_debet = 0;
            // }
            // $detail->msd_date_insert = date('Y-m-d H:i:s');
            // $detail->msd_date_update = date('Y-m-d H:i:s');
            // $detail->created_by = auth()->user()->name;
            // $detail->updated_by = auth()->user()->name;
            // $detail->save();
            if($data['mst_normal']=='debet'){
                $debet = $request->input('nominal');
                $kredit = 0;
            }else{
                $kredit = $request->input('nominal');
                $debet = 0;
            }
            $data_transaksi           = [
                // 'id_jurnal_umum'      => $id_jurnal,
                'master_id'           => $perkiraan,
                'trs_jenis_transaksi' => $data['mst_normal'],
                'trs_debet'           => $debet,
                'trs_kredit'          => $kredit,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $request->input('mst_kode_rekening'),
                'trs_nama_rekening'   => $request->input('mst_nama_rekening'),
                'trs_tipe_arus_kas'   => 'saldo_awal',
                'trs_catatan'         => 'saldo awal',
                'trs_date_insert'     => date('Y-m-d'),
                'trs_date_update'     => date('Y-m-d'),
                'trs_charge'          => 0,
                'trs_no_check_bg'     => '-',
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'created_by'          => auth()->user()->name,
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_by'          => auth()->user()->name,
                'updated_at'          => date('Y-m-d H:i:s'),
                'tgl_transaksi'       => date('Y-m-d')
            ];    
                // Transaksi::insert($data_transaksi);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'COA successfully added.']);
            return redirect()->route('kodePerkiraan.index');

        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }      

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kodePerkiraan = MasterCoa::where('master_id', $id)->firstOrFail();
        $id            = $id;
        $dataPerkiraan = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        return view('toko.akunting.kode-perkiraan.edit', compact('kodePerkiraan','id','dataPerkiraan'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try{
            $this->rules($request->all());

            $data = $request->except('_token', 'nominal');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = auth()->user()->name;

            MasterCoa::where('master_id', $id)->update($request->except('_token', 'nominal'));

            $month =  Carbon::now()->month;
            $year =  Carbon::now()->year;

            // $detail = MasterDetail::where('master_id', $id)->where('msd_year', $year)->where('msd_month', $month)->first();
            // if ($detail == null) {
            //     $detailNew = new MasterDetail();
            //     $detailNew->master_id = $master_id;
            //     $detailNew->msd_year = $year;
            //     $detailNew->msd_month = $month;
            //     if ($request->mst_normal == 'debet') {
            //     $detailNew->msd_awal_kredit = 0;
            //     $detailNew->msd_awal_debet = $request->nominal;
            //     }
            //     else {
            //     $detailNew->msd_awal_kredit = $request->nominal;
            //     $detailNew->msd_awal_debet = 0;
            //     }
            //     $detailNew->msd_date_insert = date('Y-m-d H:i:s');
            //     $detailNew->msd_date_update = date('Y-m-d H:i:s');
            //     $detailNew->created_by = auth()->user()->name;
            //     $detailNew->updated_by = auth()->user()->name;
            //     $detailNew->save();
            // }
            // else {
            //     // $detail->master_id = $master_id;
            //     $detail->msd_year = $year;
            //     $detail->msd_month = $month;
            //     if ($request->mst_normal == 'debet') {
            //         $detail->msd_awal_kredit = 0;
            //         $detail->msd_awal_debet = $request->nominal;
            //     }
            //     else {
            //         $detail->msd_awal_kredit = $request->nominal;
            //         $detail->msd_awal_debet = 0;
            //     }
            //     // $detail->msd_date_insert = date('Y-m-d H:i:s');
            //     $detail->msd_date_update = date('Y-m-d H:i:s');
            //     $detail->updated_by      = auth()->user()->name;
            //     $detail->save();
            // }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'COA was successfully updated.']);
            return redirect()->route('kodePerkiraan.index');

        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try{
            $master = MasterCoa::where('master_id', $id)->firstOrFail();
            $master->delete();

            $detailMaster = MasterDetail::where('master_id',$id)->first();
            if($detailMaster != null){
                $detailMaster->delete();
            }
            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'COA was successfully deleted.']);
            return redirect()->route('kodePerkiraan.index');
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
    }

    public function addSaldoAwal($id)
    {
        $kodePerkiraan = MasterCoa::where('master_id', $id)->firstOrFail();
        $id            = $id;
        $dataPerkiraan = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        $month         = [
            '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        return view('toko.akunting.kode-perkiraan.addSaldoAwal', compact('month','kodePerkiraan','id','dataPerkiraan'))->render();
    }

    public function simpanSaldoAwal(Request $request, $id)
    {
        \DB::beginTransaction();
        try{
            if(MasterDetail::where('msd_year',$request->tahun)->where('msd_month',$request->bulan)->where('master_id',$id)->exists()){
                \Session::flash('notification', ['level' => 'warning', 'title' => 'Warning !', 'message' => 'Saldo Awal Untuk Bulan dan Tahun sudah ada.']);
                return redirect()->route('kodePerkiraan.index');
            }else{
                $detail = new MasterDetail();
                $detail->master_id = $id;
                $detail->msd_year = $request->tahun;
                $detail->msd_month = $request->bulan;
                if ($request->mst_normal == 'debet') {
                    $detail->msd_awal_kredit = 0;
                    $detail->msd_awal_debet = $request->nominal;
                }
                else {
                    $detail->msd_awal_kredit = $request->nominal;
                    $detail->msd_awal_debet = 0;
                }
                $detail->msd_date_insert = date('Y-m-d H:i:s');
                $detail->msd_date_update = date('Y-m-d H:i:s');
                $detail->created_by = auth()->user()->name;
                $detail->updated_by = auth()->user()->name;
                $detail->save();

                \DB::commit();

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Saldo Awal berhasil ditambahkan.']);
                return redirect()->route('kodePerkiraan.index');
            }
            

        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
        
    }

    public function saldo_awal()
    {
        $month              = [
            '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $year               = [
            '2018' => '2018','2019' => '2019','2020' => '2020','2021' => '2021',
        ];
        $periode            = Periode::where('id',1)->first();
        
        $periode_month      = $periode->bulan_aktif;
        $periode_year       = $periode->tahun_aktif;
        $masterDetail       = MasterDetail::where('msd_month',$periode_month)->where('msd_year',$periode_year);
        $jml_detailPerkiraan= $masterDetail->count();
        $detailPerkiraan    = $masterDetail->get();
        $kodePerkiraan      = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        return view('toko.akunting.kode-perkiraan.saldo_awal', compact('kodePerkiraan', 'detailPerkiraan','month','year','periode_month','periode_year'));
    }

    public function getSaldoAwal(Request $request)
    {
        $month              = [
            '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $year               = [
            '2018' => '2018','2019' => '2019','2020' => '2020','2021' => '2021',
        ];
        $periode            = Periode::where('id',1)->first();
        
        $periode_month      = $request->bulan;
        $periode_year       = $request->tahun;
        $masterDetail       = MasterDetail::where('msd_month',$periode_month)->where('msd_year',$periode_year);
        $jml_detailPerkiraan= $masterDetail->count();
        $detailPerkiraan    = $masterDetail->get();
        $kodePerkiraan      = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        return view('toko.akunting.kode-perkiraan.getSaldoAwal', compact('kodePerkiraan', 'detailPerkiraan','month','year','periode_month','periode_year'));
    }

    public function edit_saldo_awal($id)
    {
        // $kodePerkiraan = MasterCoa::where('master_id', $id)->firstOrFail();
        // $id            = $id;
        // $dataPerkiraan = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        
        $kodePerkiraan = MasterDetail::where('master_detail_id', $id)->firstOrFail();
        $master_id     = $kodePerkiraan->master_id;
        $id            = $id;
        $dataPerkiraan = MasterCoa::where('mst_master_id',0)->orderBy('mst_kode_rekening','ASC')->get();
        $month         = [
            '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        return view('toko.akunting.kode-perkiraan.editSaldoAwal', compact('month','kodePerkiraan','id','dataPerkiraan','master_id'))->render();
    }

    public function update_saldo_awal(Request $request, $id)
    {
        \DB::beginTransaction();
        try{
            if($request->mst_normal=='debet'){
                $data = [
                    'msd_awal_debet'  => $request->nominal,
                    'msd_date_update' => date('Y-m-d H:i:s'),
                    'updated_by'      => auth()->user()->name
                ];
            }else{
                $data = [
                    'msd_awal_kredit' => $request->nominal,
                    'msd_date_update' => date('Y-m-d H:i:s'),
                    'updated_by'      => auth()->user()->name
                ];
            }
            MasterDetail::where('master_detail_id',$id)->update($data);
            
            \DB::commit();
            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Saldo Awal berhasil ditambahkan.']);
            return redirect()->route('kodePerkiraan.saldo-awal');

        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
        
    }
}
