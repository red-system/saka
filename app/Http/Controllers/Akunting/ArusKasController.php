<?php

namespace App\Http\Controllers\Akunting;

use App\Models\DetailProduction;
use App\Models\GoodsFlow;
use App\Models\JurnalHarian;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use App\Models\MasterDetail;
use App\Models\Periode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use DB;

class ArusKasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function index(){
        
        $data['menu']       = 'Accounting';
        $data['title']       = 'Arus Kas';
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');        
        $master                     = MasterCoa::where('mst_kas_status','yes')->get();

        
        $data['kas_awal']           = 0;
            foreach($master as $mst){
                if($mst->mst_normal=='debet'){
                    $debet      = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                    $kredit     = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                    $saldo_awal = $debet - $kredit;
                    $data['kas_awal'] = $data['kas_awal']+$saldo_awal;
                }
            }
        // }

        
        $data['transaksi']      = Transaksi::leftjoin('tb_master_coa','tb_transaksi.master_id','=','tb_master_coa.master_id')->leftjoin('tb_jurnal_umum','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_transaksi.tgl_transaksi','>=',$data['start_date'])->where('tb_transaksi.tgl_transaksi','<=',$data['end_date'])->where('mst_kas_status','yes')->get();

        return view('toko.akunting.arus-kas.index', $data);
        

    }

    public function getdata(Request $request) {
        $data['menu']       = 'Accounting';
        $data['title']       = 'Arus Kas';
        $data['start_date']         = $request->start_date;
        $data['end_date']           = $request->end_date;        
        $master                     = MasterCoa::where('mst_kas_status','yes')->get();

        
        $data['kas_awal']           = 0;
            foreach($master as $mst){
                if($mst->mst_normal=='debet'){
                    $debet      = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                    $kredit     = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                    $saldo_awal = $debet - $kredit;
                    $data['kas_awal'] = $data['kas_awal']+$saldo_awal;
                }
            }
        // }

        
        $data['transaksi']      = Transaksi::leftjoin('tb_master_coa','tb_transaksi.master_id','=','tb_master_coa.master_id')->leftjoin('tb_jurnal_umum','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_transaksi.tgl_transaksi','>=',$data['start_date'])->where('tb_transaksi.tgl_transaksi','<=',$data['end_date'])->where('mst_kas_status','yes')->get();

        return view('toko.akunting.arus-kas.data-arus-kas', $data);
    }

    public function cetak($start_date, $end_date) {
        
        $data['menu']       = 'Accounting';
        $data['title']       = 'Arus Kas';
        $data['start_date']         = $start_date;
        $data['end_date']           = $end_date;        
        $master                     = MasterCoa::where('mst_kas_status','yes')->get();

        
        $data['kas_awal']           = 0;
            foreach($master as $mst){
                if($mst->mst_normal=='debet'){
                    $debet      = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                    $kredit     = $mst->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                    $saldo_awal = $debet - $kredit;
                    $data['kas_awal'] = $data['kas_awal']+$saldo_awal;
                }
            }
        // }

        
        $data['transaksi']      = Transaksi::leftjoin('tb_master_coa','tb_transaksi.master_id','=','tb_master_coa.master_id')->leftjoin('tb_jurnal_umum','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_transaksi.tgl_transaksi','>=',$data['start_date'])->where('tb_transaksi.tgl_transaksi','<=',$data['end_date'])->where('mst_kas_status','yes')->get();

        return view('toko.akunting.arus-kas.cetak', $data);
    }
}
