<?php

namespace App\Http\Controllers\Akunting;

use App\Models\DetailProduction;
use App\Models\GoodsFlow;
use App\Models\JurnalHarian;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use App\Models\MasterDetail;
use App\Models\Periode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use DB;

class BukuBesarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['perkiraan']          = MasterCoa::orderBy('master_id','ASC')->get();
        $data['kode_coa']           = MasterCoa::orderBy('master_id','ASC')->get();
        $data['master_id']          = 0;
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');
        
        foreach ($data['perkiraan'] as $perkiraan) {
            if($perkiraan->mst_normal=='debet'){
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $debet - $kredit;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }else{
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $kredit - $debet;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }
        }
        $data['tgl_saldo_awal']     = date('Y-m-d', strtotime('-1 days', strtotime($data['start_date'])));
        
        return view('toko.akunting.buku-besar.index', $data);
    }

    public function getdata(Request $request) {
        $start_date                 = $request->tgl_awal;
        $end_date                   = $request->tgl_akhir;
        $master_id                  = $request->master_id;

        $data['perkiraan']          = MasterCoa::orderBy('master_id','ASC');
        $data['kode_coa']           = MasterCoa::orderBy('master_id','ASC')->get();
        if($master_id>0){
            $data['perkiraan']          = $data['perkiraan']->where('master_id',$master_id);
        }
        $data['perkiraan']          = $data['perkiraan']->get();
        $data['start_date']         = $start_date;
        $data['end_date']           = $end_date;
        
        foreach ($data['perkiraan'] as $perkiraan) {
            if($perkiraan->mst_normal=='debet'){
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $debet - $kredit;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }else{
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $kredit - $debet;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }
        }
        $data['tgl_saldo_awal']     = date('Y-m-d', strtotime('-1 days', strtotime($data['start_date'])));
        
        return view('toko.akunting.buku-besar.dataBukuBesar', $data);
    }

    public function cetak(Request $request, $start_date, $end_date,$master_id) {
        
        $data['perkiraan']          = MasterCoa::orderBy('master_id','ASC');
        if($master_id>0){
            $data['perkiraan'] = $data['perkiraan']->where('master_id',$master_id);
        }

        $data['start_date']         = $start_date;
        $data['end_date']           = $end_date;
        $data['perkiraan']          = $data['perkiraan']->get();
        
        foreach ($data['perkiraan'] as $perkiraan) {
            if($perkiraan->mst_normal=='debet'){
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $debet - $kredit;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }else{
                $debet      = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_debet');
                $kredit     = $perkiraan->transaksi->where('tgl_transaksi' ,'<', $data['start_date'])->sum('trs_kredit');
                $saldo_awal = $kredit - $debet;
                $data['saldo_awal'][$perkiraan->mst_kode_rekening] = $saldo_awal;
            }
        }
        $data['tgl_saldo_awal']     = date('Y-m-d', strtotime('-1 days', strtotime($data['start_date'])));

        // $pdf = \PDF::loadView('toko.akunting.buku-besar.cetak', $data)
        //     ->setPaper('a4', 'landscape');
        // return $pdf->stream();

        return view('toko.akunting.buku-besar.cetak', $data);
    }
}
