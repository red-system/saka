<?php

namespace App\Http\Controllers\Akunting;

use App\Models\DetailProduction;
use App\Models\GoodsFlow;
use App\Models\JurnalHarian;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use App\Models\MasterDetail;
use App\Models\Periode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use DB;

class NeracaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($start_date='',$end_date='',$tipe='')
    {
        // $productions = Production::get();
        // return view('toko.akunting.jurnal-harian.index', compact('productions'));
        
        $data['start_date']         = date('Y-m-d');
        $data['end_date']           = date('Y-m-d');
        if($start_date!=''||$end_date!=''){
            $data['start_date']         = $start_date;
            $data['end_date']           = $end_date;
        }  

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;
        $data['data_asset']              = MasterCoa::where('mst_posisi','asset')->where('mst_master_id',0)->get();
        $data['data_ekuitas']            = MasterCoa::where('mst_posisi','ekuitas')->where('mst_master_id',0)->get();
        $data['data_liabilitas']         = MasterCoa::where('mst_posisi','liabilitas')->get();

        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs_2->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    if($asset_childs_2->mst_normal == 'kredit'){                        
                        $asset              = $kredit-$debet;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']-$asset;
                        $total_asset_childs = $total_asset_childs-$asset;

                    }
                    if($asset_childs_2->mst_normal == 'debet'){
                        $asset              = $debet-$kredit;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']+$asset;
                        $total_asset_childs = $total_asset_childs+$asset;                       
                    }                    
                }

                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($asset_childs->mst_normal == 'kredit'){                    
                    $asset              = $kredit-$debet;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset-$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']-$asset;
                    $total_detail_asset = $total_detail_asset-$data['asset'][$asset_childs->mst_kode_rekening];
                }
                if($asset_childs->mst_normal == 'debet'){
                    $asset              = $debet-$kredit;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset+$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']+$asset;
                    $total_detail_asset = $total_detail_asset+$data['asset'][$asset_childs->mst_kode_rekening];                       
                }
            }
            
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$detail_asset->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($detail_asset->mst_normal == 'kredit'){                
                $asset              = $kredit-$debet;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset-$total_detail_asset;
                $data['total_asset']     = $data['total_asset']-$asset;
            }
            if($detail_asset->mst_normal == 'debet'){
                $asset              = $debet-$kredit;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset+$total_detail_asset;
                $data['total_asset']     = $data['total_asset']+$asset;                       
            }
        }

        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$ekuitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($ekuitas_childs->mst_normal == 'kredit'){
                    
                    $ekuitas            = $kredit-$debet;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;
                    $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                }
                if($ekuitas_childs->mst_normal == 'debet'){
                    $ekuitas              = $debet-$kredit;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                    $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas-$ekuitas;                     
                }
                // if($ekuitas_childs->mst_kode_rekening=='2403'){
                //     $ekuitas                = $this->count_rugi_laba($data['end_date']);
                //     $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                //     $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                //     $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                // }
            }

            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_ekuitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_ekuitas->mst_normal == 'kredit'){                
                $ekuitas            = $kredit-$debet;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas+$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
            }
            if($data_ekuitas->mst_normal == 'debet'){
                $ekuitas              = $debet-$kredit;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas-$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;                      
            }   

        }

        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;
            foreach ($data_liabilitas->childs as $liabilitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$liabilitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($liabilitas_childs->mst_normal == 'kredit'){                    
                    $liabilitas            = $kredit-$debet;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }
                    
                }
                if($liabilitas_childs->mst_normal == 'debet'){
                    $liabilitas              = $debet-$kredit;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }                     
                } 
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_liabilitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_liabilitas->mst_normal == 'kredit'){                
                $liabilitas            = $kredit-$debet;
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas+$total_data_liabilitas;
            }
            if($data_liabilitas->mst_normal == 'debet'){
                $liabilitas              = $debet-$kredit; 
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas-$total_data_liabilitas;                    
            }
            $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
        }

        $data['hitung']         = $data['total_liabilitas']+$data['total_ekuitas'];
        if(number_format($data['hitung'],2)==number_format($data['total_asset'],2)){
            $data['status']         = 'BALANCE';
            $data['selisih']         = 0;
        }else{
            $data['status']         = 'NOT BALANCE';
            $data['selisih']         = $data['total_asset']-$data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);
        
        return view('toko.akunting.neraca.index', $data);
    }

    public function getdata(Request $request) 
    {
        $data['start_date']         = $request->tgl_awal;
        $data['end_date']           = $request->tgl_akhir;

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;
        $data['data_asset']              = MasterCoa::where('mst_posisi','asset')->where('mst_master_id',0)->get();
        $data['data_ekuitas']            = MasterCoa::where('mst_posisi','ekuitas')->where('mst_master_id',0)->get();
        $data['data_liabilitas']         = MasterCoa::where('mst_posisi','liabilitas')->get();

        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs_2->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    if($asset_childs_2->mst_normal == 'kredit'){                        
                        $asset              = $kredit-$debet;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']-$asset;
                        $total_asset_childs = $total_asset_childs-$asset;

                    }
                    if($asset_childs_2->mst_normal == 'debet'){
                        $asset              = $debet-$kredit;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']+$asset;
                        $total_asset_childs = $total_asset_childs+$asset;                       
                    }                    
                }

                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($asset_childs->mst_normal == 'kredit'){                    
                    $asset              = $kredit-$debet;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset-$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']-$asset;
                    $total_detail_asset = $total_detail_asset-$data['asset'][$asset_childs->mst_kode_rekening];
                }
                if($asset_childs->mst_normal == 'debet'){
                    $asset              = $debet-$kredit;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset+$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']+$asset;
                    $total_detail_asset = $total_detail_asset+$data['asset'][$asset_childs->mst_kode_rekening];                       
                }
            }
            
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$detail_asset->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($detail_asset->mst_normal == 'kredit'){                
                $asset              = $kredit-$debet;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset-$total_detail_asset;
                $data['total_asset']     = $data['total_asset']-$asset;
            }
            if($detail_asset->mst_normal == 'debet'){
                $asset              = $debet-$kredit;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset+$total_detail_asset;
                $data['total_asset']     = $data['total_asset']+$asset;                       
            }
        }

        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$ekuitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($ekuitas_childs->mst_normal == 'kredit'){
                    
                    $ekuitas            = $kredit-$debet;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;
                    $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                }
                if($ekuitas_childs->mst_normal == 'debet'){
                    $ekuitas              = $debet-$kredit;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                    $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas-$ekuitas;                     
                }
                // if($ekuitas_childs->mst_kode_rekening=='2403'){
                //     $ekuitas                = $this->count_rugi_laba($data['end_date']);
                //     $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                //     $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                //     $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                // }
            }

            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_ekuitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_ekuitas->mst_normal == 'kredit'){                
                $ekuitas            = $kredit-$debet;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas+$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
            }
            if($data_ekuitas->mst_normal == 'debet'){
                $ekuitas              = $debet-$kredit;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas-$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;                      
            }   

        }

        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;
            foreach ($data_liabilitas->childs as $liabilitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$liabilitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($liabilitas_childs->mst_normal == 'kredit'){                    
                    $liabilitas            = $kredit-$debet;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }
                    
                }
                if($liabilitas_childs->mst_normal == 'debet'){
                    $liabilitas              = $debet-$kredit;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }                     
                } 
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_liabilitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_liabilitas->mst_normal == 'kredit'){                
                $liabilitas            = $kredit-$debet;
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas+$total_data_liabilitas;
            }
            if($data_liabilitas->mst_normal == 'debet'){
                $liabilitas              = $debet-$kredit; 
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas-$total_data_liabilitas;                    
            }
            $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
        }

        $data['hitung']         = $data['total_liabilitas']+$data['total_ekuitas'];
        if(number_format($data['hitung'],2)==number_format($data['total_asset'],2)){
            $data['status']         = 'BALANCE';
            $data['selisih']         = 0;
        }else{
            $data['status']         = 'NOT BALANCE';
            $data['selisih']         = $data['total_asset']-$data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);
        return view('toko.akunting.neraca.dataNeraca', $data);
    }

    public function cetak(Request $request, $tgl_awal, $tgl_akhir) {
        $data['start_date']         = $tgl_awal;
        $data['end_date']           = $tgl_akhir;

        $data['total_asset']             = 0;
        $data['total_liabilitas']        = 0;
        $data['total_ekuitas']           = 0;
        $data['data_asset']              = MasterCoa::where('mst_posisi','asset')->where('mst_master_id',0)->get();
        $data['data_ekuitas']            = MasterCoa::where('mst_posisi','ekuitas')->where('mst_master_id',0)->get();
        $data['data_liabilitas']         = MasterCoa::where('mst_posisi','liabilitas')->get();

        foreach ($data['data_asset'] as $detail_asset) {
            $total_detail_asset = 0;
            foreach ($detail_asset->childs as $asset_childs) {
                $total_asset_childs = 0;
                foreach ($asset_childs->childs as $asset_childs_2) {
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs_2->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');
                    if($asset_childs_2->mst_normal == 'kredit'){                        
                        $asset              = $kredit-$debet;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']-$asset;
                        $total_asset_childs = $total_asset_childs-$asset;

                    }
                    if($asset_childs_2->mst_normal == 'debet'){
                        $asset              = $debet-$kredit;
                        $data['asset'][$asset_childs_2->mst_kode_rekening] = $asset;
                        $data['total_asset']     = $data['total_asset']+$asset;
                        $total_asset_childs = $total_asset_childs+$asset;                       
                    }                    
                }

                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$asset_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($asset_childs->mst_normal == 'kredit'){                    
                    $asset              = $kredit-$debet;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset-$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']-$asset;
                    $total_detail_asset = $total_detail_asset-$data['asset'][$asset_childs->mst_kode_rekening];
                }
                if($asset_childs->mst_normal == 'debet'){
                    $asset              = $debet-$kredit;
                    $data['asset'][$asset_childs->mst_kode_rekening] = $asset+$total_asset_childs;
                    $data['total_asset']     = $data['total_asset']+$asset;
                    $total_detail_asset = $total_detail_asset+$data['asset'][$asset_childs->mst_kode_rekening];                       
                }
            }
            
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$detail_asset->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($detail_asset->mst_normal == 'kredit'){                
                $asset              = $kredit-$debet;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset-$total_detail_asset;
                $data['total_asset']     = $data['total_asset']-$asset;
            }
            if($detail_asset->mst_normal == 'debet'){
                $asset              = $debet-$kredit;
                $data['asset'][$detail_asset->mst_kode_rekening] = $asset+$total_detail_asset;
                $data['total_asset']     = $data['total_asset']+$asset;                       
            }
        }

        foreach ($data['data_ekuitas'] as $data_ekuitas) {
            $total_data_ekuitas = 0;
            foreach ($data_ekuitas->childs as $ekuitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$ekuitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($ekuitas_childs->mst_normal == 'kredit'){
                    
                    $ekuitas            = $kredit-$debet;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;
                    $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                }
                if($ekuitas_childs->mst_normal == 'debet'){
                    $ekuitas              = $debet-$kredit;
                    $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                    $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;
                    $total_data_ekuitas = $total_data_ekuitas-$ekuitas;                     
                }
                // if($ekuitas_childs->mst_kode_rekening=='2403'){
                //     $ekuitas                = $this->count_rugi_laba($data['end_date']);
                //     $data['ekuitas'][$ekuitas_childs->mst_kode_rekening] = $ekuitas;                    
                //     $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
                //     $total_data_ekuitas = $total_data_ekuitas+$ekuitas;
                // }
            }

            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_ekuitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_ekuitas->mst_normal == 'kredit'){                
                $ekuitas            = $kredit-$debet;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas+$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']+$ekuitas;
            }
            if($data_ekuitas->mst_normal == 'debet'){
                $ekuitas              = $debet-$kredit;
                $data['ekuitas'][$data_ekuitas->mst_kode_rekening] = $ekuitas-$total_data_ekuitas;
                $data['total_ekuitas']     = $data['total_ekuitas']-$ekuitas;                      
            }   

        }

        foreach ($data['data_liabilitas'] as $data_liabilitas) {
            $total_data_liabilitas = 0;
            foreach ($data_liabilitas->childs as $liabilitas_childs) {
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$liabilitas_childs->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($liabilitas_childs->mst_normal == 'kredit'){                    
                    $liabilitas            = $kredit-$debet;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }
                    
                }
                if($liabilitas_childs->mst_normal == 'debet'){
                    $liabilitas              = $debet-$kredit;
                    $data['liabilitas'][$liabilitas_childs->mst_kode_rekening] = $liabilitas;
                    if($data_liabilitas->mst_normal == 'kredit'){
                        $total_data_liabilitas = $total_data_liabilitas-$liabilitas;
                    }else{
                        $total_data_liabilitas = $total_data_liabilitas+$liabilitas;
                    }                     
                } 
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$data_liabilitas->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($data_liabilitas->mst_normal == 'kredit'){                
                $liabilitas            = $kredit-$debet;
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas+$total_data_liabilitas;
            }
            if($data_liabilitas->mst_normal == 'debet'){
                $liabilitas              = $debet-$kredit; 
                $data['liabilitas'][$data_liabilitas->mst_kode_rekening] = $liabilitas-$total_data_liabilitas;                    
            }
            $data['total_liabilitas']     = $data['total_liabilitas']+$liabilitas;
        }

        $data['hitung']         = $data['total_liabilitas']+$data['total_ekuitas'];
        if(number_format($data['hitung'],2)==number_format($data['total_asset'],2)){
            $data['status']         = 'BALANCE';
            $data['selisih']         = 0;
        }else{
            $data['status']         = 'NOT BALANCE';
            $data['selisih']         = $data['total_asset']-$data['hitung'];
        }
        // $data['status']         = 0;
        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);

        return view('toko.akunting.neraca.cetak', $data);
    }

    public function create()
    {
        // $kodeproduksi = $this->createKodeProduksi();
        $perkiraan = MasterCoa::all();
        return view('toko.akunting.jurnal-harian.create', compact('perkiraan'));
    }

    function perkiraan_space($subKe) {
        switch($subKe) {
            case 1: $space = '&nbsp;&nbsp;'; break;
            case 2: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 3: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 4: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            default: $space = '';
        }
        return $space;
    }

}
