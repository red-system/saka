<?php

namespace App\Http\Controllers\Akunting;

use App\Models\MasterDetail;
use App\Models\Periode;
use App\Models\JurnalHarian;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use DB;

class JurnalHarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $productions = Production::get();
        // return view('toko.akunting.jurnal-harian.index', compact('productions'));
        $periode            = Periode::where('id',1)->first();
        $data['periode_month']      = $periode->bulan_aktif;
        $data['periode_year']       = $periode->tahun_aktif;
        $data['month']              = [
            '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        return view('toko.akunting.jurnal-harian.index',$data);
    }

    function getNoTransaksi($kode){
        //buat no faktur pembelian
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        // $kode_user                = auth()->user()->kode;
        $no_po_last               = JurnalHarian::where('no_jurnal','like','%'.$kode.'/'.$bulan.$thn.'%')->max('no_jurnal');
        $lastNoUrut               = substr($no_po_last,0,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        $nextNoTransaksi              = sprintf('%04s',$nextNoUrut).'/'.$kode.'/'.$bulan.$thn;
        // $nextNoTransaksi            = sprintf('%04s',$nextNoUrut).'/'.$kode_user;
        // $data['next_no_trs']    = $nextNoTransaksi;
        $next_no_tr = $nextNoTransaksi;

        return $next_no_tr;
        //buat no faktur pembelian
    }

    public function getdata(Request $request) {
        $queryJurnal = JurnalHarian::whereBetween('jmu_tanggal', [$request->tgl_awal, $request->tgl_akhir])->orderBy('jmu_tanggal','ASC');
        $data['dataJurnal'] = $queryJurnal->get();
        // return view('toko.akunting.jurnal-harian.dataJurnalHarian', compact('dataJurnal'))->render();
        return view('toko.akunting.jurnal-harian.dataJurnalHarian', $data);
    }

    public function cetakJurnal(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $queryJurnal = JurnalHarian::whereBetween('jmu_tanggal', [$start, $end])->orderBy('jmu_tanggal','ASC');
        
        $data['dataJurnal'] = $queryJurnal->get();

        // $pdf = \PDF::loadView('toko.akunting.jurnal-harian.cetakJurnal', $data)
        //     ->setPaper('a4', 'landscape');
        // return $pdf->stream();

        return view('toko.akunting.jurnal-harian.cetakJurnal', $data);
    }

    public function create()
    {
        // $kodeproduksi = $this->createKodeProduksi();
        $perkiraan = MasterCoa::all();
        $no_jurnal = $this->getNoTransaksi('JU');
        return view('toko.akunting.jurnal-harian.create', compact('perkiraan','no_jurnal'));
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try{

            // Data Transaksi
            $master_id        = $request->input('master_id');
            $jenis_transaksi  = $request->input('jenis_transaksi');
            $debet            = $request->input('debet');
            $kredit           = $request->input('kredit');
            $tipe_arus_kas    = $request->input('tipe');
            $catatan          = $request->input('catatan');

            $year             = date('Y', strtotime($request->tgl_jmu));
            $month            = date('m', strtotime($request->tgl_jmu));
            $day              = date('d', strtotime($request->tgl_jmu));

            $last_no_jmu           = JurnalHarian::where('jmu_tanggal',$request->tgl_jmu)->max('jmu_no');
            if($last_no_jmu==0){
                $next_no_jmu                   = 0+1;
            }else{
                $next_no_jmu                   = $last_no_jmu+1;
            }

            $dataJurnal         = new JurnalHarian();
            $dataJurnal->no_jurnal          = $request->no_bukti;
            $dataJurnal->jmu_tanggal        = $request->tgl_jmu;
            $dataJurnal->jmu_no             = $next_no_jmu;
            $dataJurnal->jmu_keterangan     = $request->keterangan;
            $dataJurnal->jmu_year           = $year;
            $dataJurnal->jmu_month          = $month;
            $dataJurnal->jmu_day            = $day;
            $dataJurnal->jmu_date_insert    = date('Y-m-d');
            $dataJurnal->created_by         = auth()->user()->name;
            $dataJurnal->updated_by         = auth()->user()->name;
            $dataJurnal->created_at         = date('Y-m-d H:i:s');
            $dataJurnal->updated_at         = date('Y-m-d H:i:s');
            
            $dataJurnal->save();
            $id_jurnal  = $dataJurnal->id_jurnal;

            foreach($master_id as $key=>$val) {
                $master             = MasterCoa::find($master_id[$key]);
                $trs_kode_rekening  = $master->mst_kode_rekening;
                $trs_nama_rekening  = $master->mst_nama_rekening;
                   
            
                $data_transaksi           = [
                    'id_jurnal_umum'      => $id_jurnal,
                    'master_id'           => $master_id[$key],
                    'trs_jenis_transaksi' => $jenis_transaksi[$key],
                    'trs_debet'           => $debet[$key],
                    'trs_kredit'          => $kredit[$key],
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening,
                    'trs_nama_rekening'   => $trs_nama_rekening,
                    'trs_tipe_arus_kas'   => $tipe_arus_kas[$key],
                    'trs_catatan'         => $catatan[$key],
                    'trs_date_insert'     => date('Y-m-d'),
                    'trs_date_update'     => date('Y-m-d'),
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => '-',
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'created_by'          => auth()->user()->name,
                    'created_at'          => auth()->user()->name,
                    'updated_by'          => date('Y-m-d H:i:s'),
                    'updated_at'          => date('Y-m-d H:i:s'),
                    'tgl_transaksi'       => $request->tgl_jmu
                ];    
                Transaksi::insert($data_transaksi);
    
            }
            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Daily Journal Data saved successfully']);
            return redirect()->route('jurnalHarian.index');

            
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
        
    }

    public function edit($id_jurnal){
        $data['perkiraan'] = MasterCoa::all();
        $data['id_jurnal'] = $id_jurnal;
        $data['jurnalUmum'] = Jurnalharian::find($id_jurnal);
        return view('toko.akunting.jurnal-harian.edit', $data);
    }

    public function update(Request $request,$id_jurnal)
    {
        \DB::beginTransaction();
        try{

            // Data Transaksi
            $master_id        = $request->input('master_id');
            $jenis_transaksi  = $request->input('jenis_transaksi');
            $debet            = $request->input('debet');
            $kredit           = $request->input('kredit');
            $tipe_arus_kas    = $request->input('tipe');
            $catatan          = $request->input('catatan');

            $year             = date('Y', strtotime($request->tgl_jmu));
            $month            = date('m', strtotime($request->tgl_jmu));
            $day              = date('d', strtotime($request->tgl_jmu));

            $last_no_jmu           = JurnalHarian::where('jmu_tanggal',$request->tgl_jmu)->max('jmu_no');
            if($last_no_jmu==0){
                $next_no_jmu                   = 0+1;
            }else{
                $next_no_jmu                   = $last_no_jmu+1;
            }

            $dataJurnal         = [];
            $dataJurnal['no_jurnal']          = $request->no_bukti;
            $dataJurnal['jmu_tanggal']        = $request->tgl_jmu;
            // $dataJurnal['jmu_no']             = $next_no_jmu;
            $dataJurnal['jmu_keterangan']     = $request->keterangan;
            $dataJurnal['jmu_year']           = $year;
            $dataJurnal['jmu_month']          = $month;
            $dataJurnal['jmu_day']            = $day;
            $dataJurnal['jmu_date_insert']    = date('Y-m-d');
            // $dataJurnal['created_by']         = auth()->user()->name;
            $dataJurnal['updated_by']         = auth()->user()->name;
            // $dataJurnal['created_at']         = date('Y-m-d H:i:s');
            $dataJurnal['updated_at']         = date('Y-m-d H:i:s');
            
            // $dataJurnal->save();
            // $id_jurnal  = $dataJurnal->id_jurnal;
            JurnalHarian::where('id_jurnal',$id_jurnal)->update($dataJurnal);
            Transaksi::where('id_jurnal_umum',$id_jurnal)->delete();

            foreach($master_id as $key=>$val) {
                $master             = MasterCoa::find($master_id[$key]);
                $trs_kode_rekening  = $master->mst_kode_rekening;
                $trs_nama_rekening  = $master->mst_nama_rekening;
                   
            
                $data_transaksi           = [
                    'id_jurnal_umum'      => $id_jurnal,
                    'master_id'           => $master_id[$key],
                    'trs_jenis_transaksi' => $jenis_transaksi[$key],
                    'trs_debet'           => $debet[$key],
                    'trs_kredit'          => $kredit[$key],
                    'user_id'             => 0,
                    'trs_year'            => $year,
                    'trs_month'           => $month,
                    'trs_kode_rekening'   => $trs_kode_rekening,
                    'trs_nama_rekening'   => $trs_nama_rekening,
                    'trs_tipe_arus_kas'   => $tipe_arus_kas[$key],
                    'trs_catatan'         => $catatan[$key],
                    'trs_date_insert'     => date('Y-m-d'),
                    'trs_date_update'     => date('Y-m-d'),
                    'trs_charge'          => 0,
                    'trs_no_check_bg'     => '-',
                    'trs_tgl_pencairan'   => date('Y-m-d'),
                    'trs_setor'           => 0,
                    'created_by'          => auth()->user()->name,
                    'created_at'          => date('Y-m-d H:i:s'),
                    'updated_by'          => auth()->user()->name,
                    'updated_at'          => date('Y-m-d H:i:s'),
                ];    
                Transaksi::insert($data_transaksi);
    
            }
            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Daily Journal successfully changed']);
            return redirect()->route('jurnalHarian.index');

            
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
        
    }

    public function posting(){
        \DB::beginTransaction();
        try{
            $periode                    = Periode::first();
            // if($periode->bulan_aktif=='1'){
            //     $data['prev_month']      = '12';
            //     $data['prev_year']       = $periode->tahun_aktif-1;
            // }else{
            //     $data['prev_month']      = $periode->bulan_aktif-1;
            //     $data['prev_year']       = $periode->tahun_aktif;
            // }   
            $data['bulan_aktif']        = $periode->bulan_aktif;
            $data['tahun_aktif']        = $periode->tahun_aktif;

            $detailPerkiraan = MasterDetail::where('msd_year',$data['tahun_aktif'])->where('msd_month',$data['bulan_aktif'])->get();

            if($data['bulan_aktif']=='12'){
                $tahun_aktif_baru       = $data['tahun_aktif']+1;
                $bulan_aktif_baru       = '1';
            }else{
                $tahun_aktif_baru       = $data['tahun_aktif'];
                $bulan_aktif_baru       = $data['bulan_aktif']+1;
            }

            foreach($detailPerkiraan as $pkr){
                if($pkr->master->mst_normal == 'kredit'){
                    $saldo_awal=$pkr->msd_awal_kredit;
                }
                if($pkr->master->mst_normal == 'debet'){
                    $saldo_awal=$pkr->msd_awal_debet;
                }
                foreach($pkr->master->transaksi->where('trs_year',$data['tahun_aktif'])->where('trs_month',$data['bulan_aktif']) as $transaksi){
                    if($pkr->master->mst_normal == 'kredit'){
                        $saldo_awal=$saldo_awal+$transaksi->trs_kredit-$transaksi->trs_debet;
                    }
                    if($pkr->master->mst_normal == 'debet'){
                        $saldo_awal=$saldo_awal-$transaksi->trs_kredit+$transaksi->trs_debet;
                    }                
                }
                if($pkr->master->mst_normal == 'kredit'){
                    $data_master_detail     = [
                        'master_id'         => $pkr->master_id,
                        'msd_year'          => $tahun_aktif_baru,
                        'msd_month'         => $bulan_aktif_baru,
                        'msd_awal_kredit'   => $saldo_awal,
                        'msd_awal_debet'    => 0,
                        'msd_date_insert'   => date('Y-m-d'),
                        'msd_date_update'   => date('Y-m-d'),
                        'created_at'        => date('Y-m-d'),
                        'updated_at'        => date('Y-m-d'),
                        'created_by'        => auth()->user()->name,
                        'updated_by'        => auth()->user()->name
                    ];
                }
                if($pkr->master->mst_normal == 'debet'){
                    $data_master_detail     = [
                        'master_id'         => $pkr->master_id,
                        'msd_year'          => $tahun_aktif_baru,
                        'msd_month'         => $bulan_aktif_baru,
                        'msd_awal_kredit'   => 0,
                        'msd_awal_debet'    => $saldo_awal,
                        'msd_date_insert'   => date('Y-m-d'),
                        'msd_date_update'   => date('Y-m-d'),
                        'created_at'        => date('Y-m-d'),
                        'updated_at'        => date('Y-m-d'),
                        'created_by'        => auth()->user()->name,
                        'updated_by'        => auth()->user()->name
                    ];
                }
                MasterDetail::insert($data_master_detail);
                
            }

            $data_periode_baru      = [
                'bulan_aktif'       => $bulan_aktif_baru,
                'tahun_aktif'       => $tahun_aktif_baru
            ];
            Periode::where('id',1)->update($data_periode_baru);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Successful Journal Posted']);
            return redirect()->route('bukuBesar.index');

        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();      
            
        }
    }
}
