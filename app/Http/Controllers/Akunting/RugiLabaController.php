<?php

namespace App\Http\Controllers\Akunting;

use App\Models\DetailProduction;
use App\Models\GoodsFlow;
use App\Models\JurnalHarian;
use App\Models\MasterCoa;
use App\Models\Transaksi;
use App\Models\MasterDetail;
use App\Models\Periode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use DB;

class RugiLabaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        $data['title']          = 'Rugi Laba';

        //laba kotor
        $data['data_laba_kotor']            = MasterCoa::where('mst_posisi','=','laba_kotor')->where('mst_master_id',0)->get();
        $data['data_laba_bersih']           = MasterCoa::where('mst_posisi','=','laba_bersih')->where('mst_master_id',0)->get();
        $data['total_laba_kotor']           = 0;
        $data['total_laba_bersih']          = 0;

        foreach($data['data_laba_kotor'] as $laba_kotor_1){
            $total_laba_kotor = 0;
            foreach($laba_kotor_1->childs as $laba_kotor_2){
                $total_laba_kotor_2 = 0;
                foreach($laba_kotor_2->childs as $laba_kotor_3){
                    $total_laba_kotor_3 = 0;
                    foreach($laba_kotor_3->childs as $laba_kotor_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_kotor_4->mst_normal == 'kredit'){                                                        
                            $laba_kotor                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }
                        }if($laba_kotor_4->mst_normal == 'debet'){
                            $laba_kotor                                             = $debet-$kredit;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_kotor_3->mst_normal == 'kredit'){
                        $laba_kotor                                             = $kredit-$debet;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                        if($laba_kotor_2->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }if($laba_kotor_3->mst_normal == 'debet'){
                        $laba_kotor                                             = $debet-$kredit;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                        if($laba_kotor_3->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_kotor_2->mst_normal == 'kredit'){
                    $laba_kotor                                             = $kredit-$debet;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }
                }if($laba_kotor_2->mst_normal == 'debet'){
                    $laba_kotor                                             = $debet-$kredit;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_kotor_1->mst_normal == 'kredit'){   
                $laba_kotor                                             = $kredit-$debet;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                
            }if($laba_kotor_1->mst_normal == 'debet'){
                $laba_kotor                                             = $debet-$kredit;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
            }
        }

        foreach($data['data_laba_bersih'] as $laba_bersih_1){
            $total_laba_bersih = 0;
            foreach($laba_bersih_1->childs as $laba_bersih_2){
                $total_laba_bersih_2 = 0;
                foreach($laba_bersih_2->childs as $laba_bersih_3){
                    $total_laba_bersih_3 = 0;
                    foreach($laba_bersih_3->childs as $laba_bersih_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_bersih_4->mst_normal == 'kredit'){                                                        
                            $laba_bersih                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }
                        }if($laba_bersih_4->mst_normal == 'debet'){
                            $laba_bersih                                             = $debet-$kredit;
                            $data['laba_bersih'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_bersih_3->mst_normal == 'kredit'){
                        $laba_bersih                                             = $kredit-$debet;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                        if($laba_bersih_2->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }if($laba_bersih_3->mst_normal == 'debet'){
                        $laba_bersih                                             = $debet-$kredit;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                        if($laba_bersih_3->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_bersih_2->mst_normal == 'kredit'){
                    $laba_bersih                                             = $kredit-$debet;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }
                }if($laba_bersih_2->mst_normal == 'debet'){
                    $laba_bersih                                             = $debet-$kredit;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_bersih_1->mst_normal == 'kredit'){   
                $laba_bersih                                             = $kredit-$debet;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                
            }if($laba_bersih_1->mst_normal == 'debet'){
                $laba_bersih                                             = $debet-$kredit;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
            }
        }

        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);
        
        return view('toko.akunting.rugi-laba.index', $data);
    }

    public function getdata(Request $request) 
    {
        $data['start_date']     = $request->tgl_awal;
        $data['end_date']       = $request->tgl_akhir;
        $data['title']          = 'Rugi Laba';

        //laba kotor
        $data['data_laba_kotor']            = MasterCoa::where('mst_posisi','=','laba_kotor')->where('mst_master_id',0)->get();
        $data['data_laba_bersih']           = MasterCoa::where('mst_posisi','=','laba_bersih')->where('mst_master_id',0)->get();
        $data['total_laba_kotor']           = 0;
        $data['total_laba_bersih']          = 0;

        foreach($data['data_laba_kotor'] as $laba_kotor_1){
            $total_laba_kotor = 0;
            foreach($laba_kotor_1->childs as $laba_kotor_2){
                $total_laba_kotor_2 = 0;
                foreach($laba_kotor_2->childs as $laba_kotor_3){
                    $total_laba_kotor_3 = 0;
                    foreach($laba_kotor_3->childs as $laba_kotor_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_kotor_4->mst_normal == 'kredit'){                                                        
                            $laba_kotor                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }
                        }if($laba_kotor_4->mst_normal == 'debet'){
                            $laba_kotor                                             = $debet-$kredit;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_kotor_3->mst_normal == 'kredit'){
                        $laba_kotor                                             = $kredit-$debet;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                        if($laba_kotor_2->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }if($laba_kotor_3->mst_normal == 'debet'){
                        $laba_kotor                                             = $debet-$kredit;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                        if($laba_kotor_3->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_kotor_2->mst_normal == 'kredit'){
                    $laba_kotor                                             = $kredit-$debet;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }
                }if($laba_kotor_2->mst_normal == 'debet'){
                    $laba_kotor                                             = $debet-$kredit;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_kotor_1->mst_normal == 'kredit'){   
                $laba_kotor                                             = $kredit-$debet;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                
            }if($laba_kotor_1->mst_normal == 'debet'){
                $laba_kotor                                             = $debet-$kredit;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
            }
        }

        foreach($data['data_laba_bersih'] as $laba_bersih_1){
            $total_laba_bersih = 0;
            foreach($laba_bersih_1->childs as $laba_bersih_2){
                $total_laba_bersih_2 = 0;
                foreach($laba_bersih_2->childs as $laba_bersih_3){
                    $total_laba_bersih_3 = 0;
                    foreach($laba_bersih_3->childs as $laba_bersih_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_bersih_4->mst_normal == 'kredit'){                                                        
                            $laba_bersih                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }
                        }if($laba_bersih_4->mst_normal == 'debet'){
                            $laba_bersih                                             = $debet-$kredit;
                            $data['laba_bersih'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_bersih_3->mst_normal == 'kredit'){
                        $laba_bersih                                             = $kredit-$debet;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                        if($laba_bersih_2->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }if($laba_bersih_3->mst_normal == 'debet'){
                        $laba_bersih                                             = $debet-$kredit;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                        if($laba_bersih_3->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_bersih_2->mst_normal == 'kredit'){
                    $laba_bersih                                             = $kredit-$debet;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }
                }if($laba_bersih_2->mst_normal == 'debet'){
                    $laba_bersih                                             = $debet-$kredit;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_bersih_1->mst_normal == 'kredit'){   
                $laba_bersih                                             = $kredit-$debet;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                
            }if($laba_bersih_1->mst_normal == 'debet'){
                $laba_bersih                                             = $debet-$kredit;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
            }
        }

        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);
        return view('toko.akunting.rugi-laba.dataRugiLaba', $data);
    }

    public function cetak(Request $request, $start_date, $end_date) 
    {
        $data['start_date']     = $start_date;
        $data['end_date']       = $end_date;
        $data['title']          = 'Rugi Laba';

        //laba kotor
        $data['data_laba_kotor']            = MasterCoa::where('mst_posisi','=','laba_kotor')->where('mst_master_id',0)->get();
        $data['data_laba_bersih']           = MasterCoa::where('mst_posisi','=','laba_bersih')->where('mst_master_id',0)->get();
        $data['total_laba_kotor']           = 0;
        $data['total_laba_bersih']          = 0;

        foreach($data['data_laba_kotor'] as $laba_kotor_1){
            $total_laba_kotor = 0;
            foreach($laba_kotor_1->childs as $laba_kotor_2){
                $total_laba_kotor_2 = 0;
                foreach($laba_kotor_2->childs as $laba_kotor_3){
                    $total_laba_kotor_3 = 0;
                    foreach($laba_kotor_3->childs as $laba_kotor_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_kotor_4->mst_normal == 'kredit'){                                                        
                            $laba_kotor                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }
                        }if($laba_kotor_4->mst_normal == 'debet'){
                            $laba_kotor                                             = $debet-$kredit;
                            $data['laba_kotor'][$laba_kotor_4->mst_kode_rekening]   = $laba_kotor;
                            $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                            if($laba_kotor_3->mst_normal == 'kredit'){
                                $total_laba_kotor_3 = $total_laba_kotor_3-$laba_kotor;
                            }else{
                                $total_laba_kotor_3 = $total_laba_kotor_3+$laba_kotor;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_kotor_3->mst_normal == 'kredit'){
                        $laba_kotor                                             = $kredit-$debet;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                        if($laba_kotor_2->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }if($laba_kotor_3->mst_normal == 'debet'){
                        $laba_kotor                                             = $debet-$kredit;
                        $data['laba_kotor'][$laba_kotor_3->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_3;
                        $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                        if($laba_kotor_3->mst_normal == 'kredit'){
                            $total_laba_kotor_2 = $total_laba_kotor_2-$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }else{
                            $total_laba_kotor_2 = $total_laba_kotor_2+$data['laba_kotor'][$laba_kotor_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_kotor_2->mst_normal == 'kredit'){
                    $laba_kotor                                             = $kredit-$debet;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }
                }if($laba_kotor_2->mst_normal == 'debet'){
                    $laba_kotor                                             = $debet-$kredit;
                    $data['laba_kotor'][$laba_kotor_2->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor_2;
                    $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
                    if($laba_kotor_1->mst_normal == 'kredit'){
                        $total_laba_kotor = $total_laba_kotor-$laba_kotor+$total_laba_kotor_2;
                    }else{
                        $total_laba_kotor = $total_laba_kotor+$laba_kotor+$total_laba_kotor_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_kotor_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_kotor_1->mst_normal == 'kredit'){   
                $laba_kotor                                             = $kredit-$debet;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']+$laba_kotor;
                
            }if($laba_kotor_1->mst_normal == 'debet'){
                $laba_kotor                                             = $debet-$kredit;
                $data['laba_kotor'][$laba_kotor_1->mst_kode_rekening]   = $laba_kotor+$total_laba_kotor;
                $data['total_laba_kotor']                               = $data['total_laba_kotor']-$laba_kotor;
            }
        }

        foreach($data['data_laba_bersih'] as $laba_bersih_1){
            $total_laba_bersih = 0;
            foreach($laba_bersih_1->childs as $laba_bersih_2){
                $total_laba_bersih_2 = 0;
                foreach($laba_bersih_2->childs as $laba_bersih_3){
                    $total_laba_bersih_3 = 0;
                    foreach($laba_bersih_3->childs as $laba_bersih_4){
                        $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_4->master_id);
                        $kredit             = $query->sum('trs_kredit');
                        $debet              = $query->sum('trs_debet');
                        if($laba_bersih_4->mst_normal == 'kredit'){                                                        
                            $laba_bersih                                             = $kredit-$debet;
                            $data['laba_kotor'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }
                        }if($laba_bersih_4->mst_normal == 'debet'){
                            $laba_bersih                                             = $debet-$kredit;
                            $data['laba_bersih'][$laba_bersih_4->mst_kode_rekening]   = $laba_bersih;
                            $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                            if($laba_bersih_3->mst_normal == 'kredit'){
                                $total_laba_bersih_3 = $total_laba_bersih_3-$laba_bersih;
                            }else{
                                $total_laba_bersih_3 = $total_laba_bersih_3+$laba_bersih;
                            }
                        }
                    }
                    $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_3->master_id);
                    $kredit             = $query->sum('trs_kredit');
                    $debet              = $query->sum('trs_debet');

                    if($laba_bersih_3->mst_normal == 'kredit'){
                        $laba_bersih                                             = $kredit-$debet;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                        if($laba_bersih_2->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }if($laba_bersih_3->mst_normal == 'debet'){
                        $laba_bersih                                             = $debet-$kredit;
                        $data['laba_bersih'][$laba_bersih_3->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_3;
                        $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                        if($laba_bersih_3->mst_normal == 'kredit'){
                            $total_laba_bersih_2 = $total_laba_bersih_2-$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }else{
                            $total_laba_bersih_2 = $total_laba_bersih_2+$data['laba_bersih'][$laba_bersih_3->mst_kode_rekening];
                        }
                    }
                }
                $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_2->master_id);
                $kredit             = $query->sum('trs_kredit');
                $debet              = $query->sum('trs_debet');
                if($laba_bersih_2->mst_normal == 'kredit'){
                    $laba_bersih                                             = $kredit-$debet;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }
                }if($laba_bersih_2->mst_normal == 'debet'){
                    $laba_bersih                                             = $debet-$kredit;
                    $data['laba_bersih'][$laba_bersih_2->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih_2;
                    $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
                    if($laba_bersih_1->mst_normal == 'kredit'){
                        $total_laba_bersih = $total_laba_bersih-$laba_bersih+$total_laba_bersih_2;
                    }else{
                        $total_laba_bersih = $total_laba_bersih+$laba_bersih+$total_laba_bersih_2;
                    }
                }
            }
            $query              = JurnalHarian::leftJoin('tb_transaksi','tb_transaksi.id_jurnal_umum','=','tb_jurnal_umum.id_jurnal')->where('tb_jurnal_umum.jmu_tanggal','>=',$data['start_date'])->where('tb_jurnal_umum.jmu_tanggal','<=',$data['end_date'])->where('tb_transaksi.master_id',$laba_bersih_1->master_id);
            $kredit             = $query->sum('trs_kredit');
            $debet              = $query->sum('trs_debet');
            if($laba_bersih_1->mst_normal == 'kredit'){   
                $laba_bersih                                             = $kredit-$debet;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']+$laba_bersih;
                
            }if($laba_bersih_1->mst_normal == 'debet'){
                $laba_bersih                                             = $debet-$kredit;
                $data['laba_bersih'][$laba_bersih_1->mst_kode_rekening]   = $laba_bersih+$total_laba_bersih;
                $data['total_laba_bersih']                               = $data['total_laba_bersih']-$laba_bersih;
            }
        }

        $data['space1'] = $this->perkiraan_space(1);
        $data['space2'] = $this->perkiraan_space(2);
        $data['space3'] = $this->perkiraan_space(3);
        $data['space4'] = $this->perkiraan_space(4);

        return view('toko.akunting.rugi-laba.cetak', $data);
    }

    function perkiraan_space($subKe) {
        switch($subKe) {
            case 1: $space = '&nbsp;'; break;
            case 2: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 3: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            case 4: $space = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'; break;
            default: $space = '';
        }
        return $space;
    }
}
