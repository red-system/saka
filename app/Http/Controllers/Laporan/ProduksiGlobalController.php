<?php

namespace App\Http\Controllers\Laporan;

use App\Models\Production;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProduksiGlobalController extends Controller
{
    public function index() {
        return view('toko.laporan.produksi.index');
    }

    public function getdata(Request $request) {
        $queryproduksi = Production::whereBetween('tgl_produksi', [$request->tgl_awal, $request->tgl_akhir])
                                    ->where('status', 'finish');

        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('kd_produsen', $request->produsen);
        }

        $dataproduksi = $queryproduksi->get();
        return view('toko.laporan.produksi.dataproduksi', compact('dataproduksi'))->render();
    }

    public function show($id) {
        $produksi = Production::where('status', 'finish')->where('id', $id)->firstOrFail();
        $detailproduksi = $produksi->detail_production;
        return view('toko.laporan.produksi.detailproduksi', compact('produksi', 'detailproduksi'))->render();
    }

    public function cetakProduksi(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $queryproduksi = Production::whereBetween('tgl_produksi', [$start, $end])->where('status', 'finish');
        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('kd_produsen', $request->produsen);
        }
        $data['dataproduksi'] = $queryproduksi->get();

        return view('toko.laporan.produksi.cetakproduksi', $data);
    }

    public function exportProduksi(Request $request, $start, $end) {
        $queryproduksi = Production::whereBetween('tgl_produksi', [$start, $end])->where('status', 'finish');
        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('kd_produsen', $request->produsen);
        }
        $dataproduksi = $queryproduksi->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-produksi';
        $namaprodusen = '';
        if ($request->produsen != 'all') {
            $produsen = Produsen::where('kd_produsen', $request->produsen)->first();
            $nama = $nama.'-'.str_slug($produsen->nama_produsen);
            $namaprodusen = $produsen->nama_produsen.' ';
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $dataproduksi, $namaprodusen) {
            $excel->setTitle('Laporan Produksi '.$namaprodusen.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Produksi';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $dataproduksi, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Produksi Global');
                $sheet->mergeCells('A2:G2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:G3');

                $row = 5;
                $sheet->SetCellValue('A'.$row, 'Kode Produksi');
                $sheet->SetCellValue('B'.$row, 'Tanggal');
                $sheet->SetCellValue('C'.$row, 'Produsen');
                $sheet->SetCellValue('D'.$row, 'Total');
                $sheet->SetCellValue('E'.$row, 'Ppn');
                $sheet->SetCellValue('F'.$row, 'Grand Total');
                $row++;

                $totalSub = 0; $totalPpn = 0; $totalGrand = 0;
                foreach ($dataproduksi as $dpd) {
                    $sheet->SetCellValue('A'.$row, $dpd->kd_produksi);
                    $sheet->SetCellValue('B'.$row, $dpd->tgl_produksi);
                    $sheet->SetCellValue('C'.$row, $dpd->produsen->nama_produsen);
                    $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($dpd->total, 2, "," ,"."));
                    $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($dpd->ppn_nominal, 2, "," ,"."));
                    $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($dpd->grand_total, 2, "," ,"."));

                    $totalSub = $totalSub + $dpd->total;
                    $totalPpn = $totalPpn + $dpd->ppn_nominal;
                    $totalGrand = $totalGrand + $dpd->grand_total;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':C'.$row);
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalSub, 2, "," ,"."));
                $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($totalPpn, 2, "," ,"."));
                $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($totalGrand, 2, "," ,"."));

                $sheet->getStyle('A5'.':F'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':F3')->applyFromArray($stylecenter);
                $sheet->getStyle('D5'.':F5')->applyFromArray($stylecenter);
                $sheet->getStyle('D6'.':F'.$row)->applyFromArray($styleright);

            });
        })->export('xlsx');
    }
}
