<?php

namespace App\Http\Controllers\Laporan;

use App\Models\ProductionMaterial;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenggunaanBahanController extends Controller
{
    public function index() {
        return view('toko.laporan.penggunaan-bahan.index');
    }

    public function getdata(Request $request) {
        $querypgbahan = ProductionMaterial::join('tb_production', 'tb_production_material.kd_produksi', '=', 'tb_production.kd_produksi')
                                        ->whereBetween('tb_production.tgl_produksi', [$request->tgl_awal, $request->tgl_akhir])
                                        ->where('tb_production.status', 'finish');

        if ($request->produsen != 'all') {
            $querypgbahan = $querypgbahan->where('tb_production.kd_produsen', $request->produsen);
        }

        $datapenggunaanbahan = $querypgbahan->selectRaw('tb_production_material.*, sum(qty_penggunaan_material) as totalQty')->groupBy('tb_production_material.kd_material')->get();
        return view('toko.laporan.penggunaan-bahan.datapenggunaan', compact('datapenggunaanbahan'))->render();
    }

    public function cetakPenggunaan(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $querypgbahan = ProductionMaterial::join('tb_production', 'tb_production_material.kd_produksi', '=', 'tb_production.kd_produksi')
            ->whereBetween('tb_production.tgl_produksi', [$start, $end])
            ->where('tb_production.status', 'finish');

        if ($request->produsen != 'all') {
            $querypgbahan = $querypgbahan->where('tb_production.kd_produsen', $request->produsen);
            $data['vendor'] = Produsen::where('kd_produsen', $request->produsen)->first();
        } else {
            $data['vendor'] = null;
        }

        $data['penggunaanbahan'] = $querypgbahan->selectRaw('tb_production_material.*, sum(qty_penggunaan_material) as totalQty')->groupBy('tb_production_material.kd_material')->get();

        return view('toko.laporan.penggunaan-bahan.cetakpenggunaan', $data);
    }

    public function exportPenggunaan(Request $request, $start, $end) {
        $querypgbahan = ProductionMaterial::join('tb_production', 'tb_production_material.kd_produksi', '=', 'tb_production.kd_produksi')
            ->whereBetween('tb_production.tgl_produksi', [$start, $end])
            ->where('tb_production.status', 'finish');

        if ($request->produsen != 'all') {
            $querypgbahan = $querypgbahan->where('tb_production.kd_produsen', $request->produsen);
        }

        $datapenggunaanbahan = $querypgbahan->selectRaw('tb_production_material.*, sum(qty_penggunaan_material) as totalQty')->groupBy('tb_production_material.kd_material')->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-penggunaan-bahan';
        $namaprodusen = '';
        $produsen = null;
        if ($request->produsen != 'all') {
            $produsen = Produsen::where('kd_produsen', $request->produsen)->first();
            $nama = $nama.'-'.str_slug($produsen->nama_produsen);
            $namaprodusen = $produsen->nama_produsen.' ';
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $datapenggunaanbahan, $namaprodusen, $produsen) {
            $excel->setTitle('Laporan Penggunaan Bahan '.$namaprodusen.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Penggunaan Bahan';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $datapenggunaanbahan, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright, $produsen) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Penggunaan Bahan');
                $sheet->mergeCells('A2:F2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:F3');
                $sheet->SetCellValue('A4', !empty($produsen) ? $produsen->nama_produsen : 'Semua Vendor');
                $sheet->mergeCells('A4:F4');

                $row = 6;
                $sheet->SetCellValue('A'.$row, 'Kode');
                $sheet->SetCellValue('B'.$row, 'Name');
                $sheet->SetCellValue('C'.$row, 'Material Type');
                $sheet->SetCellValue('D'.$row, 'Supplier');
                $sheet->SetCellValue('E'.$row, 'Qty');
                $sheet->SetCellValue('F'.$row, 'Satuan');
                $row++;

                foreach ($datapenggunaanbahan as $dpb) {
                    $sheet->SetCellValue('A'.$row, $dpb->kd_material);
                    $sheet->SetCellValue('B'.$row, $dpb->material->name);
                    $sheet->SetCellValue('C'.$row, $dpb->material->human_type);
                    $sheet->SetCellValue('D'.$row, $dpb->material->supplier->nama_supplier);
                    $sheet->SetCellValue('E'.$row, $dpb->totalQty);
                    $sheet->SetCellValue('F'.$row, $dpb->satuan->satuan);
                    $row++;
                }

                $sheet->getStyle('A6'.':F'.($row - 1))->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':F4')->applyFromArray($stylecenter);
                $sheet->getStyle('D6'.':F6')->applyFromArray($stylecenter);
                $sheet->getStyle('E7'.':E'.($row - 1))->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
