<?php

namespace App\Http\Controllers\Laporan;

use App\Models\Location;
use App\Models\Payment;
use App\Models\Piutang;
use App\Models\ReturSales;
use App\Models\Sales;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DetailSales;

class PenjualanGlobalController extends Controller
{
    public function index() {
        return view('toko.laporan.penjualan-global.index');
    }

    public function getdata(Request $request) {
        if ($request->type != 'all') {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->join('tb_payment', 'tb_sales.no_faktur', '=', 'tb_payment.no_faktur')
                ->where('tb_payment.type_pembayaran', $request->type)
                ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        } else {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        }

        $queryretur=ReturSales::join('tb_sales','tb_retur_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$request->tgl_awal, $request->tgl_akhir]);

        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if ($request->location != 'all') {
                $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
                $queryretur=$queryretur->where('tb_location.kd_location', $request->location);
            }
        } else {
            $querypenjualan = $querypenjualan->where('tb_location.kd_location', auth()->user()->kd_location);
            $queryretur=$queryretur->where('tb_location.kd_location', auth()->user()->kd_location);
        }
        $datapenjualan = $querypenjualan->select('tb_sales.*', 'tb_location.location_name')->groupBy('tb_sales.no_faktur')->get();
        $dataretur =$queryretur->get();
        
        $totalQtyTerjual=DetailSales::whereIn('no_faktur', $querypenjualan->select('no_faktur')->pluck('no_faktur'))->get()->sum('qty');  
        //dd($totalQtyTerjual);
    
        return view('toko.laporan.penjualan-global.datapenjualan', compact('datapenjualan', 'dataretur','totalQtyTerjual'))->render();        
    
    }

    public function show($id) {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $penjualan = Sales::where('id', $id)->firstOrFail();
        } else {
            $penjualan = Sales::where('kd_location', auth()->user()->kd_location)->where('id', $id)->firstOrFail();
        }
        $detailsales = $penjualan->detail_sales;
        return view('toko.laporan.penjualan-global.detailpenjualan', compact('penjualan', 'detailsales'))->render();
    }

    public function cetakPenjualan(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        if ($request->type != 'all') {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->join('tb_payment', 'tb_sales.no_faktur', '=', 'tb_payment.no_faktur')
                ->where('tb_payment.type_pembayaran', $request->type)
                ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        } else {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        }

        $queryretur=ReturSales::join('tb_sales','tb_retur_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$start, $end]);
        if ($request->has('location')) {
            if ($request->location != 'all') {
                $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
                $queryretur=$queryretur->where('tb_location.kd_location', $request->location);
                $data['location'] = Location::where('tb_location.kd_location', $request->location)->first(); 
            } else {
                $data['location'] = null;
            }
        } else {
            $querypenjualan = $querypenjualan->where('tb_location.kd_location', auth()->user()->kd_location);
            $queryretur=$queryretur->where('tb_location.kd_location', auth()->user()->kd_location);
            $data['location'] = Location::where('kd_location', auth()->user()->kd_location)->first();
        }
        $data['datapenjualan'] = $querypenjualan->groupBy('tb_sales.no_faktur')->get();
        $data['dataretur'] = $queryretur->get();
        $data['totalQtyTerjual']=DetailSales::whereIn('no_faktur', $querypenjualan->select('no_faktur')->pluck('no_faktur'))->get()->sum('qty');  
        return view('toko.laporan.penjualan-global.cetakpenjualan', $data);
    }

    public function exportPenjualan(Request $request, $start, $end) {
        if ($request->type != 'all') {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->join('tb_payment', 'tb_sales.no_faktur', '=', 'tb_payment.no_faktur')
                                    ->where('tb_payment.type_pembayaran', $request->type)
                                    ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        } else {
            $querypenjualan = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->join('tb_payment', 'tb_sales.no_faktur', '=', 'tb_payment.no_faktur')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        }

        $queryretur=ReturSales::join('tb_sales','tb_retur_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$start, $end]);
        if ($request->has('location')) {
            if ($request->location != 'all') {
                $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
                $queryretur=$queryretur->where('tb_location.kd_location', $request->location);
                $location = Location::where('kd_location', $request->location)->first();
            } else {
                $location = null;
            }
        } else {
            $querypenjualan = $querypenjualan->where('tb_location.kd_location', auth()->user()->kd_location);
            $queryretur=$queryretur->where('tb_location.kd_location', auth()->user()->kd_location);
            $location = Location::where('kd_location', auth()->user()->kd_location)->first();
        }
        $datapenjualan = $querypenjualan->groupBy('tb_sales.no_faktur')->get();
        $dataretur = $queryretur->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-penjualan';
        if ($request->location != '') {
            $nama = $nama.'-'.str_slug($request->location);
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $datapenjualan, $location, $dataretur) {
            $excel->setTitle('Laporan Penjualan '.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Penjualan';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $datapenjualan, $dataretur, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright, $location) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Laporan Penjualan');
                $sheet->mergeCells('A2:J2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:J3');
                $sheet->SetCellValue('A4', !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi');
                $sheet->mergeCells('A4:J4');

                $sheet->SetCellValue('A6', 'Sales');
                $sheet->mergeCells('A6:J6');

                $row = 7;
                $sheet->SetCellValue('A'.$row, 'Invoice Number');
                $sheet->SetCellValue('B'.$row, 'Date');
                $sheet->SetCellValue('C'.$row, 'Customer');
                $sheet->SetCellValue('D'.$row, 'Total');
                $sheet->SetCellValue('E'.$row, 'Disc');
                $sheet->SetCellValue('F'.$row, 'Consignment Fee');
                $sheet->SetCellValue('G'.$row, 'Grand Total');
                $sheet->getStyle('D'.$row.':G'.$row)->applyFromArray($stylecenter);
                $sheet->SetCellValue('H'.$row, 'Payment');
                $sheet->SetCellValue('I'.$row, 'Location');
                $sheet->SetCellValue('J'.$row, 'Status Terbayar');
                $row++;

                $indexData = $row;
                $totalSub = 0; $totalDisc = 0; $totalGrand = 0; $totalFee = 0;
                foreach ($datapenjualan as $dpj) {
                    $sheet->SetCellValue('A'.$row, $dpj->no_faktur);
                    $sheet->SetCellValue('B'.$row, date('Y-m-d', strtotime($dpj->tgl)));
                    $sheet->SetCellValue('C'.$row, $dpj->nama_pelanggan);
                    $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($dpj->total, 2, "," ,"."));
                    $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($dpj->disc_nominal, 2, "," ,"."));
                    $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($dpj->komisi_nominal, 2, "," ,"."));
                    $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($dpj->grand_total, 2, "," ,"."));
                    $sheet->SetCellValue('I'.$row, $dpj->location_name);
                    $sheet->SetCellValue('J'.$row, $dpj->status_terbayar == '1' ? 'Sudah Terbayar' : 'Belum Terbayar');

                    $txtPayment = '';
                    if($dpj->paymentSales->count() > 0) {
                        foreach($dpj->paymentSales as $payment) {
                            if($payment == $dpj->paymentSales->last()) {
                                $txtPayment .= $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.');
                            } else {
                                $txtPayment .= $payment->human_payment_type.($payment->bank_pembayaran != '' ? ' - '.$payment->bank_pembayaran : '' ).': '.number_format($payment->payment, 0, '', '.').', ';
                            }
                        }
                    }
                    $sheet->SetCellValue('H'.$row, $txtPayment);

                    $totalSub = $totalSub + $dpj->total;
                    $totalDisc = $totalDisc + $dpj->disc_nominal;
                    $totalGrand = $totalGrand + $dpj->grand_total;
                    $totalFee = $totalFee + $dpj->komisi_nominal;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':C'.$row);
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalSub, 2, "," ,"."));
                $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($totalDisc, 2, "," ,"."));
                $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($totalFee, 2, "," ,"."));
                $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($totalGrand, 2, "," ,"."));
                $sheet->getStyle('D'.$indexData.':G'.$row)->applyFromArray($styleright);

                $sheet->getStyle('A7'.':J'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':H4')->applyFromArray($stylecenter);

                $sheet->SetCellValue('A'.($row + 2), 'Retur Sales');
                $sheet->mergeCells('A'.($row + 2).':J'.($row + 2));

                $row = $row + 3;
                $indexData2 = $row;
                $sheet->SetCellValue('A'.$row, 'Invoice Number');
                $sheet->SetCellValue('B'.$row, 'Retur Number');
                $sheet->SetCellValue('C'.$row, 'Retur Date');
                $sheet->SetCellValue('D'.$row, 'Total Retur');
                $sheet->SetCellValue('E'.$row, 'Additional Cost');
                $sheet->SetCellValue('F'.$row, 'Reason');
                $sheet->SetCellValue('G'.$row, 'Location');

                $row++;

                $totalRetur = 0; $totalTambahan = 0;
                foreach ($dataretur as $dr) {
                    $sheet->SetCellValue('A'.$row, $dr->no_retur);
                    $sheet->SetCellValue('B'.$row, $dr->no_faktur);
                    $sheet->SetCellValue('C'.$row, date('Y-m-d', strtotime($dr->tgl_pengembalian)));
                    $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($dr->total_retur, 2, ',', '.'));
                    $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($dr->tambahan_biaya, 2, ',', '.'));
                    $sheet->SetCellValue('F'.$row, $dr->human_alasan);
                    $sheet->SetCellValue('G'.$row, $dr->location_name);
                    $row++;

                    $totalRetur = $totalRetur + $dr->total_retur;
                    $totalTambahan = $totalTambahan + $dr->tambahan_biaya;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':C'.$row);
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalRetur, 2, "," ,"."));
                $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($totalTambahan, 2, "," ,"."));
                $sheet->getStyle('D'.$indexData.':E'.$row)->applyFromArray($styleright);
                $sheet->SetCellValue('F'.$row, '');

                $sheet->getStyle('A'.$indexData2.':G'.$row)->applyFromArray($styleallbd);

                $sheet->SetCellValue('A'.($row + 2), 'Grand Total');
                $sheet->mergeCells('A'.($row + 2).':G'.($row + 2));

                $row = $row + 3;
                $indexData3 = $row;
                $sheet->SetCellValue('A'.$row, 'Total Discount');
                $sheet->SetCellValue('B'.$row, 'Total Cash');
                $sheet->SetCellValue('C'.$row, 'Total Transfer');
                $sheet->SetCellValue('D'.$row, 'Total Piutang');
                $row++;

                if ($datapenjualan->count() > 0) {
                    $totaldiskon = $datapenjualan->sum('disc_nominal');
                } else {
                    $totaldiskon = 0;
                }
                $sheet->SetCellValue('A'.$row, 'Rp. '.number_format($totaldiskon, 2, ',', '.'));

                if ($datapenjualan->count() > 0) {
                    $totalcash = Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', 'tunai')->sum('payment');
                } else {
                    $totalcash = 0;
                }
                $sheet->SetCellValue('B'.$row, 'Rp. '.number_format($totalcash, 2, ',', '.'));

                if ($datapenjualan->count() > 0) {
                    $totaltransfer = Payment::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->where('type_pembayaran', '!=', 'tunai')->sum('payment');
                } else {
                    $totaltransfer = 0;
                }
                $sheet->SetCellValue('C'.$row, 'Rp. '.number_format($totaltransfer, 2, ',', '.'));

                if ($datapenjualan->count() > 0) {
                    $totalpiutang = Piutang::whereIn('no_faktur', $datapenjualan->pluck('no_faktur'))->sum('sisa_piutang');
                } else {
                    $totalpiutang = 0;
                }
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalpiutang, 2, ',', '.'));

                $row++;
                if ($datapenjualan->count() > 0) {
                    $totalPenjualan = $datapenjualan->sum('grand_total');
                } else {
                    $totalPenjualan = 0;
                }

                if ($dataretur->count() > 0) {
                    $totalReturPenjualan = $dataretur->sum('total_retur');
                } else {
                    $totalReturPenjualan = 0;
                }
                $sheet->SetCellValue('A'.$row, 'Grand Total Penjualan: Rp. '.number_format($totalPenjualan - $totalReturPenjualan, 2, ',', '.'));
                $sheet->mergeCells('A'.$row.':D'.$row);
                $sheet->getStyle('A'.$row)->applyFromArray($stylecenter);

                $sheet->getStyle('A'.$indexData3.':D'.$row)->applyFromArray($styleallbd);
            });
        })->export('xlsx');
    }

    public function setTerbayar(Request $request) {
        $sales = Sales::where('id', $request->id)->where('kd_location', $request->location)->firstOrFail();
        if ($sales->update(['status_terbayar' => $request->terbayar])) {
            return 'OK';
        } else {
            return 'FAIL';
        }
    }
}
