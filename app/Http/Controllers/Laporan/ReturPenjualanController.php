<?php

namespace App\Http\Controllers\Laporan;

use App\Models\DetailReturSales;
use App\Models\Location;
use App\Models\ReturSales;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReturPenjualanController extends Controller
{
    public function index() {
        $location = Location::all();
        $listLocation = [];
        foreach ($location as $lc) {
            $listLocation[$lc->kd_location] = $lc->location_name.' - '.$lc->human_type;
        }
        return view('toko.laporan.retur-sales.index', compact('listLocation'));
    }

    public function getdata(Request $request) {
        $queryretur = DetailReturSales::join('tb_retur_sales', 'tb_detail_retur.no_retur', '=', 'tb_retur_sales.no_retur')
                                    ->join('tb_sales', 'tb_retur_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
                                    ->join('tb_product', 'tb_detail_retur.kd_produk', '=', 'tb_product.kd_produk')
                                    ->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if ($request->location != 'all') {
                $queryretur = $queryretur->where('tb_sales.kd_location', $request->location);
            }
        } else {
            $queryretur = $queryretur->where('tb_sales.kd_location', auth()->user()->kd_location);
        }
        $dataretur = $queryretur->selectRaw('tb_retur_sales.no_retur, tb_retur_sales.no_faktur, tb_retur_sales.tgl_pengembalian, tb_retur_sales.pelaksana, tb_product.deskripsi, tb_detail_retur.qty_retur, tb_retur_sales.alasan, tb_retur_sales.alasan_lain,tb_location.location_name')->get();
        // dd($dataretur);
        $alasanRetur = ReturSales::alasanList();
        return view('toko.laporan.retur-sales.dataretur', compact('dataretur', 'alasanRetur'))->render();
    }

    public function cetakRetur(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $queryretur = DetailReturSales::join('tb_retur_sales', 'tb_detail_retur.no_retur', '=', 'tb_retur_sales.no_retur')
            ->join('tb_sales', 'tb_retur_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
            ->join('tb_product', 'tb_detail_retur.kd_produk', '=', 'tb_product.kd_produk')
            ->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$start, $end]);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if ($request->location != 'all') {
                $queryretur = $queryretur->where('tb_sales.kd_location', $request->location);
                $data['location'] = Location::where('kd_location', $request->location)->first();
            } else {
                $data['location'] = null;
            }
        } else {
            $queryretur = $queryretur->where('tb_sales.kd_location', auth()->user()->kd_location);
        }
        $data['dataretur'] = $queryretur->selectRaw('tb_retur_sales.no_retur, tb_retur_sales.no_faktur, tb_retur_sales.tgl_pengembalian, tb_retur_sales.pelaksana, tb_product.deskripsi, tb_detail_retur.qty_retur, tb_retur_sales.alasan, tb_retur_sales.alasan_lain,tb_location.location_name')->get();
        $data['alasanRetur'] = ReturSales::alasanList();

        return view('toko.laporan.retur-sales.cetakretur', $data);
    }

    public function exportRetur(Request $request, $start, $end) {
        $queryretur = DetailReturSales::join('tb_retur_sales', 'tb_detail_retur.no_retur', '=', 'tb_retur_sales.no_retur')
            ->join('tb_sales', 'tb_retur_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
            ->join('tb_product', 'tb_detail_retur.kd_produk', '=', 'tb_product.kd_produk')
            ->whereBetween(\DB::raw('LEFT(tgl_pengembalian, 10)'), [$start, $end]);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if ($request->location != 'all') {
                $queryretur = $queryretur->where('tb_sales.kd_location', $request->location);
                $location = Location::where('kd_location', $request->location)->first();
            } else {
                $location = null;
            }
        } else {
            $queryretur = $queryretur->where('tb_sales.kd_location', auth()->user()->kd_location);
        }
        $dataretur = $queryretur->selectRaw('tb_retur_sales.no_retur, tb_retur_sales.no_faktur, tb_retur_sales.tgl_pengembalian, tb_retur_sales.pelaksana, tb_product.deskripsi, tb_detail_retur.qty_retur, tb_retur_sales.alasan, tb_retur_sales.alasan_lain,tb_location.location_name')->get();
        // dd($dataretur);
        $alasanRetur = ReturSales::alasanList();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-retur-penjualan';
        if ($request->location != 'all') {
            $nama = $nama.'-'.str_slug($request->location);
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $dataretur, $location, $alasanRetur) {
            $excel->setTitle('Retur Penjualan '.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Retur Penjualan';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $dataretur, $location, $alasanRetur, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Retur Sales');
                $sheet->mergeCells('A2:H2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:H3');
                $sheet->SetCellValue('A4', !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi');
                $sheet->mergeCells('A4:H4');

                $row = 6;
                $sheet->SetCellValue('A'.$row, 'Retur Number');
                $sheet->SetCellValue('B'.$row, 'Invoice Number');
                $sheet->SetCellValue('C'.$row, 'Date');
                $sheet->SetCellValue('D'.$row, 'Doer');
                $sheet->SetCellValue('E'.$row, 'Product');
                $sheet->SetCellValue('F'.$row, 'Qty Retur');
                $sheet->SetCellValue('G'.$row, 'Reason');
                $sheet->SetCellValue('H'.$row, 'Other Reason');
                $sheet->SetCellValue('I'.$row, 'Location');
                $sheet->getStyle('C'.$row.':H'.$row)->applyFromArray($stylecenter);
                $row++;

                $indexData = $row;
                foreach ($dataretur as $dr) {
                    $sheet->SetCellValue('A'.$row, $dr->no_retur);
                    $sheet->SetCellValue('B'.$row, $dr->no_faktur);
                    $sheet->SetCellValue('C'.$row, date('Y-m-d', strtotime($dr->tgl_pengembalian)));
                    $sheet->SetCellValue('D'.$row, $dr->pelaksana);
                    $sheet->SetCellValue('E'.$row, $dr->deskripsi);
                    $sheet->SetCellValue('F'.$row, $dr->qty_retur);
                    $sheet->SetCellValue('G'.$row, $alasanRetur[$dr->alasan]);
                    $sheet->SetCellValue('H'.$row, $dr->alasan_lain);
                    $sheet->SetCellValue('H'.$row, $dr->location_name);
                    $row++;
                }

                $sheet->getStyle('A6'.':H'.($row - 1))->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':H4')->applyFromArray($stylecenter);
                $sheet->getStyle('F6'.':F'.($row - 1))->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
