<?php

namespace App\Http\Controllers\Laporan;

use App\Models\DetailSales;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sales;

class PenjualanStokController extends Controller
{
    public function index() {
        return view('toko.laporan.penjualan-stok.index');
    }

    public function getdata(Request $request) {
        // $querypenjualan = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->join('tb_product','tb_detail_sales.kd_produk','=','tb_detail_sales.kd_produk')
        //                 ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        // if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
        //     if ($request->location != 'all') {
        //         $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
        //     }
        // } else {
        //     $querypenjualan = $querypenjualan->where('tb_sales.kd_location', auth()->user()->kd_location);
        // }
        // $datapenjualan = $querypenjualan->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty, tb_detail_sales.price, sum(tb_detail_sales.disc_nominal*tb_detail_sales.qty) as tglDiscTotal, sum(tb_detail_sales.subtotal) as tglSubTotal,tb_location.location_name')
        //                                 ->groupBy('kd_produk')->get();
        // $datapenjualan = $querypenjualan->selectRaw('tb_detail_sales.kd_produk, tb_detail_sales.qty, tb_detail_sales.price, sum(tb_detail_sales.disc_nominal*tb_detail_sales.qty) as tglDiscTotal, sum(tb_detail_sales.subtotal) as tglSubTotal,tb_location.location_name')
        //                                 ->get();
        
        $from = $request->tgl_awal;
        $to = $request->tgl_akhir;
        $location=$request->location;
        //dd($request);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesDisc = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesDisc = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
            $salesDisc = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        $datapenjualan = DetailSales::whereIn('no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))->selectRaw('kd_produk, sum(qty) as totalQty,sum(subtotal) as tglSubtotal,size_product,price')->groupBy('kd_produk')->groupBy('size_product')->orderBy('totalQty', 'desc')->get();
        $totalDisc=$salesDisc->selectRaw('sum(disc_nominal) as totalDiscGlobal,sum(additional_disc) as totalDiscAdditional')->get();
        
        // dd($totalDisc);
        return view('toko.laporan.penjualan-stok.datapenjualan', compact('datapenjualan','totalDisc'))->render();
    }

    public function cetakPenjualan(Request $request, $start, $end) {
        // $data['start'] = $start;
        // $data['end'] = $end;

        // $querypenjualan = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
        //     ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        // if ($request->has('location')) {
        //     if ($request->location != 'all') {
        //         $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
        //         $data['location'] = Location::where('kd_location', $request->location)->first();
        //     } else {
        //         $data['location'] = null;
        //     }
        // } else {
        //     $querypenjualan = $querypenjualan->where('tb_sales.kd_location', auth()->user()->kd_location);
        //     $data['location'] = Location::where('kd_location', auth()->user()->kd_location)->first();
        // }
        // $data['datapenjualan'] = $querypenjualan->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty, tb_detail_sales.price, sum(tb_detail_sales.disc_nominal*tb_detail_sales.qty) as tglDiscTotal, sum(tb_detail_sales.subtotal) as tglSubTotal,tb_location.location_name')
        //                                         ->groupBy('kd_produk')->get();
                                                
                                                $from = $start;
                                                $to = $end;
                                                $location=$request->location;
                                                
                                                $data['start'] = $start;
                                                $data['end'] = $end;
                                                $data['location']=$request->location;
                                                
                                                // dd($request);
                                                if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
                                                    if($location=='all'){
                                                        $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                                                        $salesDisc = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                                                        $data['location'] = null;
                                                    }else{
                                                        $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                                                        $salesDisc = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                                                        $data['location'] = Location::where('kd_location', $request->location)->first();
                                                    }
                                                    
                                                } else {
                                                    $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
                                                    $salesDisc = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
                                                    $data['location'] = Location::where('kd_location', auth()->user()->kd_location)->first();
                                                }
                                                $data['datapenjualan'] = DetailSales::whereIn('no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))->selectRaw('kd_produk, sum(qty) as totalQty,sum(subtotal) as tglSubtotal,size_product,price')->groupBy('kd_produk')->groupBy('size_product')->orderBy('totalQty', 'desc')->get();
                                                $data['totalDc']=$salesDisc->selectRaw('sum(disc_nominal) as totalDiscGlobal,sum(additional_disc) as totalDiscAdditional')->get();
                                                // dd($data['totalDc']);
                                                return view('toko.laporan.penjualan-stok.cetakpenjualan', $data);
    }

    public function exportPenjualan(Request $request, $start, $end) {
        $querypenjualan = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
            ->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        if ($request->has('location')) {
            if ($request->location != 'all') {
                $querypenjualan = $querypenjualan->where('tb_sales.kd_location', $request->location);
                $location = Location::where('kd_location', $request->location)->first();
            } else {
                $location = null;
            }
        } else {
            $querypenjualan = $querypenjualan->where('tb_sales.kd_location', auth()->user()->kd_location);
            $location = Location::where('kd_location', auth()->user()->kd_location)->first();
        }
        $datapenjualan = $querypenjualan->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty, tb_detail_sales.price, sum(tb_detail_sales.disc_nominal*tb_detail_sales.qty) as tglDiscTotal, sum(tb_detail_sales.subtotal) as tglSubTotal,tb_location.location_name')
                                        ->groupBy('kd_produk')->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-penjualan-stok';
        if ($request->location != 'all') {
            $nama = $nama.'-'.str_slug($request->location);
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $datapenjualan, $location) {
            $excel->setTitle('Laporan Penjualan '.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Penjualan';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $datapenjualan, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright, $location) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Penjualan Stok');
                $sheet->mergeCells('A2:F2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:F3');
                $sheet->SetCellValue('A4', !empty($location) ? ($location->location_name.' ('.$location->human_type.')') : 'Semua Lokasi');
                $sheet->mergeCells('A4:F4');

                $row = 6;
                $sheet->SetCellValue('A'.$row, 'Kode');
                $sheet->SetCellValue('B'.$row, 'Desc');
                $sheet->SetCellValue('C'.$row, 'Qty');
                $sheet->SetCellValue('D'.$row, 'Price');
                $sheet->SetCellValue('E'.$row, 'Total Discount');
                $sheet->SetCellValue('F'.$row, 'Sub Total');
                $sheet->SetCellValue('G'.$row, 'Location');
                $sheet->getStyle('C'.$row.':F'.$row)->applyFromArray($stylecenter);
                $row++;

                $indexData = $row;
                $totalQty = 0; $totalPrice = 0; $totalDisc = 0; $totalSub = 0;
                foreach ($datapenjualan as $dpj) {
                    $sheet->SetCellValue('A'.$row, $dpj->kd_produk);
                    $sheet->SetCellValue('B'.$row, $dpj->produk->deskripsi);
                    $sheet->SetCellValue('C'.$row, $dpj->totalQty);
                    $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($dpj->price, 2, "," ,"."));
                    $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($dpj->tglDiscTotal, 2, "," ,"."));
                    $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($dpj->tglSubTotal, 2, "," ,"."));
                    $sheet->SetCellValue('G'.$row, $dpj->location_name);

                    $totalQty = $totalQty + $dpj->totalQty;
                    $totalPrice = $totalPrice + $dpj->price;
                    $totalDisc = $totalDisc + $dpj->tglDiscTotal;
                    $totalSub = $totalSub + $dpj->tglSubTotal;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->SetCellValue('C'.$row, $totalQty);
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalPrice, 2, "," ,"."));
                $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($totalDisc, 2, "," ,"."));
                $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($totalSub, 2, "," ,"."));

                $sheet->getStyle('D'.$indexData.':F'.$row)->applyFromArray($styleright);

                $sheet->getStyle('A6'.':F'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':F4')->applyFromArray($stylecenter);
                $sheet->getStyle('C6'.':C'.$row)->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
