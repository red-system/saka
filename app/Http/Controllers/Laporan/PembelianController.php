<?php

namespace App\Http\Controllers\Laporan;

use App\Models\Purchasing;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PembelianController extends Controller
{
    public function index() {
        return view('toko.laporan.pembelian.index');
    }

    public function getdata(Request $request) {
        $querypembelian = Purchasing::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('kd_supplier', $request->supplier);
        }
        $datapembelian = $querypembelian->get();
        return view('toko.laporan.pembelian.datapembelian', compact('datapembelian'))->render();
    }

    public function show($id) {
        $pembelian = Purchasing::where('id', $id)->firstOrFail();
        $detailpurchasing = $pembelian->detail_purchasing;
        return view('toko.laporan.pembelian.detailpembelian', compact('pembelian', 'detailpurchasing'))->render();
    }

    public function cetakPembelian(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $querypembelian = Purchasing::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('kd_supplier', $request->supplier);
        }
        $data['datapembelian'] = $querypembelian->get();

        return view('toko.laporan.pembelian.cetakpembelian', $data);
    }

    public function exportPembelian(Request $request, $start, $end) {
        $querypembelian = Purchasing::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end]);
        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('kd_supplier', $request->supplier);
        }
        $datapembelian = $querypembelian->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-pembelian';
        $namasupplier = '';
        if ($request->supplier != 'all') {
            $supplier = Supplier::where('kd_supplier', $request->supplier)->first();
            $nama = $nama.'-'.str_slug($supplier->nama_supplier);
            $namasupplier = $supplier->nama_supplier.' ';
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $datapembelian, $namasupplier) {
            $excel->setTitle('Laporan Pembelian '.$namasupplier.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Pembelian';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $datapembelian, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Laporan Pembelian Global');
                $sheet->mergeCells('A2:H2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:H3');

                $row = 5;
                $sheet->SetCellValue('A'.$row, 'No Faktur');
                $sheet->SetCellValue('B'.$row, 'Tanggal');
                $sheet->SetCellValue('C'.$row, 'Supplier');
                $sheet->SetCellValue('D'.$row, 'Total');
                $sheet->SetCellValue('E'.$row, 'Ppn');
                $sheet->SetCellValue('F'.$row, 'Disc');
                $sheet->SetCellValue('G'.$row, 'Biaya Tambahan');
                $sheet->SetCellValue('H'.$row, 'Grand Total');
                $sheet->getStyle('D'.$row.':H'.$row)->applyFromArray($stylecenter);
                $row++;

                $indexData = $row;
                $totalSub = 0; $totalPpn = 0; $totalDisc = 0; $totalBTambahan = 0; $totalGrand = 0;
                foreach ($datapembelian as $dpm) {
                    $sheet->SetCellValue('A'.$row, $dpm->no_faktur);
                    $sheet->SetCellValue('B'.$row, date('Y-m-d', strtotime($dpm->tgl)));
                    $sheet->SetCellValue('C'.$row, $dpm->supplier->nama_supplier);
                    $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($dpm->total, 2, "," ,"."));
                    $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($dpm->ppn_nominal, 2, "," ,"."));
                    $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($dpm->disc_nominal, 2, "," ,"."));
                    $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($dpm->biaya_tambahan, 2, "," ,"."));
                    $sheet->SetCellValue('H'.$row, 'Rp. '.number_format($dpm->grand_total, 2, "," ,"."));

                    $totalSub = $totalSub + $dpm->total;
                    $totalPpn = $totalPpn + $dpm->ppn_nominal;
                    $totalDisc = $totalDisc + $dpm->disc_nominal;
                    $totalBTambahan = $totalBTambahan + $dpm->biaya_tambahan;
                    $totalGrand = $totalGrand + $dpm->grand_total;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':C'.$row);
                $sheet->SetCellValue('D'.$row, 'Rp. '.number_format($totalSub, 2, "," ,"."));
                $sheet->SetCellValue('E'.$row, 'Rp. '.number_format($totalPpn, 2, "," ,"."));
                $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($totalDisc, 2, "," ,"."));
                $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($totalBTambahan, 2, "," ,"."));
                $sheet->SetCellValue('H'.$row, 'Rp. '.number_format($totalGrand, 2, "," ,"."));

                $sheet->getStyle('D'.$indexData.':H'.$row)->applyFromArray($styleright);

                $sheet->getStyle('A5'.':H'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':H3')->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
