<?php

namespace App\Http\Controllers\Laporan;

use App\Models\DetailPurchasing;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PembelianMaterialController extends Controller
{
    public function index() {
        return view('toko.laporan.pembelian-material.index');
    }

    public function getdata(Request $request) {
        $querypembelian = DetailPurchasing::join('tb_purchasing', 'tb_detail_purchasing.kd_purchasing', '=', 'tb_purchasing.kd_purchasing')
                                            ->whereBetween(\DB::raw('LEFT(tb_purchasing.tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir]);
        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('tb_purchasing.kd_supplier', $request->supplier);
        }

        $datapembelian = $querypembelian->selectRaw('tb_detail_purchasing.*, tb_purchasing.kd_supplier, LEFT(tb_purchasing.tgl, 10) as tanggal, sum(qty) as tglQty')
                                        ->groupBy(['tanggal', 'kd_material', 'kd_supplier'])->get();
        return view('toko.laporan.pembelian-material.datapembelian', compact('datapembelian'))->render();
    }

    public function cetakPembelian(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;

        $querypembelian = DetailPurchasing::join('tb_purchasing', 'tb_detail_purchasing.kd_purchasing', '=', 'tb_purchasing.kd_purchasing')
            ->whereBetween(\DB::raw('LEFT(tb_purchasing.tgl, 10)'), [$start, $end]);

        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('tb_purchasing.kd_supplier', $request->supplier);
        }

        $data['datapembelian'] = $querypembelian->selectRaw('tb_detail_purchasing.*, tb_purchasing.kd_supplier, LEFT(tb_purchasing.tgl, 10) as tanggal, sum(qty) as tglQty')
                                        ->groupBy(['tanggal', 'kd_material', 'kd_supplier'])->get();

        return view('toko.laporan.pembelian-material.cetakpembelian', $data);
    }

    public function exportPembelian(Request $request, $start, $end) {
        $querypembelian = DetailPurchasing::join('tb_purchasing', 'tb_detail_purchasing.kd_purchasing', '=', 'tb_purchasing.kd_purchasing')
            ->whereBetween(\DB::raw('LEFT(tb_purchasing.tgl, 10)'), [$start, $end]);

        if ($request->supplier != 'all') {
            $querypembelian = $querypembelian->where('tb_purchasing.kd_supplier', $request->supplier);
        }

        $datapembelian = $querypembelian->selectRaw('tb_detail_purchasing.*, tb_purchasing.kd_supplier, LEFT(tb_purchasing.tgl, 10) as tanggal, sum(qty) as tglQty')
            ->groupBy(['tanggal', 'kd_material', 'kd_supplier'])->get();

        $nama = 'laporan-pembelian-material';
        $namasupplier = '';
        if ($request->supplier != 'all') {
            $supplier = Supplier::where('kd_supplier', $request->supplier)->first();
            $nama = $nama.'-'.str_slug($supplier->nama_supplier);
            $namasupplier = $supplier->nama_supplier.' ';
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $datapembelian, $namasupplier) {
            $excel->setTitle('Laporan Pembelian Material '.$namasupplier.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Pembelian';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $datapembelian, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Pembelian Material');
                $sheet->mergeCells('A2:G2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:G3');

                $row = 5;
                $sheet->SetCellValue('A'.$row, 'Tanggal');
                $sheet->SetCellValue('B'.$row, 'Kode');
                $sheet->SetCellValue('C'.$row, 'Name');
                $sheet->SetCellValue('D'.$row, 'Material Type');
                $sheet->SetCellValue('E'.$row, 'Supplier');
                $sheet->SetCellValue('F'.$row, 'Qty');
                $sheet->SetCellValue('G'.$row, 'Price');
                $sheet->getStyle('F'.$row.':G'.$row)->applyFromArray($stylecenter);
                $row++;

                $indexData = $row;
                $totalQty = 0; $totalPrice = 0;
                foreach ($datapembelian as $dpm) {
                    $sheet->SetCellValue('A'.$row, $dpm->tanggal);
                    $sheet->SetCellValue('B'.$row, $dpm->kd_material);
                    $sheet->SetCellValue('C'.$row, $dpm->name);
                    $sheet->SetCellValue('D'.$row, $dpm->human_type);
                    $sheet->SetCellValue('E'.$row, $dpm->purchasing->supplier->nama_supplier);
                    $sheet->SetCellValue('F'.$row, $dpm->tglQty);
                    $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($dpm->price, 2, "," ,"."));

                    $totalQty = $totalQty + $dpm->tglQty;
                    $totalPrice = $totalPrice + $dpm->price;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':E'.$row);
                $sheet->SetCellValue('F'.$row, $totalQty);
                $sheet->SetCellValue('G'.$row, 'Rp. '.number_format($totalPrice, 2, "," ,"."));

                $sheet->getStyle('G'.$indexData.':G'.$row)->applyFromArray($styleright);

                $sheet->getStyle('A5'.':G'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':G3')->applyFromArray($stylecenter);
                $sheet->getStyle('F5'.':F'.$row)->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
