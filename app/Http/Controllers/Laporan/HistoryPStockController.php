<?php

namespace App\Http\Controllers\Laporan;

use App\Models\DetailSales;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryPStockController extends Controller
{
    public function index() {
        $location = Location::all();
        $listLocation = [];
        foreach ($location as $lc) {
            $listLocation[$lc->kd_location] = $lc->location_name.' - '.$lc->human_type;
        }
        return view('toko.laporan.history-penjualan-stock.index', compact('listLocation'));
    }

    public function cetakPenjualan(Request $request, $start, $end) {
        $penjualan = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
                                        ->whereBetween(\DB::raw('LEFT(tb_sales.tgl, 10)'), [$start, $end]);

        if ($request->has('location')) {
            if ($request->location != 'all') {
                $penjualan = $penjualan->where('tb_sales.kd_location', $request->location);
                $location = Location::where('kd_location', $request->location)->first();
            } else {
                $location = null;
            }
        } else {
            $penjualan = $penjualan->where('tb_sales.kd_location', auth()->user()->kd_location);
            $location = Location::where('kd_location', auth()->user()->kd_location)->first();
        }

        $penjualan = $penjualan->selectRaw('tb_sales.no_faktur, tb_sales.tgl, tb_sales.nama_pelanggan, tb_detail_sales.kd_produk, tb_detail_sales.no_seri, tb_detail_sales.qty, tb_detail_sales.price,tb_location.location_name,tb_detail_sales.price_type, (tb_detail_sales.qty*tb_detail_sales.disc_nominal) as totalDisc, tb_detail_sales.subtotal')->get();

        return view('toko.laporan.history-penjualan-stock.cetakpenjualan', compact('penjualan', 'location', 'start', 'end'));
    }
}
