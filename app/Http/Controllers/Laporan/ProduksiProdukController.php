<?php

namespace App\Http\Controllers\Laporan;

use App\Models\DetailProduction;
use App\Models\HistoryProductionProgress;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProduksiProdukController extends Controller
{
    public function index() {
        return view('toko.laporan.produksi-produk.index');
    }

    public function getdata(Request $request) {
        $queryproduksi = HistoryProductionProgress::join('tb_production', 'tb_history_production_progress.kd_produksi', '=', 'tb_production.kd_produksi')
                                        ->whereBetween(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), [$request->tgl_awal, $request->tgl_akhir])
                                        ->whereIn('tb_production.status', ['on_progress', 'finish']);

        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('tb_production.kd_produsen', $request->produsen);
        }

        $dataproduksi = $queryproduksi->selectRaw('LEFT(tb_history_production_progress.created_at, 10) as tanggal, tb_history_production_progress.deskripsi,  sum(tb_history_production_progress.qty_progress) as totalQty, tb_history_production_progress.kd_produksi, tb_production.kd_produsen')
                                    ->groupBy(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), 'tb_history_production_progress.deskripsi', 'tb_production.kd_produsen')->get();
        return view('toko.laporan.produksi-produk.dataproduksi', compact('dataproduksi'))->render();
    }

    public function cetakProduksi(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $start;

        $queryproduksi = HistoryProductionProgress::join('tb_production', 'tb_history_production_progress.kd_produksi', '=', 'tb_production.kd_produksi')
            ->whereBetween(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), [$start, $end])
            ->whereIn('tb_production.status', ['on_progress', 'finish']);

        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('tb_production.kd_produsen', $request->produsen);
        }

        $data['dataproduksi'] = $queryproduksi->selectRaw('LEFT(tb_history_production_progress.created_at, 10) as tanggal, tb_history_production_progress.deskripsi,  sum(tb_history_production_progress.qty_progress) as totalQty, tb_history_production_progress.kd_produksi, tb_production.kd_produsen')
                                                ->groupBy(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), 'tb_history_production_progress.deskripsi', 'tb_production.kd_produsen')->get();

        return view('toko.laporan.produksi-produk.cetakproduksi', $data);
    }

    public function exportProduksi(Request $request, $start, $end) {
        $queryproduksi = HistoryProductionProgress::join('tb_production', 'tb_history_production_progress.kd_produksi', '=', 'tb_production.kd_produksi')
            ->whereBetween(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), [$start, $end])
            ->whereIn('tb_production.status', ['on_progress', 'finish']);

        if ($request->produsen != 'all') {
            $queryproduksi = $queryproduksi->where('tb_production.kd_produsen', $request->produsen);
        }

        $dataproduksi = $queryproduksi->selectRaw('LEFT(tb_history_production_progress.created_at, 10) as tanggal, tb_history_production_progress.deskripsi,  sum(tb_history_production_progress.qty_progress) as totalQty, tb_history_production_progress.kd_produksi, tb_production.kd_produsen')
                                    ->groupBy(\DB::raw('LEFT(tb_history_production_progress.created_at, 10)'), 'tb_history_production_progress.deskripsi', 'tb_production.kd_produsen')->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'laporan-produksi-produk';
        $namaprodusen = '';
        if ($request->produsen != 'all') {
            $produsen = Produsen::where('kd_produsen', $request->produsen)->first();
            $nama = $nama.'-'.str_slug($produsen->nama_produsen);
            $namaprodusen = $produsen->nama_produsen.' ';
        }

        $nama = $nama.'-'.date('Ymd', strtotime($start)).'-'.date('Ymd', strtotime($end));

        \Excel::create($nama, function($excel) use ($nama, $start, $end, $dataproduksi, $namaprodusen) {
            $excel->setTitle('Laporan Produksi Produk '.$namaprodusen.date('d F Y', strtotime($start)).' - '.date('d F Y', strtotime($end)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Laporan Produksi';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($start, $end, $dataproduksi, $stylecenter, $stylevcenter, $styledef, $styleallbd) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Produksi Produk');
                $sheet->mergeCells('A2:D2');
                $sheet->SetCellValue('A3', $start. ' S/D '.$end);
                $sheet->mergeCells('A3:D3');

                $row = 5;
                $sheet->SetCellValue('A'.$row, 'Tanggal');
                $sheet->SetCellValue('B'.$row, 'Desc');
                $sheet->SetCellValue('C'.$row, 'Qty');
                $sheet->SetCellValue('D'.$row, 'Vendor');
                $row++;

                $totalQty = 0;
                foreach ($dataproduksi as $dpd) {
                    $sheet->SetCellValue('A'.$row, $dpd->tanggal);
                    $sheet->SetCellValue('B'.$row, $dpd->deskripsi);
                    $sheet->SetCellValue('C'.$row, $dpd->totalQty);
                    $sheet->SetCellValue('D'.$row, $dpd->production ? ($dpd->production->produsen ? $dpd->production->produsen->nama_produsen : '') : '');

                    $totalQty = $totalQty + $dpd->totalQty;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':B'.$row);
                $sheet->SetCellValue('C'.$row, $totalQty);

                $sheet->getStyle('A5'.':D'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':D3')->applyFromArray($stylecenter);
                $sheet->getStyle('C5'.':C'.$row)->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
