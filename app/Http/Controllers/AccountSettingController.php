<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AccountSettingController extends Controller
{
    public function myprofile() {
        $user = \Auth::user();
        return view('accountsetting.myprofile', compact('user'));
    }

    public function updateprofile(Request $request) {
        $user =\Auth::user();
        $datauser = $request->only('name', 'username', 'email');
        if ($request->hasFile('imgprofile')) {
            if ($user->img_profile != '') {
                User::deleteAttachment($user->img_profile);
            }
            $datauser['img_profile'] = User::saveAttachment($request->file('imgprofile'), $request->name);
        }
        $user->update($datauser);

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Profile updated']);
        return redirect()->route('myprofile.index');
    }

    public function changepassword() {
        return view('accountsetting.edit-password');
    }

    public function updatepassword(Request $request) {
        $this->validate($request, [
            'current_password' => 'required|min:8',
            'password' => 'required|min:8|confirmed',
        ]);

        $user = User::findOrFail(\Auth::user()->id);

        if (\Hash::check($request->current_password, $user->password)) {
            $user->password = \Hash::make($request->password);
            $user->save();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The password has been changed.']);
            \Auth::logout();
            return redirect('/');
        }

        \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => 'Your current password is not compatible with our database.']);
        return back();
    }
}
