<?php

namespace App\Http\Controllers;

use App\Mail\BroadcastCustomer;
use App\Models\DetailSales;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Piutang;

class DashboardController extends Controller
{
    function index() {
        if (\Gate::allows('as_accounting')) {
            $mlayout = 'layouts.master-topnav';
        } elseif (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        return view('dashboard.index', compact('mlayout'));
    }

    public function getData(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        // dd($to);
        //dd($location);

        // if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
        //     $sales = Sales::where(function ($query) use ($from, $to) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
        // } else {
        //     $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        // }
        
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesCat = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesTSC = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesTot = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesCat = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesTSC = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
                $salesTot = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
           
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
            $salesCat = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
            $salesTSC = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
            $salesTot = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        
        
        
        
        
        $totalsales = $sales->sum('grand_total');

        $netgrandtotal = $sales->selectRaw('(grand_total - komisi_nominal) as netgrandtotal')->get()->sum('netgrandtotal');
        $totalHpp = DetailSales::whereIn('no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))->selectRaw('(hpp*qty) as totalHpp')->get()->sum('totalHpp');
        $netprofit = $netgrandtotal - $totalHpp;

        //$numbertransaction = $sales->count();
        // 
        
        if($location=='all'){
            $numbertransaction = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->count();
        }else{
            $numbertransaction = Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where('tb_sales.kd_location',$location)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->count();
        }
        
        //dd($numbertransaction);
        $avgsalestransaction = $numbertransaction > 0 ? $totalsales/$numbertransaction : 0;

        // $topproductsales = DetailSales::whereIn('no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))->selectRaw('kd_produk, sum(qty) as totalQty,size_product')->groupBy('kd_produk')->groupBy('size_product')->orderBy('totalQty', 'desc')->limit(6)->get();
        // dd($topproductsales);
        
        if($location=='all'){
            $topproductsales = DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty,tb_detail_sales.size_product')->groupBy('tb_detail_sales.kd_produk')->groupBy('tb_detail_sales.size_product')->orderBy('totalQty', 'desc')->limit(5)->get();
        }else{
            $topproductsales = DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where('tb_sales.kd_location',$location)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty,tb_detail_sales.size_product')->groupBy('tb_detail_sales.kd_produk')->groupBy('tb_detail_sales.size_product')->orderBy('totalQty', 'desc')->limit(5)->get();
        }
        
        $allPiutang=Piutang::get()->sum('sisa_piutang');
        //dd($allPiutang);
        
        // $dailysales = $sales->selectRaw('LEFT(tgl, 10) as tanggal, sum(grand_total) as totalSales')->groupBy('tanggal')->get();
        //$dailysales = $salesTot->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->selectRaw('LEFT(tgl, 10) as tanggal,sum(grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->groupBy('tb_location.kd_location')->limit(5)->get();
        $dailysales=Sales::join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->selectRaw('LEFT(tgl, 10) as tanggal,sum(grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->groupBy('tb_location.kd_location')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->limit(5)->get();
        $labelSales = '';
        $ammountSales = '';
        $labelSalesTemp = '';
        $ammountSalesTemp = '';
        foreach ($dailysales as $ds) {
            //$ammountSalesTempFirst=$ds->totalSales;
            //dd($ammountSalesTempFirst);
            if ($ds == $dailysales->last()) {
                $labelSales .= '"'.$ds->location_name.'"';
                $ammountSales .= ($ds->totalSales/$totalsales)*100;
            } else {
                $labelSales .= '"'.$ds->location_name.'",';
                $ammountSales .= (($ds->totalSales/$totalsales)*100).',';
            }   
        }
        //dd($ammountSales);

        //$dailysaleslist = $sales->selectRaw('LEFT(tgl, 10) as tanggal, sum(grand_total) as totalSales,tb_location.kd_location as kdLocation,tb_location.location_name as locationName')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->groupBy('tb_location.kd_location')->get();
        // dd($salesTot->get());
        // dd($dailysales);
        
        
        if($location=='all'){
            $topcategorysales = DetailSales::join('tb_product', 'tb_detail_sales.kd_produk', '=', 'tb_product.kd_produk')
            ->join('tb_kat', 'tb_product.kd_kat', '=', 'tb_kat.kd_kat')->join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
            ->where('tb_kat.type_kat', 'produk')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)
            ->selectRaw('tb_kat.kd_kat, tb_kat.kategori, sum(tb_detail_sales.qty) as totalQty')
            ->groupBy('tb_kat.kd_kat')->orderBy('totalQty', 'desc')->orderBy('tb_detail_sales.kd_produk','asc')
            ->get();
        }else{
            $topcategorysales = DetailSales::join('tb_product', 'tb_detail_sales.kd_produk', '=', 'tb_product.kd_produk')
            ->join('tb_kat', 'tb_product.kd_kat', '=', 'tb_kat.kd_kat')->join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')
            ->where('tb_kat.type_kat', 'produk')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->where('tb_sales.kd_location',$location)
            ->selectRaw('tb_kat.kd_kat, tb_kat.kategori, sum(tb_detail_sales.qty) as totalQty')
            ->groupBy('tb_kat.kd_kat')->orderBy('totalQty', 'desc')->orderBy('tb_detail_sales.kd_produk','asc')
            ->get();
        }    
        
        
                                        
        //$totalQtyTerjual=DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->whereBetween(\DB::raw('LEFT(tb_sales.tgl, 10)'),[$from, $to])->get();
                                                                       
        if($location=='all'){
            $totalQtyTerjual=DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->get()->sum('qty');
        }else{
            $totalQtyTerjual=DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where('tb_sales.kd_location',$location)->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->get()->sum('qty');
        }                                                                                                   
                                                                       
        //dd($totalQtyTerjual);
        //$totalQtyTerjual=100;
        

        $labelCategory = '';
        $ammountCategory = '';
        foreach ($topcategorysales as $tcs) {
            if ($tcs == $topcategorysales->last()) {
                $labelCategory .= '"'.$tcs->kategori.'"';
                $ammountCategory .= $tcs->totalQty;
            } else {
                $labelCategory .= '"'.$tcs->kategori.'",';
                $ammountCategory .= $tcs->totalQty.',';
            }
        }
        
        //dd($topcategorysales);
        
        
        // $sales1 = $sales;
        // $sales2 = $sales;
        
        $sales1 = $salesTSC;
        $sales2 = $sales;
        
       
       $topshopingcustomers = $sales1
       ->selectRaw('nama_pelanggan, sum(grand_total) as totalShoping')
       ->groupBy('nama_pelanggan')
       ->orderBy('totalShoping', 'desc')
       ->limit(5)
       ->get();
       
       $topsalesperson_raw = $sales2
       ->where('salesuser_id', '!=', '')
       ->selectRaw('salesuser_id, sum(grand_total) as totalSales')
       ->groupBy('salesuser_id')
       ->orderBy('totalSales', 'desc')
       ->limit(5)
       ->get();
       
       
       
       $topsales=Sales::leftjoin('tb_user','tb_sales.salesuser_id','=','tb_user.id')->leftjoin('tb_location','tb_user.kd_location','=','tb_location.kd_location')->selectRaw('tb_user.name,sum(tb_sales.grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->groupBy('tb_sales.salesuser_id')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->limit(5)->get();
       //dd($tes);
       
       $topsalesperson = [];
       $salesuser_id_arr = [];
       foreach($topsalesperson_raw as $key=>$row) {
       $salesuser_id_arr[] = $row->salesuser_id;
        $topsalesperson[$key] = $row;
       }
       array_unique($salesuser_id_arr);
       $salesuser_total = [];
       foreach($salesuser_id_arr as $salesuser_id) {
        $total = 0;
        $name = '';
        foreach($topsalesperson_raw as $row) {
            if($row->salesuser_id == $salesuser_id) {
                $total += $row->totalSales;
                $name = $row->sales_user ? $row->sales_user->name : '';
            }
        }
    
        $salesuser_total[$salesuser_id] = [
            'name'=>$name,
            'total'=>$total
        ];
        
       }
       
       //dd($salesuser_total);
       
        
        //dd($topshopingcustomers);
        
        // dd($topsalesperson);

        return view('dashboard._data', compact('location','totalsales', 'netprofit', 'topproductsales', 'from', 'to','totalsales','dailysales' ,'labelSales', 'ammountSales', 'topshopingcustomers', 'labelCategory', 'ammountCategory', 'numbertransaction', 'avgsalestransaction', 'topsalesperson','salesuser_total','allPiutang','totalQtyTerjual','topsales'))->render();
    }

    public function viewTopPrd(Request $request) {
        $from = '';
        $to = '';
        $location='';
        //dd($request);

        if ($request->from != '') {
            $from = $request->from;
        }

        if ($request->to != '') {
            $to = $request->to;
        }
        if ($request->location != '') {
            $location = $request->location;
        }
        //dd($location);
        return view('dashboard.view-top-produk', compact('from', 'to','location'));
    }

    public function getDataTopPrd(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        //dd($request);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        
        
        //$topproductsales = DetailSales::whereIn('no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))->selectRaw('kd_produk, sum(qty) as totalQty,size_product')->groupBy('kd_produk')->groupBy('size_product')->orderBy('totalQty', 'desc')->get();
        // 
        // ->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)
        if($location=='all'){
            $topproductsales = DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty,tb_detail_sales.size_product')->groupBy('tb_detail_sales.kd_produk')->groupBy('tb_detail_sales.size_product')->orderBy('totalQty', 'desc')->get();
        }else{
            $topproductsales = DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where('tb_sales.kd_location',$location)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tb_sales.tgl, 10)'),'<=',$to)->selectRaw('tb_detail_sales.kd_produk, sum(tb_detail_sales.qty) as totalQty,tb_detail_sales.size_product')->groupBy('tb_detail_sales.kd_produk')->groupBy('tb_detail_sales.size_product')->orderBy('totalQty', 'desc')->get();
        }
        
        // dd($topproductsales);
        return view('dashboard._data-view-top-produk', compact('topproductsales'));
    }

    public function viewAllCst(Request $request) {
        $from = '';
        $to = '';
        $location=$request->location;
        
        

        if ($request->from != '') {
            $from = $request->from;
        }

        if ($request->to != '') {
            $to = $request->to;
        }
        
        if ($request->location != '') {
            $location = $request->location;
        }
        
        return view('dashboard.view-all-customer', compact('from', 'to','location'));
    }

    public function getAllCst(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        
        //dd($request);
        
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        $topshopingcustomers = $sales->selectRaw('nama_pelanggan, max(email) as email, sum(grand_total) as totalShoping')->groupBy('nama_pelanggan')->orderBy('totalShoping', 'desc')->get();
        return view('dashboard._data-view-all-customer', compact('topshopingcustomers'));
    }

    public function broadcastCst(Request $request) {
        $listcustomer = $request->listcustomer;
        $namapelanggan = $request->namapelanggan;
        $subject = $request->subject;
        $message = $request->message;

        foreach ($listcustomer as $index => $email) {
            if ($email != '') {
                Mail::to($email)->queue(new BroadcastCustomer($namapelanggan[$index], $subject, $message));
            }
        }
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Broadcast email has been sent to the customer.']);
        return back();
    }

    public function viewAllCat(Request $request) {
        $from = '';
        $to = '';
        $location=$request->location;
        
        //dd($request);

        if ($request->from != '') {
            $from = $request->from;
        }

        if ($request->to != '') {
            $to = $request->to;
        }
        
        if ($request->location != '') {
            $location = $request->location;
        }
        return view('dashboard.view-all-category', compact('from', 'to','location'));
    }

    public function getAllCat(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        //dd($request);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        $topcategorysales = DetailSales::join('tb_product', 'tb_detail_sales.kd_produk', '=', 'tb_product.kd_produk')
            ->join('tb_kat', 'tb_product.kd_kat', '=', 'tb_kat.kd_kat')
            ->whereIn('tb_detail_sales.no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))
            ->where('tb_kat.type_kat', 'produk')
            ->selectRaw('tb_kat.kd_kat, tb_kat.kategori, sum(tb_detail_sales.qty) as totalQty')
            ->groupBy('tb_kat.kd_kat')->orderBy('totalQty', 'desc')
            ->get();
        //dd($topcategorysales);
        
        return view('dashboard._data-view-all-category', compact('topcategorysales'));
    }

    public function viewAllSales(Request $request) {
        $from = '';
        $to = '';
        $location='';

        if ($request->from != '') {
            $from = $request->from;
        }

        if ($request->to != '') {
            $to = $request->to;
        }
        
        if ($request->location != '') {
            $location = $request->location;
        }
        return view('dashboard.view-all-sales', compact('from', 'to','location'));
    }

    public function getAllSales(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        //$topsalesusers = $sales->where('salesuser_id', '!=', '')->selectRaw('salesuser_id, sum(grand_total) as totalSales')->groupBy('salesuser_id')->orderBy('totalSales', 'desc')->get();
        // $topsalesusers=Sales::leftjoin('tb_user','tb_sales.salesuser_id','=','tb_user.id')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->selectRaw('tb_user.name,sum(tb_sales.grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->groupBy('tb_sales.salesuser_id')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->limit(5)->get();
        $topsales=Sales::leftjoin('tb_user','tb_sales.salesuser_id','=','tb_user.id')->leftjoin('tb_location','tb_user.kd_location','=','tb_location.kd_location')->selectRaw('tb_user.name,sum(tb_sales.grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->groupBy('tb_sales.salesuser_id')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->limit(5)->get();
        //dd($topsalesusers);
        return view('dashboard._data-view-all-sales', compact('topsales'));
    }
    
    public function viewAllStoreContribution(Request $request) {
        $from = '';
        $to = '';
        $location='';

        if ($request->from != '') {
            $from = $request->from;
        }

        if ($request->to != '') {
            $to = $request->to;
        }
        
        if ($request->location != '') {
            $location = $request->location;
        }
        
        return view('dashboard.view-all-store-contribution', compact('from', 'to','location'));
    }
    
    public function getAllStoreContribution(Request $request) {
        $from = $request->tglawal;
        $to = $request->tglakhir;
        $location=$request->location;
        
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            if($location=='all'){
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }else{
                $sales = Sales::where(function ($query) use ($from, $to,$location) { $query->where('kd_location', $location)->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); })->orWhere(function ($query) use ($from, $to) { $query->where('type_sales', 'konsinyasi')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]); });
            }
            
        } else {
            $sales = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->whereBetween(\DB::raw('LEFT(tgl, 10)'), [$from, $to]);
        }
        $totalsales = $sales->sum('grand_total');
        // $dailysales = $sales->selectRaw('LEFT(tgl, 10) as tanggal, sum(grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->groupBy('tb_location.kd_location')->orderBy('totalSales','desc')->get();
        //whereIn('tb_detail_sales.no_faktur', $sales->select('no_faktur')->pluck('no_faktur'))
        
        //$dailysales = $sales->selectRaw('LEFT(tgl, 10) as tanggal, sum(grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->groupBy('tb_location.kd_location')->orderBy('totalSales','desc')->get();
        $dailysales =Sales::selectRaw('LEFT(tgl, 10) as tanggal, sum(grand_total) as totalSales,tb_location.kd_location,tb_location.location_name')->join('tb_location','tb_sales.kd_location','=','tb_location.kd_location')->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->groupBy('tb_location.kd_location')->orderBy('totalSales','desc')->get();
        
        //dd($dailysales);
        $i=0;
        foreach($dailysales as $item){
            $queryTotalQty=DetailSales::join('tb_sales','tb_detail_sales.no_faktur','=','tb_sales.no_faktur')->selectRaw('sum(tb_detail_sales.qty) as totalQty')->where('tb_sales.kd_location','=',$item->kd_location)->where(\DB::raw('LEFT(tgl, 10)'),'>=',$from)->where(\DB::raw('LEFT(tgl, 10)'),'<=',$to)->get();
            foreach($queryTotalQty as $tot){
                $dailysales[$i]->totalQty=$tot->totalQty;
            }
           $i++;
        }
        //$topsalesusers = $sales->where('salesuser_id', '!=', '')->selectRaw('salesuser_id, sum(grand_total) as totalSales')->groupBy('salesuser_id')->orderBy('totalSales', 'desc')->get();
        // dd($dailysales);
        return view('dashboard._data-view-kontribusi-store', compact('dailysales','totalsales'));
    }
    
}
