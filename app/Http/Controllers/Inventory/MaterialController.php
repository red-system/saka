<?php

namespace App\Http\Controllers\Inventory;

use App\Helpers\Konversi;
use App\Models\GoodsFlow;
use App\Models\KomposisiProduk;
use App\Models\Material;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::get();
        return view('toko.inventory.material.index', compact('materials'));
    }

    public function detailMaterial(Request $request) {
        $material = Material::where('kd_material', $request->kdMaterial)->first();
        $material['harga'] = \Konversi::database_to_localcurrency($material->harga);
        return ['success' => !empty($material) ? true : false, 'material' => $material];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_material' => 'required',
            'name' => 'required',
            'kd_kat' => 'required|exists:tb_kat,kd_kat',
            'type_material' => 'required|in:'.implode(',', Material::allowedType()),
            'minimal_stock' => 'required|numeric|min:0',
            'harga' => 'required',
            'qty' => 'required|numeric|min:0',
            'kd_satuan' => 'required|exists:tb_satuan,kd_satuan',
            'kd_supplier' => 'required|exists:tb_supplier,kd_supplier'
        ]);

        \DB::beginTransaction();
        try {
            $material = Material::where('kd_material', $request->kd_material)->first();
            $harga = Konversi::localcurrency_to_database($request->harga);

            if (!empty($material)) {
                $laststock = $material->qty + $request->qty;
                $material->update(['minimal_stock' => $request->minimal_stock, 'qty' => $laststock, 'harga' => ($harga > $material->harga) ? $harga : $material->harga]);

                $dtgflow = [];
                $dtgflow['tgl'] = date('Y-m-d H:i:s');
                $dtgflow['kode'] = $material->kd_material;
                $dtgflow['nama'] = $material->name;
                $dtgflow['qty_in'] = $request->qty;
                $dtgflow['satuan_in'] = $material->satuan->satuan;
                $dtgflow['qty_out'] = 0;
                $dtgflow['satuan_out'] = $material->satuan->satuan;
                $dtgflow['last_stock'] = $laststock;
                $dtgflow['keterangan'] = 'Material In';
                $dtgflow['remark'] = 'Add Material';
                $dtgflow['kd_location'] = auth()->user()->kd_location;
                $dtgflow['type_flow'] = 'material';
                GoodsFlow::create($dtgflow);

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material updated successfully']);
            } else {
                $data = $request->all();
                $data['harga'] = $harga;
                $newmaterial = Material::create($data);

                $laststock = $newmaterial->qty;
                $dtgflow = [];
                $dtgflow['tgl'] = date('Y-m-d H:i:s');
                $dtgflow['kode'] = $newmaterial->kd_material;
                $dtgflow['nama'] = $newmaterial->name;
                $dtgflow['qty_in'] = $newmaterial->qty;
                $dtgflow['satuan_in'] = $newmaterial->satuan->satuan;
                $dtgflow['qty_out'] = 0;
                $dtgflow['satuan_out'] = $newmaterial->satuan->satuan;
                $dtgflow['last_stock'] = $laststock;
                $dtgflow['keterangan'] = 'Material In';
                $dtgflow['remark'] = 'Add Material';
                $dtgflow['kd_location'] = auth()->user()->kd_location;
                $dtgflow['type_flow'] = 'material';
                GoodsFlow::create($dtgflow);

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material added successfully.']);
            }

            \DB::commit();

            return redirect()->route('material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$material = Material::where('id', $id)->firstOrFail();
        return view('toko.inventory.material.edit', compact('material'))->render();*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$material = Material::where('id', $id)->firstOrFail();
        $data = $request->all();
        $material->update($data);

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material berhasil diupdate.']);
        return redirect()->route('material.index');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::where('id', $id)->firstOrFail();
        $cekkp = KomposisiProduk::where('kd_material', $material->kd_material)->count();
        if ($cekkp > 0) {
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => 'Material failed to be deleted, please check material data on product composition']);
        } else {
            \DB::beginTransaction();
            try {
                $material->delete();

                \DB::commit();

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material was deleted.']);
            } catch (\Exception $e) {
                \DB::rollback();
                \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
                return back()->withInput();
            }
        }
        return redirect()->route('material.index');
    }
}
