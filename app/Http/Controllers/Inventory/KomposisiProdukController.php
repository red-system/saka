<?php

namespace App\Http\Controllers\Inventory;

use App\Models\KomposisiProduk;
use App\Models\Material;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KomposisiProdukController extends Controller
{
    public function index($prd_id)
    {
        $produk = Product::where('id', $prd_id)->firstOrFail();
        $komposisiproduks = $produk->komposisi_product;
        return view('toko.inventory.produk.composition.index', compact('produk', 'komposisiproduks'));
    }

    public function getmaterial(Request $request) {
        $material = Material::where('kd_material', $request->kd_material)->firstOrFail();
        $data = ['satuan' => $material->satuan->satuan, 'name' => $material->name];
        return $data;
    }

    public function store(Request $request, $prd_id)
    {
        $produk = Product::where('id', $prd_id)->firstOrFail();
        $this->validate($request, [
            'kd_material' => 'required|exists:tb_material,kd_material',
            'qty' => 'required|numeric'
        ]);
        $material = Material::where('kd_material', $request->kd_material)->first();
        $data = $request->only('kd_material', 'qty');
        $data['kd_produk'] = $produk->kd_produk;
        $data['kd_satuan'] = $material->kd_satuan;
        KomposisiProduk::create($data);

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The composition of the product was saved.']);
        return redirect()->route('komposisi-produk.index', $produk->id);
    }

    public function edit($id)
    {
        $komposisi = KomposisiProduk::where('id', $id)->firstOrFail();
        return view('toko.inventory.produk.composition.edit', compact('komposisi'));
    }

    public function update(Request $request, $id)
    {
        $komposisi = KomposisiProduk::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_material' => 'required|exists:tb_material,kd_material',
            'qty' => 'required|numeric'
        ]);
        $material = Material::where('kd_material', $request->kd_material)->first();
        $data = $request->only('kd_material', 'qty');
        $data['kd_satuan'] = $material->kd_satuan;
        $komposisi->update($data);

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product composition updated successfully.']);
        return redirect()->route('komposisi-produk.index', $komposisi->product->id);
    }

    public function destroy($id)
    {
        $komposisi = KomposisiProduk::where('id', $id)->firstOrFail();
        $prdid = $komposisi->product->id;
        $komposisi->delete();

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The product composition was successfully deleted.']);
        return redirect()->route('komposisi-produk.index', $prdid);
    }
}
