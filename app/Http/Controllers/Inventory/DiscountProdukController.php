<?php

namespace App\Http\Controllers\Inventory;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountProdukController extends Controller
{
    public function index() {
        $dataproduk = Product::get();
        return view('toko.inventory.produk.diskon.index', compact('dataproduk'));
    }

    public function setDiskon(Request $request) {
        $this->validate($request, ['discount' => 'required|min:0|max:100']);
        if (count($request->listproduk) > 0) {
            foreach ($request->listproduk as $rpd) {
                $produk = Product::findOrFail($rpd);
                $produk->update(['disc_persen' => intval($request->discount), 'disc_nominal' => (intval($request->discount)*$produk->price)/100]);
            }
            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Discount settings have been saved']);
        } else {
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => 'Please check the product data first.']);
        }
        return redirect()->route('diskon-produk.index');
    }
}
