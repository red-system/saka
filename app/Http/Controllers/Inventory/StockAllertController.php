<?php

namespace App\Http\Controllers\Inventory;

use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockAllertController extends Controller
{
    public function index() {
        $querystocks = Stock::leftJoin('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk');
        if (\Gate::denies('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $stockallerts = $querystocks->selectRaw('tb_stock.kd_produk, tb_product.deskripsi, tb_stock.kd_location, tb_product.minimal_stock, sum(qty) as totalQty, (SELECT SUM(qty) AS prdQty FROM tb_stock WHERE tb_stock.kd_produk = tb_product.kd_produk) AS globalStock')
                                    ->groupBy('tb_stock.kd_produk', 'tb_stock.kd_location')->get();
        return view('toko.inventory.stockallert.index', compact('stockallerts'));
    }

    public function createCosting(Request $request) {
        $reqProduk = $request->listproduk;
        $reqKebutuhan = $request->kebutuhan;

        $listproduk = '';
        foreach ($reqProduk as $index => $val) {
            $listproduk .= $val.':'.$reqKebutuhan[$index].';';
        }
        $listproduk = rtrim($listproduk, ';');
        return redirect('production/costing/create?listproduk='.$listproduk);
    }
}
