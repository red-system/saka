<?php

namespace App\Http\Controllers\Inventory;

use App\Models\Location;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StokProdukLocationController extends Controller
{

    public function index() {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        return view('toko.inventory.produk.stock-bylocation.index', compact('mlayout'));
    }
    
    

    public function getStock(Request $request) {
        $location=$request->location;
        //$dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk')->leftjoin('tb_kat','tb_product.kd_kat','=','tb_kat.kd_kat')->where('kd_location', $request->location);
        
        if($location=='all'){
            $dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk')->leftjoin('tb_kat','tb_product.kd_kat','=','tb_kat.kd_kat');
        }else{
            $dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk')->leftjoin('tb_kat','tb_product.kd_kat','=','tb_kat.kd_kat')->where('kd_location', $request->location);
        }
        
        if ($request->search != '') {
            $dataStock = $dataStock->where(function ($query) use ($request) {
                $query->where('tb_stock.kd_produk', 'like', '%'.$request->search.'%')->orWhere('tb_product.deskripsi', 'like', '%'.$request->search.'%');
            });
        }
        $dataStock = $dataStock->selectRaw('tb_stock.kd_produk,tb_kat.kategori, tb_product.deskripsi, tb_stock.kd_location, sum(qty) as totalQty, sum(qty*hpp) as totalPrdValue')->groupBy('tb_stock.kd_produk')->get();
        //dd($dataStock);
        $i=0;
        foreach($dataStock as $row){
            $dataStock[$i]->globalLocation=$request->location;
            $i++;
        }
        
        //dd($dataStock);
        return view('toko.inventory.produk.stock-bylocation.datastock', compact('dataStock'));
    }

    public function detailSeri($kd_produk, $kd_location) {
        
        if($kd_location=='all'){
            $dataStock = Stock::join('tb_location','tb_stock.kd_location','=','tb_location.kd_location')->where('tb_stock.kd_produk', $kd_produk)->get();
        }
        else{
            
            $dataStock = Stock::join('tb_location','tb_stock.kd_location','=','tb_location.kd_location')->where('tb_location.kd_location', $kd_location)->where('tb_stock.kd_produk', $kd_produk)->get();
        }
        
        //dd($dataStock);
        
        return view('toko.inventory.produk.stock-bylocation.detailseri', compact('dataStock'));
    }

    public function printStock(Request $request, $kdlocation) {
        //dd($kdlocation);
        if($kdlocation=='all'){
            $data['location'] = Location::where('kd_location', $kdlocation)->first();
            $dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk');
        }else{
            $data['location'] = Location::where('kd_location', $kdlocation)->first();
            $dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk')->where('kd_location', $kdlocation);
        }
        
        if ($request->search != '') {
            $dataStock = $dataStock->where(function ($query) use ($request) {
                $query->where('tb_stock.kd_produk', 'like', '%'.$request->search.'%')->orWhere('tb_product.deskripsi', 'like', '%'.$request->search.'%');
            });
        }
        $data['datastock'] = $dataStock->selectRaw('tb_stock.kd_produk, tb_product.deskripsi, tb_stock.kd_location, sum(qty) as totalQty, sum(qty*hpp) as totalPrdValue')->groupBy('tb_stock.kd_produk')->get();
        return view('toko.inventory.produk.stock-bylocation.cetakStock', $data);
    }

    public function exportStock(Request $request, $kdlocation) {
        $location = Location::where('kd_location', $kdlocation)->first();

        $dataStock = Stock::join('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk')->where('kd_location', $kdlocation);
        if ($request->search != '') {
            $dataStock = $dataStock->where(function ($query) use ($request) {
                $query->where('tb_stock.kd_produk', 'like', '%'.$request->search.'%')->orWhere('tb_product.deskripsi', 'like', '%'.$request->search.'%');
            });
        }
        $datastock = $dataStock->selectRaw('tb_stock.kd_produk, tb_product.deskripsi, tb_stock.kd_location, sum(qty) as totalQty, sum(qty*hpp) as totalPrdValue')->groupBy('tb_stock.kd_produk')->get();

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'data-stok-'.str_slug($location->location_name).'-'.date('YmdHi');

        \Excel::create($nama, function($excel) use ($nama, $datastock, $location) {
            $excel->setTitle('Data Stock '.$location->location_name.' '.date('d F Y').' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Data Stock';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($datastock, $location, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A2', 'Data Stok '.$location->location_name);
                $sheet->mergeCells('A2:G2');

                $row = 4;
                $sheet->SetCellValue('A'.$row, 'Kode Produk');
                $sheet->SetCellValue('B'.$row, 'Desc');
                $sheet->SetCellValue('C'.$row, 'Material');
                $sheet->SetCellValue('D'.$row, 'Min Stock');
                $sheet->SetCellValue('E'.$row, 'Stock');
                $sheet->SetCellValue('F'.$row, 'Product Value');
                $row++;

                $totalQty = 0; $totalProductValue = 0;
                foreach ($datastock as $ds) {
                    $sheet->SetCellValue('A'.$row, $ds->kd_produk);
                    $sheet->SetCellValue('B'.$row, $ds->deskripsi);
                    $sheet->SetCellValue('C'.$row, $ds->product->material);
                    $sheet->SetCellValue('D'.$row, $ds->product->minimal_stock);
                    $sheet->SetCellValue('E'.$row, $ds->totalQty);
                    $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($ds->totalPrdValue, 2, "," ,"."));

                    $totalQty = $totalQty + $ds->totalQty;
                    $totalProductValue = $totalProductValue + $ds->totalPrdValue;
                    $row++;
                }

                $sheet->SetCellValue('A'.$row, 'Total');
                $sheet->mergeCells('A'.$row.':D'.$row);
                $sheet->SetCellValue('E'.$row, number_format($totalQty, 0, "." ,"."));
                $sheet->SetCellValue('F'.$row, 'Rp. '.number_format($totalProductValue, 0, "," ,"."));

                $sheet->getStyle('A4'.':F'.$row)->applyFromArray($styleallbd);
                $sheet->getStyle('A2'.':F3')->applyFromArray($stylecenter);
                $sheet->getStyle('E4'.':E'.$row)->applyFromArray($stylecenter);
                $sheet->getStyle('F4'.':F'.$row)->applyFromArray($styleright);
            });
        })->export('xlsx');
    }
}
