<?php

namespace App\Http\Controllers\Inventory;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('toko.inventory.produk.index');
    }

    public function getData() {
        $dataproduct = Product::leftJoin('tb_kat', 'tb_product.kd_kat', '=', 'tb_kat.kd_kat')
                                ->leftJoin('tb_collection', 'tb_product.collection_id', '=', 'tb_collection.id')
                                ->leftJoin('tb_stock', 'tb_product.kd_produk', '=', 'tb_stock.kd_produk')
                                ->selectRaw('tb_product.id, tb_product.kd_produk, tb_product.deskripsi, tb_product.material, tb_product.weight, tb_kat.kategori, tb_product.wholesale_price, tb_product.price, tb_collection.collection, tb_product.disc_persen, tb_product.minimal_stock, sum(tb_stock.qty) as global_stock')
                                ->groupBy('tb_product.kd_produk');

        return Datatables::of($dataproduct)
                        ->editColumn('disc_persen', function ($data) {
                            return $data->disc_persen.'%';
                        })
                        ->editColumn('global_stock', function ($data) {
                            return $this->count_glb_stock($data->kd_produk);
                        })
                        ->addColumn('action', function ($data) {
                            return view('toko.inventory.produk._action', ['produk' => $data]);
                        })
                        ->rawColumns(['action'])
                        ->make(true);

    }

    public function getKode(Request $request) {
        $prefix = '';

        if ($request->kategori != '') {
            $prefix = $prefix.$request->kategori;
        } else {
            $prefix = $prefix.'XX';
        }

        if ($request->stylenumber != '') {
            $styleNumber = Product::styleNumberList();
            $prefix = $prefix.$styleNumber[$request->stylenumber]['kode'];
        } else {
            $prefix = $prefix.'XXX';
        }

        if ($request->material != '') {
            $material = Product::materialList();
            $prefix = $prefix.$material[$request->material]['kode'];
        } else {
            $prefix = $prefix.'X';
        }

        if ($request->stonecolor != '') {
            $stoneColor = Product::stoneColorList();
            $prefix = $prefix.$stoneColor[$request->stonecolor]['kode'];
        } else {
            $prefix = $prefix.'XXX';
        }

        if ($request->size != '') {
            $size = Product::sizeList();
            $prefix = $prefix.$size[$request->size]['kode'];
        } else {
            $prefix = $prefix.'XX';
        }

        $lastdata = Product::where(\DB::raw('LEFT(kd_produk, '.strlen($prefix).')'), $prefix)->selectRaw('RIGHT(kd_produk, 3) as no_produk')->orderBy('no_produk', 'desc')->first();
        if (!empty($lastdata)) {
            $now = intval($lastdata->no_produk)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $prefix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_produk' => 'required|unique:tb_product,kd_produk',
            'deskripsi' => 'required',
            'kd_kat' => 'required|exists:tb_kat,kd_kat',
            'material' => 'required',
            'weight' => 'required|numeric|min:0',
            'price' => 'required',
            'wholesale_price' => 'required',
            'minimal_stock' => 'required|numeric|min:0',
            'collection_id' => 'required|exists:tb_collection,id'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->except('_token', 'imgproduct');
            $data['price'] = \Konversi::localcurrency_to_database($request->price);
            $data['wholesale_price'] = \Konversi::localcurrency_to_database($request->wholesale_price);
            if ($request->hasFile('imgproduct')) {
                $data['img_produk'] = Product::saveAttachment($request->file('imgproduct'), $request->kd_produk);
            }
            $product = Product::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product added successfully.']);
            return redirect()->route('produk.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Product::where('id', $id)->firstOrFail();
        return view('toko.inventory.produk.edit', compact('produk'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produk = Product::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_produk' => 'required|unique:tb_product,kd_produk,'.$produk->id,
            'deskripsi' => 'required',
            'kd_kat' => 'required|exists:tb_kat,kd_kat',
            'material' => 'required',
            'weight' => 'required|numeric|min:0',
            'price' => 'required',
            'wholesale_price' => 'required',
            'minimal_stock' => 'required|numeric|min:0',
            'collection_id' => 'required|exists:tb_collection,id'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->except('_token', 'imgproduct');
            $data['price'] = \Konversi::localcurrency_to_database($request->price);
            $data['wholesale_price'] = \Konversi::localcurrency_to_database($request->wholesale_price);
            if ($request->hasFile('imgproduct')) {
                if ($produk->img_produk != '') {
                    Product::deleteAttachment($produk->img_produk);
                }
                $data['img_produk'] = Product::saveAttachment($request->file('imgproduct'), $request->kd_produk);
            }
            $produk->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product updated successfully.']);
            return redirect()->route('produk.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Product::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            if ($produk->img_produk != '') {
                Product::deleteAttachment($produk->img_produk);
            }
            $produk->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The product was successfully deleted.']);
            return redirect()->route('produk.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function barcodePrint($id) {
        $produk = Product::where('id', $id)->firstOrFail();
        $pdf = \PDF::loadView('toko.inventory.produk.print-barcode', compact('produk'))
            ->setPaper('a5', 'potrait');
        return $pdf->stream();
        //return view('toko.inventory.produk.print-barcode', compact('produk'));
    }

    public function count_glb_stock($kd_produk){
        $glb_stock = Stock::where('kd_produk',$kd_produk);
        if(auth()->user()->kd_location!='SLC-0001'){
            $glb_stock = $glb_stock->where('kd_location',auth()->user()->kd_location);
        }

        $glb_stock = $glb_stock->sum('qty');

        return $glb_stock;
    }

    protected function cekkdproduk($kdproduk)
    {

        $produk = Product::where('kd_produk', $kdproduk)->count();

        $return = [
          'count' => $produk,
        ];

        return $return;
    }
}
