<?php

namespace App\Http\Controllers\Inventory;

use App\Models\GoodsFlow;
use App\Models\Location;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StokProdukController extends Controller
{
    public function index($prd_id)
    {
        $produk = Product::where('id', $prd_id)->firstOrFail();
        $noseri = $this->createNoSeri($produk);
        if (\Gate::denies('as_owner_or_manager_kantorpusat')) {
            $produkstocks = $produk->stock()->where('kd_location', auth()->user()->kd_location)->get();
        } else {
            $produkstocks = $produk->stock;
        }
        return view('toko.inventory.produk.stock.index', compact('produk', 'produkstocks', 'noseri'));
    }

    protected function createNoSeri($produk) {
        $preffix = $produk->kd_produk.'-';
        $last = Stock::where(\DB::raw('LEFT(no_seri_produk, '.strlen($preffix).')'), $preffix)->orderBy('id', 'desc')->first();
        if (!empty($last)) {
            $noseri = str_replace($preffix, '', $last->no_seri_produk);
            $no = intval($noseri)+1;
        } else {
            $no = '1';
        }

        return $preffix.$no;
    }

    public function getlocation(Request $request) {
        return Location::where('kd_location', $request->kd_location)->firstOrFail();
    }

    public function store(Request $request, $prd_id)
    {
        $produk = Product::where('id', $prd_id)->firstOrFail();
        $this->validate($request, [
            'no_seri_produk' => 'required|unique:tb_stock,no_seri_produk',
            'qty' => 'required|numeric',
            'size_product' => 'required|numeric',
            'hpp' => 'required|numeric'
        ]);

        if(\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $kdlocation = $request->kd_location;
        } else {
            $kdlocation = auth()->user()->kd_location;
        }

        DB::beginTransaction();
        try {
            $cekstock = Stock::where('kd_produk', $produk->kd_produk)->where('no_seri_produk', $request->no_seri_produk)->where('kd_location', $kdlocation)->first();
            if (!empty($cekstock)) {
                $laststock = $cekstock->qty + $request->qty;
                $cekstock->update(['qty' => $laststock, 'hpp' => $request->hpp]);
            } else {
                $laststock = $request->qty;
                $data = $request->only('size_product','qty', 'hpp', 'no_seri_produk');
                $data['kd_produk'] = $produk->kd_produk;
                $data['kd_location'] = $kdlocation;
                Stock::create($data);
            }

            $dtgflow = [];
            $dtgflow['tgl'] = date('Y-m-d H:i:s');
            $dtgflow['kode'] = $produk->kd_produk;
            $dtgflow['no_seri'] = $request->no_seri_produk;
            $dtgflow['nama'] = $produk->deskripsi;
            $dtgflow['qty_in'] = $request->qty;
            $dtgflow['satuan_in'] = 'pcs';
            $dtgflow['qty_out'] = 0;
            $dtgflow['satuan_out'] = 'pcs';
            $dtgflow['last_stock'] = $laststock;
            $dtgflow['keterangan'] = 'Stock In';
            $dtgflow['remark'] = 'Add Stock';
            $dtgflow['kd_location'] = $kdlocation;
            $dtgflow['type_flow'] = 'produk';
            GoodsFlow::create($dtgflow);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Stock data saved successfully.']);
            return redirect()->route('stok-produk.index', $produk->id);
        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function destroy($id)
    {
        $stock = Stock::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $prdid = $stock->product->id;
            $stock->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Stock data was successfully deleted.']);
            return redirect()->route('stok-produk.index', $prdid);
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
