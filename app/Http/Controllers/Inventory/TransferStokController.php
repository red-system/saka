<?php

namespace App\Http\Controllers\Inventory;

use App\Models\DetailTransferStock;
use App\Models\GoodsFlow;
use App\Models\HistoryTransfer;
use App\Models\Location;
use App\Models\Product;
use App\Models\Stock;
use App\Models\TransferStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TransferStokController extends Controller
{
    public function index() {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }

        $transferStock = TransferStock::where('dari', auth()->user()->kd_location)->get();
        $approvalTransfer = TransferStock::where('tujuan', auth()->user()->kd_location)->whereIn('status', ['waiting-approval', 'on_progress'])->get();
        $historytransfer = HistoryTransfer::where('kd_loc_dari', auth()->user()->kd_location)->orWhere('kd_loc_tujuan', auth()->user()->kd_location)->get();
        return view('toko.inventory.transfer-stock.index', compact('transferStock', 'approvalTransfer', 'historytransfer', 'mlayout'));
    }

    public function create() {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        return view('toko.inventory.transfer-stock.create', compact('mlayout'));
    }

    public function downloadStock() {
        $stocks = Stock::where('kd_location', auth()->user()->kd_location)->orderBy('kd_produk','asc')->get();
        $nama = 'data-stock-'.auth()->user()->kd_location.'-'.date('Ymd');
        header('Content-Type: text/xlsx; charset=utf-8');
    header("Content-Disposition: attachment; filename=$nama");

    $output = fopen('php://output', 'w');

        \Excel::create($nama, function($excel) use ($nama, $stocks) {
             
            $excel->setTitle('Data Stock '.auth()->user()->location->location_name.' - '.date('d F Y').' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'DataStock';

            $excel->sheet($sheetname1, function($sheet)  use ($stocks) {
                $sheet->SetCellValue('A1', 'kd_produk');
                $sheet->SetCellValue('B1', 'no_seri');
                $sheet->SetCellValue('C1', 'deskripsi');
                $sheet->SetCellValue('D1', 'qty_transfer');

                $row = 2;
                foreach ($stocks as $st) {
                    $sheet->SetCellValue('A'.$row, $st->kd_produk);
                    $sheet->SetCellValue('B'.$row, $st->no_seri_produk);
                    $sheet->SetCellValue('C'.$row, $st->product ? $st->product->deskripsi : '');
                    $sheet->SetCellValue('D'.$row, 0);
                    $row++;
                }
            });
        })->export('xlsx');
    }

    public function readImport(Request $request) {
        $this->validate($request, [ 'file_import' => 'required|mimes:xlsx' ]);
        $excel = $request->file('file_import');

        $excels = \Excel::selectSheetsByIndex(0)->load($excel, function($reader) {

        })->get();

        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        return view('toko.inventory.transfer-stock.create-readimport', compact('excels', 'mlayout'));
    }

    public function store(Request $request) {
        $rules = [
            'tujuan' => 'required',
            'tanggal' => 'required',
            'pelaksana' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withInput()->withErrors();
        } else {
            DB::beginTransaction();
            try {
                $dataTransfer = [];
                $dataTransfer['tanggal'] = $request->tanggal;
                $dataTransfer['dari'] = auth()->user()->kd_location;
                $dataTransfer['tujuan'] = $request->tujuan;
                $dataTransfer['pelaksana'] = $request->pelaksana;
                $dataTransfer['keterangan'] = $request->keterangan;
                $dtTrf = TransferStock::create($dataTransfer);

                $reqKdProduk = $request->kdProduk;
                $reqNoSeri = $request->noSeriProduk;
                $reqQtyTransfer = $request->qtyTransfer;
                $reqDeskripsi = $request->descProduk;

                foreach ($reqKdProduk as $index => $kdproduk) {
                    if ($reqNoSeri[$index] == '') {
                        $noseri = '';
                    } else {
                        $noseri = $reqNoSeri[$index];
                    }
                    $stock = Stock::where('kd_produk', $kdproduk)->where('no_seri_produk', $noseri)->where('kd_location', auth()->user()->kd_location)->first();
                    if (!empty($stock)) {
                        if ($reqQtyTransfer[$index] > 0) {
                            $dtlTransfer = [];
                            $dtlTransfer['transferstock_id'] = $dtTrf->id;
                            $dtlTransfer['kd_produk'] = $stock->kd_produk;
                            $dtlTransfer['no_seri'] = $stock->no_seri_produk;
                            $dtlTransfer['deskripsi'] = $stock->product ? $stock->product->deskripsi : $reqDeskripsi[$index];
                            $dtlTransfer['qty_stock'] = $stock->qty;
                            $dtlTransfer['qty_transfer'] = $reqQtyTransfer[$index];
                            DetailTransferStock::create($dtlTransfer);
                        }
                    }
                }

                DB::commit();

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Transfer stock successfully saved']);
                return redirect()->route('transfer-stok.index');
            } catch (\Exception $e) {
                DB::rollback();
                \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
                return back()->withInput();
            }
        }
    }

    public function edit($id) {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->where('status', 'draft')->firstOrFail();
        $detailTransfer = $transferstok->detail_transfer;
        return view('toko.inventory.transfer-stock.edit', compact('transferstok', 'detailTransfer', 'mlayout'));
    }

    public function update(Request $request, $id) {
        $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->where('status', 'draft')->firstOrFail();
        $rules = [
            'tujuan' => 'required',
            'tanggal' => 'required',
            'pelaksana' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withInput()->withErrors();
        } else {
            DB::beginTransaction();
            try {
                $dataTransfer = [];
                $dataTransfer['tanggal'] = $request->tanggal;
                $dataTransfer['tujuan'] = $request->tujuan;
                $dataTransfer['pelaksana'] = $request->pelaksana;
                $dataTransfer['keterangan'] = $request->keterangan;
                $transferstok->update($dataTransfer);

                $reqKdProduk = $request->kdProduk;
                $reqNoSeri = $request->noSeriProduk;
                $reqQtyTransfer = $request->qtyTransfer;
                $reqDeskripsi = $request->descProduk;

                foreach ($reqKdProduk as $index => $kdproduk) {
                    if ($reqNoSeri[$index] == '') {
                        $noseri = '';
                    } else {
                        $noseri = $reqNoSeri[$index];
                    }
                    $stock = Stock::where('kd_produk', $kdproduk)->where('no_seri_produk', $noseri)->where('kd_location', auth()->user()->kd_location)->first();
                    $detailtransfer = DetailTransferStock::where('id', $index)->where('transferstock_id', $transferstok->id)->first();
                    if (!empty($stock) && !empty($detailtransfer)) {
                        if ($reqQtyTransfer[$index] > 0) {
                            $dtlTransfer = [];
                            $dtlTransfer['kd_produk'] = $stock->kd_produk;
                            $dtlTransfer['no_seri'] = $stock->no_seri_produk;
                            $dtlTransfer['deskripsi'] = $stock->product ? $stock->product->deskripsi : $reqDeskripsi[$index];
                            $dtlTransfer['qty_stock'] = $stock->qty;
                            $dtlTransfer['qty_transfer'] = $reqQtyTransfer[$index];
                            $detailtransfer->update($dtlTransfer);
                        }
                    }
                }

                DB::commit();

                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Transfer stock successfully saved']);
                return redirect()->route('transfer-stok.index');
            } catch (\Exception $e) {
                DB::rollback();
                \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
                return back()->withInput();
            }
        }
    }

    public function show($id) {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        // $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->orWhere('tujuan', auth()->user()->kd_location)->firstOrFail();
        $transferstok = TransferStock::where('id', $id)->firstOrFail();
        //$detailTransfer = $transferstok->detail_transfer;
        $detailTransfer= DetailTransferStock::join('tb_product','tb_detail_transfer.kd_produk','=','tb_product.kd_produk')->where('tb_detail_transfer.transferstock_id', $id)->get();
        //$product=$detailTransfer->productDetailTransfer;
        //dd($detailTransfer);
        return view('toko.inventory.transfer-stock.show', compact('transferstok', 'detailTransfer', 'mlayout'))->render();
    }

    public function cetakTransfer($id) {
        $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->whereIn('status', ['waiting-approval', 'on_progress'])->firstOrFail();
        //$detailTransfer = $transferstok->detail_transfer;
        $detailTransfer= DetailTransferStock::join('tb_product','tb_detail_transfer.kd_produk','=','tb_product.kd_produk')->where('tb_detail_transfer.transferstock_id', $id)->get();
        $pdf = \PDF::loadView('toko.inventory.transfer-stock.print_pdf', compact('transferstok', 'detailTransfer'))->setPaper('a4', 'potrait');
        return $pdf->download('Transfer Stok '.$transferstok->tanggal.' '.$transferstok->dari.' to '.$transferstok->tujuan.'.pdf');
    }

    public function publish(Request $request, $id) {
        $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->where('status', 'draft')->firstOrFail();
        $transferstok->update(['status' => 'waiting-approval']);
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Transfer Stock has been changed to Waiting Approval']);
        return redirect()->route('transfer-stok.index');
    }

    public function destroy(Request $request, $id) {
        $transferstok = TransferStock::where('id', $id)->where('dari', auth()->user()->kd_location)->whereIn('status', ['draft', 'waiting-approval'])->firstOrFail();
        $transferstok->update(['status' => 'cancel']);
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Transfer stock has been canceled.']);
        return redirect()->route('transfer-stok.index');
    }

    public function detailApproval($id) {
        if (\Gate::allows('as_sales')) {
            $mlayout = 'layouts.master-pos';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $transferstok = TransferStock::where('tujuan', auth()->user()->kd_location)->where('id', $id)->whereIn('status', ['waiting-approval', 'on_progress'])->firstOrFail();
        $detailTransfer = $transferstok->detail_transfer;
        return view('toko.inventory.transfer-stock.detailapproval', compact('transferstok', 'detailTransfer', 'mlayout'));
    }

    public function storeApproval(Request $request, $id) {
        $transferstok = TransferStock::where('tujuan', auth()->user()->kd_location)->where('id', $id)->whereIn('status', ['waiting-approval', 'on_progress'])->firstOrFail();
        \DB::beginTransaction();
        try {
            $reqListTransfer = $request->listtransfer;
            $jmlApprove = 0;
            foreach ($reqListTransfer as $ltrf) {
                $dtlTrf = $transferstok->detail_transfer()->where('id', $ltrf)->first();
                if (!empty($dtlTrf)) {
                    $dtlTrf->update(['status' => 'approved']);

                    $stock = Stock::where('kd_produk', $dtlTrf->kd_produk)->where('no_seri_produk', $dtlTrf->no_seri)->where('kd_location', $dtlTrf->transferstok->dari)->first();
                    $findstockdest = Stock::where('kd_produk', $dtlTrf->kd_produk)->where('no_seri_produk', $dtlTrf->no_seri)->where('kd_location', $dtlTrf->transferstok->tujuan)->first();
                    if (!empty($findstockdest)) {
                        $laststocktujuan = $findstockdest->qty + $dtlTrf->qty_transfer;
                        $findstockdest->update(['qty' => $laststocktujuan, 'hpp' => $stock->hpp]);
                    } else {
                        $laststocktujuan = $dtlTrf->qty_transfer;
                        $dataStockTransfer = [];
                        $dataStockTransfer['kd_produk'] = $stock->kd_produk;
                        $dataStockTransfer['no_seri_produk'] = $this->createNoSeri($stock->kd_produk);
                        $dataStockTransfer['kd_location'] = $dtlTrf->transferstok->tujuan;
                        $dataStockTransfer['qty'] = $laststocktujuan;
                        $dataStockTransfer['hpp'] = $stock->hpp;
                        Stock::create($dataStockTransfer);
                    }
                    $laststocksumber = $stock->qty > $dtlTrf->qty_transfer ? ($stock->qty - $dtlTrf->qty_transfer) : 0;
                    $stock->update(['qty' => $laststocksumber]);

                    $locTujuan = Location::where('kd_location', $dtlTrf->transferstok->tujuan)->first();

                    $dtgflowsumber = [];
                    $dtgflowsumber['tgl'] = date('Y-m-d H:i:s');
                    $dtgflowsumber['kode'] = $stock->kd_produk;
                    $dtgflowsumber['no_seri'] = $stock->no_seri_produk;
                    $dtgflowsumber['nama'] = $stock->product->deskripsi;
                    $dtgflowsumber['qty_in'] = 0;
                    $dtgflowsumber['satuan_in'] = 'pcs';
                    $dtgflowsumber['qty_out'] = $dtlTrf->qty_transfer;
                    $dtgflowsumber['satuan_out'] = 'pcs';
                    $dtgflowsumber['last_stock'] = $laststocksumber;
                    $dtgflowsumber['keterangan'] = 'Stock Out';
                    $dtgflowsumber['remark'] = 'Stock Transfer to '.(!empty($locTujuan) ? $locTujuan->location_name : $dtlTrf->transferstok->tujuan);
                    $dtgflowsumber['kd_location'] = $stock->kd_location;
                    $dtgflowsumber['type_flow'] = 'produk';
                    GoodsFlow::create($dtgflowsumber);

                    $dtgflowtujuan = [];
                    $dtgflowtujuan['tgl'] = date('Y-m-d H:i:s');
                    $dtgflowtujuan['kode'] = $stock->kd_produk;
                    $dtgflowtujuan['no_seri'] = $stock->no_seri_produk;
                    $dtgflowtujuan['nama'] = $stock->product->deskripsi;
                    $dtgflowtujuan['qty_in'] = $dtlTrf->qty_transfer;
                    $dtgflowtujuan['satuan_in'] = 'pcs';
                    $dtgflowtujuan['qty_out'] = 0;
                    $dtgflowtujuan['satuan_out'] = 'pcs';
                    $dtgflowtujuan['last_stock'] = $laststocktujuan;
                    $dtgflowtujuan['keterangan'] = 'Stock In';
                    $dtgflowtujuan['remark'] = 'Stock Transfer from '.($stock->location ? $stock->location->location_name : $stock->kd_location);
                    $dtgflowtujuan['kd_location'] = $locTujuan->kd_location;
                    $dtgflowtujuan['type_flow'] = 'produk';
                    GoodsFlow::create($dtgflowtujuan);

                    $dataHistory = [];
                    $dataHistory['tgl'] = date('Y-m-d H:i:s');
                    $dataHistory['kode'] = $stock->kd_produk;
                    $dataHistory['no_seri'] = $stock->no_seri_produk;
                    $dataHistory['deskripsi'] = $stock->product->deskripsi;
                    $dataHistory['qty_stock'] = $stock->qty;
                    $dataHistory['kd_loc_dari'] = $dtlTrf->transferstok->dari;
                    $dataHistory['dari'] = $stock->location->location_name;
                    $dataHistory['pelaksana'] = $dtlTrf->transferstok->pelaksana;
                    $dataHistory['kd_loc_tujuan'] = $dtlTrf->transferstok->tujuan;
                    $dataHistory['tujuan'] = $locTujuan->location_name;
                    $dataHistory['penerima'] = auth()->user()->name;
                    $dataHistory['qty_transfer'] = $dtlTrf->qty_transfer;
                    HistoryTransfer::create($dataHistory);
                }
                $jmlApprove++;
            }

            if ($transferstok->status == 'waiting-approval' && $jmlApprove > 0) {
                $transferstok->update(['status' => 'on_progress']);
            }

            \DB::commit();

            if ($jmlApprove > 0) {
                \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => $jmlApprove.' product data approved']);
            } else {
                \Session::flash('notification', ['level' => 'error', 'title' => 'Approval Kosong', 'message' => 'There is no product data that has been approved']);
            }
            return redirect()->route('transfer-stok.index');
        } catch (\Exception $e) {
            return $e;
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function setFinish(Request $request, $id) {
        $transferstok = TransferStock::where('id', $id)->where('tujuan', auth()->user()->kd_location)->where('status', 'on_progress')->firstOrFail();
        \DB::beginTransaction();
        try {
            $transferstok->update(['status' => 'finish']);
            if (DetailTransferStock::where('transferstock_id', $transferstok->id)->where('status', 'waiting-approval')->count() > 0) {
                DetailTransferStock::where('transferstock_id', $transferstok->id)->where('status', 'waiting-approval')->update(['status' => 'rejected']);
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The stock transfer process has been completed.']);
            return redirect()->route('transfer-stok.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    protected function createNoSeri($kd_produk) {
        $preffix = $kd_produk.'-';
        $last = Stock::where(\DB::raw('LEFT(no_seri_produk, '.strlen($preffix).')'), $preffix)->orderBy('no_seri_produk', 'desc')->first();
        if (!empty($last)) {
            $noseri = str_replace($preffix, '', $last->no_seri_produk);
            $no = intval($noseri)+1;
        } else {
            $no = '1';
        }

        return $preffix.$no;
    }
}
