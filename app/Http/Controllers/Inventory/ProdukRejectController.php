<?php

namespace App\Http\Controllers\Inventory;

use App\Models\GoodsFlow;
use App\Models\ProductReject;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProdukRejectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks = Stock::has('product')->where('kd_location', auth()->user()->kd_location)->get();
        $historyReject = ProductReject::where('kd_location', auth()->user()->kd_location)->get();
        return view('toko.inventory.productreject.index', compact('stocks', 'historyReject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock = Stock::where('kd_location', auth()->user()->kd_location)->where('kd_produk', $request->kd_product)->firstOrFail();

        $this->validate($request, [
            'qty' => 'required',
            'keterangan' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $laststock = $stock->qty - $request->qty;
            $stock->update(['qty' => $laststock]);

            $dtPReject = [];
            $dtPReject['kd_location'] = $stock->kd_location;
            $dtPReject['tgl_reject_product'] = date('Y-m-d');
            $dtPReject['kd_product'] = $stock->kd_produk;
            $dtPReject['no_seri'] = $stock->no_seri_produk;
            $dtPReject['deskripsi'] = $stock->product->deskripsi;
            $dtPReject['material'] = $stock->product->material;
            $dtPReject['qty'] = $request->qty;
            $dtPReject['keterangan'] = $request->keterangan;
            ProductReject::create($dtPReject);

            $dtgflow = [];
            $dtgflow['tgl'] = date('Y-m-d H:i:s');
            $dtgflow['kode'] = $stock->kd_produk;
            $dtgflow['no_seri'] = $stock->no_seri_produk;
            $dtgflow['nama'] = $stock->product->deskripsi;
            $dtgflow['qty_in'] = 0;
            $dtgflow['satuan_in'] = 'pcs';
            $dtgflow['qty_out'] = $request->qty;
            $dtgflow['satuan_out'] = 'pcs';
            $dtgflow['last_stock'] = $laststock;
            $dtgflow['keterangan'] = 'Stock Out';
            $dtgflow['remark'] = 'Product Reject';
            $dtgflow['kd_location'] = $stock->kd_location;
            $dtgflow['type_flow'] = 'produk';
            GoodsFlow::create($dtgflow);

            if ($stock->qty <= 0) {
                $stock->delete();
            }
            
            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Reject product is successful, stock data was updated.']);
            return redirect()->route('product-reject.index');
        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
