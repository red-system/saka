<?php

namespace App\Http\Controllers\Inventory;

use App\Models\DetailSales;
use App\Models\Location;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SummaryStockOutletController extends Controller
{
    public function index() {
        return view('toko.inventory.summarystock-outlet.index');
    }

    public function getdata(Request $request) {
        $products = Product::get();
        $outlets = Location::whereIn('kd_location', $request->location)->get();
        $tglawal = $request->tgl_awal;
        $tglakhir = $request->tgl_akhir;
        $stocks = Stock::get();
        $detailsales = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')
                                    ->whereBetween('tgl', [$tglawal, $tglakhir])
                                    ->selectRaw('tb_detail_sales.*, tb_sales.kd_location, (qty - qty_retur) as saleQty')
                                    ->get();
        return view('toko.inventory.summarystock-outlet.datastok', compact('products', 'outlets', 'tglawal', 'tglakhir', 'stocks', 'detailsales'))->render();
    }

    public function exportStock(Request $request, $tglawal, $tglakhir) {

        require_once(base_path('vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataType.php'));

        $nama = 'summary-stok-outlet-'.date('YmdHi');

        \Excel::create($nama, function($excel) use ($tglawal, $tglakhir, $request) {
            $excel->setTitle('Summary Stock Outlet '.date('d F Y').' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'Data Stock';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $styleFontRed = array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => 'FF0000')
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($tglawal, $tglakhir, $request, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright, $styleFontRed) {
                $kdlocation = explode(',', $request->location);

                $products = Product::get();
                $outlets = Location::whereIn('kd_location', $kdlocation)->get();
                $stocks = Stock::get();
                $detailsales = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')
                    ->whereBetween('tgl', [$tglawal, $tglakhir])
                    ->selectRaw('tb_detail_sales.*, tb_sales.kd_location, (qty - qty_retur) as saleQty')
                    ->get();

                $sheet->getDefaultStyle()->applyFromArray($styledef);

                $sheet->SetCellValue('A1', 'Summary Stock per Outlet');


                $sheet->SetCellValue('A3', 'Kode Produk');
                $sheet->mergeCells('A3:A4');

                $sheet->SetCellValue('B3', 'Produk');
                $sheet->mergeCells('B3:B4');

                $sheet->getStyle('A3:B4')->applyFromArray($styleallbd);
                $sheet->getStyle('A3:B4')->applyFromArray($stylecenter);

                $colOutlet = 'C';
                foreach ($outlets as $outlet) {
                    $colStock = $colOutlet;
                    $sheet->SetCellValue($colOutlet.'4', 'Stok');
                    $colOutlet++;
                    $sheet->SetCellValue($colOutlet.'4', 'Sales');

                    $sheet->SetCellValue($colStock.'3', $outlet->location_name);
                    $sheet->mergeCells($colStock.'3:'.$colOutlet.'3');
                    $sheet->getStyle($colStock.'3:'.$colOutlet.'4')->applyFromArray($styleallbd);
                    $sheet->getStyle($colStock.'3:'.$colOutlet.'4')->applyFromArray($stylecenter);
                    $colOutlet++;
                }

                $row = 5;
                foreach ($products as $prd) {
                    $sheet->SetCellValue('A'.$row, $prd->kd_produk);
                    $sheet->SetCellValue('B'.$row, $prd->deskripsi);
                    $sheet->getStyle('A'.$row.':B'.$row)->applyFromArray($styleallbd);

                    $colOutlet = 'C';
                    foreach ($outlets as $outlet) {
                        $stock = $stocks->where('kd_produk', $prd->kd_produk)->where('kd_location', $outlet->kd_location)->sum('qty');
                        $sales = $detailsales->where('kd_produk', $prd->kd_produk)->where('kd_location', $outlet->kd_location)->sum('saleQty');

                        $colStock = $colOutlet;
                        $sheet->SetCellValue($colOutlet.$row, $stock);
                        if($stock <= $prd->minimal_stock) {
                            $sheet->getStyle($colOutlet.$row)->applyFromArray($styleFontRed);
                        }

                        $colOutlet++;
                        $sheet->SetCellValue($colOutlet.$row, $sales);

                        $sheet->getStyle($colStock.$row.':'.$colOutlet.$row)->applyFromArray($styleallbd);
                        $sheet->getStyle($colStock.$row.':'.$colOutlet.$row)->applyFromArray($stylecenter);
                        $colOutlet++;
                    }
                    $row++;
                }
            });
        })->export('xlsx');
    }

    public function index2() {
        return view('toko.inventory.summarystock-outlet.index2');
    }

    public function getdata2(Request $request) {
        $products = Product::get();
        $tglawal = $request->tgl_awal;
        $tglakhir = $request->tgl_akhir;
        $stocks = Stock::where('kd_location', $request->location)->get();
        $detailsales = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')
            ->whereBetween('tgl', [$tglawal, $tglakhir])
            ->where('kd_location', $request->location)
            ->selectRaw('tb_detail_sales.*, (qty - qty_retur) as saleQty')
            ->get();
        return view('toko.inventory.summarystock-outlet.datastok2', compact('products', 'tglawal', 'tglakhir', 'stocks', 'detailsales'))->render();
    }
}
