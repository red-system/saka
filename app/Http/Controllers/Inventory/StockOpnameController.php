<?php

namespace App\Http\Controllers\Inventory;

use App\Models\DetailStockOpname;
use App\Models\HistorySO;
use App\Models\Location;
use App\Models\Product;
use App\Models\Stock;
use App\Models\StockOpname;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StockOpnameController extends Controller
{
    public function index() {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $locations = Location::get();
            return view('toko.inventory.stockopname.index', compact('locations'));
        } else {
            $location = Location::where('kd_location', auth()->user()->kd_location)->firstOrFail();
            $stockopname = StockOpname::where('kd_location', $location->kd_location)->get();
            $historyso = HistorySO::where('kd_location', $location->kd_location)->get();
            return view('toko.inventory.stockopname.index-location', compact('location', 'stockopname', 'historyso'));
        }
    }

    public function location($kdlocation) {
        $location = Location::where('kd_location', $kdlocation)->firstOrFail();
        $stockopname = StockOpname::where('kd_location', $location->kd_location)->get();
        $historyso = HistorySO::where('kd_location', $location->kd_location)->get();
        return view('toko.inventory.stockopname.index-location', compact('location', 'stockopname', 'historyso'));
    }

    public function create(Request $request) {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $location = Location::where('kd_location', $request->location)->firstOrFail();
        } else {
            $location = Location::where('kd_location', auth()->user()->kd_location)->firstOrFail();
        }
        return view('toko.inventory.stockopname.create', compact('location'));
    }

    public function store(Request $request) {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $location = Location::where('kd_location', $request->location)->firstOrFail();
        } else {
            $location = Location::where('kd_location', auth()->user()->kd_location)->firstOrFail();
        }

        \DB::beginTransaction();
        try {
            $dataSO = [];
            $dataSO['kd_location'] = $location->kd_location;
            $dataSO['tgl_opname'] = $request->tgl_opname;
            $dataSO['pelaksana'] = $request->pelaksana;
            $dataSO['status'] = $request->status;
            $so = StockOpname::create($dataSO);

            foreach ($request->stockQty as $index => $value) {
                if ($value > 0) {
                    $detailSo = [];
                    $detailSo['stockopname_id'] = $so->id;
                    $detailSo['stock_id'] = $index;
                    $detailSo['qty_so'] = $value;
                    DetailStockOpname::create($detailSo);
                }
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Stock opname data has been saved']);
            if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
                return redirect()->route('stock-opname.location', $location->kd_location);
            } else {
                return redirect()->route('stock-opname.index');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function detail($id) {
        $so = StockOpname::where('id', $id)->firstOrFail();
        $datastock = $so->location->stock;
        return view('toko.inventory.stockopname.detail', compact('so', 'datastock'));
    }

    public function update(Request $request, $id) {
        $so = StockOpname::where('id', $id)->where('kd_location', auth()->user()->kd_location)->where('status', 'unpublish')->firstOrFail();

        \DB::beginTransaction();
        try {
            $dataSO = [];
            //$dataSO['kd_location'] = $location->kd_location;
            $dataSO['tgl_opname'] = $request->tgl_opname;
            $dataSO['pelaksana'] = $request->pelaksana;
            $dataSO['status'] = $request->status;
            $so->update($dataSO);

            foreach ($request->stockQty as $index => $value) {
                $dtlSO = DetailStockOpname::where('stockopname_id', $so->id)->where('stock_id', $index)->first();
                if (!empty($dtlSO)) {
                    if ($value > 0) {
                        $dtlSO->update(['qty_so' => $value]);
                    } else {
                        $dtlSO->delete();
                    }
                } else {
                    if ($value > 0) {
                        $detailSo = [];
                        $detailSo['stockopname_id'] = $so->id;
                        $detailSo['stock_id'] = $index;
                        $detailSo['qty_so'] = $value;
                        DetailStockOpname::create($detailSo);
                    }
                }
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Stock opname data has been saved']);
            if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
                return redirect()->route('stock-opname.location', $so->kd_location);
            } else {
                return redirect()->route('stock-opname.index');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function cancel($id) {
        $so = StockOpname::where('id', $id)->where('kd_location', auth()->user()->kd_location)->where('status', 'unpublish')->firstOrFail();
        $kdloc = $so->kd_location;
        $so->delete();

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Stock opname data was successfully canceled']);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            return redirect()->route('stock-opname.location', $kdloc);
        } else {
            return redirect()->route('stock-opname.index');
        }
    }

    public function penyesuaian($id) {
        $so = StockOpname::where('status', 'publish')->where('id', $id)->firstOrFail();
        $detailSO = $so->detail_stock_opname;
        return view('toko.inventory.stockopname.penyesuaian', compact('so', 'detailSO'));
    }

    public function updatePenyesuaian(Request $request, $id) {
        $so = StockOpname::where('status', 'publish')->where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $so->update(['status' => 'finish']);

            foreach ($request->stockPenyesuaian as $index => $value) {
                $dtlSO = DetailStockOpname::where('stockopname_id', $so->id)->where('id', $index)->first();

                if ($dtlSO->stock->qty != $value) {
                    $stock = $dtlSO->stock;

                    $dataHistory = [];
                    $dataHistory['tgl'] = $so->tgl_opname;
                    $dataHistory['kode'] = $stock->kd_produk;
                    $dataHistory['no_seri'] = $stock->no_seri_produk;
                    $dataHistory['deskripsi'] = $stock->product->deskripsi;
                    $dataHistory['kd_location'] = $so->kd_location;
                    $dataHistory['location'] = $stock->location->location_name;
                    $dataHistory['stock_awal'] = $stock->qty;
                    $dataHistory['stock_akhir'] = $value;
                    $dataHistory['pelaksana'] = $so->pelaksana;
                    HistorySO::create($dataHistory);

                    $stock->update(['qty' => $value]);
                }

                $dtlSO->update(['qty_penyesuaian' => $value]);
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Adjustment data for the stock ops saved']);
            if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
                return redirect()->route('stock-opname.location', $so->kd_location);
            } else {
                return redirect()->route('stock-opname.index');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function downloadStock($kdloc) {
        $location = Location::where('kd_location', $kdloc)->firstOrFail();
        $stocks = Stock::where('kd_location', $location->kd_location)->get();
        $nama = 'data-stock-'.$kdloc.'-'.date('Ymd');

        \Excel::create($nama, function($excel) use ($nama, $stocks, $location) {
            $excel->setTitle('Data Stock '.$location->location_name.' - '.date('d F Y').' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'DataStock';

            $excel->sheet($sheetname1, function($sheet)  use ($stocks) {
                $sheet->SetCellValue('A1', 'kd_produk');
                $sheet->SetCellValue('B1', 'no_seri');
                $sheet->SetCellValue('C1', 'deskripsi');
                $sheet->SetCellValue('D1', 'qty');

                $row = 2;
                foreach ($stocks as $st) {
                    $sheet->SetCellValue('A'.$row, $st->kd_produk);
                    $sheet->SetCellValue('B'.$row, $st->no_seri_produk);
                    $sheet->SetCellValue('C'.$row, $st->product ? $st->product->deskripsi : '');
                    $sheet->SetCellValue('D'.$row, '');
                    $row++;
                }
            });
        })->export('xlsx');
    }

    public function readImport(Request $request) {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $location = Location::where('kd_location', $request->location)->firstOrFail();
        } else {
            $location = Location::where('kd_location', auth()->user()->kd_location)->firstOrFail();
        }
        $datastock = $location->stock;

        $this->validate($request, [ 'file_import' => 'required|mimes:xlsx' ]);
        $excel = $request->file('file_import');

        $excels = \Excel::selectSheetsByIndex(0)->load($excel, function($reader) {

        })->get();

        return view('toko.inventory.stockopname.create-readimport', compact('excels', 'location', 'datastock'));
    }

    public function export($id) {
        $so = StockOpname::where('id', $id)->firstOrFail();
        $datastock = $so->detail_stock_opname;

        $nama = 'stock-opname-'.date('Ymd', strtotime($so->tgl_opname)).'-'.$so->kd_location;

        \Excel::create($nama, function($excel) use ($so, $datastock) {
            $excel->setTitle('Stock Opname '.$so->kd_location.' '.date('d F Y', strtotime($so->tgl_opname)).' Saka Karya Bali')
                ->setCreator('Saka Karya Bali')
                ->setCompany('Saka Karya Bali');

            $sheetname1 = 'StockOpname';

            $stylecenter = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styleright = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $stylevcenter = array(
                'alignment' => array(
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );

            $styledef = array(
                'font'  => array(
                    'size'  => 12,
                    'name'  => 'Times New Roman',
                )
            );

            $styleallbd = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $excel->sheet($sheetname1, function($sheet)  use ($so, $datastock, $stylecenter, $stylevcenter, $styledef, $styleallbd, $styleright) {
                $sheet->getDefaultStyle()->applyFromArray($styledef);
                if ($so->status == 'finish') {
                    $lastrow = 'E';
                } else {
                    $lastrow = 'D';
                }

                $sheet->SetCellValue('A2', 'Stock Opname');
                $sheet->mergeCells('A2:'.$lastrow.'2');
                $sheet->SetCellValue('A3', 'Tanggal: '.$so->tgl_opname);
                $sheet->mergeCells('A3:'.$lastrow.'3');
                $sheet->SetCellValue('A4', 'Pelaksana: '.$so->pelaksana);
                $sheet->mergeCells('A4:'.$lastrow.'4');
                $sheet->SetCellValue('A5', 'Status: '.$so->human_status);
                $sheet->mergeCells('A5:'.$lastrow.'5');

                $row = 7;
                $sheet->SetCellValue('A'.$row, 'Kode Produk');
                $sheet->SetCellValue('B'.$row, 'Kode Produk');
                $sheet->SetCellValue('C'.$row, 'No Seri');
                $sheet->SetCellValue('D'.$row, 'Qty');
                if ($so->status == 'finish') {
                    $sheet->SetCellValue('E' . $row, 'Qty Penyesuaian');
                }
                $row++;

                foreach ($datastock as $dso) {
                    $sheet->SetCellValue('A'.$row, $dso->stock->kd_produk);
                    $sheet->SetCellValue('B'.$row, $dso->stock->product->deskripsi);
                    $sheet->SetCellValue('C'.$row, $dso->stock->no_seri_produk);
                    $sheet->SetCellValue('D'.$row, $dso->qty_so);
                    if ($so->status == 'finish') {
                        $sheet->SetCellValue('E'.$row, $dso->qty_penyesuaian);
                    }
                    $row++;
                }

                $sheet->getStyle('A7'.':'.$lastrow.($row - 1))->applyFromArray($styleallbd);
                $sheet->getStyle('D7'.':'.$lastrow.($row - 1))->applyFromArray($stylecenter);
            });
        })->export('xlsx');
    }
}
