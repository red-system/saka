<?php

namespace App\Http\Controllers\Inventory;

use App\Models\GoodsFlow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SummaryStockController extends Controller
{
    public function index() {
        return view('toko.inventory.summarystock.index');
    }

    public function getdata(Request $request) {
        $type = $request->type;
        $querystocks = GoodsFlow::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir])
            ->where('type_flow', $type);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', $request->location);
        } else {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $querystocks = $querystocks->selectRaw('tb_goods_flow.*, sum(qty_in) as total_qty_in, sum(qty_out) as total_qty_out');

        if ($type == 'produk') {
            $datastocks = $querystocks->groupBy(\DB::raw('LEFT(tgl, 10)'), 'kode', 'no_seri', 'keterangan')->get();
        } else {
            $datastocks = $querystocks->groupBy(\DB::raw('LEFT(tgl, 10)'), 'kode', 'keterangan')->get();
        }
        return view('toko.inventory.summarystock.datastok', compact('datastocks', 'type'))->render();
    }

    public function cetakStock(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;
        $data['type'] = $request->type;

        $querystocks = GoodsFlow::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end])
            ->where('type_flow', $request->type);
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', $request->location);
        } else {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $querystocks = $querystocks->selectRaw('tb_goods_flow.*, sum(qty_in) as total_qty_in, sum(qty_out) as total_qty_out');

        if ($request->type == 'produk') {
            $data['datastocks'] = $querystocks->groupBy(\DB::raw('LEFT(tgl, 10)'), 'kode', 'no_seri', 'keterangan')->get();
        } else {
            $data['datastocks'] = $querystocks->groupBy(\DB::raw('LEFT(tgl, 10)'), 'kode', 'keterangan')->get();
        }
        return view('toko.inventory.summarystock.cetakstock', $data);
    }
}
