<?php

namespace App\Http\Controllers\Inventory;

use App\Models\DetailProduction;
use App\Models\DetailSales;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SummaryStockGlobalController extends Controller
{
    public function index() {
        return view('toko.inventory.summarystock-global.index');
    }

    public function getdata(Request $request) {
        $products = Product::get();
        $tglawal = $request->tgl_awal;
        $tglakhir = $request->tgl_akhir;
        $stocks = Stock::get();
        $detailsales = DetailSales::join('tb_sales', 'tb_detail_sales.no_faktur', '=', 'tb_sales.no_faktur')
            ->whereBetween('tgl', [$tglawal, $tglakhir])
            ->selectRaw('tb_detail_sales.*, tb_sales.kd_location, (qty - qty_retur) as saleQty')
            ->get();
        $detailproduction = DetailProduction::join('tb_production', 'tb_detail_production.kd_produksi', '=', 'tb_production.kd_produksi')
            ->whereBetween('tgl_produksi', [$tglawal, $tglakhir])
            ->orWhereBetween('deadline', [$tglawal, $tglakhir])
            ->where('tb_production.status', '!=', 'cancel')
            ->selectRaw('tb_detail_production.*, nama_produsen, peruntukan, remark')
            ->get();
        return view('toko.inventory.summarystock-global.datastok', compact('products', 'tglawal', 'tglakhir', 'stocks', 'detailsales', 'detailproduction'))->render();
    }
}
