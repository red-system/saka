<?php

namespace App\Http\Controllers\Inventory;

use App\Models\DetailMaterialOpname;
use App\Models\HistoryMaterialOpname;
use App\Models\Material;
use App\Models\MaterialOpname;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MaterialOpnameController extends Controller
{
    public function index() {
        $materialopname = MaterialOpname::get();
        $historymo = HistoryMaterialOpname::get();
        return view('toko.inventory.materialopname.index', compact('materialopname', 'historymo'));
    }

    public function create() {
        $materials = Material::get();
        return view('toko.inventory.materialopname.create', compact('materials'));
    }

    public function store(Request $request) {
        \DB::beginTransaction();
        try {
            $dataMO = [];
            $dataMO['tgl_opname'] = $request->tgl_opname;
            $dataMO['pelaksana'] = $request->pelaksana;
            $dataMO['status'] = $request->status;
            $mo = MaterialOpname::create($dataMO);

            foreach ($request->stockQty as $index => $value) {
                if ($value > 0) {
                    $detailMo = [];
                    $detailMo['materialopname_id'] = $mo->id;
                    $detailMo['material_id'] = $index;
                    $detailMo['qty_so'] = $value;
                    DetailMaterialOpname::create($detailMo);
                }
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Hospitalized material data has been saved']);
            return redirect()->route('material-opname.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function detail($id) {
        $mo = MaterialOpname::where('id', $id)->firstOrFail();
        $materials = Material::get();
        return view('toko.inventory.materialopname.detail', compact('mo', 'materials'));
    }

    public function update(Request $request, $id) {
        $mo = MaterialOpname::where('id', $id)->where('status', 'unpublish')->firstOrFail();

        \DB::beginTransaction();
        try {
            $dataMO = [];
            $dataMO['tgl_opname'] = $request->tgl_opname;
            $dataMO['pelaksana'] = $request->pelaksana;
            $dataMO['status'] = $request->status;
            $mo->update($dataMO);

            foreach ($request->stockQty as $index => $value) {
                $dtlMO = DetailMaterialOpname::where('materialopname_id', $mo->id)->where('material_id', $index)->first();
                if (!empty($dtlMO)) {
                    if ($value > 0) {
                        $dtlMO->update(['qty_so' => $value]);
                    } else {
                        $dtlMO->delete();
                    }
                } else {
                    if ($value > 0) {
                        $detailMo = [];
                        $detailMo['materialopname_id'] = $mo->id;
                        $detailMo['material_id'] = $index;
                        $detailMo['qty_so'] = $value;
                        DetailMaterialOpname::create($detailMo);
                    }
                }
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Hospitalized material data has been saved']);
            return redirect()->route('material-opname.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function cancel($id) {
        $mo = MaterialOpname::where('id', $id)->where('status', 'unpublish')->firstOrFail();
        $mo->delete();

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The opname material data was successfully canceled']);
        return redirect()->route('material-opname.index');
    }

    public function penyesuaian($id) {
        $mo = MaterialOpname::where('status', 'publish')->where('id', $id)->firstOrFail();
        $detailMO = $mo->detail_material_opname;
        return view('toko.inventory.materialopname.penyesuaian', compact('mo', 'detailMO'));
    }

    public function updatePenyesuaian(Request $request, $id) {
        $mo = MaterialOpname::where('status', 'publish')->where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $mo->update(['status' => 'finish']);

            foreach ($request->stockPenyesuaian as $index => $value) {
                $dtlMO = DetailMaterialOpname::where('materialopname_id', $mo->id)->where('id', $index)->first();

                if ($dtlMO->material->qty != $value) {
                    $material = $dtlMO->material;

                    $dataHistory = [];
                    $dataHistory['tgl'] = $mo->tgl_opname;
                    $dataHistory['kode'] = $material->kd_material;
                    $dataHistory['nama_material'] = $material->name;
                    $dataHistory['kategori'] = $material->kategori->kategori;
                    $dataHistory['type_material'] = $material->human_type;
                    $dataHistory['satuan'] = $material->satuan->satuan;
                    $dataHistory['supplier'] = $material->supplier->nama_supplier;
                    $dataHistory['qty_awal'] = $material->qty;
                    $dataHistory['qty_akhir'] = $value;
                    $dataHistory['pelaksana'] = $mo->pelaksana;
                    HistoryMaterialOpname::create($dataHistory);

                    $material->update(['qty' => $value]);
                }

                $dtlMO->update(['qty_penyesuaian' => $value]);
            }

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The hospitalization material for the adjustment data has been saved']);
            return redirect()->route('material-opname.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
