<?php

namespace App\Http\Controllers\Inventory;

use App\Models\GoodsFlow;
use App\Models\Material;
use App\Models\MaterialReject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MaterialRejectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::get();
        $materialReject = MaterialReject::get();
        return view('toko.inventory.materialreject.index', compact('materials', 'materialReject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material = Material::where('kd_material', $request->kd_material)->firstOrFail();
        $this->validate($request, [
            'qty' => 'required',
            'keterangan' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $laststock = $material->qty - $request->qty;
            $material->update(['qty' => $laststock]);

            $dtMReject = [];
            $dtMReject['tgl_reject_material'] = date('Y-m-d');
            $dtMReject['kd_material'] = $material->kd_material;
            $dtMReject['name'] = $material->name;
            $dtMReject['type_material'] = $material->type_material;
            $dtMReject['qty'] = $request->qty;
            $dtMReject['keterangan'] = $request->keterangan;
            MaterialReject::create($dtMReject);

            $dtgflow = [];
            $dtgflow['tgl'] = date('Y-m-d H:i:s');
            $dtgflow['kode'] = $material->kd_material;
            $dtgflow['nama'] = $material->name;
            $dtgflow['qty_in'] = 0;
            $dtgflow['satuan_in'] = $material->satuan->satuan;
            $dtgflow['qty_out'] = $request->qty;
            $dtgflow['satuan_out'] = $material->satuan->satuan;
            $dtgflow['last_stock'] = $laststock;
            $dtgflow['keterangan'] = 'Material Out';
            $dtgflow['remark'] = 'Material Reject';
            $dtgflow['kd_location'] = auth()->user()->kd_location;
            $dtgflow['type_flow'] = 'material';
            GoodsFlow::create($dtgflow);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Reject material is successful, data stock material has been updated.']);
            return redirect()->route('material-reject.index');

        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
