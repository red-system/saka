<?php

namespace App\Http\Controllers\Inventory;

use App\Models\GoodsFlow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockCardController extends Controller
{
    public function index() {
        return view('toko.inventory.stockcard.index');
    }

    public function getListItem(Request $request) {
        $goodflows = GoodsFlow::where('type_flow', $request->type)->where('kd_location', $request->location)->groupBy('kode')->get();
        $result = [['id' => 'all', 'text' => 'Semua Item']];
        foreach ($goodflows as $gf) {
            array_push($result, ['id' => $gf->kode, 'text' => $gf->nama]);
        }
        return $result;
    }

    public function getdata(Request $request) {
        $type = $request->type;
        $querystocks = GoodsFlow::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$request->tgl_awal, $request->tgl_akhir])
                                ->where('type_flow', $type);

        if ($request->item != 'all') {
            $querystocks = $querystocks->where('kode', $request->item);
        }
        
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', $request->location);
        } else {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $datastocks = $querystocks->get();
        return view('toko.inventory.stockcard.datastok', compact('datastocks', 'type'))->render();
    }

    public function cetakStock(Request $request, $start, $end) {
        $data['start'] = $start;
        $data['end'] = $end;
        $data['type'] = $request->type;

        $querystocks = GoodsFlow::whereBetween(\DB::raw('LEFT(tgl, 10)'), [$start, $end])
            ->where('type_flow', $request->type);

        if ($request->item != 'all') {
            $querystocks = $querystocks->where('kode', $request->item);
        }

        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', $request->location);
        } else {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $data['datastocks'] = $querystocks->get();

        return view('toko.inventory.stockcard.cetakstock', $data);
    }
}
