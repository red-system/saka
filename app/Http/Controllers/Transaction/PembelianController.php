<?php

namespace App\Http\Controllers\Transaction;

use App\Models\DetailPurchasing;
use App\Models\GoodsFlow;
use App\Models\Material;
use App\Models\Purchasing;
use App\Models\Satuan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PembelianController extends Controller
{
    public function index() {
        $datamaterial = Material::get();
        $kdpembelian = $this->createNoPembelian();
        return view('toko.transaction.pembelian.index', compact('datamaterial', 'kdpembelian'));
    }

    public function addForm() {
        return view('toko.transaction.pembelian._add-form')->render();
    }

    protected function createNoPembelian() {
        $preffix = 'PMB-'.date('ymd').'-';
        $lastpembelian = Purchasing::where(\DB::raw('LEFT(kd_purchasing, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_purchasing, 3) as no_purchasing')->orderBy('no_purchasing', 'desc')->first();

        if (!empty($lastpembelian)) {
            $now = intval($lastpembelian->no_purchasing)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    public function detailMaterial(Request $request) {
        $material = Material::where('kd_material', $request->kdMaterial)->first();
        return ['success' => !empty($material) ? true : false, 'material' => $material];
    }

    public function storePembelian(Request $request) {
        $kdMaterial = $request->kdMaterial;
        $namaMaterial = $request->namaMaterial;
        $kategoriMaterial = $request->kategoriMaterial;
        $typeMaterial = $request->typeMaterial;
        $minStockMaterial = $request->minStockMaterial;
        $priceMaterial = $request->priceMaterial;
        $qtyMaterial = $request->qtyMaterial;
        $satuanMaterial = $request->satuanMaterial;

        DB::beginTransaction();
        try {
            $rowRule = [
                'kd_material' => 'required',
                'price' => 'required|min:1',
                'qty' => 'required|min:1',
            ];

            $subtotal = 0;
            foreach ($kdMaterial as $index => $kdm) {
                $dataDPurchasing = [];
                $dataDPurchasing['kd_material'] = $kdm;
                $dataDPurchasing['price'] = \Konversi::localcurrency_to_database($priceMaterial[$index]);
                $dataDPurchasing['qty'] = $qtyMaterial[$index];

                $validator = \Validator::make($dataDPurchasing, $rowRule);
                if (!$validator->fails()) {
                    $dataDPurchasing['name'] = $namaMaterial[$index];
                    $dataDPurchasing['kd_kat'] = $kategoriMaterial[$index];
                    $dataDPurchasing['type'] = $typeMaterial[$index];
                    $dataDPurchasing['minimal_stocks'] = $minStockMaterial[$index];
                    $satuan = Satuan::where('kd_satuan', $satuanMaterial[$index])->first();
                    $dataDPurchasing['satuan'] = !empty($satuan) ? $satuan->satuan : '-';
                    $dataDPurchasing['kd_purchasing'] = $request->kode_pembelian;
                    DetailPurchasing::create($dataDPurchasing);

                    $subtotal = $subtotal + (\Konversi::localcurrency_to_database($priceMaterial[$index])*$qtyMaterial[$index]);

                    $dataMaterial = [];
                    $dataMaterial['kd_material'] = $kdm;
                    $dataMaterial['name'] = $namaMaterial[$index];
                    $dataMaterial['kd_kat'] = $kategoriMaterial[$index];
                    $dataMaterial['type_material'] = $typeMaterial[$index];
                    $dataMaterial['minimal_stock'] = $minStockMaterial[$index];
                    $dataMaterial['kd_satuan'] = $satuanMaterial[$index];
                    $dataMaterial['kd_supplier'] = $request->supplier;

                    $dtgflow = [];
                    $dtgflow['tgl'] = date('Y-m-d H:i:s');

                    $cekmaterial = Material::where('kd_material', $kdm)->first();
                    if (!empty($cekmaterial)) {
                        $dataMaterial['qty'] = $cekmaterial->qty + $qtyMaterial[$index];
                        $dataMaterial['harga'] = $dataDPurchasing['price'] > $cekmaterial->harga ? $dataDPurchasing['price'] : $cekmaterial->harga;
                        $cekmaterial->update($dataMaterial);

                        $dtgflow['kode'] = $cekmaterial->kd_material;
                        $dtgflow['nama'] = $cekmaterial->name;
                        $dtgflow['qty_in'] = $qtyMaterial[$index];
                        $dtgflow['satuan_in'] = $cekmaterial->satuan->satuan;
                        $dtgflow['satuan_out'] = $cekmaterial->satuan->satuan;
                    } else {
                        $dataMaterial['qty'] = $qtyMaterial[$index];
                        $dataMaterial['harga'] = $dataDPurchasing['price'];
                        $newmaterial = Material::create($dataMaterial);

                        $dtgflow['kode'] = $newmaterial->kd_material;
                        $dtgflow['nama'] = $newmaterial->name;
                        $dtgflow['qty_in'] = $qtyMaterial[$index];
                        $dtgflow['satuan_in'] = $newmaterial->satuan->satuan;
                        $dtgflow['satuan_out'] = $newmaterial->satuan->satuan;
                    }

                    $dtgflow['qty_out'] = 0;
                    $dtgflow['last_stock'] = $dataMaterial['qty'];
                    $dtgflow['keterangan'] = 'Material In';
                    $dtgflow['remark'] = 'Purchasing #'.$request->kode_pembelian;
                    $dtgflow['kd_location'] = auth()->user()->kd_location;
                    $dtgflow['type_flow'] = 'material';
                    GoodsFlow::create($dtgflow);
                }
            }

            $totalPurchasing = $subtotal;
            $dataPurchasing = [];
            $dataPurchasing['kd_purchasing'] = $request->kode_pembelian;
            $dataPurchasing['no_faktur'] = $request->no_faktur;
            $dataPurchasing['tgl'] = $request->tanggal;
            $dataPurchasing['kd_supplier'] = $request->supplier;
            $dataPurchasing['total'] = $subtotal;

            $dataPurchasing['biaya_tambahan'] = $request->biaya_tambahan != '' ? \Konversi::localcurrency_to_database($request->biaya_tambahan) : 0;
            $totalPurchasing = $totalPurchasing + ($request->biaya_tambahan != '' ? \Konversi::localcurrency_to_database($request->biaya_tambahan) : 0);

            $dataPurchasing['disc_persen'] = $request->disc != '' ? $request->disc : 0;
            $dataPurchasing['disc_nominal'] = ($totalPurchasing*$dataPurchasing['disc_persen'])/100;
            $totalPurchasing = $totalPurchasing - (($totalPurchasing*$dataPurchasing['disc_persen'])/100);

            $dataPurchasing['ppn_persen'] = $request->ppn;
            $dataPurchasing['ppn_nominal'] = ($totalPurchasing*$request->ppn)/100;
            $totalPurchasing = $totalPurchasing + (($totalPurchasing*$request->ppn)/100);
            $dataPurchasing['grand_total'] = $totalPurchasing;
            $purchasing = Purchasing::create($dataPurchasing);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Purchase data has been saved']);
            return redirect()->route('pembelian.index');

        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
