<?php

namespace App\Http\Controllers\Transaction;

use App\Models\DetailReturSales;
use App\Models\GoodsFlow;
use App\Models\ProductReject;
use App\Models\ReturSales;
use App\Models\Sales;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReturSalesController extends Controller
{
    public function index() {
        $querysales = Sales::whereHas('detail_sales', function ($query) {
            $query->where('qty', '>', 0);
        });
        if (\Gate::allows('as_sales')) {
            $querysales = $querysales->where('kd_location', auth()->user()->kd_location);
        }
        $datasales = $querysales->get();
        $historyretur = ReturSales::whereIn('no_faktur', $datasales->pluck('no_faktur'))->get();

        return view('toko.transaction.retursales.index', compact('datasales', 'historyretur'));
    }

    public function create($nofaktur) {
        $sales = new Sales();
        if (\Gate::allows('as_sales')) {
            $sales = $sales->where('kd_location', auth()->user()->kd_location);
        }
        $sales = $sales->where('no_faktur', $nofaktur)->firstOrFail();
        return view('toko.transaction.retursales.create', compact('sales'));
    }

    public function store(Request $request, $nofaktur) {
        if (\Gate::allows('as_sales')) {
            $sales = Sales::where('no_faktur', $nofaktur)->where('kd_location', auth()->user()->kd_location)->firstOrFail();
        } else {
            $sales = Sales::where('no_faktur', $nofaktur)->firstOrFail();
        }

        $qty_retur = $request->qty_retur;
        $tidakadaretur = true;
        foreach ($qty_retur as $qr) {
            if ($qr > 0) {
                $tidakadaretur = false;
            }
        }

        if ($tidakadaretur) {
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => 'No items / items will be returned']);
            return back()->withInput();
        }

        DB::beginTransaction();
        try {
            $dtRetur = [];
            $dtRetur['no_retur'] = $this->createNoRetur($sales->kd_location);
            $dtRetur['tgl_pengembalian'] = $request->tgl_pengembalian;
            $dtRetur['pelaksana'] = $request->pelaksana;
            $dtRetur['alasan'] = $request->alasan;
            $dtRetur['alasan_lain'] = $request->alasan_lain;
            $dtRetur['total_retur'] = \Konversi::localcurrency_to_database($request->total_retur);
            $dtRetur['tambahan_biaya'] = \Konversi::localcurrency_to_database($request->tambahan_biaya);
            $dtRetur['no_faktur'] = $sales->no_faktur;
            $retur = ReturSales::create($dtRetur);

            $totalRetur = 0;
            foreach ($sales->detail_sales as $dsale) {
                if ($qty_retur[$dsale->id] > 0) {
                    $detailRetur = [];
                    $detailRetur['no_detail_retur'] = $this->createDetailNoRetur($retur->no_retur);
                    $detailRetur['kd_produk'] = $dsale->kd_produk;
                    $detailRetur['no_seri'] = $dsale->no_seri;
                    $detailRetur['price'] = $dsale->price;
                    $detailRetur['hpp'] = $dsale->hpp;
                    $detailRetur['qty_retur'] = $qty_retur[$dsale->id];
                    $hargaRetur = $dsale->price - (($dsale->price*$dsale->disc_persen)/100);
                    $detailRetur['disc_persen'] = $dsale->disc_persen;
                    $detailRetur['disc_nominal'] = ($dsale->price*$dsale->disc_persen)/100;
                    $detailRetur['total_retur'] = $detailRetur['qty_retur']*$hargaRetur;
                    $detailRetur['no_retur'] = $retur->no_retur;
                    DetailReturSales::create($detailRetur);

                    $totalRetur = $totalRetur + $detailRetur['total_retur'];
                }
            }

            $this->handleBarangRetur($request, $sales);
            //$this->handleSalesRetur($request, $sales, $retur->tambahan_biaya);
            $this->handleAccountingRetur($request, $sales);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Sales returns were successful, sales data was updated.']);
            return redirect()->route('retur-sales.detail', $retur->no_retur);
        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    protected function createNoRetur($kdlocation) {
        $preffix = 'RTR-'.strtoupper($kdlocation).'-'.date('ymd').'-';
        $lastretur = ReturSales::where(\DB::raw('LEFT(no_retur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_retur, 3) as noretur')->orderBy('noretur', 'desc')->first();

        if (!empty($lastretur)) {
            $now = intval($lastretur->noretur)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    protected function createDetailNoRetur($noretur) {
        $preffix = $noretur;
        $lastretur = DetailReturSales::where(\DB::raw('LEFT(no_detail_retur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_detail_retur, 3) as no_detailretur')->orderBy('no_detailretur', 'desc')->first();

        if (!empty($lastretur)) {
            $now = intval($lastretur->no_detailretur)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    protected function handleBarangRetur($request, $sales) {
        $qty_retur = $request->qty_retur;
        foreach ($sales->detail_sales as $dsale) {
            if ($qty_retur[$dsale->id] > 0) {
                if ($request->alasan == 'rusak') {
                    $dtPReject = [];
                    $dtPReject['kd_location'] = $sales->kd_location;
                    $dtPReject['tgl_reject_product'] = date('Y-m-d', strtotime($request->tgl_pengembalian));
                    $dtPReject['kd_product'] = $dsale->kd_produk;
                    $dtPReject['no_seri'] =  $dsale->no_seri;
                    $dtPReject['deskripsi'] = $dsale->produk->deskripsi;
                    $dtPReject['material'] = $dsale->produk->material;
                    $dtPReject['qty'] = $qty_retur[$dsale->id];
                    $dtPReject['keterangan'] = 'Retur Sales';
                    ProductReject::create($dtPReject);
                } else {
                    $stock = Stock::where('kd_produk', $dsale->kd_produk)->where('no_seri_produk', $dsale->no_seri)->where('kd_location', $sales->kd_location)->first();
                    if (!empty($stock)) {
                        $laststock = $stock->qty + $qty_retur[$dsale->id];
                        $stock->update(['qty' => $laststock]);
                    } else {
                        $laststock = $qty_retur[$dsale->id];
                        $dtStock = [];
                        $dtStock['kd_produk'] = $dsale->kd_produk;
                        $dtStock['no_seri_produk'] = $dsale->no_seri;
                        $dtStock['kd_location'] = $sales->kd_location;
                        $dtStock['qty'] = $laststock;
                        $dtStock['hpp'] = $dsale->hpp;
                        $stock = Stock::create($dtStock);
                    }

                    $dtgflow = [];
                    $dtgflow['tgl'] = date('Y-m-d H:i:s');
                    $dtgflow['kode'] = $stock->kd_produk;
                    $dtgflow['no_seri'] = $dsale->no_seri;
                    $dtgflow['nama'] = $stock->product->deskripsi;
                    $dtgflow['qty_in'] = $qty_retur[$dsale->id];
                    $dtgflow['satuan_in'] = 'pcs';
                    $dtgflow['qty_out'] = 0;
                    $dtgflow['satuan_out'] = 'pcs';
                    $dtgflow['last_stock'] = $laststock;
                    $dtgflow['keterangan'] = 'Stock In';
                    $dtgflow['remark'] = 'Retur Sales #'.$sales->no_faktur;
                    $dtgflow['kd_location'] = $stock->kd_location;
                    $dtgflow['type_flow'] = 'produk';
                    GoodsFlow::create($dtgflow);
                }

                $dsale->update(['qty_retur' => $dsale->qty_retur + $qty_retur[$dsale->id]]);
            }
        }
    }

    /*protected function handleSalesRetur($request, $sales, $biayatambahan) {
        $subtotal = 0;
        $qty_retur = $request->qty_retur;
        foreach ($sales->detail_sales as $dsale) {
            if ($qty_retur[$dsale->id] > 0) {
                $udSale = [];
                $udSale['qty'] = $dsale->qty - $qty_retur[$dsale->id];
                $hargaPrd = $dsale->price - (($dsale->price*$dsale->disc_persen)/100);
                $udSale['disc_nominal'] = (($dsale->price*$dsale->disc_persen)/100)*$udSale['qty'];
                $udSale['subtotal'] = $hargaPrd*$udSale['qty'];
                $dsale->update($udSale);
            }

            $subtotal = $subtotal + $dsale->subtotal;
        }

        if ($sales->surcharge != '') {
            $surcharges = unserialize($sales->surcharge);
            if (count($surcharges) > 0) {
                foreach ($surcharges as $surcharge) {
                    if ($surcharge['biaya_tambahan'] > 0) {
                        $subtotal = $subtotal + $surcharge['biaya_tambahan'];
                    }
                }
            }
        }

        $grandTotal = $subtotal;
        $dtSale = [];
        $dtSale['total'] = $subtotal;

        $ppnnominal = ($grandTotal*$sales->ppn_persen)/100;
        $grandTotal = $grandTotal + $ppnnominal;
        $dtSale['ppn_nominal'] = $ppnnominal;

        $discnominal = ($grandTotal*$sales->disc_persen)/100;
        $grandTotal = $grandTotal - $discnominal;
        $dtSale['disc_nominal'] = $discnominal;

        $grandTotal = $grandTotal - $biayatambahan;
        $dtSale['grand_total'] = $grandTotal;
        $dtSale['komisi_nominal'] = ($grandTotal*$sales->komisi_persen)/100;

        $sales->update($dtSale);
    }*/

    protected function handleAccountingRetur($request, $sales) {

    }

    public function detail($noretur) {
        $retursales = ReturSales::where('no_retur', $noretur)->firstOrFail();
        return view('toko.transaction.retursales.detail', compact('retursales'));
    }

    public function printRetur($noretur) {
        $retursales = ReturSales::where('no_retur', $noretur)->firstOrFail();
        $detail_retur = $retursales->detail_retur;
        $location = $retursales->sales->location;

        $pdf = \PDF::loadView('toko.transaction.retursales.print_retur_pdf', compact('retursales', 'detail_retur', 'location'))
            ->setPaper([0, 0, 272.126, 396.85]);
        return $pdf->stream('Nota Retur '.$retursales->no_retur.'.pdf');
    }
}
