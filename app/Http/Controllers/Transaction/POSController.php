<?php

namespace App\Http\Controllers\Transaction;

use App\Models\Currency;
use App\Models\DetailSales;
use App\Models\GoodsFlow;
use App\Models\Kategori;
use App\Models\Location;
use App\Models\Payment;
use App\Models\Piutang;
use App\Models\Product;
use App\Models\Sales;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class POSController extends Controller
{
    public function index() {
        if (\Gate::allows('as_owner_or_manager_kantorpusat')) {
            $salestoday = Sales::where(function ($query) { $query->where('kd_location', auth()->user()->kd_location)->where(\DB::raw('LEFT(tgl, 10)'), date('Y-m-d')); })->orWhere(function ($query) { $query->where('type_sales', 'konsinyasi')->where(\DB::raw('LEFT(tgl, 10)'), date('Y-m-d')); })->get();
        } else {
            $salestoday = Sales::where('kd_location', auth()->user()->kd_location)->where('type_sales', 'langsung')->where(\DB::raw('LEFT(tgl, 10)'), date('Y-m-d'))->get();;
        }
        return view('toko.transaction.pos.index', compact('salestoday'));
    }

    public function getCartItem() {
        $orderProduk = session()->get('order.produk');
        if (session()->has('order.surcharge')) {
            $surcharges = session()->get('order.surcharge');
        } else {
            $surcharges = [];
        }
        return view('toko.transaction.pos._list-cart-produk', compact('orderProduk', 'surcharges'))->render();
    }

    public function sessionOrder() {
        return session()->get('order');
    }

    public function forgetOrder() {
        return session()->forget('order');
    }

    public function getDetailKonsinyasi(Request $request) {
        $location = Location::where('kd_location', $request->toko)->where('type', 'konsinyasi')->firstOrFail();
        return $location;
    }

    public function storeCustomerPK(Request $request) {
        $toko = Location::where('kd_location', $request->toko)->firstOrFail();
        $dataOrder = [];
        $dataOrder['tgl'] = $request->tgl;
        $dataOrder['toko'] = $request->toko;
        $dataOrder['komisi'] = $request->komisi;
        $dataOrder['pelanggan'] = 'Konsinyasi - '.$toko->location_name;
        $dataOrder['no_tlp'] = '';
        $dataOrder['alamat'] = '';
        $dataOrder['email'] = '';
        $dataOrder['customer_type'] = '';
        session()->put('order.pelanggan', $dataOrder);

        $dataCurrency = [];
        $dataCurrency['used_currency'] = $request->currency;
        $dataCurrency['usd1_to_usecurr'] = floatval(str_replace(',', '.', $request->usd_to_usecurr));
        $dataCurrency['idr1_to_usecurr'] = floatval(str_replace(',', '.', $request->idr_to_usecurr));
        session()->put('order.currency', $dataCurrency);

        $nofaktur = $this->createNotaKonsinyasi($request->toko, $request->tgl);
        session()->put('order.no_faktur', $nofaktur);
        session()->put('order.type', 'konsinyasi');
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Customer data has been saved. Please continue to add order details!']);
        return redirect()->route('pos.additem.index');
    }

    protected function createNotaKonsinyasi($toko, $tgl) {
        $preffix = 'PK-'.strtoupper($toko).'-'.date('ymd', strtotime($tgl)).'-';
        $lastorder = Sales::where(\DB::raw('LEFT(no_faktur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_faktur, 3) as no_order')->orderBy('no_order', 'desc')->first();

        if (!empty($lastorder)) {
            $now = intval($lastorder->no_order)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    public function storeCustomerPL(Request $request) {
        $dataOrder = [];
        $dataOrder['tgl'] = $request->tgl;
        $dataOrder['toko'] = auth()->user()->kd_location;
        $dataOrder['komisi'] = auth()->user()->location ? auth()->user()->location->potongan : 0;
        $dataOrder['pelanggan'] = $request->pelanggan != '' ? $request->pelanggan : 'Guest';
        $dataOrder['no_tlp'] = $request->no_tlp;
        $dataOrder['alamat'] = $request->alamat;
        $dataOrder['email'] = $request->email;
        $dataOrder['customer_type'] = $request->customer_type;
        session()->put('order.pelanggan', $dataOrder);

        $dataCurrency = [];
        $dataCurrency['used_currency'] = $request->currency;
        $dataCurrency['usd1_to_usecurr'] = floatval(str_replace(',', '.', $request->usd_to_usecurr));
        $dataCurrency['idr1_to_usecurr'] = floatval(str_replace(',', '.', $request->idr_to_usecurr));
        session()->put('order.currency', $dataCurrency);

        $nofaktur = $this->createNotaLangsung($request->tgl);
        session()->put('order.no_faktur', $nofaktur);
        session()->put('order.type', 'langsung');
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Customer data has been saved. Please continue to add order details!']);
        return redirect()->route('pos.additem.index');
    }

    protected function createNotaLangsung($tgl) {
        $preffix = 'PL-'.strtoupper(auth()->user()->kd_location).'-'.date('ymd', strtotime($tgl)).'-';
        $lastorder = Sales::where(\DB::raw('LEFT(no_faktur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_faktur, 3) as no_order')->orderBy('no_order', 'desc')->first();

        if (!empty($lastorder)) {
            $now = intval($lastorder->no_order)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    public function additem() {
    
        $sessionOrder = session()->get('order');
        $kategoriproduk = Kategori::join('tb_product', 'tb_kat.kd_kat', '=', 'tb_product.kd_kat')
                                ->join('tb_stock', 'tb_product.kd_produk', '=', 'tb_stock.kd_produk')
                                ->where('tb_kat.type_kat', 'produk')
                                ->where('tb_stock.kd_location', $sessionOrder['pelanggan']['toko'])
                                ->where('tb_stock.qty', '>', 0)
                                ->selectRaw('tb_kat.*, sum(qty) as stock')
                                ->groupBy('tb_kat.kd_kat')
                                ->get();
        $allstockproduk = Product::join('tb_stock', 'tb_product.kd_produk', '=', 'tb_stock.kd_produk')
                                ->where('tb_stock.kd_location', $sessionOrder['pelanggan']['toko'])
                                ->where('tb_stock.qty', '>', 0)
                                ->selectRaw('sum(qty) as allstock')
                                ->first(); 
                                
        // dd($kategoriproduk);
        $pelanggan = $sessionOrder['pelanggan']['pelanggan'];
        if ($sessionOrder['type'] == 'konsinyasi') {
            $nofaktur = $this->createNotaKonsinyasi($sessionOrder['pelanggan']['toko'], $sessionOrder['pelanggan']['tgl']);
        } else {
            $nofaktur = $this->createNotaLangsung($sessionOrder['pelanggan']['tgl']);
        }
        return view('toko.transaction.pos.additem', compact('kategoriproduk', 'nofaktur', 'pelanggan','allstockproduk'));
    }

    public function changeCurrency(Request $request) {
        $dataCurrency = [];
        $dataCurrency['used_currency'] = $request->currency;
        $dataCurrency['usd1_to_usecurr'] = floatval(str_replace(',', '.', $request->usd_to_usecurr));
        $dataCurrency['idr1_to_usecurr'] = floatval(str_replace(',', '.', $request->idr_to_usecurr));
        session()->put('order.currency', $dataCurrency);

        $grandTotal = 0; $komisi = 0;
        $products = session()->get('order.produk');
        if(count($products) > 0) {
            foreach ($products as $index => $prd) {
                //update usecurr_retail_price, usecurr_wholesale_price, harga_produk, disc_nominal, totalHargaProduk, totalDiscountProduk di produk
                $usecurr_retail_price = $prd['retail_price']*$dataCurrency['idr1_to_usecurr'];
                $usecurr_wholesale_price = $prd['wholesale_price']*$dataCurrency['usd1_to_usecurr'];

                session()->put('order.produk.'.$index.'.usecurr_retail_price', $usecurr_retail_price);
                session()->put('order.produk.'.$index.'.usecurr_wholesale_price', $usecurr_wholesale_price);

                $usedPrice = $prd['price_type'] == 'wholesale_price' ? $usecurr_wholesale_price : $usecurr_retail_price;
                $discNominal = ($usedPrice*$prd['disc_persen'])/100;

                session()->put('order.produk.'.$index.'.harga_produk', $usedPrice);
                session()->put('order.produk.'.$index.'.disc_nominal', $discNominal);

                $totalHargaProduk = ($usedPrice - $discNominal) * intval($prd['qty']);
                $totalDiscountProduk = intval($prd['qty'])*$discNominal;

                session()->put('order.produk.'.$index.'.totalHargaProduk', $totalHargaProduk);
                session()->put('order.produk.'.$index.'.totalDiscountProduk', $totalDiscountProduk);

                $grandTotal = $grandTotal + $totalHargaProduk;
                if (session()->get('order.type') == 'konsinyasi') {
                    $komisi = $komisi + ($totalHargaProduk*session()->get('order.pelanggan.komisi')/100);
                }
            }

            //update total_global_discount jika ada
            $surcharges = session()->get('order.surcharge');
            if (count($surcharges) > 0) {
                if(isset($surcharges['global_discount'])) {
                    $totalGlobalDiscount = ($grandTotal*$surcharges['global_discount_persen'])/100;
                    session()->put('order.total_global_discount', $totalGlobalDiscount);
                    $grandTotal = $grandTotal - $totalGlobalDiscount;
                }
            }

            //update total_komisi jika konsinyasi
            if(session()->get('order.type') == 'konsinyasi') {
                session()->put('order.total_komisi', $komisi);
                $grandTotal = $grandTotal - $komisi;
            }
        }

        //update grand_total
        session()->put('order.grand_total', $grandTotal);

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Currency changes have been saved']);
        return redirect()->route('pos.additem.index');
    }

    public function getListProduk(Request $request) {
        $sessionOrder = session()->get('order');
        $produk = Product::join('tb_stock', 'tb_product.kd_produk', '=', 'tb_stock.kd_produk')->where('tb_stock.qty', '>', 0);
        
        
        
        if ($request->search != '') {
            if (is_int($request->search)) {
                $produk = $produk->where(function ($query) use ($request, $sessionOrder) {
                    $query->orWhere('tb_stock.qty', '>=', $request->search);
                    //$query->orWhere('tb_stock.qty', '>=', $request->search)->orWhere('tb_product.price', '<=', $request->search)->orWhere('tb_product.wholesale_price', '<=', $request->search);
                });
            } else {
                $produk = $produk->where(function ($query) use ($request) {
                    $query->orWhere('tb_product.kd_produk', 'like', '%'.$request->search.'%')->orWhere('deskripsi', 'like', '%'.$request->search.'%');
                });
            }
        }

        if ($request->kp != 'all') {
            $produk = $produk->where('kd_kat', $request->kp);
        }

        $dataproduk = $produk->where('tb_stock.kd_location', $sessionOrder['pelanggan']['toko'])->selectRaw('tb_product.*, qty, tb_stock.id as id_stock,tb_stock.size_product')->get();
        //dd($dataproduk);
        $currency = session()->get('order.currency');
        return view('toko.transaction.pos._data-list-produk', compact('dataproduk', 'currency'))->render();
    }

    public function addDetailItem($id) {
        $sessionOrder = session()->get('order');
        $stockproduk = Stock::where('kd_location', $sessionOrder['pelanggan']['toko'])->where('id', $id)->firstOrFail();
        //dd($stockproduk);
        $produk = $stockproduk->product;
        $currency = session()->get('order.currency');
        return view('toko.transaction.pos._modal-additem', compact('stockproduk', 'produk', 'currency'));
    }

    public function storeItem(Request $request, $id) {
        $sessionOrder = session()->get('order');
        $stockproduk = Stock::where('kd_location', $sessionOrder['pelanggan']['toko'])->where('id', $id)->firstOrFail();
        $produk = $stockproduk->product;
        //dd($request);
        $dataProduk = [];
        $dataProduk['id_stock'] = $stockproduk->id;
        $dataProduk['id_produk'] = $produk->id;
        $dataProduk['kd_produk'] = $produk->kd_produk;
        $dataProduk['no_seri'] = $stockproduk->no_seri_produk;
        $dataProduk['size_product'] = $request->size_product;
        //dd($stockproduk->size_product);
        $dataProduk['deskripsi'] = $produk->deskripsi;
        $dataProduk['retail_price'] = $produk->price;
        $dataProduk['wholesale_price'] = $produk->wholesale_price;
        $dataProduk['usecurr_retail_price'] = $produk->price*session()->get('order.currency.idr1_to_usecurr');
        $dataProduk['usecurr_wholesale_price'] = $produk->wholesale_price*session()->get('order.currency.usd1_to_usecurr');
        $dataProduk['price_type'] = $request->price_type;
        $dataProduk['hpp'] = $stockproduk->hpp;

        $usedPrice = $request->price_type == 'wholesale_price' ? $produk->wholesale_price*session()->get('order.currency.usd1_to_usecurr') : $produk->price*session()->get('order.currency.idr1_to_usecurr');
        $discNominal = ($usedPrice*$produk->disc_persen)/100;

        $dataProduk['harga_produk'] =$usedPrice;
        $dataProduk['disc_persen'] = $produk->disc_persen;
        $dataProduk['disc_nominal'] = $discNominal;
        $dataProduk['qty'] = $request->qty;
        $dataProduk['catatan'] = $request->catatan;
        $dataProduk['totalHargaProduk'] = (intval($request->qty)*$usedPrice) - intval($request->qty)*$discNominal;
        $dataProduk['totalDiscountProduk'] = intval($request->qty)*$discNominal;

        session()->push('order.produk', $dataProduk);
        return 'Success';
    }

    public function editDetailItem($idItem) {
        $sessionOrder = session()->get('order');
        $itemOnSession = session()->get('order.produk.'.$idItem);
        $stockproduk = Stock::where('kd_location', $sessionOrder['pelanggan']['toko'])->where('id', $itemOnSession['id_stock'])->firstOrFail();
        $produk = $stockproduk->product;
        $currency = session()->get('order.currency');
        return view('toko.transaction.pos._modal-edititem', compact('produk', 'itemOnSession', 'idItem', 'stockproduk', 'currency'));
    }

    public function updateItem(Request $request, $idItem) {
        $sessionOrder = session()->get('order');
        $itemOnSession = session()->get('order.produk.'.$idItem);
        $stockproduk = Stock::where('kd_location', $sessionOrder['pelanggan']['toko'])->where('id', $itemOnSession['id_stock'])->firstOrFail();
        $produk = $stockproduk->product;

        $dataProduk = [];
        $dataProduk['id_stock'] = $stockproduk->id;
        $dataProduk['id_produk'] = $produk->id;
        $dataProduk['kd_produk'] = $produk->kd_produk;
        $dataProduk['no_seri'] = $stockproduk->no_seri_produk;
        $dataProduk['deskripsi'] = $produk->deskripsi;
        $dataProduk['retail_price'] = $produk->price;
        $dataProduk['wholesale_price'] = $produk->wholesale_price;
        $dataProduk['usecurr_retail_price'] = $produk->price*session()->get('order.currency.idr1_to_usecurr');
        $dataProduk['usecurr_wholesale_price'] = $produk->wholesale_price*session()->get('order.currency.usd1_to_usecurr');

        $dataProduk['price_type'] = $request->price_type;
        $dataProduk['hpp'] = $stockproduk->hpp;

        $usedPrice = $request->price_type == 'wholesale_price' ? $produk->wholesale_price*session()->get('order.currency.usd1_to_usecurr') : $produk->price*session()->get('order.currency.idr1_to_usecurr');
        $discNominal = ($usedPrice*$produk->disc_persen)/100;

        $dataProduk['harga_produk'] =$usedPrice;
        $dataProduk['disc_persen'] = $produk->disc_persen;
        $dataProduk['disc_nominal'] = $discNominal;
        $dataProduk['qty'] = $request->qty;
        $dataProduk['catatan'] = $request->catatan;
        $dataProduk['totalHargaProduk'] = (intval($request->qty)*$usedPrice) - intval($request->qty)*$discNominal;
        $dataProduk['totalDiscountProduk'] = intval($request->qty)*$discNominal;

        session()->put('order.produk.'.$idItem, $dataProduk);
        return 'Success';
    }

    public function deleteItem(Request $request, $id) {
        session()->forget('order.produk.'.$id);
        return 'Success';
    }

    public function storeSurcharge(Request $request) {
        if ($request->has('global_disc')) {
            session()->put('order.surcharge.global_discount', 1);
            session()->put('order.surcharge.global_discount_persen', $request->global_disc_persen);
        }
        if ($request->has('additional_disc')) {
            session()->put('order.surcharge.additional_discount', 1);
            session()->put('order.surcharge.additional_discount_nominal', $request->additional_disc_nominal);
        }
        return 'Success';
    }

    public function deleteSurcharge(Request $request, $index) {
        if ($index != '') {
            if ($index == 'global-disc') {
                session()->forget('order.surcharge.global_discount');
                session()->forget('order.surcharge.global_discount_persen');
            }
        }
        return 'Success';
    }

    public function batalkanOrder(Request $request) {
        session()->forget('order');
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Order has been canceled']);
        return redirect()->route('pos.index');
    }

    public function orderDetail() {
        $sessionOrder = session()->get('order');
        if ($sessionOrder['type'] == 'konsinyasi') {
            $nofaktur = $this->createNotaKonsinyasi($sessionOrder['pelanggan']['toko'], $sessionOrder['pelanggan']['tgl']);
        } else {
            $nofaktur = $this->createNotaLangsung($sessionOrder['pelanggan']['tgl']);
        }
        session()->put('order.no_faktur', $nofaktur);
        $order = session()->get('order');
        $currency = Currency::where('kd_currency', session()->get('order.currency.used_currency'))->first();
        return view('toko.transaction.pos.order-detail', compact('order', 'currency'));
    }

    public function storeAllOrder(Request $request) {
        DB::beginTransaction();
        try {
            $order = session()->get('order');
            $orderStored = $this->storeToOrder($order, $request);
            $this->handlePayment($orderStored, $request);
            DB::commit();
            session()->forget('order');

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Order '.$order['no_faktur'].' has been saved']);
            return redirect()->route('pos.saved-order', $orderStored->no_faktur);

        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }

    }

    /**
     * @param $order
     * @param $request
     * @return mixed
     */
    protected function storeToOrder($order, $request) {
        if ($order['type'] == 'konsinyasi') {
            $nofaktur = $this->createNotaKonsinyasi($order['pelanggan']['toko'], $order['pelanggan']['tgl']);
        } else {
            $nofaktur = $this->createNotaLangsung($order['pelanggan']['tgl']);
        }

        $dataSales = [];
        $dataSales['no_faktur'] = $nofaktur;
        $dataSales['tgl'] = date('Y-m-d', strtotime($order['pelanggan']['tgl'])).' '.date('H:i:s');
        $dataSales['nama_pelanggan'] = $order['type'] == 'langsung' ? $request->pelanggan : $order['pelanggan']['pelanggan'];
        $dataSales['alamat'] = $order['type'] == 'langsung' ? $request->alamat : $order['pelanggan']['alamat'];
        $dataSales['telp'] = $order['type'] == 'langsung' ? $request->no_tlp : $order['pelanggan']['no_tlp'];
        $dataSales['email'] = $order['type'] == 'langsung' ? $request->email : $order['pelanggan']['email'];
        $dataSales['customer_type'] = $order['pelanggan']['customer_type'];

        $dataSales['used_currency'] = $order['currency']['used_currency'];
        $dataSales['usd1_to_usecurr'] = $order['currency']['usd1_to_usecurr'];
        $dataSales['idr1_to_usecurr'] = $order['currency']['idr1_to_usecurr'];

        $usercurr1_to_idr = floatval(str_replace(',', '.', $request->usercurr1_to_idr));
        $dataSales['total'] = number_format($order['sub_total_produk']*$usercurr1_to_idr, 4, '.', '');

        $dataSales['disc_persen'] = isset($order['surcharge']['global_discount']) ? $order['surcharge']['global_discount_persen'] : 0;
        $dataSales['disc_nominal'] = isset($order['surcharge']['global_discount']) ? number_format($order['total_global_discount']*$usercurr1_to_idr, 4, '.', '') : 0;
        
        $dataSales['additional_disc'] = isset($order['surcharge']['additional_discount']) ? number_format($order['surcharge']['additional_discount_nominal']*$usercurr1_to_idr, 4, '.', '') : 0;

        $dataSales['grand_total'] = number_format($order['grand_total']*$usercurr1_to_idr, 4, '.', '');

        $dataSales['type_sales'] = $order['type'];
        $dataSales['kd_location'] = $order['pelanggan']['toko'];

        $dataSales['komisi_persen'] = $order['type'] == 'konsinyasi' ? $order['pelanggan']['komisi'] : 0;
        $dataSales['komisi_nominal'] = $order['type'] == 'konsinyasi' ? number_format($order['total_komisi']*$usercurr1_to_idr, 4, '.', '') : 0;

        $dataSales['surcharge'] = isset($order['surcharge']['others']) ? serialize($order['surcharge']['others']) : '';
        $dataSales['usercurr1_to_idr'] = number_format($usercurr1_to_idr, 4, '.', '');
        $dataSales['salesuser_id'] = $request->salesuser_id;
        $storedOrder = Sales::create($dataSales);

        foreach ($order['produk'] as $op) {
            $produk = Product::where('kd_produk', $op['kd_produk'])->first();
            $dataProduk = [];
            $dataProduk['no_faktur'] = $nofaktur;
            $dataProduk['kd_produk'] = $op['kd_produk'];
            $dataProduk['no_seri'] = $op['no_seri'];
            $dataProduk['size_product'] = $op['size_product'];

            $dataProduk['base_retail_price'] = $op['retail_price'];
            $dataProduk['base_wholesale_price'] = $op['wholesale_price'];

            $dataProduk['retail_price'] = number_format($op['usecurr_retail_price']*$usercurr1_to_idr, 4, '.', '');
            $dataProduk['wholesale_price'] = number_format($op['usecurr_wholesale_price']*$usercurr1_to_idr, 4, '.', '');

            $dataProduk['price_type'] = $op['price_type'];
            $dataProduk['price'] = number_format($op['harga_produk']*$usercurr1_to_idr, 4, '.', '');
            $dataProduk['hpp'] = $op['hpp'];
            $dataProduk['qty'] = $op['qty'];

            $dataProduk['catatan'] = $op['catatan'];
            $dataProduk['disc_persen'] = $op['disc_persen'];
            $dataProduk['disc_nominal'] = number_format($op['disc_nominal']*$usercurr1_to_idr, 4, '.', '');
            $dataProduk['subtotal'] = number_format($op['totalHargaProduk']*$usercurr1_to_idr, 4, '.', '');
            DetailSales::create($dataProduk);

            $stock = Stock::where('id', $op['id_stock'])->where('kd_location', $order['pelanggan']['toko'])->first();

            $dtgflow = [];
            $dtgflow['tgl'] = date('Y-m-d', strtotime($order['pelanggan']['tgl'])).' '.date('H:i:s');
            $dtgflow['kode'] = $produk->kd_produk;
            $dtgflow['no_seri'] = !empty($stock) ? $stock->no_seri_produk : '';
            $dtgflow['nama'] = $produk->deskripsi;

            if (!empty($stock)) {
                $laststock = $stock->qty - $op['qty'];
                if ($laststock > 0) {
                    $stock->update(['qty' => $laststock]);
                } else {
                    $stock->delete();
                }
            } else {
                $laststock = 0;
            }

            $dtgflow['qty_in'] = 0;
            $dtgflow['satuan_in'] = 'pcs';
            $dtgflow['qty_out'] = $op['qty'];
            $dtgflow['satuan_out'] = 'pcs';
            $dtgflow['last_stock'] = $laststock;
            $dtgflow['keterangan'] = 'Stock Out';
            $dtgflow['remark'] = 'Sales #'.$nofaktur;
            $dtgflow['kd_location'] = $order['pelanggan']['toko'];
            $dtgflow['type_flow'] = 'produk';
            GoodsFlow::create($dtgflow);
        }

        return $storedOrder;
    }

    protected function handlePayment($orderStored, $request) {
        $paymentType = $request->paymentType;
        $bank = $request->bank;
        $due_date= $request->dueDate;
        $paymentPrice = $request->paymentPrice;

        $rowRules = [
            'type_pembayaran' => 'required',
            'payment' => 'required'
        ];

        $totalPayment = 0;
        $terbayar = 1;
        foreach ($paymentPrice as $index => $ppayment) {
            $dataPayment = [];
            $dataPayment['type_pembayaran'] = $paymentType[$index];
            $dataPayment['bank_pembayaran'] = $bank[$index];
            $dataPayment['due_date'] = $due_date[$index];
            $dataPayment['payment'] = \Konversi::localcurrency_to_database($ppayment);
            $dataPayment['no_faktur'] = $orderStored->no_faktur;

            if($paymentType[$index] == 'piutang'){
                $date=Carbon::now();
                $dateToday=$date->toDateString();
                $namaCus=session()->get('order.pelanggan.pelanggan');
                Piutang::create(['due_date' => $due_date[$index],'no_faktur' => $orderStored->no_faktur,'tgl_piutang'=>$dateToday, 'nama'=>$namaCus,'kode_perkiraan'=>'1301','pp_keterangan'=>'Piutang penjualan '.$namaCus,'total_piutang' => \Konversi::localcurrency_to_database($ppayment),'sisa_piutang' => \Konversi::localcurrency_to_database($ppayment)]);
                $terbayar = 0;
            }

            $orderStored->update(['status_terbayar' => $terbayar]);

            $validator = \Validator::make($dataPayment, $rowRules);
            if ($validator->passes()) {
                $totalPayment = $totalPayment + \Konversi::localcurrency_to_database($ppayment);
                Payment::create($dataPayment);
            }
        }

        if ($totalPayment < $orderStored->grand_total ) {
            $sisaPiutang = $orderStored->grand_total - $totalPayment;
            Piutang::create(['no_faktur' => $orderStored->no_faktur, 'sisa_piutang' => $sisaPiutang]);
        }


    }

    public function savedOrder($id) {
        $sales = Sales::where('no_faktur', $id)->firstOrFail();
        $detailsales = $sales->detail_sales;
        $currency = Currency::where('kd_currency', $sales->used_currency)->first();
        return view('toko.transaction.pos.savedorder-detail', compact('sales', 'detailsales', 'currency'));
    }

    public function printOrder($id) {
        $sales = Sales::where('no_faktur', $id)->firstOrFail();
        $detailsales = $sales->detail_sales;
        $piutangSales = $sales->piutang;

        //dd($sales);
        if ($sales->type_sales == 'konsinyasi') {
            $pdf = \PDF::loadView('toko.transaction.pos.print_order_pdf_a4', compact('sales', 'detailsales','piutangSales'))->setPaper('a4', 'potrait');
        } else {
            $pdf = \PDF::loadView('toko.transaction.pos.print_order_pdf', compact('sales', 'detailsales','piutangSales'))->setPaper([0, 0, 272.126, 396.85]);
        }
        return $pdf->stream('Nota Penjualan '.$sales->no_faktur.'.pdf');
    }
}
