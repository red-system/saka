<?php

namespace App\Http\Controllers\Transaction;

use App\Models\DetailReturPurchasing;
use App\Models\GoodsFlow;
use App\Models\Location;
use App\Models\Material;
use App\Models\MaterialReject;
use App\Models\Purchasing;
use App\Models\ReturPurchasing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReturPurchasingController extends Controller
{
    public function index() {
        $datapurchasing = Purchasing::whereHas('detail_purchasing', function ($query) { $query->where('qty', '>', 0); })->get();
        $historyretur = ReturPurchasing::whereIn('no_faktur', $datapurchasing->pluck('no_faktur'))->get();
        return view('toko.transaction.returpurchasing.index', compact('datapurchasing', 'historyretur'));
    }

    public function create($nofaktur) {
        $purchasing = Purchasing::where('no_faktur', $nofaktur)->firstOrFail();
        return view('toko.transaction.returpurchasing.create', compact('purchasing'));
    }

    public function store(Request $request, $nofaktur) {
        $purchasing = Purchasing::where('no_faktur', $nofaktur)->firstOrFail();

        $qty_retur = $request->qty_retur;
        $tidakadaretur = true;
        foreach ($qty_retur as $qr) {
            if ($qr > 0) {
                $tidakadaretur = false;
            }
        }

        if ($tidakadaretur) {
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => 'No material will be returned']);
            return back()->withInput();
        }

        DB::beginTransaction();
        try {
            $dtRetur = [];
            $dtRetur['no_retur'] = $this->createNoRetur();
            $dtRetur['tgl_pengembalian'] = $request->tgl_pengembalian;
            $dtRetur['pelaksana'] = $request->pelaksana;
            $dtRetur['alasan'] = $request->alasan;
            $dtRetur['alasan_lain'] = $request->alasan_lain;
            $dtRetur['total_retur'] = \Konversi::localcurrency_to_database($request->total_retur);
            $dtRetur['tambahan_biaya'] = \Konversi::localcurrency_to_database($request->tambahan_biaya);
            $dtRetur['no_faktur'] = $purchasing->no_faktur;
            $retur = ReturPurchasing::create($dtRetur);

            $totalRetur = 0;
            foreach ($purchasing->detail_purchasing as $dpurchasing) {
                if ($qty_retur[$dpurchasing->id] > 0) {
                    $detailRetur = [];
                    $detailRetur['no_detail_retur'] = $this->createDetailNoRetur($retur->no_retur);
                    $detailRetur['kd_material'] = $dpurchasing->kd_material;
                    $detailRetur['price'] = $dpurchasing->price;
                    $detailRetur['qty_retur'] = $qty_retur[$dpurchasing->id];
                    $detailRetur['total_retur'] = $detailRetur['qty_retur']*$dpurchasing->price;
                    $detailRetur['no_retur'] = $retur->no_retur;
                    DetailReturPurchasing::create($detailRetur);

                    $totalRetur = $totalRetur + $detailRetur['total_retur'];
                }
            }

            $ppnnominal = ($totalRetur*$purchasing->ppn_persen)/100;
            $totalRetur = $totalRetur + $ppnnominal;
            $discnominal = ($totalRetur*$purchasing->disc_persen)/100;
            $totalRetur = $totalRetur - $discnominal;

            $retur->update([
                'ppn_persen' => $purchasing->ppn_persen,
                'ppn_nominal' => $ppnnominal,
                'disc_persen' => $purchasing->disc_persen,
                'disc_nominal' => $discnominal,
                'total_retur' => $totalRetur
            ]);

            $this->handleMaterialRetur($request, $purchasing);
            //$this->handlePurchasingRetur($request, $purchasing, $retur->tambahan_biaya);
            $this->handleAccountingRetur($request, $purchasing);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Purchasing returns were successful, inventory data was updated.']);
            return redirect()->route('retur-purchasing.detail', $retur->no_retur);
        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    protected function createNoRetur() {
        $preffix = 'RTR-'.date('ymd').'-';
        $lastretur = ReturPurchasing::where(\DB::raw('LEFT(no_retur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_retur, 3) as noretur')->orderBy('noretur', 'desc')->first();

        if (!empty($lastretur)) {
            $now = intval($lastretur->noretur)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    protected function createDetailNoRetur($noretur) {
        $preffix = $noretur;
        $lastretur = DetailReturPurchasing::where(\DB::raw('LEFT(no_detail_retur, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(no_detail_retur, 3) as no_detailretur')->orderBy('no_detailretur', 'desc')->first();

        if (!empty($lastretur)) {
            $now = intval($lastretur->no_detailretur)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    protected function handleMaterialRetur($request, $purchasing) {
        $qty_retur = $request->qty_retur;
        foreach ($purchasing->detail_purchasing as $dpurchasing) {
            if ($qty_retur[$dpurchasing->id] > 0) {
                if ($request->alasan == 'rusak') {
                    $dtMReject = [];
                    $dtMReject['tgl_reject_material'] = date('Y-m-d', strtotime($request->tgl_pengembalian));
                    $dtMReject['kd_material'] = $dpurchasing->kd_material;
                    $dtMReject['name'] = $dpurchasing->material->name;
                    $dtMReject['type_material'] = $dpurchasing->material->type_material;
                    $dtMReject['qty'] = $qty_retur[$dpurchasing->id];
                    $dtMReject['keterangan'] = 'Retur Purchasing';
                    MaterialReject::create($dtMReject);
                } else {
                    $material = Material::where('kd_material', $dpurchasing->kd_material)->firstOrFail();
                    $laststock = $material->qty - $qty_retur[$dpurchasing->id];
                    $material->update(['qty' => $laststock]);

                    $dtgflow = [];
                    $dtgflow['tgl'] = date('Y-m-d H:i:s');
                    $dtgflow['kode'] = $material->kd_material;
                    $dtgflow['nama'] = $material->name;
                    $dtgflow['qty_in'] = 0;
                    $dtgflow['satuan_in'] = $material->satuan->satuan;
                    $dtgflow['qty_out'] = $qty_retur[$dpurchasing->id];
                    $dtgflow['satuan_out'] = $material->satuan->satuan;
                    $dtgflow['last_stock'] = $laststock;
                    $dtgflow['keterangan'] = 'Material Out';
                    $dtgflow['remark'] = 'Retur Purchasing #'.$purchasing->kd_purchasing;
                    $dtgflow['kd_location'] = auth()->user()->kd_location;
                    $dtgflow['type_flow'] = 'material';
                    GoodsFlow::create($dtgflow);
                }
            }
        }
    }

    protected function handlePurchasingRetur($request, $purchasing, $biayatambahan) {
        $subtotal = 0;
        $qty_retur = $request->qty_retur;
        foreach ($purchasing->detail_purchasing as $dpurchasing) {
            if ($qty_retur[$dpurchasing->id] > 0) {
                $udPurchasing = [];
                $udPurchasing['qty'] = $dpurchasing->qty - $qty_retur[$dpurchasing->id];
                $dpurchasing->update($udPurchasing);
            }
            $subtotal = $subtotal + ($dpurchasing->price*$dpurchasing->qty);
        }

        $grandTotal = $subtotal;
        $dtPurchasing = [];
        $dtPurchasing['total'] = $subtotal;

        $ppnnominal = ($grandTotal*$purchasing->ppn_persen)/100;
        $grandTotal = $grandTotal + $ppnnominal;
        $dtPurchasing['ppn_nominal'] = $ppnnominal;

        $discnominal = ($grandTotal*$purchasing->disc_persen)/100;
        $grandTotal = $grandTotal - $discnominal;
        $dtPurchasing['disc_nominal'] = $discnominal;

        $grandTotal = $grandTotal - $biayatambahan;
        $dtPurchasing['grand_total'] = $grandTotal;

        $purchasing->update($dtPurchasing);
    }

    protected function handleAccountingRetur($request, $purchasing) {

    }

    public function detail($noretur) {
        $returpurchasing = ReturPurchasing::where('no_retur', $noretur)->firstOrFail();
        return view('toko.transaction.returpurchasing.detail', compact('returpurchasing'));
    }

    public function printRetur($noretur) {
        $returpurchasing = ReturPurchasing::where('no_retur', $noretur)->firstOrFail();
        $detail_retur = $returpurchasing->detail_retur;
        $location = Location::where('kd_location', auth()->user()->kd_location)->first();

        $pdf = \PDF::loadView('toko.transaction.returpurchasing.print_retur_pdf', compact('returpurchasing', 'detail_retur', 'location'))
            ->setPaper([0, 0, 272.126, 396.85]);
        return $pdf->stream('Nota Retur Pembelian '.$returpurchasing->no_retur.'.pdf');
    }
}
