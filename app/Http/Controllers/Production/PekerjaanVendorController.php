<?php

namespace App\Http\Controllers\Production;

use App\Models\DetailProduction;
use App\Models\Location;
use App\Models\Production;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PekerjaanVendorController extends Controller
{
    public function index() {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $vendors = Produsen::get();
        return view('toko.production.pekerjaan-vendor.index', compact('vendors', 'mlayout'));
    }

    public function show($kdprodusen) {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $vendor = Produsen::where('kd_produsen', $kdprodusen)->firstOrFail();
        $onprogress = Production::where('kd_produsen', $vendor->kd_produsen)->where('status', 'on_progress')->orderBy('tgl_produksi', 'asc')->get();
        return view('toko.production.pekerjaan-vendor.show', compact('mlayout', 'vendor', 'onprogress'));
    }

    public function getHistory(Request $request, $kdprodusen) {
        $vendor = Produsen::where('kd_produsen', $kdprodusen)->firstOrFail();
        $onprogress = Production::where('kd_produsen', $kdprodusen)
                                ->where(function ($query) use ($request) {
                                    $query->whereBetween('tgl_produksi', [$request->tglawal, $request->tglakhir])
                                        ->orWhereBetween('deadline', [$request->tglawal, $request->tglakhir]);
                                })
                                ->where('status', 'finish')
                                ->orderBy('tgl_produksi', 'desc')
                                ->get();
        return view('toko.production.pekerjaan-vendor.data-history', compact('onprogress','vendor'))->render();
    }

    public function cetakLaporan($start, $end) {
        $pekerjaanvendor = DetailProduction::join('tb_production', 'tb_detail_production.kd_produksi', '=', 'tb_production.kd_produksi')
                                            ->where(function ($query) use ($start, $end) {
                                                $query->whereBetween('tgl_produksi', [$start, $end])
                                                    ->orWhereBetween('deadline', [$start, $end]);
                                            })->selectRaw('tb_detail_production.*, tb_production.kd_produsen')->get();
        return view('toko.production.pekerjaan-vendor.cetaklaporan', compact('pekerjaanvendor', 'start', 'end'));
    }
}
