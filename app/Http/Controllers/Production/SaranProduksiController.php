<?php

namespace App\Http\Controllers\Production;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaranProduksiController extends Controller
{
    public function index() {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $cekproduksi = Product::join('tb_komposisi_produk', 'tb_product.kd_produk', '=', 'tb_komposisi_produk.kd_produk')
            ->join('tb_material', 'tb_komposisi_produk.kd_material', '=', 'tb_material.kd_material')
            ->selectRaw('tb_product.kd_produk, deskripsi, tb_komposisi_produk.qty as mtp_qty, tb_material.qty as mt_qty, tb_material.qty DIV tb_komposisi_produk.qty as jml_produksi')
            ->orderBy('jml_produksi', 'asc')->groupBy('tb_product.kd_produk')->get();
        return view('toko.production.saran-produksi.index', compact('cekproduksi', 'mlayout'));
    }
}
