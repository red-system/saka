<?php

namespace App\Http\Controllers\Production;

use App\Helpers\Konversi;
use App\Models\Costing;
use App\Models\DetailProduction;
use App\Models\GoodsFlow;
use App\Models\HistoryProductionProgress;
use App\Models\Material;
use App\Models\Product;
use App\Models\Production;
use App\Models\ProductionMaterial;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProduksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $productions = Production::whereNotIn('status', ['finish', 'cancel'])->get();
        $historyproductions = Production::whereIn('status', ['finish', 'cancel'])->get();
        return view('toko.production.produksi.index', compact('productions', 'historyproductions', 'mlayout'));
    }

    protected function createKodeProduksi() {
        $preffix = 'PRD-'.date('ymd').'-';
        $lastproduksi = Production::where(\DB::raw('LEFT(kd_produksi, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_produksi, 3) as kode_produksi')->orderBy('kode_produksi', 'desc')->first();

        if (!empty($lastproduksi)) {
            $now = intval($lastproduksi->kode_produksi)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $kodeproduksi = $this->createKodeProduksi();
        if ($request->has('costing')) {
            $costing = Costing::where('kd_costing', $request->costing)->where('status', 'on_progress')->first();
        } else {
            $costing = null;
        }
        return view('toko.production.produksi.create', compact('kodeproduksi', 'mlayout', 'costing'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $dataProduksi = [];
            $dataProduksi['kd_produksi'] = $this->createKodeProduksi();
            $dataProduksi['tgl_produksi'] = $request->tgl_produksi;
            $dataProduksi['deadline'] = $request->deadline;

            $produsen = Produsen::where('kd_produsen', $request->kd_produsen)->first();
            $dataProduksi['kd_produsen'] = !empty($produsen) ? $produsen->kd_produsen : $request->kd_produsen;
            $dataProduksi['nama_produsen'] = !empty($produsen) ? $produsen->nama_produsen : '';

            $dataProduksi['remark'] = $request->remark;
            $total = Konversi::localcurrency_to_database($request->total);
            $ppnNominal = ($total*$request->ppn_persen)/100;
            $dataProduksi['total'] = $total;
            $dataProduksi['ppn_persen'] = $request->ppn_persen;
            $dataProduksi['ppn_nominal'] = $ppnNominal;
            $dataProduksi['grand_total'] = $total + $ppnNominal;
            $dataProduksi['status'] = 'draft';
            if ($request->has('kd_costing')) {
                $dataProduksi['kd_costing'] = $request->kd_costing;
                Costing::where('kd_costing', $request->kd_costing)->where('status', 'on_progress')->update(['status' => 'finish']);
            }
            $produksi = Production::create($dataProduksi);

            $this->upsertItem($request, $produksi);

            $this->upsertMaterial($request, $produksi);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Production data saved successfully']);
            return redirect()->route('produksi.index');

        } catch (\Exception $e) {
            return $e;
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produksi = Production::where('kd_produksi', $id)->firstOrFail();
        $itemProduksi = $produksi->detail_production;
        $materialProduksi = $produksi->production_material;
        return view('toko.production.produksi.show', compact('produksi', 'itemProduksi', 'materialProduksi'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $produksi = Production::where('kd_produksi', $id)->where('status', 'draft')->firstOrFail();
        $itemProduksi = $produksi->detail_production;
        $materialProduksi = $produksi->production_material()->orderBy('kebutuhan', 'asc')->get();
        return view('toko.production.produksi.edit', compact('produksi', 'itemProduksi', 'materialProduksi', 'mlayout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $produksi = Production::where('kd_produksi', $id)->where('status', 'draft')->firstOrFail();
            $dataProduksi = [];
            $dataProduksi['tgl_produksi'] = $request->tgl_produksi;
            $dataProduksi['deadline'] = $request->deadline;

            $produsen = Produsen::where('kd_produsen', $request->kd_produsen)->first();
            $dataProduksi['kd_produsen'] = !empty($produsen) ? $produsen->kd_produsen : $request->kd_produsen;
            $dataProduksi['nama_produsen'] = !empty($produsen) ? $produsen->nama_produsen : '';

            $dataProduksi['remark'] = $request->remark;
            $total = Konversi::localcurrency_to_database($request->total);
            $ppnNominal = ($total*$request->ppn_persen)/100;
            $dataProduksi['total'] = $total;
            $dataProduksi['ppn_persen'] = $request->ppn_persen;
            $dataProduksi['ppn_nominal'] = $ppnNominal;
            $dataProduksi['grand_total'] = $total + $ppnNominal;
            $produksi->update($dataProduksi);

            $deletedItem = DetailProduction::where('kd_produksi', $produksi->kd_produksi)->whereNotIn('kd_detail_production', $request->itemId)->get();
            if ($deletedItem->count() > 0) {
                $this->manageDeleteItem($deletedItem);
            }
            $this->upsertItem($request, $produksi);

            $deletedMaterial = ProductionMaterial::where('kd_produksi', $produksi->kd_produksi)->whereNotIn('id', $request->materialId)->get();
            if ($deletedMaterial->count() > 0) {
                $this->manageDeleteMaterial($deletedMaterial, $produksi->kd_produksi);
            }
            $this->upsertMaterial($request, $produksi);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Production data was successfully changed']);
            return redirect()->route('produksi.index');
        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $production = Production::where('id', $id)->whereIn('status', ['draft', 'on_progress'])->firstOrFail();

        DB::beginTransaction();
        try {
            $detailProduction = DetailProduction::where('kd_produksi', $production->kd_produksi)->whereRaw('qty_progress < qty')->get();
            $materialDikembalikan = [];
            if ($detailProduction->count() > 0) {
                foreach ($detailProduction as $dp) {
                    $qtyPrdBatal = $dp->qty - $dp->qty_progress;

                    $komposisiprd = $dp->product->komposisi_product;
                    foreach ($komposisiprd as $kprd) {
                        $dikembalikan = $kprd->qty*$qtyPrdBatal;
                        if (isset($materialDikembalikan[$kprd->kd_material])) {
                            $materialDikembalikan[$kprd->kd_material] = $materialDikembalikan[$kprd->kd_material] + $dikembalikan;
                        } else {
                            $materialDikembalikan[$kprd->kd_material] = $dikembalikan;
                        }
                    }
                }
            }

            //Kembalikan stok
            foreach ($materialDikembalikan as $index => $val) {
                $prdMaterial = ProductionMaterial::where('kd_produksi', $production->kd_produksi)->where('kd_material', $index)->first();
                if (!empty($prdMaterial)) {
                    $material = Material::where('kd_material', $index)->first();
                    $qtyKembali = $val > $prdMaterial->qty_penggunaan_material ? $prdMaterial->qty_penggunaan_material : $val;
                    $laststock = $material->qty + $qtyKembali;
                    $material->update(['qty' => $laststock]);

                    $dtgflow = [];
                    $dtgflow['tgl'] = date('Y-m-d H:i:s');
                    $dtgflow['kode'] = $material->kd_material;
                    $dtgflow['nama'] = $material->name;
                    $dtgflow['qty_in'] = $qtyKembali;
                    $dtgflow['satuan_in'] = $material->satuan->satuan;
                    $dtgflow['qty_out'] = 0;
                    $dtgflow['satuan_out'] = $material->satuan->satuan;
                    $dtgflow['last_stock'] = $laststock;
                    $dtgflow['keterangan'] = 'Material In';
                    $dtgflow['remark'] = 'Cancel Production #'.$production->kd_produksi;
                    $dtgflow['kd_location'] = auth()->user()->kd_location;
                    $dtgflow['type_flow'] = 'material';
                    GoodsFlow::create($dtgflow);
                }
            }

            $production->update(['status' => 'cancel']);

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Production data has been canceled']);
            return redirect()->route('produksi.index');

        } catch (\Exception $e) {
            DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function printProduksi($kdproduksi) {
        $produksi = Production::where('kd_produksi', $kdproduksi)->firstOrFail();
        $itemProduksi = $produksi->detail_production;

        $pdf = \PDF::loadView('toko.production.produksi.print', compact('produksi', 'itemProduksi'))->setPaper('a4', 'p');
        return $pdf->stream('Production '.$produksi->kd_produksi.'.pdf');
    }

    public function getListMaterialEdit(Request $request, $kdproduksi) {
        $produksi = Production::where('kd_produksi', $kdproduksi)->firstOrFail();
        $oldProductionMaterial = $produksi->production_material;

        $reqproduk = rtrim($request->produk, '|');
        $products = explode('|', $reqproduk);

        $allMaterial = [];
        foreach ($products as $product) {
            $prd = explode(',', $product);
            $dtPrd = Product::where('kd_produk', $prd[0])->first();

            $komposisiPrd = $dtPrd->komposisi_product;
            if($komposisiPrd->count() > 0) {
                foreach ($komposisiPrd as $kmpPrd) {
                    $cekpenggunaanmaterial = $oldProductionMaterial->where('kd_material', $kmpPrd->kd_material)->first();
                    if (isset($allMaterial[$kmpPrd->kd_material])) {
                        $kebutuhan = $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] + $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty + (!empty($cekpenggunaanmaterial) ? $cekpenggunaanmaterial->qty_penggunaan_material : 0);

                        $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] = $kebutuhan;
                        $allMaterial[$kmpPrd->kd_material]['qty_tersedia'] = $tersedia;
                        $allMaterial[$kmpPrd->kd_material]['qty_kurang'] = $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0;
                    } else {
                        $kebutuhan = $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty + (!empty($cekpenggunaanmaterial) ? $cekpenggunaanmaterial->qty_penggunaan_material : 0);
                        $allMaterial[$kmpPrd->kd_material] = ['kd_material' => $kmpPrd->kd_material, 'view_material' => $kmpPrd->kd_material.' - '.$kmpPrd->material->name, 'kd_satuan' => $kmpPrd->kd_satuan, 'satuan' => $kmpPrd->satuan->satuan, 'qty_kebutuhan' => $kebutuhan, 'qty_tersedia' => $tersedia, 'qty_kurang' => $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0];
                    }
                }
            }
        }
        return view('toko.production.produksi._listmaterialedit', compact('allMaterial', 'oldProductionMaterial'));
    }

    public function getListMaterial(Request $request) {
        $reqproduk = rtrim($request->produk, '|');
        $products = explode('|', $reqproduk);

        $allMaterial = [];
        foreach ($products as $product) {
            $prd = explode(',', $product);
            $dtPrd = Product::where('kd_produk', $prd[0])->first();

            $komposisiPrd = $dtPrd->komposisi_product;
            if($komposisiPrd->count() > 0) {
                foreach ($komposisiPrd as $kmpPrd) {
                    if (isset($allMaterial[$kmpPrd->kd_material])) {
                        $kebutuhan = $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] + $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;

                        $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] = $kebutuhan;
                        $allMaterial[$kmpPrd->kd_material]['qty_tersedia'] = $tersedia;
                        $allMaterial[$kmpPrd->kd_material]['qty_kurang'] = $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0;
                    } else {
                        $kebutuhan = $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;
                        $allMaterial[$kmpPrd->kd_material] = ['kd_material' => $kmpPrd->kd_material, 'view_material' => $kmpPrd->kd_material.' - '.$kmpPrd->material->name, 'kd_satuan' => $kmpPrd->kd_satuan, 'satuan' => $kmpPrd->satuan->satuan, 'qty_kebutuhan' => $kebutuhan, 'qty_tersedia' => $tersedia, 'qty_kurang' => $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0];
                    }
                }
            }
        }
        return view('toko.production.produksi._listmaterial', compact('allMaterial'));
    }

    public function getDetailMaterial(Request $request) {
        $material = Material::where('kd_material', $request->kdmaterial)->firstOrFail();
        return ['namaMaterial' => $material->name, 'satuanMaterial' => $material->satuan->satuan, 'qtyStock' => $material->qty];
    }

    public function upsertItem($request, $produksi) {
        if ($request->has('itemId')) {
            $itemId = $request->itemId;
        } else {
            $itemId = [];
        }

        $itemProductKode = $request->productKode;
        $itemQty = $request->itemQty;
        $itemBiaya = $request->itemBiaya;
        $itemRemark = $request->itemRemark;

        $itemRules = [
            'kd_produk' => 'required',
            'qty' => 'required|numeric'
        ];
        foreach ($itemProductKode as $index => $productKode) {
            $dataItem = [];
            $dataItem['kd_produk'] = $productKode;
            $dataItem['qty'] = $itemQty[$index];
            $itemValidator = \Validator::make($dataItem, $itemRules);
            if (!$itemValidator->fails()) {
                $product = Product::where('kd_produk', $productKode)->first();
                $dataItem['deskripsi'] = $product->deskripsi;
                $dataItem['kd_kat'] = $product->kd_kat;
                $dataItem['nama_kategori'] = $product->kategori ? $product->kategori->kategori : '';
                $dataItem['material'] = $product->material;

                $dataItem['biaya_produksi'] = \Konversi::localcurrency_to_database($itemBiaya[$index]);
                $dataItem['subtotal_produksi'] = $dataItem['biaya_produksi']*$dataItem['qty'];
                $dataItem['remark'] = $itemRemark[$index];

                if (isset($itemId[$productKode])) {
                    //update
                    $itemDProduction = DetailProduction::where('kd_detail_production', $itemId[$productKode])->where('kd_produksi', $produksi->kd_produksi)->first();
                    if ($request->hasFile('itemDesign.'.$index)) {
                        if ($itemDProduction->img_design != '') {
                            DetailProduction::deleteAttachment($itemDProduction->img_design);
                        }
                        $dataItem['img_design'] = DetailProduction::saveAttachment($request->file('itemDesign.'.$index), $produksi->kd_produksi.'-'.str_random(8));
                    }
                    $itemDProduction->update($dataItem);
                } else {
                    //insert
                    if ($request->hasFile('itemDesign.'.$index)) {
                        $dataItem['img_design'] = DetailProduction::saveAttachment($request->file('itemDesign.'.$index), $produksi->kd_produksi.'-'.str_random(8));
                    } elseif ($request->has('costingItemDesign.'.$productKode)) {
                        if ($request->costingItemDesign[$productKode] != '') {
                            $dataItem['img_design'] = DetailProduction::saveFromCosting($request->costingItemDesign[$productKode]);
                        }
                    } else {
                        $dataItem['img_design'] = '';
                    }
                    $dataItem['kd_produksi'] = $produksi->kd_produksi;
                    DetailProduction::create($dataItem);
                }
            }
        }
    }

    protected function manageDeleteItem($deletedItem) {
        foreach ($deletedItem as $dp) {
            if ($dp->img_design != '') {
                DetailProduction::deleteAttachment($dp->img_design);
            }
            $dp->delete();
        }
    }

    protected function upsertMaterial($request, $produksi) {
        if ($request->has('materialId')) {
            $materialId = $request->materialId;
        } else {
            $materialId = [];
        }

        $materialKode = $request->materialKode;
        $materialQtyKebutuhan = $request->materialQtyKebutuhan;
        $materialKebutuhan = $request->materialKebutuhan;

        $materialRules = [
            'kd_material' => 'required|exists:tb_material,kd_material',
            'qty_kebutuhan' => 'required|numeric'
        ];
        foreach ($materialKode as $index => $kdmaterial) {
            $dataMaterial = [];
            $dataMaterial['kd_material'] = $kdmaterial;
            $dataMaterial['qty_kebutuhan'] = $materialQtyKebutuhan[$index];
            $materialValidator = \Validator::make($dataMaterial, $materialRules);
            if (!$materialValidator->fails()) {
                $material = Material::where('kd_material', $kdmaterial)->first();
                $dataMaterial['nama_material'] = $material->name;
                $dataMaterial['kd_satuan'] = $material->kd_satuan;
                $dataMaterial['nama_satuan'] = $material->satuan ? $material->satuan->satuan : '';
                $dataMaterial['kebutuhan'] = $materialKebutuhan[$index];

                if (isset($materialId[$kdmaterial])) {
                    //update
                    $materialProduction = ProductionMaterial::where('id', $materialId[$kdmaterial])->where('kd_produksi', $produksi->kd_produksi)->first();

                    if ($materialProduction->kd_material == $kdmaterial) {
                        $dataMaterial['qty_tersedia'] = $material->qty + $materialProduction->qty_penggunaan_material;
                        $dataMaterial['qty_kurang'] = $dataMaterial['qty_kebutuhan'] > $dataMaterial['qty_tersedia'] ? ($dataMaterial['qty_kebutuhan'] - $dataMaterial['qty_tersedia']) : 0;
                        $dataMaterial['qty_penggunaan_material'] = $dataMaterial['qty_kebutuhan'] > $dataMaterial['qty_tersedia'] ? $dataMaterial['qty_tersedia'] : $dataMaterial['qty_kebutuhan'];

                        //jika material tidak diubah
                        $material = $materialProduction->material;
                        if ($materialProduction->qty_penggunaan_material > $dataMaterial['qty_penggunaan_material']) {
                            //tambah stock material
                            $qtyselisih = $materialProduction->qty_penggunaan_material - $dataMaterial['qty_penggunaan_material'];
                            $laststock = $material->qty + $qtyselisih;
                            $material->update(['qty' => $laststock]);

                            $dtgflow = [];
                            $dtgflow['tgl'] = date('Y-m-d H:i:s');
                            $dtgflow['kode'] = $material->kd_material;
                            $dtgflow['nama'] = $material->name;
                            $dtgflow['qty_in'] = $qtyselisih;
                            $dtgflow['satuan_in'] = $material->satuan->satuan;
                            $dtgflow['qty_out'] = 0;
                            $dtgflow['satuan_out'] = $material->satuan->satuan;
                            $dtgflow['last_stock'] = $laststock;
                            $dtgflow['keterangan'] = 'Material In';
                            $dtgflow['remark'] = 'Change Production #'.$produksi->kd_produksi;
                            $dtgflow['kd_location'] = auth()->user()->kd_location;
                            $dtgflow['type_flow'] = 'material';
                            GoodsFlow::create($dtgflow);
                        } elseif ($materialProduction->qty_penggunaan_material < $dataMaterial['qty_penggunaan_material']) {
                            //kurangi stock material
                            $qtyselisih = intval($dataMaterial['qty_penggunaan_material']) - $materialProduction->qty_penggunaan_material;
                            $laststock = $material->qty > $qtyselisih ?  ($material->qty - $qtyselisih) : 0;
                            $material->update(['qty' => $laststock]);

                            $dtgflow = [];
                            $dtgflow['tgl'] = date('Y-m-d H:i:s');
                            $dtgflow['kode'] = $material->kd_material;
                            $dtgflow['nama'] = $material->name;
                            $dtgflow['qty_in'] = 0;
                            $dtgflow['satuan_in'] = $material->satuan->satuan;
                            $dtgflow['qty_out'] = $qtyselisih;
                            $dtgflow['satuan_out'] = $material->satuan->satuan;
                            $dtgflow['last_stock'] = $laststock;
                            $dtgflow['keterangan'] = 'Material Out';
                            $dtgflow['remark'] = 'Change Production #'.$produksi->kd_produksi;
                            $dtgflow['kd_location'] = auth()->user()->kd_location;
                            $dtgflow['type_flow'] = 'material';
                            GoodsFlow::create($dtgflow);
                        }
                    } else {
                        //jika material diubah
                        //kembalikan stock material sebelumnya
                        $oldMaterial = $materialProduction->material;
                        $laststock = $oldMaterial->qty + $materialProduction->qty_penggunaan_material;
                        $oldMaterial->update(['qty' => $laststock]);

                        $dtgflow = [];
                        $dtgflow['tgl'] = date('Y-m-d H:i:s');
                        $dtgflow['kode'] = $oldMaterial->kd_material;
                        $dtgflow['nama'] = $oldMaterial->name;
                        $dtgflow['qty_in'] = $materialProduction->qty_penggunaan_material;
                        $dtgflow['satuan_in'] = $oldMaterial->satuan->satuan;
                        $dtgflow['qty_out'] = 0;
                        $dtgflow['satuan_out'] = $oldMaterial->satuan->satuan;
                        $dtgflow['last_stock'] = $laststock;
                        $dtgflow['keterangan'] = 'Material In';
                        $dtgflow['remark'] = 'Change Production #'.$produksi->kd_produksi;
                        $dtgflow['kd_location'] = auth()->user()->kd_location;
                        $dtgflow['type_flow'] = 'material';
                        GoodsFlow::create($dtgflow);

                        //kurangi material baru
                        $dataMaterial['qty_tersedia'] = $material->qty;
                        $dataMaterial['qty_kurang'] = $dataMaterial['qty_kebutuhan'] > $material->qty ? ($dataMaterial['qty_kebutuhan'] - $material->qty) : 0;
                        $dataMaterial['qty_penggunaan_material'] = $dataMaterial['qty_kebutuhan'] > $dataMaterial['qty_tersedia'] ? $dataMaterial['qty_tersedia'] : $dataMaterial['qty_kebutuhan'];

                        $laststock = $material->qty > $dataMaterial['qty_penggunaan_material'] ? ($material->qty - $dataMaterial['qty_penggunaan_material']) : 0;
                        $material->update(['qty' => $laststock]);

                        $dtgflow = [];
                        $dtgflow['tgl'] = date('Y-m-d H:i:s');
                        $dtgflow['kode'] = $material->kd_material;
                        $dtgflow['nama'] = $material->name;
                        $dtgflow['qty_in'] = 0;
                        $dtgflow['satuan_in'] = $material->satuan->satuan;
                        $dtgflow['qty_out'] = $dataMaterial['qty_penggunaan_material'];
                        $dtgflow['satuan_out'] = $material->satuan->satuan;
                        $dtgflow['last_stock'] = $laststock;
                        $dtgflow['keterangan'] = 'Material Out';
                        $dtgflow['remark'] = 'Change Production #'.$produksi->kd_produksi;
                        $dtgflow['kd_location'] = auth()->user()->kd_location;
                        $dtgflow['type_flow'] = 'material';
                        GoodsFlow::create($dtgflow);
                    }
                    $materialProduction->update($dataMaterial);
                } else {
                    //insert
                    $dataMaterial['qty_tersedia'] = $material->qty;
                    $dataMaterial['qty_kurang'] = $dataMaterial['qty_kebutuhan'] > $material->qty ? ($dataMaterial['qty_kebutuhan'] - $material->qty) : 0;
                    $dataMaterial['qty_penggunaan_material'] = $dataMaterial['qty_kebutuhan'] > $dataMaterial['qty_tersedia'] ? $dataMaterial['qty_tersedia'] : $dataMaterial['qty_kebutuhan'];

                    $dataMaterial['kd_produksi'] = $produksi->kd_produksi;
                    ProductionMaterial::create($dataMaterial);

                    //kurangi material
                    $laststock = $material->qty > $dataMaterial['qty_penggunaan_material'] ? ($material->qty - $dataMaterial['qty_penggunaan_material']) : 0;
                    $material->update(['qty' => $laststock]);

                    $dtgflow = [];
                    $dtgflow['tgl'] = date('Y-m-d H:i:s');
                    $dtgflow['kode'] = $material->kd_material;
                    $dtgflow['nama'] = $material->name;
                    $dtgflow['qty_in'] = 0;
                    $dtgflow['satuan_in'] = $material->satuan->satuan;
                    $dtgflow['qty_out'] = $dataMaterial['qty_penggunaan_material'];
                    $dtgflow['satuan_out'] = $material->satuan->satuan;
                    $dtgflow['last_stock'] = $laststock;
                    $dtgflow['keterangan'] = 'Material Out';
                    $dtgflow['remark'] = 'Add Production #'.$produksi->kd_produksi;
                    $dtgflow['kd_location'] = auth()->user()->kd_location;
                    $dtgflow['type_flow'] = 'material';
                    GoodsFlow::create($dtgflow);
                }
            }
        }
    }

    protected function manageDeleteMaterial($deletedMaterial, $kdproduksi) {
        foreach ($deletedMaterial as $dpm) {
            $material = Material::where('kd_material', $dpm->kd_material)->first();
            $laststock = $material->qty + $dpm->qty_penggunaan_material;
            $material->update(['qty' => $laststock]);

            $dtgflow = [];
            $dtgflow['tgl'] = date('Y-m-d H:i:s');
            $dtgflow['kode'] = $material->kd_material;
            $dtgflow['nama'] = $material->name;
            $dtgflow['qty_in'] = $dpm->qty_penggunaan_material;
            $dtgflow['satuan_in'] = $material->satuan->satuan;
            $dtgflow['qty_out'] = 0;
            $dtgflow['satuan_out'] = $material->satuan->satuan;
            $dtgflow['last_stock'] = $laststock;
            $dtgflow['keterangan'] = 'Material In';
            $dtgflow['remark'] = 'Change Production #'.$kdproduksi;
            $dtgflow['kd_location'] = auth()->user()->kd_location;
            $dtgflow['type_flow'] = 'material';
            GoodsFlow::create($dtgflow);

            $dpm->delete();
        }
    }

    public function publish(Request $request, $id) {
        $production = Production::where('id', $id)->where('status', 'draft')->firstOrFail();
        $production->update(['status' => 'on_progress']);
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The production status has been changed to On Progress.']);
        return redirect()->route('produksi.index');
    }

    public function setfinish(Request $request, $id) {
        $production = Production::where('id', $id)->where('status', 'on_progress')->firstOrFail();
        $production->update(['status' => 'finish']);

        //act to accounting

        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Production status has been changed to finish.']);
        return redirect()->route('produksi.index');
    }

    public function progress($id) {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $produksi = Production::where('kd_produksi', $id)->whereIn('status', ['on_progress', 'finish'])->firstOrFail();
        $itemProduksi = $produksi->detail_production;
        $historyProduction = $produksi->history_production;
        return view('toko.production.produksi.progress', compact('produksi', 'itemProduksi', 'historyProduction', 'mlayout'));
    }

    public function updateProgress(Request $request, $id) {
        $produksi = Production::where('kd_produksi', $id)->where('status', 'on_progress')->firstOrFail();
        $newProgress = $request->newProgress;
        $totalSave = 0;
        foreach ($newProgress as $index => $val) {
            if ($val > 0) {
                $dtlProduksi = DetailProduction::where('kd_produksi', $produksi->kd_produksi)->where('kd_detail_production', $index)->first();
                if (!empty($dtlProduksi)) {
                    $dtlProduksi->update(['qty_progress' => $dtlProduksi->qty_progress + $val]);

                    $dtHistory = [];
                    $dtHistory['id'] = uniqid('', true);
                    $dtHistory['tanggal'] = $request->tanggal;
                    $dtHistory['kd_produk'] = $dtlProduksi->kd_produk;
                    $dtHistory['deskripsi'] = $dtlProduksi->deskripsi;
                    $dtHistory['qty_progress'] = $val;
                    $dtHistory['kd_produksi'] = $produksi->kd_produksi;
                    HistoryProductionProgress::create($dtHistory);

                    $totalSave++;
                }
            }
        }

        if ($totalSave > 0) {
            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => $totalSave.' progress data has been saved']);
        } else {
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error!', 'message' => 'There is no data progress stored']);
        }
        return redirect()->route('produksi.progress', $id);
    }
}
