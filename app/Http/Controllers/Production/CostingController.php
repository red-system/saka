<?php

namespace App\Http\Controllers\Production;

use App\Models\Costing;
use App\Models\CostingMaterial;
use App\Models\DetailCosting;
use App\Models\Material;
use App\Models\Product;
use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $costings = Costing::whereNotIn('status', ['finish', 'cancel'])->get();
        $historycostings = Costing::whereIn('status', ['finish', 'cancel'])->get();
        return view('toko.production.costing.index', compact('costings', 'historycostings', 'mlayout'));
    }

    protected function createKodeCosting() {
        $preffix = 'CST-'.date('ymd').'-';
        $lastdata = Costing::where(\DB::raw('LEFT(kd_costing, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_costing, 3) as kode_costing')->orderBy('kode_costing', 'desc')->first();

        if (!empty($lastdata)) {
            $now = intval($lastdata->kode_costing)+1;
            $no = str_pad($now, 3, '0', STR_PAD_LEFT);
        } else {
            $no = '001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $kodecosting = $this->createKodeCosting();

        $listproduk = [];
        if ($request->has('listproduk')) {
            $alrtPrd = explode(';', $request->listproduk);
            foreach ($alrtPrd as $aprd) {
                $prd = explode(':', $aprd);
                array_push($listproduk, ['kd_produk' => $prd[0], 'kebutuhan' => $prd[1]]);
            }
        }
        return view('toko.production.costing.create', compact('kodecosting', 'mlayout', 'listproduk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $dataCosting = [];
            $dataCosting['kd_costing'] = $this->createKodeCosting();
            $dataCosting['tgl_produksi'] = $request->tgl_produksi;
            $dataCosting['deadline'] = $request->deadline;

            $produsen = Produsen::where('kd_produsen', $request->kd_produsen)->first();
            $dataCosting['kd_produsen'] = !empty($produsen) ? $produsen->kd_produsen : $request->kd_produsen;
            $dataCosting['nama_produsen'] = !empty($produsen) ? $produsen->nama_produsen : '';

            $dataCosting['remark'] = $request->remark;
            $total = \Konversi::localcurrency_to_database($request->total);
            $ppnNominal = ($total*$request->ppn_persen)/100;
            $dataCosting['total'] = $total;
            $dataCosting['ppn_persen'] = $request->ppn_persen;
            $dataCosting['ppn_nominal'] = $ppnNominal;
            $dataCosting['grand_total'] = $total + $ppnNominal;
            $dataCosting['status'] = 'draft';
            $costing = Costing::create($dataCosting);

            $this->upsertItem($request, $costing);

            $this->upsertMaterial($request, $costing);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data costing successfully saved']);
            return redirect()->route('costing.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $costing = Costing::where('kd_costing', $id)->firstOrFail();
        $itemCosting = $costing->detail_costing;
        $materialCosting = $costing->costing_material;
        return view('toko.production.costing.show', compact('costing', 'itemCosting', 'materialCosting'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (\Gate::allows('as_production')) {
            $mlayout = 'layouts.master-topnav';
        } else {
            $mlayout = 'layouts.master-toko';
        }
        $costing = Costing::where('kd_costing', $id)->where('status', 'draft')->firstOrFail();
        $itemCosting = $costing->detail_costing;
        $materialCosting = $costing->costing_material;
        return view('toko.production.costing.edit', compact('costing', 'itemCosting', 'materialCosting', 'mlayout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $costing = Costing::where('kd_costing', $id)->where('status', 'draft')->firstOrFail();
            $dataCosting = [];
            $dataCosting['tgl_produksi'] = $request->tgl_produksi;
            $dataCosting['deadline'] = $request->deadline;

            $produsen = Produsen::where('kd_produsen', $request->kd_produsen)->first();
            $dataCosting['kd_produsen'] = !empty($produsen) ? $produsen->kd_produsen : $request->kd_produsen;
            $dataCosting['nama_produsen'] = !empty($produsen) ? $produsen->nama_produsen : '';

            $dataCosting['remark'] = $request->remark;
            $total = \Konversi::localcurrency_to_database($request->total);
            $ppnNominal = ($total*$request->ppn_persen)/100;
            $dataCosting['total'] = $total;
            $dataCosting['ppn_persen'] = $request->ppn_persen;
            $dataCosting['ppn_nominal'] = $ppnNominal;
            $dataCosting['grand_total'] = $total + $ppnNominal;
            $costing->update($dataCosting);

            $deletedItem = DetailCosting::where('kd_costing', $costing->kd_costing)->whereNotIn('id', $request->itemId)->get();
            if ($deletedItem->count() > 0) {
                $this->manageDeleteItem($deletedItem);
            }
            $this->upsertItem($request, $costing);

            if (CostingMaterial::where('kd_costing', $costing->kd_costing)->whereNotIn('id', $request->materialId)->count() > 0) {
                CostingMaterial::where('kd_costing', $costing->kd_costing)->whereNotIn('id', $request->materialId)->delete();
            }
            $this->upsertMaterial($request, $costing);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Production data was successfully changed']);
            return redirect()->route('costing.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $costing = Costing::where('id', $id)->where('status', 'draft')->firstOrFail();
        \DB::beginTransaction();
        try {
            $detailCosting = DetailCosting::where('kd_costing', $costing->kd_costing)->get();
            if ($detailCosting->count() > 0) {
                foreach ($detailCosting as $dc) {
                    if ($dc->img_design != '') {
                        DetailCosting::deleteAttachment($dc->img_design);
                    }
                    $dc->delete();
                }
            }

            if (CostingMaterial::where('kd_costing', $costing->kd_costing)->count() > 0) {
                CostingMaterial::where('kd_costing', $costing->kd_costing)->delete();
            }

            $costing->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data costing has been deleted']);
            return redirect()->route('costing.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    public function publish(Request $request, $id) {
        $costing = Costing::where('id', $id)->where('status', 'draft')->firstOrFail();
        $costing->update(['status' => 'on_progress']);
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'The status costing has been changed to On Progress.']);
        return redirect()->route('costing.index');
    }

    public function cancel(Request $request, $id) {
        $costing = Costing::where('id', $id)->where('status', 'on_progress')->firstOrFail();
        $costing->update(['status' => 'cancel']);
        \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data costing has been canceled.']);
        return redirect()->route('costing.index');
    }

    public function getListMaterial(Request $request) {
        $reqproduk = rtrim($request->produk, '|');
        $products = explode('|', $reqproduk);

        $allMaterial = [];
        foreach ($products as $product) {
            $prd = explode(',', $product);
            $dtPrd = Product::where('kd_produk', $prd[0])->first();

            $komposisiPrd = $dtPrd->komposisi_product;
            if($komposisiPrd->count() > 0) {
                foreach ($komposisiPrd as $kmpPrd) {
                    if (isset($allMaterial[$kmpPrd->kd_material])) {
                        $kebutuhan = $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] + $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;

                        $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] = $kebutuhan;
                        $allMaterial[$kmpPrd->kd_material]['qty_tersedia'] = $tersedia;
                        $allMaterial[$kmpPrd->kd_material]['qty_kurang'] = $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0;
                    } else {
                        $kebutuhan = $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;
                        $allMaterial[$kmpPrd->kd_material] = ['kd_material' => $kmpPrd->kd_material, 'view_material' => $kmpPrd->kd_material.' - '.$kmpPrd->material->name, 'kd_satuan' => $kmpPrd->kd_satuan, 'satuan' => $kmpPrd->satuan->satuan, 'qty_kebutuhan' => $kebutuhan, 'qty_tersedia' => $tersedia, 'qty_kurang' => $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0];
                    }
                }
            }
        }
        return view('toko.production.costing._listmaterial', compact('allMaterial'));
    }

    public function getListMaterialEdit(Request $request, $kdcosting) {
        $costing = Costing::where('kd_costing', $kdcosting)->firstOrFail();
        $oldCostingMaterial = $costing->costing_material;

        $reqproduk = rtrim($request->produk, '|');
        $products = explode('|', $reqproduk);

        $allMaterial = [];
        foreach ($products as $product) {
            $prd = explode(',', $product);
            $dtPrd = Product::where('kd_produk', $prd[0])->first();

            $komposisiPrd = $dtPrd->komposisi_product;
            if($komposisiPrd->count() > 0) {
                foreach ($komposisiPrd as $kmpPrd) {
                    if (isset($allMaterial[$kmpPrd->kd_material])) {
                        $kebutuhan = $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] + $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;

                        $allMaterial[$kmpPrd->kd_material]['qty_kebutuhan'] = $kebutuhan;
                        $allMaterial[$kmpPrd->kd_material]['qty_tersedia'] = $tersedia;
                        $allMaterial[$kmpPrd->kd_material]['qty_kurang'] = $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0;
                    } else {
                        $kebutuhan = $kmpPrd->qty*$prd[1];
                        $tersedia = $kmpPrd->material->qty;
                        $allMaterial[$kmpPrd->kd_material] = ['kd_material' => $kmpPrd->kd_material, 'view_material' => $kmpPrd->kd_material.' - '.$kmpPrd->material->name, 'kd_satuan' => $kmpPrd->kd_satuan, 'satuan' => $kmpPrd->satuan->satuan, 'qty_kebutuhan' => $kebutuhan, 'qty_tersedia' => $tersedia, 'qty_kurang' => $kebutuhan > $tersedia ? ($kebutuhan - $tersedia) : 0];
                    }
                }
            }
        }
        return view('toko.production.costing._listmaterialedit', compact('allMaterial', 'oldCostingMaterial'));
    }

    public function getDetailMaterial(Request $request) {
        $material = Material::where('kd_material', $request->kdmaterial)->firstOrFail();
        return ['namaMaterial' => $material->name, 'satuanMaterial' => $material->satuan->satuan, 'qtyStock' => $material->qty];
    }

    public function upsertItem($request, $costing) {
        if ($request->has('itemId')) {
            $itemId = $request->itemId;
        } else {
            $itemId = [];
        }

        $itemProductKode = $request->productKode;
        $itemQty = $request->itemQty;
        $itemBiaya = $request->itemBiaya;

        $itemRules = [
            'kd_produk' => 'required',
            'qty' => 'required|numeric'
        ];
        foreach ($itemProductKode as $index => $productKode) {
            $dataItem = [];
            $dataItem['kd_produk'] = $productKode;
            $dataItem['qty'] = $itemQty[$index];
            $itemValidator = \Validator::make($dataItem, $itemRules);
            if (!$itemValidator->fails()) {
                $product = Product::where('kd_produk', $productKode)->first();
                $dataItem['deskripsi'] = $product->deskripsi;
                $dataItem['kd_kat'] = $product->kd_kat;
                $dataItem['nama_kategori'] = $product->kategori ? $product->kategori->kategori : '';
                $dataItem['material'] = $product->material;

                $dataItem['biaya_produksi'] = \Konversi::localcurrency_to_database($itemBiaya[$index]);
                $dataItem['subtotal_produksi'] = $dataItem['biaya_produksi']*$dataItem['qty'];

                if (isset($itemId[$productKode])) {
                    //update
                    $itemCosting = DetailCosting::where('id', $itemId[$productKode])->where('kd_costing', $costing->kd_costing)->first();
                    if ($request->hasFile('itemDesign.'.$index)) {
                        if ($itemCosting->img_design != '') {
                            DetailCosting::deleteAttachment($itemCosting->img_design);
                        }
                        $dataItem['img_design'] = DetailCosting::saveAttachment($request->file('itemDesign.'.$index), $costing->kd_costing.'-'.str_random(8));
                    }
                    $itemCosting->update($dataItem);
                } else {
                    //insert
                    if ($request->hasFile('itemDesign.'.$index)) {
                        $dataItem['img_design'] = DetailCosting::saveAttachment($request->file('itemDesign.'.$index), $costing->kd_costing.'-'.str_random(8));
                    } else {
                        $dataItem['img_design'] = '';
                    }
                    $dataItem['kd_costing'] = $costing->kd_costing;
                    DetailCosting::create($dataItem);
                }
            }
        }
    }

    protected function manageDeleteItem($deletedItem) {
        foreach ($deletedItem as $dc) {
            if ($dc->img_design != '') {
                DetailCosting::deleteAttachment($dc->img_design);
            }
            $dc->delete();
        }
    }

    protected function upsertMaterial($request, $costing) {
        if ($request->has('materialId')) {
            $materialId = $request->materialId;
        } else {
            $materialId = [];
        }

        $materialKode = $request->materialKode;
        $materialQtyKebutuhan = $request->materialQtyKebutuhan;

        $materialRules = [
            'kd_material' => 'required|exists:tb_material,kd_material',
            'qty_kebutuhan' => 'required|numeric'
        ];
        foreach ($materialKode as $index => $kdmaterial) {
            $dataMaterial = [];
            $dataMaterial['kd_material'] = $kdmaterial;
            $dataMaterial['qty_kebutuhan'] = $materialQtyKebutuhan[$index];
            $materialValidator = \Validator::make($dataMaterial, $materialRules);
            if (!$materialValidator->fails()) {
                $material = Material::where('kd_material', $kdmaterial)->first();
                $dataMaterial['nama_material'] = $material->name;
                $dataMaterial['kd_satuan'] = $material->kd_satuan;
                $dataMaterial['nama_satuan'] = $material->satuan ? $material->satuan->satuan : '';

                $dataMaterial['qty_tersedia'] = $material->qty;
                $dataMaterial['qty_kurang'] = $dataMaterial['qty_kebutuhan'] > $material->qty ? ($dataMaterial['qty_kebutuhan'] - $material->qty) : 0;

                if (isset($materialId[$kdmaterial])) {
                    //update
                    $materialCosting = CostingMaterial::where('id', $materialId[$kdmaterial])->where('kd_costing', $costing->kd_costing)->first();
                    $materialCosting->update($dataMaterial);
                } else {
                    //insert
                    $dataMaterial['kd_costing'] = $costing->kd_costing;
                    CostingMaterial::create($dataMaterial);
                }
            }
        }
    }
}
