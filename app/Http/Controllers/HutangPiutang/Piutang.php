<?php

namespace App\Http\Controllers\HutangPiutang;

use Illuminate\Http\Request;
// use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\JurnalHarian;
use App\Models\Transaksi;
use App\Models\MasterCoa;
use App\Models\mDetailPerkiraan;
use App\Models\mPembayaranHutangPiutang;
use App\Models\Supplier;
use App\Models\mKaryawan;
use App\Models\mHutang;
use App\Models\mPiutang;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Validator;
use Redirect;
use PDF;
use DB;
// use Barryvdh\DomPDF;

class Piutang extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        // $this->main         = new Main();
        $this->title        = 'AR (Sales)';
        $this->kodeLabel    = '';
    }

    public function addForm() {
        return view('toko.hutang-piutang._add-form')->render();
    }

    function index() {
        
        $data['title']              = $this->title;
        $data['data_piutang']        = mPiutang::where('no_faktur','like','%SLC%')->orderBy('due_date','ASC')->get();;
        $data['menu']               = 'Hutang Piutang';


        // return view('hutang-piutang/penerimaan-hutang-piutang', $data);
        return view('toko.hutang-piutang.piutang.index', $data);
    }

    function piutang_lain() {
        
        $data['title']              = 'AR (Others)';
        $data['data_piutang']        = mPiutang::where('no_faktur','like','%AR%')->get();
        $data['menu']               = 'Hutang Piutang';


        // return view('hutang-piutang/penerimaan-hutang-piutang', $data);
        return view('toko.hutang-piutang.piutang.index', $data);
    }

    protected function get_no_pembayaran($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('ym');
        $lastorder = mPembayaranHutangPiutang::where('no_pembayaran', 'like', '%'.$preffix)->max('no_pembayaran');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    protected function getNoTransaksiAR($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('my');
        $lastorder = mPiutang::where('no_piutang_pelanggan', 'like', '%'.$preffix.'%')->max('no_piutang_pelanggan');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    function get_data($kode='') {
        
        $data['hutang'] = mPiutang::find($kode);
        $data['perkiraan']  = MasterCoa::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = 'BR';
        $data['no_transaksi'] = $this->get_no_pembayaran($kode);
        return view('toko.hutang-piutang.piutang.payment', $data);
    }

    function rules($request) {
        $rules = [
            'tgl_transaksi'             => 'required',
            'kode_bukti'                => 'required',
            'keterangan'                => 'required',
        ];

        $customeMessage = [
            'required'  =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    public function insert(Request $request){
        // $this->rules($request->all());

        \DB::beginTransaction();
        try {
            $date_db            = date('Y-m-d H:i:s');
            // Jurnal Umum
          //$kode_bukti_id = $request->input('kode_bukti_id');

          // Transaksi
            $no_transaksi          = $request->input('no_transaksi');
            $master_id          = $request->input('master_id');
            $payment            = $request->input('payment');
            $charge             = $request->input('charge');
            $payment_total      = $request->input('payment_total');
            $no_check_bg        = $request->input('no_check_bg');
            $tgl_pencairan      = $request->input('tgl_pencairan');
            $tgl_transaksi      = $request->input('tgl_transaksi');
            $setor              = $request->input('setor');
            $keterangan         = $request->input('keterangan');
            $tipe_arus_kas      = "Operasi";
            $kode_perkiraan     = $request->input('kode_perkiraan');
            $pp_invoice         = $request->input('pp_invoice');
            $no_faktur          = $request->input('no_piutang_pelanggan');
            $cus_kode           = $request->input('cus_kode');
            // $cus                = mCustomer::find($cus_kode)->first();
            $cus_nama           = $request->input('atas_nama');
            $kode_perkiraan           = $request->input('kode_perkiraan');

            $tipe_pembayaran           = $request->input('tipe_pembayaran');
            // $data['kodeCustomer'] = $this->main->kodeLabel('customer');


          // General
            $year               = date('Y');
            $month              = date('m');
            $day                = date('d');
            $where              = [
                'jmu_year'      => $year,
                'jmu_month'     => $month,
                'jmu_day'       => $day
            ];

            $last_no_jurnal     = JurnalHarian::where('jmu_tanggal',$tgl_transaksi)->max('jmu_no');
            $jmu_no             = $last_no_jurnal+1;


            // $data_jurnal                    = new JurnalHarian();
            // $data_jurnal->no_jurnal        = $no_transaksi;
            // // $data_jurnal->status            = 'pending';
            // $data_jurnal->id_pel            = $cus_kode;
            // $data_jurnal->jmu_tanggal       = $tgl_transaksi;
            // $data_jurnal->jmu_no            = $jmu_no;
            // $data_jurnal->jmu_keterangan        = 'Pembayaran Piutang';
            // $data_jurnal->jmu_year              = date('Y', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_month         = date('m', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_day           = date('d', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_date_insert   = date('Y-m-d H:i:s');
            // // $data_jurnal->jmu_date_update   = date('Y-m-d H:i:s');
            // $data_jurnal->created_by        = auth()->user()->name;
            // $data_jurnal->updated_by        = auth()->user()->name;
            // $data_jurnal->reference_number  = $kode_bukti;
            // $data_jurnal->save();
            // $jurnal_umum_id = $data_jurnal->id_jurnal;

            $pp_amountAwal          = mPiutang::where('id',$pp_invoice)->first();
            $pp_amount              = $pp_amountAwal['sisa_piutang'];
            $total_payment          = 0;

            foreach($master_id as $key => $val) {
                // $master                 = MasterCoa::find($master_id[$key]);
                // $trs_kode_rekening      = $master->mst_kode_rekening;
                // $trs_nama_rekening      = $master->mst_nama_rekening;

                // $data_transaksi             = [
                //     'id_jurnal_umum'        => $jurnal_umum_id,
                //     'master_id'             => $master_id[$key],
                //     'trs_jenis_transaksi'   => 'debet',
                //     'trs_debet'             => $payment[$key],
                //     'trs_kredit'            => 0,
                //     'user_id'               => 0,
                //     'trs_year'              => $year,
                //     'trs_month'             => $month,
                //     'trs_kode_rekening'     => $trs_kode_rekening,
                //     'trs_nama_rekening'     => $trs_nama_rekening,
                //     'trs_tipe_arus_kas'     => $tipe_arus_kas,
                //     'trs_catatan'           => $keterangan[$key],
                //     'trs_date_insert'       => $date_db,
                //     'trs_date_update'       => $date_db,
                //     'trs_charge'            => 0,
                //     'trs_no_check_bg'       => $no_check_bg[$key],
                //     'trs_tgl_pencairan'     => date('Y-m-d'),
                //     'trs_setor'             => $payment[$key],
                //     'tgl_transaksi'         => $tgl_transaksi
                // ];
                // Transaksi::insert($data_transaksi);

                
                $total_payment      = $total_payment + $payment[$key];
                $pp_amount          = $pp_amount - $payment[$key];
                $angka_pp_amount    = number_format($pp_amount,2);
                    
                if($angka_pp_amount == '0.00'){
                    mPiutang::where('id', $pp_invoice)->update(['sisa_piutang'=>$pp_amount,'pp_status'=>'Lunas']);
                }else{
                    mPiutang::where('id', $pp_invoice)->update(['sisa_piutang'=>$pp_amount]);
                }
                //$terbayar = $payment[$key];
                $terbayar = $total_payment;

                // if($trs_kode_rekening=='1303'){
                //     $hutang_cek            = new mHutangCek([
                //         'no_cek_bg'             => $no_cek_bg[$key],
                //         'tgl_pencairan'         => date('Y-m-d', strtotime($tgl_pencairan[$key])),
                //         'total_cek'             => $payment[$key],
                //         'nama_bank'             => $nama_bank[$key],
                //         'cek_untuk'             => $spl_nama,
                //         'keterangan_cek'        => $keterangan[$key],
                //         'created_at'            => date('Y-m-d'),
                //         'created_by'            => auth()->user()->Karyawan->kry_nama,
                //         'updated_at'            => date('Y-m-d'),
                //         'updated_by'            => auth()->user()->Karyawan->kry_nama
                //     ]);
        
                //     $hutang_cek->save();
                // }

                $pembayaran = new mPembayaranHutangPiutang();
                $pembayaran->no_pembayaran      = $no_transaksi;
                $pembayaran->tanggal      = $tgl_transaksi;
                $pembayaran->nominal_bayar      = $payment[$key];
                $pembayaran->id_hutang_piutang      = $pp_invoice;
                $pembayaran->jenis      = 'piutang';
                $pembayaran->created_at      = date('Y-m-d H:i:s');
                $pembayaran->created_by      = auth()->user()->name;
                $pembayaran->tipe_pembayaran = $tipe_pembayaran[$key];
                $pembayaran->save();

                

            }

            // $master_piutang                = MasterCoa::where('mst_kode_rekening', $kode_perkiraan)->first();
            // $master_id_piutang             = $master_piutang->master_id;
            // $trs_kode_rekening_piutang     = $master_piutang->mst_kode_rekening;
            // $trs_nama_rekening_piutang     = $master_piutang->mst_nama_rekening;

            // $data_transaksi_piutang    = [
            //     'id_jurnal_umum'      => $jurnal_umum_id,
            //     'master_id'           => $master_id_piutang,
            //     'trs_jenis_transaksi' => 'kredit',
            //     'trs_debet'           => 0,
            //     'trs_kredit'          => $total_payment,
            //     'user_id'             => 0,
            //     'trs_year'            => $year,
            //     'trs_month'           => $month,
            //     'trs_kode_rekening'   => $trs_kode_rekening_piutang,
            //     'trs_nama_rekening'   => $trs_nama_rekening_piutang,
            //     'trs_tipe_arus_kas'   => $tipe_arus_kas,
            //     'trs_catatan'         => "Pembayaran Piutang",
            //     'trs_date_insert'     => $date_db,
            //     'trs_date_update'     => $date_db,
            //     'trs_charge'          => 0,
            //     'trs_no_check_bg'     => 0,
            //     'trs_tgl_pencairan'   => date('Y-m-d'),
            //     'trs_setor'           => $total_payment,
            //     'tgl_transaksi'         => $tgl_transaksi
            // ];
            // Transaksi::insert($data_transaksi_piutang);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data berhasil disimpan']);

            return [
                'redirect'=>route('piutang-pelanggan.index')
            ];
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }

        
    }
}
