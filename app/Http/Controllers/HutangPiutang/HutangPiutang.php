<?php

namespace App\Http\Controllers\HutangPiutang;

use Illuminate\Http\Request;
// use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\JurnalHarian;
use App\Models\Transaksi;
use App\Models\MasterCoa;
use App\Models\mDetailPerkiraan;
use App\Models\mPeriode;
use App\Models\Supplier;
use App\Models\mKaryawan;
use App\Models\mHutang;
use App\Models\mPiutang;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Validator;
use Redirect;
use PDF;
use DB;
// use Barryvdh\DomPDF;

class HutangPiutang extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        // $this->main         = new Main();
        $this->title        = 'Penerimaan Hutang/Piutang';
        $this->kodeLabel    = '';
    }

    public function addForm() {
        return view('toko.hutang-piutang._add-form')->render();
    }

    function index() {
        // $breadcrumb                     = [
        //     'Hutang/Piutang'                => '',
        //     'Penerimaan Hutang/Piutang'     => route('penerimaan-hutang-piutang')
        // ];
        // $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
        // $data['menu']               = 'hutangSuplier';
        // $data['bulan']              = date('m');
        // $data['tahun_periode']      = date('Y');
        $data['title']              = $this->title;
        $data['akun']            = MasterCoa::orderBy('mst_kode_rekening','ASC')->where('mst_posisi','asset')->orWhere('mst_posisi','liabilitas')->get();
        $data['akun_asset']         = MasterCoa::orderBy('mst_kode_rekening','ASC')->get();
        $data['supplier']           = Supplier::orderBy('id','ASC')->get();
        // $data['customer']           = mSupplier::orderBy('spl_kode','ASC')->get();
        $data['karyawan']           = Supplier::orderBy('id','ASC')->get();
        $data['no_ap']              = $this->getNoTransaksiAP('AP');
        $data['no_ar']              = $this->getNoTransaksiAR('AR');


        // return view('hutang-piutang/penerimaan-hutang-piutang', $data);
        return view('toko.hutang-piutang.penerimaan-hutang-piutang', $data);
    }

    protected function getNoTransaksiAP($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('my');
        $lastorder = mHutang::where('no_faktur_hutang', 'like', '%'.$preffix.'%')->max('no_faktur_hutang');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    protected function getNoTransaksiAR($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('ym');
        $lastorder = mPiutang::where('no_faktur', 'like', '%'.$preffix.'%')->max('no_faktur');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    function rules($request) {
        $rules = [
            'tgl_transaksi'             => 'required',
            'kode_bukti'                => 'required',
            'keterangan'                => 'required',
        ];

        $customeMessage = [
            'required'  =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        DB::beginTransaction();
        try{
            $this->rules($request->all());
            $jenis              = $request->jenis;
            $keterangan         = $request->keterangan;
            $kode_bukti         = $request->kode_bukti;
            $tgl_transaksi      = date('Y-m-d', strtotime($request->tgl_transaksi));
            $nama               = $request->nama;
            $atas_nama          = $request->atas_nama;
            $nama_akun          = $request->nama_akun;
            $total              = $request->total_debet;

            $master_id          = $request->input('master_id');
            $nominal            = $request->input('debet');
            $note               = $request->input('catatan');

            // $last_no_jurnal     = JurnalHarian::where('jmu_tanggal',$tgl_transaksi)->max('jmu_no');
            // $jmu_no             = $last_no_jurnal+1;


            // $data_jurnal                    = new JurnalHarian();
            // $data_jurnal->no_jurnal        = $kode_bukti;
            // // $data_jurnal->status            = 'pending';
            // $data_jurnal->id_pel            = $nama;
            // $data_jurnal->jmu_tanggal       = $tgl_transaksi;
            // $data_jurnal->jmu_no            = $jmu_no;
            // $data_jurnal->jmu_keterangan        = $keterangan;
            // $data_jurnal->jmu_year              = date('Y', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_month         = date('m', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_day           = date('d', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_date_insert   = date('Y-m-d H:i:s');
            // // $data_jurnal->jmu_date_update   = date('Y-m-d H:i:s');
            // $data_jurnal->created_by        = auth()->user()->name;
            // $data_jurnal->updated_by        = auth()->user()->name;
            // // $data_jurnal->reference_number  = $kode_bukti;
            // $data_jurnal->save();
            // $jurnal_umum_id = $data_jurnal->id_jurnal;

            if($jenis=='ap'){
                $trs_jenis_transaksi = 'kredit';
                $trs_debet           = 0;
                $trs_kredit          = $total;
                $catatan             = 'pembentukan akun payable';
            }else{
                $trs_jenis_transaksi = 'debet';
                $trs_kredit          = 0;
                $trs_debet           = $total;
                $catatan             = 'pembentukan akun receivable';
            }

            $akun       = MasterCoa::find($nama_akun);

            // //insert nama akun di tb_transaksi
            // $transaksi                              = new Transaksi();
            // $transaksi->id_jurnal_umum              = $jurnal_umum_id;
            // $transaksi->master_id                   = $nama_akun;
            // $transaksi->trs_jenis_transaksi         = $trs_jenis_transaksi;
            // $transaksi->tgl_transaksi               = $tgl_transaksi;
            // $transaksi->trs_debet                   = $trs_debet;
            // $transaksi->trs_kredit                  = $trs_kredit;
            // // $transaksi->user_id                     = auth()->user()->user_kode;
            // $transaksi->trs_year                    = date('Y', strtotime($tgl_transaksi));
            // $transaksi->trs_month                   = date('m', strtotime($tgl_transaksi));
            // $transaksi->trs_kode_rekening           = $akun->mst_kode_rekening;
            // $transaksi->trs_nama_rekening           = $akun->mst_nama_rekening;
            // $transaksi->trs_tipe_arus_kas           = 'operasi';
            // $transaksi->trs_catatan                 = $catatan;
            // $transaksi->trs_date_insert             = date('Y-m-d H:i:s');
            // $transaksi->trs_date_update             = date('Y-m-d H:i:s');
            // // $transaksi->trs_no_check_bg             = $jurnal_umum_id;
            // // $transaksi->trs_tgl_pencairan           = $jurnal_umum_id;
            // // $transaksi->trs_setor                   = $jurnal_umum_id;
            // // $transaksi->created_at                  = $jurnal_umum_id;
            // // $transaksi->updated_at                  = $jurnal_umum_id;
            // $transaksi->save();

            // foreach ($master_id as $key => $value) {
            //     if($jenis=='ar'){
            //         $trs_jenis_transaksi = 'kredit';
            //         $trs_debet           = 0;
            //         $trs_kredit          = $nominal[$key];
            //         $catatan             = $note[$key];
            //     }else{
            //         $trs_jenis_transaksi = 'debet';
            //         $trs_kredit          = 0;
            //         $trs_debet           = $nominal[$key];
            //         $catatan             = $note[$key];
            //     }

            //     $akun_2       = MasterCoa::find($master_id[$key]);

            //     //insert nama akun di tb_transaksi
            //     $transaksi                              = new Transaksi();
            //     $transaksi->id_jurnal_umum              = $jurnal_umum_id;
            //     $transaksi->master_id                   = $master_id[$key];
            //     $transaksi->trs_jenis_transaksi         = $trs_jenis_transaksi;
            //     $transaksi->tgl_transaksi               = $tgl_transaksi;
            //     $transaksi->trs_debet                   = $trs_debet;
            //     $transaksi->trs_kredit                  = $trs_kredit;
            //     // $transaksi->user_id                     = auth()->user()->user_kode;
            //     $transaksi->trs_year                    = date('Y', strtotime($tgl_transaksi));
            //     $transaksi->trs_month                   = date('m', strtotime($tgl_transaksi));
            //     // $transaksi->trs_kode_rekening           = $akun_2->mst_kode_rekening;
            //     // $transaksi->trs_nama_rekening           = $akun_2->mst_nama_rekening;
            //     $transaksi->trs_tipe_arus_kas           = 'operasi';
            //     $transaksi->trs_catatan                 = $catatan;
            //     $transaksi->trs_date_insert             = date('Y-m-d H:i:s');
            //     $transaksi->trs_date_update             = date('Y-m-d H:i:s');
            //     // $transaksi->trs_no_check_bg             = $jurnal_umum_id;
            //     // $transaksi->trs_tgl_pencairan           = $jurnal_umum_id;
            //     // $transaksi->trs_setor                   = $jurnal_umum_id;
            //     // $transaksi->created_at                  = $jurnal_umum_id;
            //     // $transaksi->updated_at                  = $jurnal_umum_id;
            //     $transaksi->save();
            // }
            // mTransaksi::where('master_id',null)->delete();

            //input ke tabel hutang/piutang
            if($jenis=='ap'){
                //input ke tabel hutang supplier
                $data_hutang                    = new mHutang();
                $data_hutang->no_faktur_hutang  = $kode_bukti;
                $data_hutang->js_jatuh_tempo    = date('Y-m-d',strtotime('+30 day',strtotime($tgl_transaksi)));
                $data_hutang->tanggal           = $tgl_transaksi;
                $data_hutang->ps_no_faktur      = null;
                $data_hutang->spl_kode          = $nama;                
                $data_hutang->atas_nama          = $atas_nama;
                $data_hutang->hs_amount         = $total;
                $data_hutang->sisa_amount       = $total;
                $data_hutang->hs_keterangan     = $keterangan;
                // $data_hutang->hs_status         = $kode_bukti;
                $data_hutang->kode_perkiraan    = $akun->mst_kode_rekening;
                $data_hutang->created_at        = date('Y-m-d H:i:s');
                $data_hutang->updated_at        = date('Y-m-d H:i:s');
                $data_hutang->created_by        = auth()->user()->name;
                $data_hutang->updated_by        = auth()->user()->name;
                $data_hutang->save();
            }else{
                //input ke tabel piutang pelanggan
                $data_piutang                       = new mPiutang();
                $data_piutang->no_faktur            = $kode_bukti;
                $data_piutang->tgl_piutang          = $tgl_transaksi;
                $data_piutang->due_date             = date('Y-m-d',strtotime('+30 day',strtotime($tgl_transaksi)));
                // $data_piutang->tanggal              = $tgl_transaksi;
                // $data_piutang->pp_no_faktur         = null;
                $data_piutang->cus_kode             = $nama;
                $data_piutang->nama                 = $atas_nama;
                $data_piutang->pp_amount            = $total;
                $data_piutang->pp_keterangan        = $keterangan;
                // $data_piutang->pp_status            = $kode_bukti;
                $data_piutang->kode_perkiraan       = $akun->mst_kode_rekening;
                $data_piutang->created_at           = date('Y-m-d H:i:s');
                $data_piutang->updated_at           = date('Y-m-d H:i:s');
                $data_piutang->sisa_piutang         = $total;
                // $data_piutang->created_by           = auth()->user()->name;
                // $data_piutang->updated_by           = auth()->user()->name;
                $data_piutang->save();
            }

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data berhasil disimpan']);
            return redirect()->route('penerimaan-hutang-piutang.index');
            
            
        }catch (Exception $e) {
            throw $e;

            DB::rollBack();
          
        }
        
    }

    function edit($jurnal_umum_id='') {
        $breadcrumb                     = [
            'Accounting'                => '',
            'Jurnal Umum'               => route('jurnalUmum'),
            'Edit Jurnal Umum'          => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $data['bulan']      = date('m');
        $data['tahun_periode']      = date('Y');
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15',
                                '16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        $data['jurnalUmum'] = mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->first();
        $data['jml_debet']  = mTransaksi::where('jurnal_umum_id',$data['jurnalUmum']->jurnal_umum_id)->sum('trs_debet');
        $data['jml_kredit'] = mTransaksi::where('jurnal_umum_id',$data['jurnalUmum']->jurnal_umum_id)->sum('trs_kredit');
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        return view('jurnalUmum/editJurnalUmum', $data);
    }

    function update(Request $request, $jurnal_umum_id) {
        $this->rules($request->all());
        $date_db          = date('Y-m-d H:i:s');
        // Jurnal Umum
        
        
        // Transaksi
        $master_id        = $request->input('master_id');
        $jenis_transaksi  = $request->input('jenis_transaksi');
        $debet            = $request->input('debet');
        $kredit           = $request->input('kredit');
        $tipe_arus_kas    = $request->input('tipe_arus_kas');
        $catatan          = $request->input('catatan');
        $transaksi_id     = $request->input('transaksi_id');

        //data jurnal umum
        $tgl_transaksi      = $request->input('tgl_transaksi');
        $kode_bukti         = $request->input('kode_bukti');
        $keterangan         = $request->input('keterangan');
        $jmu_no             = $request->input('jmu_no');
        $jurnal_umum_id     = $request->input('jurnal_umum_id');
        
        // General
        $year             = date('Y', strtotime($tgl_transaksi));
        $month            = date('m', strtotime($tgl_transaksi));
        $day              = date('d', strtotime($tgl_transaksi));
        $where            = [
            'jmu_year'    => $year,
            'jmu_month'   => $month,
            'jmu_day'     => $day
        ];
        
        $data_jurnal          = [
            'no_invoice'      => $kode_bukti,
            'jmu_tanggal'     => date('Y-m-d',strtotime($tgl_transaksi)),
            // 'jmu_no'          => $jmu_no,
            // 'jmu_year'        => $year,
            // 'jmu_month'       => $month,
            // 'jmu_day'         => $day,
            'jmu_keterangan'  => $keterangan,
            'jmu_date_update' => $date_db,
        ];
        mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update($data_jurnal);

        mTransaksi::where('jurnal_umum_id',$jurnal_umum_id)->delete();
        
        foreach($master_id as $key=>$val) {
            $master             = mPerkiraan::find($master_id[$key]);
            $trs_kode_rekening  = $master->mst_kode_rekening;
            $trs_nama_rekening  = $master->mst_nama_rekening;
               
        
            $data_transaksi           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id[$key],
                'trs_jenis_transaksi' => $jenis_transaksi[$key],
                'trs_debet'           => $debet[$key],
                'trs_kredit'          => $kredit[$key],
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening,
                'trs_nama_rekening'   => $trs_nama_rekening,
                'trs_tipe_arus_kas'   => $tipe_arus_kas[$key],
                'trs_catatan'         => $catatan[$key],
                'trs_date_update'     => $date_db,
                'trs_date_insert'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => '',
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d',strtotime($tgl_transaksi))
            ];

            mTransaksi::insert($data_transaksi);

        }

        //delete data ac_transaksi yg lebih
         mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
         //

        return [
            'redirect'=>route('jurnalUmum')
        ];
    }

    function delete($id) {
        mCheque::where('id', $id)->delete();
    }

    function jurnalUmumCutOff($month='', $year='', $jenis=''){
        $breadcrumb         = [
            'Jurnal Umum'   => route('jurnalUmum'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['bulan']      = $month;
        $data['tahun_periode']      = $year;
        $data['start_date']      = $month;
        $data['end_date']      = $year;
        $data['jns']      = 0;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;

        $data['jurnalUmum'] = mJurnalUmum::whereBetween('jmu_tanggal', [$month, $year])->orderBy('jmu_tanggal','ASC')->get();
        $data['jml_debet']  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_debet');
        $data['jml_kredit'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_kredit');
        if($jenis!='' && $jenis!=0){
            $data['jns']      = $jenis;
        }
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        return view('jurnalUmum/jurnalUmumList', $data);
    }

    function pilihPeriode(Request $request) {
        // $queryJurnal = mJurnalUmum::whereBetween('jmu_tanggal', [$request->tgl_awal, $request->tgl_akhir])->orderBy('jmu_tanggal','ASC');
        // $data['dataJurnal'] = $queryJurnal->get();
        // // return view('toko.akunting.jurnal-harian.dataJurnalHarian', compact('dataJurnal'))->render();
        // return view('toko.akunting.jurnal-harian.dataJurnalHarian', $data);
        $month  = $request->input('start_date');
        $year   = $request->input('end_date');
        $jenis   = $request->input('jenis');
        return [
          'redirect'=>route('jurnalUmumCutOff', ['bulan'=>$month, 'tahun'=>$year, 'jenis'=>$jenis]),
        ];
    }

    function postingBukuBesar(){
        $periode                    = mPeriode::first();
        $data['bulan_aktif']        = $periode->bulan_aktif;
        $data['tahun_aktif']        = $periode->tahun_aktif;

        $detailPerkiraan = mDetailPerkiraan::where('msd_year',$data['tahun_aktif'])->where('msd_month',$data['bulan_aktif'])->get();

        if($data['bulan_aktif']=='12'){
            $tahun_aktif_baru       = $data['tahun_aktif']+1;
            $bulan_aktif_baru       = '1';
        }else{
            $tahun_aktif_baru       = $data['tahun_aktif'];
            $bulan_aktif_baru       = $data['bulan_aktif']+1;
        }

        foreach($detailPerkiraan as $pkr){
            if($pkr->perkiraan->mst_normal == 'kredit'){
                $saldo_awal=$pkr->msd_awal_kredit;
            }
            if($pkr->perkiraan->mst_normal == 'debet'){
                $saldo_awal=$pkr->msd_awal_debet;
            }
            foreach($pkr->perkiraan->transaksi->where('trs_year',$data['tahun_aktif'])->where('trs_month',$data['bulan_aktif']) as $transaksi){
                if($pkr->perkiraan->mst_normal == 'kredit'){
                    $saldo_awal=$saldo_awal+$transaksi->trs_kredit-$transaksi->trs_debet;
                }
                if($pkr->perkiraan->mst_normal == 'debet'){
                    $saldo_awal=$saldo_awal-$transaksi->trs_kredit+$transaksi->trs_debet;
                }                
            }
            if($pkr->perkiraan->mst_normal == 'kredit'){
                $data_master_detail     = [
                    'master_id'         => $pkr->master_id,
                    'msd_year'          => $tahun_aktif_baru,
                    'msd_month'         => $bulan_aktif_baru,
                    'msd_awal_kredit'   => $saldo_awal,
                    'msd_awal_debet'    => 0,
                    'msd_date_insert'   => date('Y-m-d'),
                    'msd_date_update'   => date('Y-m-d'),
                    'created_at'        => date('Y-m-d'),
                    'updated_at'        => date('Y-m-d')
                ];
            }
            if($pkr->perkiraan->mst_normal == 'debet'){
                $data_master_detail     = [
                    'master_id'         => $pkr->master_id,
                    'msd_year'          => $tahun_aktif_baru,
                    'msd_month'         => $bulan_aktif_baru,
                    'msd_awal_kredit'   => 0,
                    'msd_awal_debet'    => $saldo_awal,
                    'msd_date_insert'   => date('Y-m-d'),
                    'msd_date_update'   => date('Y-m-d'),
                    'created_at'        => date('Y-m-d'),
                    'updated_at'        => date('Y-m-d')
                ];
            }
            mDetailPerkiraan::insert($data_master_detail);
            
        }

        $data_periode_baru      = [
            'bulan_aktif'       => $bulan_aktif_baru,
            'tahun_aktif'       => $tahun_aktif_baru
        ];
        mPeriode::where('id',1)->update($data_periode_baru);

        // redirect(route('bukuBesar'));
        
        return Redirect::to('/bukuBesar')->with('success', 'Posting Buku Besar Berhasil');

    }

    function printJurnalUmum($month='', $year=''){
        $breadcrumb         = [
            'Jurnal Umum'   => route('jurnalUmum'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['bulan']      = $month;
        $data['tahun_periode']      = $year;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
                                if($data['bulan_aktif']=='1'){
                                    $data['bln']='Januari';
                                }
                                if($data['bulan_aktif']=='2'){
                                    $data['bln']='Pebruari';
                                }
                                if($data['bulan_aktif']=='3'){
                                    $data['bln']='Maret';
                                }
                                if($data['bulan_aktif']=='4'){
                                    $data['bln']='April';
                                }
                                if($data['bulan_aktif']=='5'){
                                    $data['bln']='Mei';
                                }
                                if($data['bulan_aktif']=='6'){
                                    $data['bln']='Juni';
                                }
                                if($data['bulan_aktif']=='7'){
                                    $data['bln']='Juli';
                                }
                                if($data['bulan_aktif']=='8'){
                                    $data['bln']='Agustus';
                                }
                                if($data['bulan_aktif']=='9'){
                                    $data['bln']='September';
                                }
                                if($data['bulan_aktif']=='10'){
                                    $data['bln']='Oktober';
                                }
                                if($data['bulan_aktif']=='11'){
                                    $data['bln']='November';
                                }
                                if($data['bulan_aktif']=='12'){
                                    $data['bln']='Desember';
                                }
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['jurnalUmum'] = mJurnalUmum::whereBetween('jmu_tanggal', [$month, $year])->orderBy('jmu_tanggal','ASC')->get();
        $data['jml_debet']  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_debet');
        $data['jml_kredit'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_kredit');
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        // $pdf = new DomPDF();


        // $pdf = PDF::loadView('jurnalUmum.printJurnalUmum2', $data)
        //           ->setPaper('a4', 'potrait');
        // // return $pdf->download('jurnal umum periode '. $data['bln'].'-'.$data['tahun_periode'].'.pdf');
        // return $pdf->stream();

        return view('jurnalUmum/printJurnalUmum2', $data);
    }
}
