<?php

namespace App\Http\Controllers\HutangPiutang;

use Illuminate\Http\Request;
// use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\JurnalHarian;
use App\Models\Transaksi;
use App\Models\MasterCoa;
use App\Models\mDetailPerkiraan;
use App\Models\mPembayaranHutangPiutang;
use App\Models\Supplier;
use App\Models\mKaryawan;
use App\Models\mHutang;
use App\Models\mPiutang;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Validator;
use Redirect;
use PDF;
use DB;
// use Barryvdh\DomPDF;

class HutangSupplier extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        // $this->main         = new Main();
        $this->title        = 'AP (Production & Purchasing)';
        $this->kodeLabel    = '';
    }

    public function addForm() {
        return view('toko.hutang-piutang._add-form')->render();
    }

    function index() {
        
        $data['title']              = $this->title;
        $data['data_hutang']        = mHutang::where('no_faktur_hutang','like','%PMB%')->get();
        $data['menu']               = 'Hutang Piutang';


        // return view('hutang-piutang/penerimaan-hutang-piutang', $data);
        return view('toko.hutang-piutang.hutang-supplier.index', $data);
    }

    function hutang_lain() {
        
        $data['title']              = 'AP (Others)';
        $data['data_hutang']        = mHutang::where('no_faktur_hutang','like','%AP%')->get();
        $data['menu']               = 'Hutang Piutang';


        // return view('hutang-piutang/penerimaan-hutang-piutang', $data);
        return view('toko.hutang-piutang.hutang-supplier.index', $data);
    }

    protected function get_no_pembayaran($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('ym');
        $lastorder = mPembayaranHutangPiutang::where('no_pembayaran', 'like', '%'.$preffix)->max('no_pembayaran');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    protected function getNoTransaksiAR($kode) {
        // $kode = $this->main->kodeLabel('invoice');
        $preffix = '.'.$kode.'.'.date('my');
        $lastorder = mPiutang::where('no_piutang_pelanggan', 'like', '%'.$preffix.'%')->max('no_piutang_pelanggan');
        if ($lastorder != null) {
          $lastno = substr($lastorder, 0, 4);
          $now = $lastno+1;
          $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
          $no = '0001';
        }

        return $no.$preffix;
    }

    function get_data($kode='') {
        
        $data['hutang'] = mHutang::find($kode);
        $data['perkiraan']  = MasterCoa::orderBy('mst_kode_rekening','ASC')->get();
        $kode               = 'BR';
        $data['no_transaksi'] = $this->get_no_pembayaran($kode);
        return view('toko.hutang-piutang.hutang-supplier.payment', $data);
    }

    function rules($request) {
        $rules = [
            'tgl_transaksi'             => 'required',
            'kode_bukti'                => 'required',
            'keterangan'                => 'required',
        ];

        $customeMessage = [
            'required'  =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        DB::beginTransaction();
        try{
            $tgl_transaksi              = $request->input('tgl_transaksi');
            $no_transaksi               = $request->input('no_transaksi');
            $no_faktur                  = $request->input('no_faktur');
            $kode_hutang                = $request->input('kode_hutang');
            $nama                       = $request->input('nama');
            $kode_nama                  = $request->input('kode_nama');
            $kode_perkiraan             = $request->input('kode_perkiraan');
            $amount_sisa                = $request->input('amount_sisa');

            //payment
            $master_id                  = $request->input('master_id');
            $payment                    = $request->input('payment');
            $payment_total              = $request->input('payment_total');
            $keterangan                 = $request->input('keterangan');
            $tipe_pembayaran           = $request->input('tipe_pembayaran');

            // $last_no_jurnal     = JurnalHarian::where('jmu_tanggal',$tgl_transaksi)->max('jmu_no');
            // $jmu_no             = $last_no_jurnal+1;


            // $data_jurnal                    = new JurnalHarian();
            // $data_jurnal->no_jurnal         = $no_transaksi;
            // // $data_jurnal->status            = 'pending';
            // $data_jurnal->id_pel            = $kode_nama;
            // $data_jurnal->jmu_tanggal       = $tgl_transaksi;
            // $data_jurnal->jmu_no            = $jmu_no;
            // $data_jurnal->jmu_keterangan    = 'Pembayaran Hutang No : '.$no_faktur;
            // $data_jurnal->jmu_year          = date('Y', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_month         = date('m', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_day           = date('d', strtotime($tgl_transaksi));
            // $data_jurnal->jmu_date_insert   = date('Y-m-d H:i:s');
            // // $data_jurnal->jmu_date_update   = date('Y-m-d H:i:s');
            // $data_jurnal->created_by        = auth()->user()->name;
            // $data_jurnal->updated_by        = auth()->user()->name;
            // // $data_jurnal->reference_number  = $kode_bukti;
            // $data_jurnal->save();
            // $id_jurnal = $data_jurnal->id_jurnal;

            $hutang                 = mHutang::where('hs_kode',$kode_hutang)->first();
            $sisa_amount              = $hutang['sisa_amount'];
            $total_payment          = 0;

            foreach($master_id as $key=>$val) {
                $master             = MasterCoa::find($master_id[$key]);
                $trs_kode_rekening  = $master->mst_kode_rekening;
                $trs_nama_rekening  = $master->mst_nama_rekening;
                   
            
                // $data_transaksi           = [
                //     'id_jurnal_umum'      => $id_jurnal,
                //     'master_id'           => $master_id[$key],
                //     'trs_jenis_transaksi' => 'kredit',
                //     'trs_debet'           => 0,
                //     'trs_kredit'          => $payment[$key],
                //     'user_id'             => 0,
                //     'trs_year'            => date('Y', strtotime($tgl_transaksi)),
                //     'trs_month'           => date('m', strtotime($tgl_transaksi)),
                //     'trs_kode_rekening'   => $trs_kode_rekening,
                //     'trs_nama_rekening'   => $trs_nama_rekening,
                //     'trs_tipe_arus_kas'   => 'operasi',
                //     'trs_catatan'         => $keterangan[$key],
                //     'trs_date_insert'     => date('Y-m-d'),
                //     'trs_date_update'     => date('Y-m-d'),
                //     'trs_charge'          => 0,
                //     'trs_no_check_bg'     => '-',
                //     'trs_tgl_pencairan'   => date('Y-m-d'),
                //     'trs_setor'           => 0,
                //     'created_by'          => auth()->user()->name,
                //     'created_at'          => date('Y-m-d H:i:s'),
                //     'updated_by'          => auth()->user()->name,
                //     'updated_at'          => date('Y-m-d H:i:s'),
                //     'tgl_transaksi'       => $tgl_transaksi
                // ];    
                // Transaksi::insert($data_transaksi);

                $total_payment      = $total_payment + $payment[$key];
                $sisa_amount        = $sisa_amount - $payment[$key];
                $angka_pp_amount    = number_format($amount_sisa,2);
                    
                if($angka_pp_amount == '0.00'){
                    mHutang::where('hs_kode', $kode_hutang)->update(['sisa_amount'=>$amount_sisa,'hs_status'=>'lunas']);
                }else{
                    mHutang::where('hs_kode', $kode_hutang)->update(['sisa_amount'=>$amount_sisa]);
                }
                //$terbayar = $payment[$key];
                $terbayar = $total_payment;

                $pembayaran = new mPembayaranHutangPiutang();
                $pembayaran->no_pembayaran      = $no_transaksi;
                $pembayaran->tanggal      = $tgl_transaksi;
                $pembayaran->nominal_bayar      = $payment[$key];
                $pembayaran->id_hutang_piutang      = $kode_hutang;
                $pembayaran->jenis      = 'hutang';
                $pembayaran->created_at      = date('Y-m-d H:i:s');
                $pembayaran->created_by      = auth()->user()->name;
                $pembayaran->tipe_pembayaran = $tipe_pembayaran[$key];
                $pembayaran->save();
    
            }

            // $master_hutang                = MasterCoa::where('mst_kode_rekening', $kode_perkiraan)->first();
            // $master_id_hutang             = $master_hutang->master_id;
            // $trs_kode_rekening_hutang     = $master_hutang->mst_kode_rekening;
            // $trs_nama_rekening_hutang     = $master_hutang->mst_nama_rekening;

            // $data_transaksi_hutang    = [
            //     'id_jurnal_umum'      => $id_jurnal,
            //     'master_id'           => $master_id_hutang,
            //     'trs_jenis_transaksi' => 'debet',
            //     'trs_debet'           => $total_payment,
            //     'trs_kredit'          => 0,
            //     'user_id'             => 0,
            //     'trs_year'            => date('Y', strtotime($tgl_transaksi)),
            //         'trs_month'       => date('m', strtotime($tgl_transaksi)),
            //     'trs_kode_rekening'   => $trs_kode_rekening_hutang,
            //     'trs_nama_rekening'   => $trs_nama_rekening_hutang,
            //     'trs_tipe_arus_kas'   => 'operasi',
            //     'trs_catatan'         => 'pembayaran hutang',
            //     'trs_date_insert'     => date('Y-m-d'),
            //     'trs_date_update'     => date('Y-m-d'),
            //     'trs_no_check_bg'     => '-',
            //     'trs_tgl_pencairan'   => date('Y-m-d'),
            //     'trs_setor'           => 0,
            //     'created_by'          => auth()->user()->name,
            //     'created_at'          => date('Y-m-d H:i:s'),
            //     'updated_by'          => auth()->user()->name,
            //     'updated_at'          => date('Y-m-d H:i:s'),
            //     'tgl_transaksi'       => $tgl_transaksi
            // ];
            // Transaksi::insert($data_transaksi_hutang);

            

            DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data berhasil disimpan']);
            return [
                'redirect' => route('hutang-supplier.index')
            ];
            
            
        }catch (Exception $e) {
            throw $e;

            DB::rollBack();
          
        }
        
    }
}
