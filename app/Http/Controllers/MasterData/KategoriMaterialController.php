<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategorimaterials = Kategori::where('type_kat', 'material')->get();
        $kategoriproduks = Kategori::where('type_kat', 'produk')->get();
        $nokatmaterial = $this->createNoKategoriMaterial();
        $nokatproduk = $this->createNoKategoriProduk();
        return view('toko.masterdata.kategori.index', compact('kategorimaterials', 'kategoriproduks', 'nokatmaterial', 'nokatproduk'));
    }

    protected function createNoKategoriMaterial() {
        $preffix = 'KM-';
        $lastkategori = Kategori::where(\DB::raw('LEFT(kd_kat, '.strlen($preffix).')'), $preffix)->where('type_kat', 'material')->selectRaw('RIGHT(kd_kat, 4) as no_kat')->orderBy('no_kat', 'desc')->first();

        if (!empty($lastkategori)) {
            $now = intval($lastkategori->no_kat)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    protected function createNoKategoriProduk() {
        $preffix = 'KP-';
        $lastkategori = Kategori::where(\DB::raw('LEFT(kd_kat, '.strlen($preffix).')'), $preffix)->where('type_kat', 'produk')->selectRaw('RIGHT(kd_kat, 4) as no_kat')->orderBy('no_kat', 'desc')->first();

        if (!empty($lastkategori)) {
            $now = intval($lastkategori->no_kat)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_kat' => 'required|unique:tb_kat,kd_kat',
            'kategori' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $data['type_kat'] = 'material';
            Kategori::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material category successfully added.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategorimaterial = Kategori::where('id', $id)->where('type_kat', 'material')->firstOrFail();
        return view('toko.masterdata.kategori.edit-material', compact('kategorimaterial'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategorimaterial = Kategori::where('id', $id)->where('type_kat', 'material')->firstOrFail();
        $this->validate($request, [
            'kd_kat' => 'required|unique:tb_kat,kd_kat,'.$kategorimaterial->id,
            'kategori' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $kategorimaterial->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material category updated successfully.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategorimaterial = Kategori::where('id', $id)->where('type_kat', 'material')->firstOrFail();
        \DB::beginTransaction();
        try {
            $kategorimaterial->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Material category successfully deleted.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
