<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::get();
        $nosupplier = $this->createNoSupplier();
        return view('toko.masterdata.supplier.index', compact('suppliers', 'nosupplier'));
    }

    protected function createNoSupplier() {
        $preffix = 'SP-';
        $lastsupplier = Supplier::where(\DB::raw('LEFT(kd_supplier, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_supplier, 4) as no_supplier')->orderBy('no_supplier', 'desc')->first();

        if (!empty($lastsupplier)) {
            $now = intval($lastsupplier->no_supplier)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_supplier' => 'required|unique:tb_supplier,kd_supplier',
            'nama_supplier' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            Supplier::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Supplier added successfully.']);
            return redirect()->route('supplier.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::where('id', $id)->firstOrFail();
        return view('toko.masterdata.supplier.edit', compact('supplier'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_supplier' => 'required|unique:tb_supplier,kd_supplier,'.$supplier->id,
            'nama_supplier' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $supplier->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Supplier was updated.']);
            return redirect()->route('supplier.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::where('id', $id)->firstOrFail();

        \DB::beginTransaction();
        try {
            $supplier->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Supplier was successfully deleted.']);
            return redirect()->route('supplier.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
