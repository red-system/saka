<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_kat' => 'required|unique:tb_kat,kd_kat',
            'kategori' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $data['type_kat'] = 'produk';
            Kategori::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product categories successfully added.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoriproduk = Kategori::where('id', $id)->where('type_kat', 'produk')->firstOrFail();
        return view('toko.masterdata.kategori.edit-produk', compact('kategoriproduk'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategoriproduk = Kategori::where('id', $id)->where('type_kat', 'produk')->firstOrFail();
        $this->validate($request, [
            'kd_kat' => 'required|unique:tb_kat,kd_kat,'.$kategoriproduk->id,
            'kategori' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $kategoriproduk->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product category updated successfully.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategoriproduk = Kategori::where('id', $id)->where('type_kat', 'produk')->firstOrFail();
        \DB::beginTransaction();
        try {
            $kategoriproduk->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Product category was successfully deleted.']);
            return redirect()->route('kategori-material.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
