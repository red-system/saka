<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Produsen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdusenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produsens = Produsen::get();
        $noprodusen = $this->createNoProdusen();
        return view('toko.masterdata.produsen.index', compact('produsens', 'noprodusen'));
    }

    protected function createNoProdusen() {
        $preffix = 'PRD-';
        $lastprodusen = Produsen::where(\DB::raw('LEFT(kd_produsen, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_produsen, 4) as no_produsen')->orderBy('no_produsen', 'desc')->first();

        if (!empty($lastprodusen)) {
            $now = intval($lastprodusen->no_produsen)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_produsen' => 'required|unique:tb_produsen,kd_produsen',
            'nama_produsen' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            Produsen::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Manufacturer data successfully added.']);
            return redirect()->route('produsen.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produsen = Produsen::where('id', $id)->firstOrFail();
        return view('toko.masterdata.produsen.edit', compact('produsen'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produsen = Produsen::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_produsen' => 'required|unique:tb_produsen,kd_produsen,'.$produsen->id,
            'nama_produsen' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $produsen->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Manufacturer data updated successfully.']);
            return redirect()->route('produsen.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produsen = Produsen::where('id', $id)->firstOrFail();

        \DB::beginTransaction();
        try {
            $produsen->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Manufacturer data was successfully deleted.']);
            return redirect()->route('produsen.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
