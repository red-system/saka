<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Provinsi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DaerahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinsis = Provinsi::get();
        $nodaerah = $this->createNoDaerah();
        return view('toko.masterdata.daerah.index', compact('provinsis', 'nodaerah'));
    }

    protected function createNoDaerah() {
        $preffix = 'DRH-';
        $lastdaerah = Provinsi::where(\DB::raw('LEFT(kd_provinsi, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_provinsi, 4) as no_daerah')->orderBy('no_daerah', 'desc')->first();

        if (!empty($lastdaerah)) {
            $now = intval($lastdaerah->no_daerah)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_provinsi' => 'required|unique:tb_provinsi,kd_provinsi',
            'provinsi' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            Provinsi::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Regional data successfully added.']);
            return redirect()->route('daerah.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provinsi = Provinsi::where('id', $id)->firstOrFail();
        return view('toko.masterdata.daerah.edit', compact('provinsi'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $provinsi = Provinsi::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_provinsi' => 'required|unique:tb_provinsi,kd_provinsi,'.$provinsi->id,
            'provinsi' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $provinsi->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Regional data updated successfully.']);
            return redirect()->route('daerah.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $provinsi = Provinsi::where('id', $id)->firstOrFail();
            $provinsi->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Regional data successfully deleted.']);
            return redirect()->route('daerah.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
