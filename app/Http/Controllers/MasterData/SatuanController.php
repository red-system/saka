<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Satuan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $satuans = Satuan::get();
        $nosatuan = $this->createNoSatuan();
        return view('toko.masterdata.satuan.index', compact('satuans', 'nosatuan'));
    }

    protected function createNoSatuan() {
        $preffix = 'STN-';
        $lastsatuan = Satuan::where(\DB::raw('LEFT(kd_satuan, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_satuan, 4) as no_satuan')->orderBy('no_satuan', 'desc')->first();

        if (!empty($lastsatuan)) {
            $now = intval($lastsatuan->no_satuan)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_satuan' => 'required|unique:tb_satuan,kd_satuan',
            'satuan' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            Satuan::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Unit data successfully added.']);
            return redirect()->route('satuan.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $satuan = Satuan::where('id', $id)->firstOrFail();
        return view('toko.masterdata.satuan.edit', compact('satuan'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $satuan = Satuan::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_satuan' => 'required|unique:tb_satuan,kd_satuan,'.$satuan->id,
            'satuan' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $satuan->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Unit data updated successfully.']);
            return redirect()->route('satuan.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satuan = Satuan::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $satuan->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Unit data successfully deleted.']);
            return redirect()->route('satuan.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
