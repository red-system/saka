<?php

namespace App\Http\Controllers\MasterData;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('toko.masterdata.datauser.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'role' => 'required|in:'.implode(',', User::allowedRole()),
            'password' => 'required|confirmed',
            'kd_location' => 'required|exists:tb_location,kd_location'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->only('name', 'username', 'email', 'role', 'kd_location');
            $data['password'] = bcrypt($request->password);
            User::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'User data successfully added.']);
            return redirect()->route('data-user.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->firstOrFail();
        return view('toko.masterdata.datauser.edit', compact('user'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'role' => 'required|in:'.implode(',', User::allowedRole()),
            'kd_location' => 'required|exists:tb_location,kd_location'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->only('name', 'username', 'email', 'role', 'kd_location');
            if ($request->password != '') {
                $data['password'] = bcrypt($request->password);
            }
            $user->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'User data updated successfully.']);
            return redirect()->route('data-user.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            if ($user->img_profile != '') {
                User::deleteAttachment($user->img_profile);
            }
            $user->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'User data was successfully deleted.']);
            return redirect()->route('data-user.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
