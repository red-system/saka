<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = Currency::get();
        //$nocurr = $this->createNoCurrency();
        return view('toko.masterdata.currency.index', compact('currencies'));
    }


    protected function createNoCurrency() {
        $preffix = 'CR-';
        $lastcur = Currency::where(\DB::raw('LEFT(kd_currency, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_currency, 4) as no_currency')->orderBy('no_currency', 'desc')->first();

        if (!empty($lastcur)) {
            $now = intval($lastcur->no_currency)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_currency' => 'required|unique:tb_currency,kd_currency',
            'currency' => 'required',
            'remark' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->only('kd_currency', 'currency', 'remark');
            Currency::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Currency added successfully.']);
            return redirect()->route('currency.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        return view('toko.masterdata.currency.edit', compact('currency'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_currency' => 'required|unique:tb_currency,kd_currency,'.$currency->id,
            'currency' => 'required',
            'remark' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->only('kd_currency', 'currency', 'remark');
            $currency->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Currency updated successfully.']);
            return redirect()->route('currency.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $currency->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Currency successfully deleted.']);
            return redirect()->route('currency.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
