<?php

namespace App\Http\Controllers\MasterData;

use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::get();
        $nolocation = $this->createNoLocation();
        return view('toko.masterdata.location.index', compact('locations', 'nolocation'));
    }

    protected function createNoLocation() {
        $preffix = 'SLC-';
        $lastlocation = Location::where(\DB::raw('LEFT(kd_location, '.strlen($preffix).')'), $preffix)->selectRaw('RIGHT(kd_location, 4) as no_location')->orderBy('no_location', 'desc')->first();

        if (!empty($lastlocation)) {
            $now = intval($lastlocation->no_location)+1;
            $no = str_pad($now, 4, '0', STR_PAD_LEFT);
        } else {
            $no = '0001';
        }

        return $preffix.$no;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kd_location' => 'required|unique:tb_location,kd_location',
            'location_name' => 'required',
            'pic' => 'required',
            'alamat' => 'required',
            'potongan' => 'required|numeric|min:0|max:100',
            'kd_provinsi' => 'required|exists:tb_provinsi,kd_provinsi',
            'type' => 'required|in:'.implode(',', Location::allowedType()),
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            Location::create($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Location added successfully.']);
            return redirect()->route('location.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::where('id', $id)->firstOrFail();
        return view('toko.masterdata.location.edit', compact('location'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'kd_location' => 'required|unique:tb_location,kd_location,'.$location->id,
            'location_name' => 'required',
            'pic' => 'required',
            'alamat' => 'required',
            'potongan' => 'required|numeric|min:0|max:100',
            'kd_provinsi' => 'required|exists:tb_provinsi,kd_provinsi',
            'type' => 'required|in:'.implode(',', Location::allowedType()),
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();
            $location->update($data);

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data location was updated successfully.']);
            return redirect()->route('location.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::where('id', $id)->firstOrFail();
        \DB::beginTransaction();
        try {
            $location->delete();

            \DB::commit();

            \Session::flash('notification', ['level' => 'success', 'title' => 'Success', 'message' => 'Data location was successfully deleted.']);
            return redirect()->route('location.index');
        } catch (\Exception $e) {
            \DB::rollback();
            \Session::flash('notification', ['level' => 'error', 'title' => 'Error', 'message' => $e->getMessage()]);
            return back()->withInput();
        }
    }
}
