<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;

class CurrencyConversionController extends Controller
{
    public function convert($from, $to, $ammount) {
        $client = new Client();
        $crawler = $client->request('GET', 'https://id.exchange-rates.org/converter/'.$from.'/'.$to.'/'.$ammount);
        $result = $crawler->filter('span[id="ctl00_M_lblToAmount"]');

        if ($result->count() > 0) {
            return \Response::json(['success' => true, 'result' => str_replace('.', '', $result->text())]);
        } else {
            return \Response::json(['success' => false, 'result' => null]);
        }
    }
}
