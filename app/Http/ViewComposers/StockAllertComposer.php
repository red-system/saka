<?php

namespace App\Http\ViewComposers;

use App\Models\Stock;
use Illuminate\View\View;

class StockAllertComposer
{
    public function compose(View $view)
    {
        $querystocks = Stock::leftJoin('tb_product', 'tb_stock.kd_produk', '=', 'tb_product.kd_produk');
        if (\Gate::denies('as_owner_or_manager_kantorpusat')) {
            $querystocks = $querystocks->where('kd_location', auth()->user()->kd_location);
        }
        $jmlallert = $querystocks->selectRaw('tb_stock.kd_produk, tb_stock.kd_location, tb_product.minimal_stock, sum(qty) as totalQty')
                                ->groupBy('tb_stock.kd_produk', 'tb_stock.kd_location')->get()
                                ->filter(function ($stck) { return $stck->totalQty <= $stck->minimal_stock; })
                                ->count();
        $view->with('jmlallert', $jmlallert);
    }
}