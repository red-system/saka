<?php namespace App\Helpers;

class Konversi {
    public static function localcurrency_to_database($ammount) {
        $arrAmmount = explode(',', $ammount);
        return str_replace('.', '', $arrAmmount[0]).'.'.$arrAmmount[1];
    }

    public static function database_to_localcurrency($ammount) {
        return number_format($ammount, 2, ',', '.');
    }

    public static function database_to_localcurrency_ws($ammount) {
        return number_format($ammount, 2, '', '.');
    }
}